/*
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit.modelaccess;

import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.jenerateit.util.StringTools;

/**
 * Abstract model converter with support for info, warning and error messages.
 * 
 * @author hrr
 */
public abstract class MessageProviderModelAccess implements ModelAccessI {

	private final List<String> infoMessageBuffer = new ArrayList<String>();
	private final List<String> warningMessageBuffer = new ArrayList<String>();
	private final List<String> errorMessageBuffer = new ArrayList<String>();
	
	protected abstract Collection<?> loadElements(InputStream input, ModelAccessOptions options) throws ModelAccessException;
	
	/**
	 * Add an info message to the message buffer.
	 * 
	 * @param message the info message
	 */
	public void addInfo(final String message) {
		if (StringTools.isText(message)) {
			this.infoMessageBuffer.add(message);
		}
	}
	
	/**
	 * Add a warning message to the message buffer.
	 * 
	 * @param message the warning message
	 */
	public void addWarning(final String message) {
		addWarning(message, null);
	}
	
	/**
	 * Add a warning message with a {@link Throwable} to the message buffer.
	 * The {@link Throwable} will be reported as stack trace.
	 * 
	 * @param message the warning message
	 * @param t the exception to report
	 */
	public void addWarning(final String message, final Throwable t) {
		final StringBuffer sb = new StringBuffer();
		if (StringTools.isText(message)) {
			sb.append(message);
		}
		
		if (t != null) {
			if (sb.length() > 0) {
				sb.append(StringTools.NEWLINE);
			} else {
				sb.append("<exception only, no message available>").append(StringTools.NEWLINE);
			}
			final StringWriter sw = new StringWriter();
			t.printStackTrace(new PrintWriter(sw));
			sb.append(sw.getBuffer());
		}
		
		if (sb.length() > 0) {
			this.warningMessageBuffer.add(sb.toString());
		}
	}

	/**
	 * Add an error message to the message buffer.
	 * 
	 * @param message the error message
	 */
	public void addError(final String message) {
		addError(message, null);
	}
	
	/**
	 * Add an error message with a {@link Throwable} to the message buffer.
	 * The {@link Throwable} will be reported as stack trace.
	 * 
	 * @param message the error message
	 * @param t the exception to report
	 */
	public void addError(final String message, final Throwable t) {
		final StringBuffer sb = new StringBuffer();
		if (StringTools.isText(message)) {
			sb.append(message);
		}
		
		if (t != null) {
			if (sb.length() > 0) {
				sb.append(StringTools.NEWLINE);
			} else {
				sb.append("<exception only, no message available>").append(StringTools.NEWLINE);
			}
			final StringWriter sw = new StringWriter();
			t.printStackTrace(new PrintWriter(sw));
			sb.append(sw.getBuffer());
		}
		
		if (sb.length() > 0) {
			this.errorMessageBuffer.add(sb.toString());
		}
	}
	
	
	@Override
	public final Collection<?> getElements(InputStream input,
			ModelAccessOptions options) throws ModelAccessException {
		this.infoMessageBuffer.clear();
		this.warningMessageBuffer.clear();
		this.errorMessageBuffer.clear();
		return loadElements(input, options);
	}

	/**
	 * Getter to request all info messages from message buffer.
	 * 
	 * @return the info messages
	 * @see ModelAccessI#getInfos()
	 */
	public final List<String> getInfos() {
		return Collections.unmodifiableList(this.infoMessageBuffer);
	}

	/**
	 * Getter to request all warning messages from message buffer.
	 * 
	 * @return the warning messages
	 * @see ModelAccessI#getWarnings()
	 */
	public final List<String> getWarnings() {
		return Collections.unmodifiableList(this.warningMessageBuffer);
	}

	/**
	 * Getter to request all error messages from message buffer.
	 * 
	 * @return the error messages
	 * @see ModelAccessI#getErrors()
	 */
	public final List<String> getErrors() {
		return Collections.unmodifiableList(this.errorMessageBuffer);
	}

}

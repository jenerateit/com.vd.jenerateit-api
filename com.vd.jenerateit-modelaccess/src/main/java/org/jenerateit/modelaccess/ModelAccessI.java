 /**
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit.modelaccess;

import java.io.InputStream;
import java.util.Collection;

import org.jenerateit.util.DetailsI;
import org.jenerateit.util.MessageProviderI;



/**
 * Interface for modeling tools.
 * <p>
 * The following code snippet shows a possible use of the model access in JenerateIT
 * </p>
 * <pre><code>
 * if (modelAccess != null &amp;&amp; modelAccess.isProgramm()) {
 *     // use the old one, since this is a real GUI program
 * } else {
 *     modelAccess = new ...;
 *     ModelAccessOptions options = new ModelAccessOptions();
 *     options.put("filename", new File("C:\\test.xml"));
 *     options.put("withgui", new Boolean(true));
 *     modelAccess.init(options);
 *     modelAccess.open();
 * }
 * ...
 * if (modelAccess != null &amp;&amp; modelAccess.isOpen()) {
 *     ModelAccessOptions options = new ModelAccessOptions();
 *     options.put("filter", "Entity");
 *     return modelAccess.getElements(new FileInputStream(new File("C:\\test.xml")), options);
 * } else {
 *     throw new ApplicationException("There is no model access available to load model elements from");
 * }
 * ...
 * if (modelAccess != null &amp;&amp; modelAccess.isOpen()) {
 *     modelAccess.close();
 *     modelAccess = null;
 * }
 * </code></pre>
 *
 * @author Heinz Rohmer
 */
public interface ModelAccessI extends DetailsI, MessageProviderI {

	/**
	 * Initializes the interface to the modeling tool.
	 * 
	 * <b>Note</b>
	 * Needs to be implemented by clients if necessary.
	 * 
	 * @param options the configuration options or null if not needed
	 */
    public void init(ModelAccessOptions options);
    
    /**
     * Method to start/open the modeling tool (the model). 
     * Needs to be implemented by clients if necessary. 
     * 
     * @return the design tool as an object
     */
    public Object open();

    /**
     * Checks if the tool is already started. 
     * <p>
     * <b>NOTE</b>
     * If the implementation does not control a GUI tool <code>true</code> should be returned.
     * <p>
     * @return true if the GUI tool is started and ready for access otherwise false
     */
    public boolean isOpen();
    
    /**
     * Shutdown the modeling tool
     *
     */
    public void close();
    
    /**
     * This flag indicates if this ModelAccessI implementation wraps a GUI program and should not be
     * restarted. If this ModelAccessI implementation is just a library and my be reloaded by a new classloader
     * set it to false. In case of a real GUI program set it to true.
     * 
     * @return true if this ModelAccessI implementation wraps a GUI program otherwise false
     */
    public boolean isProgram();
    
    /**
     * Returns a list of all elements out of the model.
     * 
     * @param input the input stream with the model version 0 (e.g. an zipfile input stream)
     * @param options a map of options (e.g. filters) to use while load elements (null if not needed)
     * @return a list of model elements out of the modeling tool
     */
    public Collection<?> getElements(InputStream input, ModelAccessOptions options) throws ModelAccessException;
    
    /**
     * All elements will be selected/highlighted in the modeling tools.
     * Clients with no human interface (e.g. file access) don't need to implement this method.
     * 
     * @param elements the model elements to select 
     */
    public void selectElements(Collection<?> elements);
}


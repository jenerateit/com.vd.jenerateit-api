/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package org.jenerateit.modelaccess;

/**
 * @author hrr
 *
 */
public interface ModelAccessProviderI {

	/**
	 * Provides an instance of {@link ModelAccessI}.
	 * 
	 * <p>
	 * <b>Please note:</b> The implementer of the provider has to decide if the reuse the model access or create a new instance. 
	 * </p>
	 * 
	 * @return the model access instance
	 */
	ModelAccessI getModelAccess();
}

/*
 * Copyright (c) 2013 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package org.jenerateit.modelaccess;

import org.jenerateit.exception.JenerateITException;

/**
 * @author hrr
 *
 */
public class ModelAccessException extends JenerateITException {

	private static final long serialVersionUID = 4736635878313792504L;

	/**
	 * @param msg error message
	 */
	public ModelAccessException(String msg) {
		super(msg);
	}

	/**
	 * @param msg error message
	 * @param cause root exception
	 */
	public ModelAccessException(String msg, Throwable cause) {
		super(msg, cause);
	}

	public ModelAccessException(Throwable cause) {
		super(cause);
	}

}

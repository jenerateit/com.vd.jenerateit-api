About
=====

JenerateIT is a general purpose open source (code-)generation tool. The tool is mainly meant to be a tool for software developers and it is written in Java. In contrast to many other generation tools, it uses an API-based approach for the development of generation-logic, not a template-based approach. Nonetheless you may use templates from within your generation-logic, like for instance StringTemplate. 

JenerateIT comes with an integration into Eclipse, which is called JenerateIT-Eclipse. In other words: one user interface for JenerateIT is Eclipse. The core of JenerateIT doesn't have dependencies on Eclipse, though. With this it is fairly easy to develop integrations with other IDEs, like for instance NetBeans or IntelliJIDEA or to use JenerateIT for batch-generation, without a user interface. 

JenerateIT sets its focus on simplicity, reusability and comprehensibility. Its goal is to be able to apply generation with the least possible effort for ...

 - learning new things
 - adapting the development environment
 - changing habits

... and at the same time with the maximum benefit in regards to ... 

 - flexibility
 - efficiency

For more information please see http://jenerateit.org/index.php?id=28.


License
=======

JenerateIT may be used under the terms of either the GNU Lesser General Public License (LGPL) or the Eclipse Public License (EPL). As a recipient of JenerateIT, you may choose which license to receive the code under.

Please make sure you understand the dual license of JenerateIT and the rights and obligations that come with it. See the top-level license.txt file for more details.

For more information please see http://jenerateit.org/index.php?id=30


Quick start
===========

It is easy to get started with JenerateIT. For detailed information please see http://jenerateit.org/index.php?id=21


API Reference
=============

You may access the API reference either by using the Eclipse help or by using the online documentation http://api.jenerateit.org


Learn more
==========

* About JenerateIT
  http://jenerateit.org/about

* Release notes
  http://jenerateit.org/about/<TODO>

* User manual
  http://jenerateit.org/documentation/<TODO>

* API documentation
  http://api.jenerateit.org



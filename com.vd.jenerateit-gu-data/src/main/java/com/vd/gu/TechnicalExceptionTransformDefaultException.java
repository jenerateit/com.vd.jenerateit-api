package com.vd.gu;

import com.vd.gu.definition.basic.ErrorBean;


/**
 * error
 * exception type to be used as default, for cases where there is not an explicit exception type available
 * <table>
 * <tr><td>Generator</td><td>com.gs.vd.gen.java.retrofit.openapi:1.0</td></tr>
 * <tr><td>Element</td><td>default</td></tr>
 * <tr><td>Type</td><td>Response</td></tr>
 * <tr><td>Module</td><td>none</td></tr>
 * </table>
 */
public class TechnicalExceptionTransformDefaultException extends RuntimeException { // start of class

    private static final long serialVersionUID = 1L;
    
    private ErrorBean responseCodeDefault;
    
    /**
     * creates an instance of TechnicalExceptionTransformDefaultException
     */
    public TechnicalExceptionTransformDefaultException() {
        //DA-START:com.vd.gu.TechnicalExceptionTransformDefaultException:DA-START
        //DA-ELSE:com.vd.gu.TechnicalExceptionTransformDefaultException:DA-ELSE
        super();
        //DA-END:com.vd.gu.TechnicalExceptionTransformDefaultException:DA-END
    }
    
    /**
     * creates an instance of TechnicalExceptionTransformDefaultException
     *
     * @param arg0  the arg0
     */
    public TechnicalExceptionTransformDefaultException(String arg0) {
        //DA-START:com.vd.gu.TechnicalExceptionTransformDefaultException.String:DA-START
        //DA-ELSE:com.vd.gu.TechnicalExceptionTransformDefaultException.String:DA-ELSE
        super(arg0);
        //DA-END:com.vd.gu.TechnicalExceptionTransformDefaultException.String:DA-END
    }
    
    /**
     * creates an instance of TechnicalExceptionTransformDefaultException
     *
     * @param arg0  the arg0
     * @param arg1  the arg1
     */
    public TechnicalExceptionTransformDefaultException(String arg0, Throwable arg1) {
        //DA-START:com.vd.gu.TechnicalExceptionTransformDefaultException.String.Throwable:DA-START
        //DA-ELSE:com.vd.gu.TechnicalExceptionTransformDefaultException.String.Throwable:DA-ELSE
        super(arg0, arg1);
        //DA-END:com.vd.gu.TechnicalExceptionTransformDefaultException.String.Throwable:DA-END
    }
    
    /**
     * creates an instance of TechnicalExceptionTransformDefaultException
     *
     * @param arg0  the arg0
     */
    public TechnicalExceptionTransformDefaultException(Throwable arg0) {
        //DA-START:com.vd.gu.TechnicalExceptionTransformDefaultException.Throwable:DA-START
        //DA-ELSE:com.vd.gu.TechnicalExceptionTransformDefaultException.Throwable:DA-ELSE
        super(arg0);
        //DA-END:com.vd.gu.TechnicalExceptionTransformDefaultException.Throwable:DA-END
    }
    
    /**
     * creates an instance of TechnicalExceptionTransformDefaultException
     *
     * @param arg0  the arg0
     * @param arg1  the arg1
     * @param arg2  the arg2
     * @param arg3  the arg3
     */
    public TechnicalExceptionTransformDefaultException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
        //DA-START:com.vd.gu.TechnicalExceptionTransformDefaultException.String.Throwable.boolean.boolean:DA-START
        //DA-ELSE:com.vd.gu.TechnicalExceptionTransformDefaultException.String.Throwable.boolean.boolean:DA-ELSE
        super(arg0, arg1, arg2, arg3);
        //DA-END:com.vd.gu.TechnicalExceptionTransformDefaultException.String.Throwable.boolean.boolean:DA-END
    }
    
    
    /**
     * getter for the field responseCodeDefault
     *
     *
     *
     * @return
     */
    public ErrorBean getResponseCodeDefault() {
        //DA-START:com.vd.gu.TechnicalExceptionTransformDefaultException.getResponseCodeDefault.ErrorBean:DA-START
        //DA-ELSE:com.vd.gu.TechnicalExceptionTransformDefaultException.getResponseCodeDefault.ErrorBean:DA-ELSE
        return this.responseCodeDefault;
        //DA-END:com.vd.gu.TechnicalExceptionTransformDefaultException.getResponseCodeDefault.ErrorBean:DA-END
    }
    
    /**
     * setter for the field responseCodeDefault
     *
     *
     *
     * @param responseCodeDefault  the responseCodeDefault
     */
    public void setResponseCodeDefault(ErrorBean responseCodeDefault) {
        //DA-START:com.vd.gu.TechnicalExceptionTransformDefaultException.setResponseCodeDefault.ErrorBean:DA-START
        //DA-ELSE:com.vd.gu.TechnicalExceptionTransformDefaultException.setResponseCodeDefault.ErrorBean:DA-ELSE
        this.responseCodeDefault = responseCodeDefault;
        //DA-END:com.vd.gu.TechnicalExceptionTransformDefaultException.setResponseCodeDefault.ErrorBean:DA-END
    }
    
    //DA-START:com.vd.gu.TechnicalExceptionTransformDefaultException.additional.elements.in.type:DA-START
    //DA-ELSE:com.vd.gu.TechnicalExceptionTransformDefaultException.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.vd.gu.TechnicalExceptionTransformDefaultException.additional.elements.in.type:DA-END
} // end of java type
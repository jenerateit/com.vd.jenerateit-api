package com.vd.gu.definition;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * <table>
 * <tr><td>Generator</td><td>com.gs.vd.gen.java.retrofit.openapi:1.0</td></tr>
 * <tr><td>Element</td><td>code</td></tr>
 * <tr><td>Type</td><td>OpenAPI Schema</td></tr>
 * <tr><td>Module</td><td>none</td></tr>
 * </table>
 */
public enum ErrorCodeEnum { // start of class
    
    @JsonProperty(value="NOT_AVAILABLE")
    NOT_AVAILABLE("NOT_AVAILABLE"),
    
    @JsonProperty(value="SETUP_FAILED")
    SETUP_FAILED("SETUP_FAILED"),
    
    @JsonProperty(value="LOAD_ERROR")
    LOAD_ERROR("LOAD_ERROR"),
    
    @JsonProperty(value="TRANSFORM_ERROR")
    TRANSFORM_ERROR("TRANSFORM_ERROR"),
    
    @JsonProperty(value="UNKNOWN")
    UNKNOWN("UNKNOWN"),
    //DA-START:definition.ErrorCodeEnum.enum.constants:DA-START
    //DA-ELSE:definition.ErrorCodeEnum.enum.constants:DA-ELSE
    // add additional enum entries here 
    //DA-END:definition.ErrorCodeEnum.enum.constants:DA-END
    ;
    
    private String enumEntryValue;
    
    
    /**
     * creates an instance of ErrorCodeEnum
     *
     * @param enumEntryValue  the enumEntryValue
     */
    private ErrorCodeEnum(String enumEntryValue) {
        //DA-START:com.vd.gu.definition.ErrorCodeEnum.String:DA-START
        //DA-ELSE:com.vd.gu.definition.ErrorCodeEnum.String:DA-ELSE
        this.enumEntryValue = enumEntryValue;
        //DA-END:com.vd.gu.definition.ErrorCodeEnum.String:DA-END
    }
    
    /**
     * creates an instance of ErrorCodeEnum
     */
    private ErrorCodeEnum() {
        //DA-START:com.vd.gu.definition.ErrorCodeEnum:DA-START
        //DA-ELSE:com.vd.gu.definition.ErrorCodeEnum:DA-ELSE
        //DA-END:com.vd.gu.definition.ErrorCodeEnum:DA-END
    }
    
    
    
    /**
     * getter for the field enumEntryValue
     *
     *
     *
     * @return
     */
    public String getEnumEntryValue() {
        //DA-START:com.vd.gu.definition.ErrorCodeEnum.getEnumEntryValue.String:DA-START
        //DA-ELSE:com.vd.gu.definition.ErrorCodeEnum.getEnumEntryValue.String:DA-ELSE
        return this.enumEntryValue;
        //DA-END:com.vd.gu.definition.ErrorCodeEnum.getEnumEntryValue.String:DA-END
    }
    
    /**
     *
     * @return
     */
    @Override
    public String toString() {
        //DA-START:com.vd.gu.definition.ErrorCodeEnum.toString.String:DA-START
        //DA-ELSE:com.vd.gu.definition.ErrorCodeEnum.toString.String:DA-ELSE
        return this.enumEntryValue;
        //DA-END:com.vd.gu.definition.ErrorCodeEnum.toString.String:DA-END
    }
    
    
    
    //DA-START:com.vd.gu.definition.ErrorCodeEnum.additional.elements.in.type:DA-START
    //DA-ELSE:com.vd.gu.definition.ErrorCodeEnum.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.vd.gu.definition.ErrorCodeEnum.additional.elements.in.type:DA-END
} // end of java type
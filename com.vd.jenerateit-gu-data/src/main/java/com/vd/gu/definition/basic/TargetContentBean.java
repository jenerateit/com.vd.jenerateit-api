package com.vd.gu.definition.basic;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * <table>
 * <tr><td>Generator</td><td>com.gs.vd.gen.java.retrofit.openapi:1.0</td></tr>
 * <tr><td>Element</td><td>TargetContent</td></tr>
 * <tr><td>Type</td><td>OpenAPI Schema</td></tr>
 * <tr><td>Module</td><td>none</td></tr>
 * </table>
 */
@XmlRootElement(namespace="com.vd.gu.definition.basic")
public class TargetContentBean { // start of class

    private boolean didNotChanged;
    
    private int developerAreaCount;
    
    private int developerAreaModfied;
    
    private List<MessageBean> messages = new ArrayList<>();
    
    private FileBean file;
    
    /**
     * creates an instance of TargetContentBean
     */
    public TargetContentBean() {
        //DA-START:com.vd.gu.definition.basic.TargetContentBean:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.TargetContentBean:DA-ELSE
        //DA-END:com.vd.gu.definition.basic.TargetContentBean:DA-END
    }
    
    
    /**
     * getter for the field didNotChanged
     *
     *
     *
     * @return
     */
    //DA-START:com.vd.gu.definition.basic.TargetContentBean.isDidNotChanged.annotations:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.TargetContentBean.isDidNotChanged.annotations:DA-ELSE
    @XmlElement(name="didNotChanged")
    @JsonProperty(value="didNotChanged")
    //DA-END:com.vd.gu.definition.basic.TargetContentBean.isDidNotChanged.annotations:DA-END
    public boolean isDidNotChanged() {
        //DA-START:com.vd.gu.definition.basic.TargetContentBean.isDidNotChanged.boolean:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.TargetContentBean.isDidNotChanged.boolean:DA-ELSE
        return this.didNotChanged;
        //DA-END:com.vd.gu.definition.basic.TargetContentBean.isDidNotChanged.boolean:DA-END
    }
    
    /**
     * setter for the field didNotChanged
     *
     *
     *
     * @param didNotChanged  the didNotChanged
     */
    //DA-START:com.vd.gu.definition.basic.TargetContentBean.setDidNotChanged.boolean.annotations:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.TargetContentBean.setDidNotChanged.boolean.annotations:DA-ELSE
    //DA-END:com.vd.gu.definition.basic.TargetContentBean.setDidNotChanged.boolean.annotations:DA-END
    public void setDidNotChanged(boolean didNotChanged) {
        //DA-START:com.vd.gu.definition.basic.TargetContentBean.setDidNotChanged.boolean:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.TargetContentBean.setDidNotChanged.boolean:DA-ELSE
        this.didNotChanged = didNotChanged;
        //DA-END:com.vd.gu.definition.basic.TargetContentBean.setDidNotChanged.boolean:DA-END
    }
    
    /**
     * getter for the field developerAreaCount
     *
     *
     *
     * @return
     */
    //DA-START:com.vd.gu.definition.basic.TargetContentBean.getDeveloperAreaCount.annotations:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.TargetContentBean.getDeveloperAreaCount.annotations:DA-ELSE
    @XmlElement(name="developerAreaCount")
    @JsonProperty(value="developerAreaCount")
    //DA-END:com.vd.gu.definition.basic.TargetContentBean.getDeveloperAreaCount.annotations:DA-END
    public int getDeveloperAreaCount() {
        //DA-START:com.vd.gu.definition.basic.TargetContentBean.getDeveloperAreaCount.int:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.TargetContentBean.getDeveloperAreaCount.int:DA-ELSE
        return this.developerAreaCount;
        //DA-END:com.vd.gu.definition.basic.TargetContentBean.getDeveloperAreaCount.int:DA-END
    }
    
    /**
     * setter for the field developerAreaCount
     *
     *
     *
     * @param developerAreaCount  the developerAreaCount
     */
    //DA-START:com.vd.gu.definition.basic.TargetContentBean.setDeveloperAreaCount.int.annotations:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.TargetContentBean.setDeveloperAreaCount.int.annotations:DA-ELSE
    //DA-END:com.vd.gu.definition.basic.TargetContentBean.setDeveloperAreaCount.int.annotations:DA-END
    public void setDeveloperAreaCount(int developerAreaCount) {
        //DA-START:com.vd.gu.definition.basic.TargetContentBean.setDeveloperAreaCount.int:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.TargetContentBean.setDeveloperAreaCount.int:DA-ELSE
        this.developerAreaCount = developerAreaCount;
        //DA-END:com.vd.gu.definition.basic.TargetContentBean.setDeveloperAreaCount.int:DA-END
    }
    
    /**
     * getter for the field developerAreaModfied
     *
     *
     *
     * @return
     */
    //DA-START:com.vd.gu.definition.basic.TargetContentBean.getDeveloperAreaModfied.annotations:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.TargetContentBean.getDeveloperAreaModfied.annotations:DA-ELSE
    @XmlElement(name="developerAreaModfied")
    @JsonProperty(value="developerAreaModfied")
    //DA-END:com.vd.gu.definition.basic.TargetContentBean.getDeveloperAreaModfied.annotations:DA-END
    public int getDeveloperAreaModfied() {
        //DA-START:com.vd.gu.definition.basic.TargetContentBean.getDeveloperAreaModfied.int:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.TargetContentBean.getDeveloperAreaModfied.int:DA-ELSE
        return this.developerAreaModfied;
        //DA-END:com.vd.gu.definition.basic.TargetContentBean.getDeveloperAreaModfied.int:DA-END
    }
    
    /**
     * setter for the field developerAreaModfied
     *
     *
     *
     * @param developerAreaModfied  the developerAreaModfied
     */
    //DA-START:com.vd.gu.definition.basic.TargetContentBean.setDeveloperAreaModfied.int.annotations:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.TargetContentBean.setDeveloperAreaModfied.int.annotations:DA-ELSE
    //DA-END:com.vd.gu.definition.basic.TargetContentBean.setDeveloperAreaModfied.int.annotations:DA-END
    public void setDeveloperAreaModfied(int developerAreaModfied) {
        //DA-START:com.vd.gu.definition.basic.TargetContentBean.setDeveloperAreaModfied.int:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.TargetContentBean.setDeveloperAreaModfied.int:DA-ELSE
        this.developerAreaModfied = developerAreaModfied;
        //DA-END:com.vd.gu.definition.basic.TargetContentBean.setDeveloperAreaModfied.int:DA-END
    }
    
    /**
     * getter for the field messages
     *
     *
     *
     * @return
     */
    //DA-START:com.vd.gu.definition.basic.TargetContentBean.getMessages.annotations:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.TargetContentBean.getMessages.annotations:DA-ELSE
    @XmlElement(name="messages")
    @JsonProperty(value="messages")
    //DA-END:com.vd.gu.definition.basic.TargetContentBean.getMessages.annotations:DA-END
    public List<MessageBean> getMessages() {
        //DA-START:com.vd.gu.definition.basic.TargetContentBean.getMessages.List:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.TargetContentBean.getMessages.List:DA-ELSE
        return this.messages;
        //DA-END:com.vd.gu.definition.basic.TargetContentBean.getMessages.List:DA-END
    }
    
    /**
     * adder for the field messages
     *
     *
     *
     * @param element  the element
     * @return
     */
    //DA-START:com.vd.gu.definition.basic.TargetContentBean.addMessages.MessageBean.annotations:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.TargetContentBean.addMessages.MessageBean.annotations:DA-ELSE
    //DA-END:com.vd.gu.definition.basic.TargetContentBean.addMessages.MessageBean.annotations:DA-END
    public boolean addMessages(MessageBean element) {
        //DA-START:com.vd.gu.definition.basic.TargetContentBean.addMessages.MessageBean.boolean:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.TargetContentBean.addMessages.MessageBean.boolean:DA-ELSE
        return this.getMessages().add(element);
        //DA-END:com.vd.gu.definition.basic.TargetContentBean.addMessages.MessageBean.boolean:DA-END
    }
    
    /**
     * remover for the field messages
     *
     *
     *
     * @param element  the element
     * @return
     */
    //DA-START:com.vd.gu.definition.basic.TargetContentBean.removeMessages.MessageBean.annotations:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.TargetContentBean.removeMessages.MessageBean.annotations:DA-ELSE
    //DA-END:com.vd.gu.definition.basic.TargetContentBean.removeMessages.MessageBean.annotations:DA-END
    public boolean removeMessages(MessageBean element) {
        //DA-START:com.vd.gu.definition.basic.TargetContentBean.removeMessages.MessageBean.boolean:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.TargetContentBean.removeMessages.MessageBean.boolean:DA-ELSE
        return this.messages.remove(element);
        //DA-END:com.vd.gu.definition.basic.TargetContentBean.removeMessages.MessageBean.boolean:DA-END
    }
    
    /**
     * getter for the field file
     *
     *
     *
     * @return
     */
    //DA-START:com.vd.gu.definition.basic.TargetContentBean.getFile.annotations:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.TargetContentBean.getFile.annotations:DA-ELSE
    @XmlElement(name="file")
    @JsonProperty(value="file")
    //DA-END:com.vd.gu.definition.basic.TargetContentBean.getFile.annotations:DA-END
    public FileBean getFile() {
        //DA-START:com.vd.gu.definition.basic.TargetContentBean.getFile.FileBean:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.TargetContentBean.getFile.FileBean:DA-ELSE
        return this.file;
        //DA-END:com.vd.gu.definition.basic.TargetContentBean.getFile.FileBean:DA-END
    }
    
    /**
     * setter for the field file
     *
     *
     *
     * @param file  the file
     */
    //DA-START:com.vd.gu.definition.basic.TargetContentBean.setFile.FileBean.annotations:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.TargetContentBean.setFile.FileBean.annotations:DA-ELSE
    //DA-END:com.vd.gu.definition.basic.TargetContentBean.setFile.FileBean.annotations:DA-END
    public void setFile(FileBean file) {
        //DA-START:com.vd.gu.definition.basic.TargetContentBean.setFile.FileBean:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.TargetContentBean.setFile.FileBean:DA-ELSE
        this.file = file;
        //DA-END:com.vd.gu.definition.basic.TargetContentBean.setFile.FileBean:DA-END
    }
    
    //DA-START:com.vd.gu.definition.basic.TargetContentBean.additional.elements.in.type:DA-START
    @Override
	public String toString() {
		return "TargetContentBean [didNotChanged=" + didNotChanged + ", developerAreaCount=" + developerAreaCount
				+ ", developerAreaModfied=" + developerAreaModfied + ", number of messages=" + messages.size() + ", file=" + file + "]";
	}
    //DA-ELSE:com.vd.gu.definition.basic.TargetContentBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.vd.gu.definition.basic.TargetContentBean.additional.elements.in.type:DA-END
} // end of java type
package com.vd.gu.definition.basic;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * <table>
 * <tr><td>Generator</td><td>com.gs.vd.gen.java.retrofit.openapi:1.0</td></tr>
 * <tr><td>Element</td><td>LoadError</td></tr>
 * <tr><td>Type</td><td>OpenAPI Schema</td></tr>
 * <tr><td>Module</td><td>none</td></tr>
 * </table>
 */
@XmlRootElement(namespace="com.vd.gu.definition.basic")
public class LoadErrorBean extends ErrorBean { // start of class

    private LoadResultBean loadResult;
    
    /**
     * creates an instance of LoadErrorBean
     */
    public LoadErrorBean() {
        //DA-START:com.vd.gu.definition.basic.LoadErrorBean:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.LoadErrorBean:DA-ELSE
        super();
        //DA-END:com.vd.gu.definition.basic.LoadErrorBean:DA-END
    }
    
    
    /**
     * getter for the field loadResult
     *
     *
     *
     * @return
     */
    //DA-START:com.vd.gu.definition.basic.LoadErrorBean.getLoadResult.annotations:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.LoadErrorBean.getLoadResult.annotations:DA-ELSE
    @XmlElement(name="loadResult")
    @JsonProperty(value="loadResult")
    //DA-END:com.vd.gu.definition.basic.LoadErrorBean.getLoadResult.annotations:DA-END
    public LoadResultBean getLoadResult() {
        //DA-START:com.vd.gu.definition.basic.LoadErrorBean.getLoadResult.LoadResultBean:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.LoadErrorBean.getLoadResult.LoadResultBean:DA-ELSE
        return this.loadResult;
        //DA-END:com.vd.gu.definition.basic.LoadErrorBean.getLoadResult.LoadResultBean:DA-END
    }
    
    /**
     * setter for the field loadResult
     *
     *
     *
     * @param loadResult  the loadResult
     */
    //DA-START:com.vd.gu.definition.basic.LoadErrorBean.setLoadResult.LoadResultBean.annotations:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.LoadErrorBean.setLoadResult.LoadResultBean.annotations:DA-ELSE
    //DA-END:com.vd.gu.definition.basic.LoadErrorBean.setLoadResult.LoadResultBean.annotations:DA-END
    public void setLoadResult(LoadResultBean loadResult) {
        //DA-START:com.vd.gu.definition.basic.LoadErrorBean.setLoadResult.LoadResultBean:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.LoadErrorBean.setLoadResult.LoadResultBean:DA-ELSE
        this.loadResult = loadResult;
        //DA-END:com.vd.gu.definition.basic.LoadErrorBean.setLoadResult.LoadResultBean:DA-END
    }
    
    //DA-START:com.vd.gu.definition.basic.LoadErrorBean.additional.elements.in.type:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.LoadErrorBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.vd.gu.definition.basic.LoadErrorBean.additional.elements.in.type:DA-END
} // end of java type
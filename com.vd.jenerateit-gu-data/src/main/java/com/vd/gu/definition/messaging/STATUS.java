package com.vd.gu.definition.messaging;

public enum STATUS {
	START,
	DONE,
	ERROR,
	UP,
	DOWN,
}

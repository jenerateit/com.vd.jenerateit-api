package com.vd.gu.definition.basic;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * <table>
 * <tr><td>Generator</td><td>com.gs.vd.gen.java.retrofit.openapi:1.0</td></tr>
 * <tr><td>Element</td><td>LoadResult</td></tr>
 * <tr><td>Type</td><td>OpenAPI Schema</td></tr>
 * <tr><td>Module</td><td>none</td></tr>
 * </table>
 */
@XmlRootElement(namespace="com.vd.gu.definition.basic")
public class LoadResultBean { // start of class

    private List<MessageBean> messages = new ArrayList<>();
    
    private List<ResultBean> results = new ArrayList<>();
    
    /**
     * creates an instance of LoadResultBean
     */
    public LoadResultBean() {
        //DA-START:com.vd.gu.definition.basic.LoadResultBean:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.LoadResultBean:DA-ELSE
        //DA-END:com.vd.gu.definition.basic.LoadResultBean:DA-END
    }
    
    
    /**
     * getter for the field messages
     *
     *
     *
     * @return
     */
    //DA-START:com.vd.gu.definition.basic.LoadResultBean.getMessages.annotations:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.LoadResultBean.getMessages.annotations:DA-ELSE
    @XmlElement(name="messages")
    @JsonProperty(value="messages")
    //DA-END:com.vd.gu.definition.basic.LoadResultBean.getMessages.annotations:DA-END
    public List<MessageBean> getMessages() {
        //DA-START:com.vd.gu.definition.basic.LoadResultBean.getMessages.List:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.LoadResultBean.getMessages.List:DA-ELSE
        return this.messages;
        //DA-END:com.vd.gu.definition.basic.LoadResultBean.getMessages.List:DA-END
    }
    
    /**
     * adder for the field messages
     *
     *
     *
     * @param element  the element
     * @return
     */
    //DA-START:com.vd.gu.definition.basic.LoadResultBean.addMessages.MessageBean.annotations:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.LoadResultBean.addMessages.MessageBean.annotations:DA-ELSE
    //DA-END:com.vd.gu.definition.basic.LoadResultBean.addMessages.MessageBean.annotations:DA-END
    public boolean addMessages(MessageBean element) {
        //DA-START:com.vd.gu.definition.basic.LoadResultBean.addMessages.MessageBean.boolean:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.LoadResultBean.addMessages.MessageBean.boolean:DA-ELSE
        return this.getMessages().add(element);
        //DA-END:com.vd.gu.definition.basic.LoadResultBean.addMessages.MessageBean.boolean:DA-END
    }
    
    /**
     * remover for the field messages
     *
     *
     *
     * @param element  the element
     * @return
     */
    //DA-START:com.vd.gu.definition.basic.LoadResultBean.removeMessages.MessageBean.annotations:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.LoadResultBean.removeMessages.MessageBean.annotations:DA-ELSE
    //DA-END:com.vd.gu.definition.basic.LoadResultBean.removeMessages.MessageBean.annotations:DA-END
    public boolean removeMessages(MessageBean element) {
        //DA-START:com.vd.gu.definition.basic.LoadResultBean.removeMessages.MessageBean.boolean:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.LoadResultBean.removeMessages.MessageBean.boolean:DA-ELSE
        return this.messages.remove(element);
        //DA-END:com.vd.gu.definition.basic.LoadResultBean.removeMessages.MessageBean.boolean:DA-END
    }
    
    /**
     * getter for the field results
     *
     *
     *
     * @return
     */
    //DA-START:com.vd.gu.definition.basic.LoadResultBean.getResults.annotations:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.LoadResultBean.getResults.annotations:DA-ELSE
    @XmlElement(name="results")
    @JsonProperty(value="results")
    //DA-END:com.vd.gu.definition.basic.LoadResultBean.getResults.annotations:DA-END
    public List<ResultBean> getResults() {
        //DA-START:com.vd.gu.definition.basic.LoadResultBean.getResults.List:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.LoadResultBean.getResults.List:DA-ELSE
        return this.results;
        //DA-END:com.vd.gu.definition.basic.LoadResultBean.getResults.List:DA-END
    }
    
    /**
     * adder for the field results
     *
     *
     *
     * @param element  the element
     * @return
     */
    //DA-START:com.vd.gu.definition.basic.LoadResultBean.addResults.ResultBean.annotations:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.LoadResultBean.addResults.ResultBean.annotations:DA-ELSE
    //DA-END:com.vd.gu.definition.basic.LoadResultBean.addResults.ResultBean.annotations:DA-END
    public boolean addResults(ResultBean element) {
        //DA-START:com.vd.gu.definition.basic.LoadResultBean.addResults.ResultBean.boolean:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.LoadResultBean.addResults.ResultBean.boolean:DA-ELSE
        return this.getResults().add(element);
        //DA-END:com.vd.gu.definition.basic.LoadResultBean.addResults.ResultBean.boolean:DA-END
    }
    
    /**
     * remover for the field results
     *
     *
     *
     * @param element  the element
     * @return
     */
    //DA-START:com.vd.gu.definition.basic.LoadResultBean.removeResults.ResultBean.annotations:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.LoadResultBean.removeResults.ResultBean.annotations:DA-ELSE
    //DA-END:com.vd.gu.definition.basic.LoadResultBean.removeResults.ResultBean.annotations:DA-END
    public boolean removeResults(ResultBean element) {
        //DA-START:com.vd.gu.definition.basic.LoadResultBean.removeResults.ResultBean.boolean:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.LoadResultBean.removeResults.ResultBean.boolean:DA-ELSE
        return this.results.remove(element);
        //DA-END:com.vd.gu.definition.basic.LoadResultBean.removeResults.ResultBean.boolean:DA-END
    }
    
    //DA-START:com.vd.gu.definition.basic.LoadResultBean.additional.elements.in.type:DA-START
    public ResultBean getOrCreateResult(String virtualProject) {
    	ResultBean result = this.results.stream()
    			.filter(r -> r.getVirtualProject().equals(virtualProject))
    			.findAny().orElse(null);
    	if (result == null) {
    		result = new ResultBean();
    		result.setVirtualProject(virtualProject);
    		this.addResults(result);
    	}
    	return result;
    }
    //DA-ELSE:com.vd.gu.definition.basic.LoadResultBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.vd.gu.definition.basic.LoadResultBean.additional.elements.in.type:DA-END
} // end of java type
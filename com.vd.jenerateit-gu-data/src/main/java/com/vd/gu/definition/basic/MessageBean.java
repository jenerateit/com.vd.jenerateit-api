package com.vd.gu.definition.basic;

import javax.xml.bind.annotation.XmlRootElement;
import com.vd.gu.definition.MessageTypeEnum;
import javax.xml.bind.annotation.XmlElement;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.vd.gu.definition.MessageStepEnum;


/**
 * <table>
 * <tr><td>Generator</td><td>com.gs.vd.gen.java.retrofit.openapi:1.0</td></tr>
 * <tr><td>Element</td><td>Message</td></tr>
 * <tr><td>Type</td><td>OpenAPI Schema</td></tr>
 * <tr><td>Module</td><td>none</td></tr>
 * </table>
 */
@XmlRootElement(namespace="com.vd.gu.definition.basic")
public class MessageBean { // start of class

    private MessageTypeEnum type;
    
    private String code;
    
    private MessageStepEnum step;
    
    private String message;
    
    /**
     * creates an instance of MessageBean
     */
    public MessageBean() {
        //DA-START:com.vd.gu.definition.basic.MessageBean:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.MessageBean:DA-ELSE
        //DA-END:com.vd.gu.definition.basic.MessageBean:DA-END
    }
    
    
    /**
     * getter for the field type
     *
     *
     *
     * @return
     */
    //DA-START:com.vd.gu.definition.basic.MessageBean.getType.annotations:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.MessageBean.getType.annotations:DA-ELSE
    @XmlElement(name="type")
    @JsonProperty(value="type")
    //DA-END:com.vd.gu.definition.basic.MessageBean.getType.annotations:DA-END
    public MessageTypeEnum getType() {
        //DA-START:com.vd.gu.definition.basic.MessageBean.getType.MessageTypeEnum:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.MessageBean.getType.MessageTypeEnum:DA-ELSE
        return this.type;
        //DA-END:com.vd.gu.definition.basic.MessageBean.getType.MessageTypeEnum:DA-END
    }
    
    /**
     * setter for the field type
     *
     *
     *
     * @param type  the type
     */
    //DA-START:com.vd.gu.definition.basic.MessageBean.setType.MessageTypeEnum.annotations:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.MessageBean.setType.MessageTypeEnum.annotations:DA-ELSE
    //DA-END:com.vd.gu.definition.basic.MessageBean.setType.MessageTypeEnum.annotations:DA-END
    public void setType(MessageTypeEnum type) {
        //DA-START:com.vd.gu.definition.basic.MessageBean.setType.MessageTypeEnum:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.MessageBean.setType.MessageTypeEnum:DA-ELSE
        this.type = type;
        //DA-END:com.vd.gu.definition.basic.MessageBean.setType.MessageTypeEnum:DA-END
    }
    
    /**
     * getter for the field code
     *
     *
     *
     * @return
     */
    //DA-START:com.vd.gu.definition.basic.MessageBean.getCode.annotations:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.MessageBean.getCode.annotations:DA-ELSE
    @XmlElement(name="code")
    @JsonProperty(value="code")
    //DA-END:com.vd.gu.definition.basic.MessageBean.getCode.annotations:DA-END
    public String getCode() {
        //DA-START:com.vd.gu.definition.basic.MessageBean.getCode.String:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.MessageBean.getCode.String:DA-ELSE
        return this.code;
        //DA-END:com.vd.gu.definition.basic.MessageBean.getCode.String:DA-END
    }
    
    /**
     * setter for the field code
     *
     *
     *
     * @param code  the code
     */
    //DA-START:com.vd.gu.definition.basic.MessageBean.setCode.String.annotations:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.MessageBean.setCode.String.annotations:DA-ELSE
    //DA-END:com.vd.gu.definition.basic.MessageBean.setCode.String.annotations:DA-END
    public void setCode(String code) {
        //DA-START:com.vd.gu.definition.basic.MessageBean.setCode.String:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.MessageBean.setCode.String:DA-ELSE
        this.code = code;
        //DA-END:com.vd.gu.definition.basic.MessageBean.setCode.String:DA-END
    }
    
    /**
     * getter for the field step
     *
     *
     *
     * @return
     */
    //DA-START:com.vd.gu.definition.basic.MessageBean.getStep.annotations:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.MessageBean.getStep.annotations:DA-ELSE
    @XmlElement(name="step")
    @JsonProperty(value="step")
    //DA-END:com.vd.gu.definition.basic.MessageBean.getStep.annotations:DA-END
    public MessageStepEnum getStep() {
        //DA-START:com.vd.gu.definition.basic.MessageBean.getStep.MessageStepEnum:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.MessageBean.getStep.MessageStepEnum:DA-ELSE
        return this.step;
        //DA-END:com.vd.gu.definition.basic.MessageBean.getStep.MessageStepEnum:DA-END
    }
    
    /**
     * setter for the field step
     *
     *
     *
     * @param step  the step
     */
    //DA-START:com.vd.gu.definition.basic.MessageBean.setStep.MessageStepEnum.annotations:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.MessageBean.setStep.MessageStepEnum.annotations:DA-ELSE
    //DA-END:com.vd.gu.definition.basic.MessageBean.setStep.MessageStepEnum.annotations:DA-END
    public void setStep(MessageStepEnum step) {
        //DA-START:com.vd.gu.definition.basic.MessageBean.setStep.MessageStepEnum:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.MessageBean.setStep.MessageStepEnum:DA-ELSE
        this.step = step;
        //DA-END:com.vd.gu.definition.basic.MessageBean.setStep.MessageStepEnum:DA-END
    }
    
    /**
     * getter for the field message
     *
     *
     *
     * @return
     */
    //DA-START:com.vd.gu.definition.basic.MessageBean.getMessage.annotations:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.MessageBean.getMessage.annotations:DA-ELSE
    @XmlElement(name="message")
    @JsonProperty(value="message")
    //DA-END:com.vd.gu.definition.basic.MessageBean.getMessage.annotations:DA-END
    public String getMessage() {
        //DA-START:com.vd.gu.definition.basic.MessageBean.getMessage.String:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.MessageBean.getMessage.String:DA-ELSE
        return this.message;
        //DA-END:com.vd.gu.definition.basic.MessageBean.getMessage.String:DA-END
    }
    
    /**
     * setter for the field message
     *
     *
     *
     * @param message  the message
     */
    //DA-START:com.vd.gu.definition.basic.MessageBean.setMessage.String.annotations:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.MessageBean.setMessage.String.annotations:DA-ELSE
    //DA-END:com.vd.gu.definition.basic.MessageBean.setMessage.String.annotations:DA-END
    public void setMessage(String message) {
        //DA-START:com.vd.gu.definition.basic.MessageBean.setMessage.String:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.MessageBean.setMessage.String:DA-ELSE
        this.message = message;
        //DA-END:com.vd.gu.definition.basic.MessageBean.setMessage.String:DA-END
    }
    
    //DA-START:com.vd.gu.definition.basic.MessageBean.additional.elements.in.type:DA-START
    public MessageBean(MessageTypeEnum type, String code, MessageStepEnum step, String message) {
    	this.type = type;
    	this.code = code;
    	this.step = step;
    	this.message = message;
    }

	@Override
	public String toString() {
		return type + ":" + message;
	}
    //DA-ELSE:com.vd.gu.definition.basic.MessageBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.vd.gu.definition.basic.MessageBean.additional.elements.in.type:DA-END
} // end of java type
package com.vd.gu.definition.messaging;

public class User {
	private final String uuid;
	private final String realm;

	public User(String uuid, String realm) {
		this.uuid = uuid;
		this.realm = realm;
	}
	
	public String getUuid() {
		return uuid;
	}
	
	public String getRealm() {
		return realm;
	}
}

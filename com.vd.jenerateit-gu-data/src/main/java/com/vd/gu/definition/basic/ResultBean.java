package com.vd.gu.definition.basic;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * <table>
 * <tr><td>Generator</td><td>com.gs.vd.gen.java.retrofit.openapi:1.0</td></tr>
 * <tr><td>Element</td><td>Result</td></tr>
 * <tr><td>Type</td><td>OpenAPI Schema</td></tr>
 * <tr><td>Module</td><td>none</td></tr>
 * </table>
 */
@XmlRootElement(namespace="com.vd.gu.definition.basic")
public class ResultBean { // start of class

    private String virtualProject;
    
    private List<String> references = new ArrayList<>();
    
    /**
     * creates an instance of ResultBean
     */
    public ResultBean() {
        //DA-START:com.vd.gu.definition.basic.ResultBean:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.ResultBean:DA-ELSE
        //DA-END:com.vd.gu.definition.basic.ResultBean:DA-END
    }
    
    
    /**
     * getter for the field virtualProject
     *
     *
     *
     * @return
     */
    //DA-START:com.vd.gu.definition.basic.ResultBean.getVirtualProject.annotations:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.ResultBean.getVirtualProject.annotations:DA-ELSE
    @XmlElement(name="virtualProject")
    @JsonProperty(value="virtualProject")
    //DA-END:com.vd.gu.definition.basic.ResultBean.getVirtualProject.annotations:DA-END
    public String getVirtualProject() {
        //DA-START:com.vd.gu.definition.basic.ResultBean.getVirtualProject.String:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.ResultBean.getVirtualProject.String:DA-ELSE
        return this.virtualProject;
        //DA-END:com.vd.gu.definition.basic.ResultBean.getVirtualProject.String:DA-END
    }
    
    /**
     * setter for the field virtualProject
     *
     *
     *
     * @param virtualProject  the virtualProject
     */
    //DA-START:com.vd.gu.definition.basic.ResultBean.setVirtualProject.String.annotations:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.ResultBean.setVirtualProject.String.annotations:DA-ELSE
    //DA-END:com.vd.gu.definition.basic.ResultBean.setVirtualProject.String.annotations:DA-END
    public void setVirtualProject(String virtualProject) {
        //DA-START:com.vd.gu.definition.basic.ResultBean.setVirtualProject.String:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.ResultBean.setVirtualProject.String:DA-ELSE
        this.virtualProject = virtualProject;
        //DA-END:com.vd.gu.definition.basic.ResultBean.setVirtualProject.String:DA-END
    }
    
    /**
     * getter for the field references
     *
     *
     *
     * @return
     */
    //DA-START:com.vd.gu.definition.basic.ResultBean.getReferences.annotations:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.ResultBean.getReferences.annotations:DA-ELSE
    @XmlElement(name="references")
    @JsonProperty(value="references")
    //DA-END:com.vd.gu.definition.basic.ResultBean.getReferences.annotations:DA-END
    public List<String> getReferences() {
        //DA-START:com.vd.gu.definition.basic.ResultBean.getReferences.List:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.ResultBean.getReferences.List:DA-ELSE
        return this.references;
        //DA-END:com.vd.gu.definition.basic.ResultBean.getReferences.List:DA-END
    }
    
    /**
     * adder for the field references
     *
     *
     *
     * @param element  the element
     * @return
     */
    //DA-START:com.vd.gu.definition.basic.ResultBean.addReferences.String.annotations:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.ResultBean.addReferences.String.annotations:DA-ELSE
    //DA-END:com.vd.gu.definition.basic.ResultBean.addReferences.String.annotations:DA-END
    public boolean addReferences(String element) {
        //DA-START:com.vd.gu.definition.basic.ResultBean.addReferences.String.boolean:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.ResultBean.addReferences.String.boolean:DA-ELSE
        return this.getReferences().add(element);
        //DA-END:com.vd.gu.definition.basic.ResultBean.addReferences.String.boolean:DA-END
    }
    
    /**
     * remover for the field references
     *
     *
     *
     * @param element  the element
     * @return
     */
    //DA-START:com.vd.gu.definition.basic.ResultBean.removeReferences.String.annotations:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.ResultBean.removeReferences.String.annotations:DA-ELSE
    //DA-END:com.vd.gu.definition.basic.ResultBean.removeReferences.String.annotations:DA-END
    public boolean removeReferences(String element) {
        //DA-START:com.vd.gu.definition.basic.ResultBean.removeReferences.String.boolean:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.ResultBean.removeReferences.String.boolean:DA-ELSE
        return this.references.remove(element);
        //DA-END:com.vd.gu.definition.basic.ResultBean.removeReferences.String.boolean:DA-END
    }
    
    //DA-START:com.vd.gu.definition.basic.ResultBean.additional.elements.in.type:DA-START
    public static String getTargetPathFromReference(String reference) {
		StringTokenizer st = new StringTokenizer(reference, "/");
		String token = null;
		while (st.hasMoreTokens()) {
			token = st.nextToken();
			if ("target".compareTo(token) == 0) {
				if (st.hasMoreTokens()) {
					final StringBuilder sb = new StringBuilder(st.nextToken());
					while (st.hasMoreTokens()) {
						sb.append("/").append(st.nextToken());
					}
					return sb.toString();
				}
			}
		}
		throw new NullPointerException("reference is not a target reference: '" + reference + "'");
    }
    //DA-ELSE:com.vd.gu.definition.basic.ResultBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.vd.gu.definition.basic.ResultBean.additional.elements.in.type:DA-END
} // end of java type
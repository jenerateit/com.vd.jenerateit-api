package com.vd.gu.definition.basic;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * <table>
 * <tr><td>Generator</td><td>com.gs.vd.gen.java.retrofit.openapi:1.0</td></tr>
 * <tr><td>Element</td><td>File</td></tr>
 * <tr><td>Type</td><td>OpenAPI Schema</td></tr>
 * <tr><td>Module</td><td>none</td></tr>
 * </table>
 */
@XmlRootElement(namespace="com.vd.gu.definition.basic")
public class FileBean { // start of class

    private String path;
    
    private String charset;
    
    private String contentType;
    
    private byte[] content = new byte[0];
    
    /**
     * creates an instance of FileBean
     */
    public FileBean() {
        //DA-START:com.vd.gu.definition.basic.FileBean:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.FileBean:DA-ELSE
        //DA-END:com.vd.gu.definition.basic.FileBean:DA-END
    }
    
    
    /**
     * getter for the field path
     *
     *
     *
     * @return
     */
    //DA-START:com.vd.gu.definition.basic.FileBean.getPath.annotations:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.FileBean.getPath.annotations:DA-ELSE
    @XmlElement(name="path")
    @JsonProperty(value="path")
    //DA-END:com.vd.gu.definition.basic.FileBean.getPath.annotations:DA-END
    public String getPath() {
        //DA-START:com.vd.gu.definition.basic.FileBean.getPath.String:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.FileBean.getPath.String:DA-ELSE
        return this.path;
        //DA-END:com.vd.gu.definition.basic.FileBean.getPath.String:DA-END
    }
    
    /**
     * setter for the field path
     *
     *
     *
     * @param path  the path
     */
    //DA-START:com.vd.gu.definition.basic.FileBean.setPath.String.annotations:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.FileBean.setPath.String.annotations:DA-ELSE
    //DA-END:com.vd.gu.definition.basic.FileBean.setPath.String.annotations:DA-END
    public void setPath(String path) {
        //DA-START:com.vd.gu.definition.basic.FileBean.setPath.String:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.FileBean.setPath.String:DA-ELSE
        this.path = path;
        //DA-END:com.vd.gu.definition.basic.FileBean.setPath.String:DA-END
    }
    
    /**
     * getter for the field charset
     *
     *
     *
     * @return
     */
    //DA-START:com.vd.gu.definition.basic.FileBean.getCharset.annotations:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.FileBean.getCharset.annotations:DA-ELSE
    @XmlElement(name="charset")
    @JsonProperty(value="charset")
    //DA-END:com.vd.gu.definition.basic.FileBean.getCharset.annotations:DA-END
    public String getCharset() {
        //DA-START:com.vd.gu.definition.basic.FileBean.getCharset.String:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.FileBean.getCharset.String:DA-ELSE
        return this.charset;
        //DA-END:com.vd.gu.definition.basic.FileBean.getCharset.String:DA-END
    }
    
    /**
     * setter for the field charset
     *
     *
     *
     * @param charset  the charset
     */
    //DA-START:com.vd.gu.definition.basic.FileBean.setCharset.String.annotations:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.FileBean.setCharset.String.annotations:DA-ELSE
    //DA-END:com.vd.gu.definition.basic.FileBean.setCharset.String.annotations:DA-END
    public void setCharset(String charset) {
        //DA-START:com.vd.gu.definition.basic.FileBean.setCharset.String:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.FileBean.setCharset.String:DA-ELSE
        this.charset = charset;
        //DA-END:com.vd.gu.definition.basic.FileBean.setCharset.String:DA-END
    }
    
    /**
     * getter for the field contentType
     *
     *
     *
     * @return
     */
    //DA-START:com.vd.gu.definition.basic.FileBean.getContentType.annotations:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.FileBean.getContentType.annotations:DA-ELSE
    @XmlElement(name="contentType")
    @JsonProperty(value="contentType")
    //DA-END:com.vd.gu.definition.basic.FileBean.getContentType.annotations:DA-END
    public String getContentType() {
        //DA-START:com.vd.gu.definition.basic.FileBean.getContentType.String:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.FileBean.getContentType.String:DA-ELSE
        return this.contentType;
        //DA-END:com.vd.gu.definition.basic.FileBean.getContentType.String:DA-END
    }
    
    /**
     * setter for the field contentType
     *
     *
     *
     * @param contentType  the contentType
     */
    //DA-START:com.vd.gu.definition.basic.FileBean.setContentType.String.annotations:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.FileBean.setContentType.String.annotations:DA-ELSE
    //DA-END:com.vd.gu.definition.basic.FileBean.setContentType.String.annotations:DA-END
    public void setContentType(String contentType) {
        //DA-START:com.vd.gu.definition.basic.FileBean.setContentType.String:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.FileBean.setContentType.String:DA-ELSE
        this.contentType = contentType;
        //DA-END:com.vd.gu.definition.basic.FileBean.setContentType.String:DA-END
    }
    
    /**
     * getter for the field content
     *
     *
     *
     * @return
     */
    //DA-START:com.vd.gu.definition.basic.FileBean.getContent.annotations:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.FileBean.getContent.annotations:DA-ELSE
    @XmlElement(name="content")
    @JsonProperty(value="content")
    //DA-END:com.vd.gu.definition.basic.FileBean.getContent.annotations:DA-END
    public byte[] getContent() {
        //DA-START:com.vd.gu.definition.basic.FileBean.getContent.byte.ARRAY:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.FileBean.getContent.byte.ARRAY:DA-ELSE
        return this.content;
        //DA-END:com.vd.gu.definition.basic.FileBean.getContent.byte.ARRAY:DA-END
    }
    
    /**
     * setter for the field content
     *
     *
     *
     * @param content  the content
     */
    //DA-START:com.vd.gu.definition.basic.FileBean.setContent.byte[].annotations:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.FileBean.setContent.byte[].annotations:DA-ELSE
    //DA-END:com.vd.gu.definition.basic.FileBean.setContent.byte[].annotations:DA-END
    public void setContent(byte[] content) {
        //DA-START:com.vd.gu.definition.basic.FileBean.setContent.byte.ARRAY:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.FileBean.setContent.byte.ARRAY:DA-ELSE
        this.content = content;
        //DA-END:com.vd.gu.definition.basic.FileBean.setContent.byte.ARRAY:DA-END
    }
    
    //DA-START:com.vd.gu.definition.basic.FileBean.additional.elements.in.type:DA-START
    @Override
	public String toString() {
		return "path=" + path;
	}
    //DA-ELSE:com.vd.gu.definition.basic.FileBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.vd.gu.definition.basic.FileBean.additional.elements.in.type:DA-END
} // end of java type
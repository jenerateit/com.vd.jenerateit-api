package com.vd.gu.definition.messaging;

import java.util.Collection;

import com.vd.gu.definition.basic.OptionBean;

public class Setup extends Log {

	private Collection<OptionBean> options;
	
	public static Setup createSetupStart(User user, Generator generator, Collection<OptionBean> options) {
		return new Setup(STATUS.START, user, generator, System.currentTimeMillis(), options, null);
	}
	
	public static Setup createSetupDone(User user, Generator generator) {
		return new Setup(STATUS.DONE, user, generator, System.currentTimeMillis(), null, null);
	}
	
	public static Setup createSetupError(User user, Generator generator, Exception e) {
		return new Setup(STATUS.ERROR, user, generator, System.currentTimeMillis(), null, e);
	}
	
	private Setup(STATUS status, User user, Generator generator, long timestamp, Collection<OptionBean> options, Exception exception) {
		super(status, user, generator, timestamp, exception);
		this.options = options;
	}
	
	private Setup() { }
	
	public Collection<OptionBean> getOptions() {
		return options;
	}
}

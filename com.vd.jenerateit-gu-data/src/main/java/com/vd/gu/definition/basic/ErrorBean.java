package com.vd.gu.definition.basic;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.vd.gu.definition.ErrorCodeEnum;


/**
 * <table>
 * <tr><td>Generator</td><td>com.gs.vd.gen.java.retrofit.openapi:1.0</td></tr>
 * <tr><td>Element</td><td>Error</td></tr>
 * <tr><td>Type</td><td>OpenAPI Schema</td></tr>
 * <tr><td>Module</td><td>none</td></tr>
 * </table>
 */
@XmlRootElement(namespace="com.vd.gu.definition.basic")
public class ErrorBean { // start of class

    private ErrorCodeEnum code;
    
    private String message;
    
    /**
     * creates an instance of ErrorBean
     */
    public ErrorBean() {
        //DA-START:com.vd.gu.definition.basic.ErrorBean:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.ErrorBean:DA-ELSE
        //DA-END:com.vd.gu.definition.basic.ErrorBean:DA-END
    }
    
    
    /**
     * getter for the field code
     *
     *
     *
     * @return
     */
    //DA-START:com.vd.gu.definition.basic.ErrorBean.getCode.annotations:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.ErrorBean.getCode.annotations:DA-ELSE
    @XmlElement(name="code")
    @JsonProperty(value="code")
    //DA-END:com.vd.gu.definition.basic.ErrorBean.getCode.annotations:DA-END
    public ErrorCodeEnum getCode() {
        //DA-START:com.vd.gu.definition.basic.ErrorBean.getCode.ErrorCodeEnum:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.ErrorBean.getCode.ErrorCodeEnum:DA-ELSE
        return this.code;
        //DA-END:com.vd.gu.definition.basic.ErrorBean.getCode.ErrorCodeEnum:DA-END
    }
    
    /**
     * setter for the field code
     *
     *
     *
     * @param code  the code
     */
    //DA-START:com.vd.gu.definition.basic.ErrorBean.setCode.ErrorCodeEnum.annotations:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.ErrorBean.setCode.ErrorCodeEnum.annotations:DA-ELSE
    //DA-END:com.vd.gu.definition.basic.ErrorBean.setCode.ErrorCodeEnum.annotations:DA-END
    public void setCode(ErrorCodeEnum code) {
        //DA-START:com.vd.gu.definition.basic.ErrorBean.setCode.ErrorCodeEnum:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.ErrorBean.setCode.ErrorCodeEnum:DA-ELSE
        this.code = code;
        //DA-END:com.vd.gu.definition.basic.ErrorBean.setCode.ErrorCodeEnum:DA-END
    }
    
    /**
     * getter for the field message
     *
     *
     *
     * @return
     */
    //DA-START:com.vd.gu.definition.basic.ErrorBean.getMessage.annotations:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.ErrorBean.getMessage.annotations:DA-ELSE
    @XmlElement(name="message")
    @JsonProperty(value="message")
    //DA-END:com.vd.gu.definition.basic.ErrorBean.getMessage.annotations:DA-END
    public String getMessage() {
        //DA-START:com.vd.gu.definition.basic.ErrorBean.getMessage.String:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.ErrorBean.getMessage.String:DA-ELSE
        return this.message;
        //DA-END:com.vd.gu.definition.basic.ErrorBean.getMessage.String:DA-END
    }
    
    /**
     * setter for the field message
     *
     *
     *
     * @param message  the message
     */
    //DA-START:com.vd.gu.definition.basic.ErrorBean.setMessage.String.annotations:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.ErrorBean.setMessage.String.annotations:DA-ELSE
    //DA-END:com.vd.gu.definition.basic.ErrorBean.setMessage.String.annotations:DA-END
    public void setMessage(String message) {
        //DA-START:com.vd.gu.definition.basic.ErrorBean.setMessage.String:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.ErrorBean.setMessage.String:DA-ELSE
        this.message = message;
        //DA-END:com.vd.gu.definition.basic.ErrorBean.setMessage.String:DA-END
    }
    
    //DA-START:com.vd.gu.definition.basic.ErrorBean.additional.elements.in.type:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.ErrorBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.vd.gu.definition.basic.ErrorBean.additional.elements.in.type:DA-END
} // end of java type
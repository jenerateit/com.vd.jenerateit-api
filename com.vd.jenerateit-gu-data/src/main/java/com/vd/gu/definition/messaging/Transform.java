package com.vd.gu.definition.messaging;

public class Transform extends Log {

	private Integer transformedFiles;
	private String reference;
	
	public static Transform createTransformStart(User user, Generator generator) {
		return new Transform(STATUS.START, user, generator, System.currentTimeMillis(), null, null, null);
	}
	
	public static Transform createTransformDone(User user, Generator generator, Integer transformedFiles) {
		return new Transform(STATUS.DONE, user, generator, System.currentTimeMillis(), null, transformedFiles, null);
	}
	
	public static Transform createTransformError(User user, Generator generator, Throwable e, String reference) {
		return new Transform(STATUS.ERROR, user, generator, System.currentTimeMillis(), e, null, reference);
	}
	
	private Transform(STATUS status, User user, Generator generator, long timestamp, Throwable exception, Integer transformedFiles, String reference) {
		super(status, user, generator, timestamp, exception);
		this.transformedFiles = transformedFiles;
		this.reference = reference;
	}
	
	private Transform() { }

	public Integer getTransformedFiles() {
		return transformedFiles;
	}
	
	public String getReference() {
		return reference;
	}
}

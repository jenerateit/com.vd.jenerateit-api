package com.vd.gu.definition;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * <table>
 * <tr><td>Generator</td><td>com.gs.vd.gen.java.retrofit.openapi:1.0</td></tr>
 * <tr><td>Element</td><td>type</td></tr>
 * <tr><td>Type</td><td>OpenAPI Schema</td></tr>
 * <tr><td>Module</td><td>none</td></tr>
 * </table>
 */
public enum MessageTypeEnum { // start of class
    
    @JsonProperty(value="ERROR")
    ERROR("ERROR"),
    
    @JsonProperty(value="WARNING")
    WARNING("WARNING"),
    
    @JsonProperty(value="INFO")
    INFO("INFO"),
    //DA-START:definition.MessageTypeEnum.enum.constants:DA-START
    //DA-ELSE:definition.MessageTypeEnum.enum.constants:DA-ELSE
    // add additional enum entries here 
    //DA-END:definition.MessageTypeEnum.enum.constants:DA-END
    ;
    
    private String enumEntryValue;
    
    
    /**
     * creates an instance of MessageTypeEnum
     *
     * @param enumEntryValue  the enumEntryValue
     */
    private MessageTypeEnum(String enumEntryValue) {
        //DA-START:com.vd.gu.definition.MessageTypeEnum.String:DA-START
        //DA-ELSE:com.vd.gu.definition.MessageTypeEnum.String:DA-ELSE
        this.enumEntryValue = enumEntryValue;
        //DA-END:com.vd.gu.definition.MessageTypeEnum.String:DA-END
    }
    
    /**
     * creates an instance of MessageTypeEnum
     */
    private MessageTypeEnum() {
        //DA-START:com.vd.gu.definition.MessageTypeEnum:DA-START
        //DA-ELSE:com.vd.gu.definition.MessageTypeEnum:DA-ELSE
        //DA-END:com.vd.gu.definition.MessageTypeEnum:DA-END
    }
    
    
    
    /**
     * getter for the field enumEntryValue
     *
     *
     *
     * @return
     */
    public String getEnumEntryValue() {
        //DA-START:com.vd.gu.definition.MessageTypeEnum.getEnumEntryValue.String:DA-START
        //DA-ELSE:com.vd.gu.definition.MessageTypeEnum.getEnumEntryValue.String:DA-ELSE
        return this.enumEntryValue;
        //DA-END:com.vd.gu.definition.MessageTypeEnum.getEnumEntryValue.String:DA-END
    }
    
    /**
     *
     * @return
     */
    @Override
    public String toString() {
        //DA-START:com.vd.gu.definition.MessageTypeEnum.toString.String:DA-START
        //DA-ELSE:com.vd.gu.definition.MessageTypeEnum.toString.String:DA-ELSE
        return this.enumEntryValue;
        //DA-END:com.vd.gu.definition.MessageTypeEnum.toString.String:DA-END
    }
    
    
    
    //DA-START:com.vd.gu.definition.MessageTypeEnum.additional.elements.in.type:DA-START
    //DA-ELSE:com.vd.gu.definition.MessageTypeEnum.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.vd.gu.definition.MessageTypeEnum.additional.elements.in.type:DA-END
} // end of java type
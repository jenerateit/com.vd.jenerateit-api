package com.vd.gu.definition.messaging;

import java.util.Collection;
import java.util.stream.Collectors;

import com.vd.gu.definition.basic.FileBean;
import com.vd.gu.definition.basic.LoadResultBean;

public class Load extends Log {
	
	private Collection<String> modelFileNames;
	private LoadResultBean loadResult;
	
	public static Load createLoadStart(User user, Generator generator, Collection<FileBean> modelFiles) {
		return new Load(STATUS.START, user, generator, System.currentTimeMillis(), null, modelFiles.stream().map(FileBean::getPath).collect(Collectors.toList()), null);
	}
	
	public static Load createLoadDone(User user, Generator generator, LoadResultBean loadResult) {
		return new Load(STATUS.DONE, user, generator, System.currentTimeMillis(), null, null, loadResult);
	}
	
	public static Load createLoadError(User user, Generator generator, Throwable e, LoadResultBean loadResult) {
		return new Load(STATUS.ERROR, user, generator, System.currentTimeMillis(), e, null, loadResult);
	}
	
	private Load(STATUS status, User user, Generator generator, long timestamp, Throwable exception, Collection<String> modelFileNames, LoadResultBean loadResult) {
		super(status, user, generator, timestamp, exception);
		this.modelFileNames = modelFileNames;
		this.loadResult = loadResult;
	}
	
	private Load() { }
	
	public Collection<String> getModelFileNames() {
		return modelFileNames;
	}
	
	public LoadResultBean getLoadResult() {
		return loadResult;
	}

}

package com.vd.gu.definition;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * <table>
 * <tr><td>Generator</td><td>com.gs.vd.gen.java.retrofit.openapi:1.0</td></tr>
 * <tr><td>Element</td><td>step</td></tr>
 * <tr><td>Type</td><td>OpenAPI Schema</td></tr>
 * <tr><td>Module</td><td>none</td></tr>
 * </table>
 */
public enum MessageStepEnum { // start of class
    
    @JsonProperty(value="MODEL")
    MODEL("MODEL"),
    
    @JsonProperty(value="LOAD")
    LOAD("LOAD"),
    
    @JsonProperty(value="TRANSFORM")
    TRANSFORM("TRANSFORM"),
    //DA-START:definition.MessageStepEnum.enum.constants:DA-START
    //DA-ELSE:definition.MessageStepEnum.enum.constants:DA-ELSE
    // add additional enum entries here 
    //DA-END:definition.MessageStepEnum.enum.constants:DA-END
    ;
    
    private String enumEntryValue;
    
    
    /**
     * creates an instance of MessageStepEnum
     *
     * @param enumEntryValue  the enumEntryValue
     */
    private MessageStepEnum(String enumEntryValue) {
        //DA-START:com.vd.gu.definition.MessageStepEnum.String:DA-START
        //DA-ELSE:com.vd.gu.definition.MessageStepEnum.String:DA-ELSE
        this.enumEntryValue = enumEntryValue;
        //DA-END:com.vd.gu.definition.MessageStepEnum.String:DA-END
    }
    
    /**
     * creates an instance of MessageStepEnum
     */
    private MessageStepEnum() {
        //DA-START:com.vd.gu.definition.MessageStepEnum:DA-START
        //DA-ELSE:com.vd.gu.definition.MessageStepEnum:DA-ELSE
        //DA-END:com.vd.gu.definition.MessageStepEnum:DA-END
    }
    
    
    
    /**
     * getter for the field enumEntryValue
     *
     *
     *
     * @return
     */
    public String getEnumEntryValue() {
        //DA-START:com.vd.gu.definition.MessageStepEnum.getEnumEntryValue.String:DA-START
        //DA-ELSE:com.vd.gu.definition.MessageStepEnum.getEnumEntryValue.String:DA-ELSE
        return this.enumEntryValue;
        //DA-END:com.vd.gu.definition.MessageStepEnum.getEnumEntryValue.String:DA-END
    }
    
    /**
     *
     * @return
     */
    @Override
    public String toString() {
        //DA-START:com.vd.gu.definition.MessageStepEnum.toString.String:DA-START
        //DA-ELSE:com.vd.gu.definition.MessageStepEnum.toString.String:DA-ELSE
        return this.enumEntryValue;
        //DA-END:com.vd.gu.definition.MessageStepEnum.toString.String:DA-END
    }
    
    
    
    //DA-START:com.vd.gu.definition.MessageStepEnum.additional.elements.in.type:DA-START
    //DA-ELSE:com.vd.gu.definition.MessageStepEnum.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.vd.gu.definition.MessageStepEnum.additional.elements.in.type:DA-END
} // end of java type
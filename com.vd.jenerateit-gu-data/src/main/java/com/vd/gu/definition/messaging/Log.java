package com.vd.gu.definition.messaging;

import java.io.PrintWriter;
import java.io.StringWriter;

public abstract class Log {

	private STATUS status;
	private User user;
	private String generatorSymbolicName;
	private String generatorVersionShort;
	private String generatorVersionLong;
	private long timestamp;
	private String exception;
	
	protected Log(STATUS status, User user, Generator generator, long timestamp, Throwable exception) {
		this.status = status;
		this.user = user;
		this.generatorSymbolicName = generator.getSymbolicName();
		this.generatorVersionShort = generator.getVersionShort();
		this.generatorVersionLong = generator.getVersionLong();
		this.timestamp = timestamp;
		if (exception != null) {
			StringWriter stringWriter = new StringWriter();
			PrintWriter printWriter = new PrintWriter(stringWriter);
			exception.printStackTrace(printWriter);
			this.exception = stringWriter.toString();
		} else {
			this.exception = null;
		}
	}
	
	protected Log() { }
	
	public STATUS getStatus() {
		return status;
	}
	
	public User getUser() {
		return user;
	}
	
	public String getGeneratorSymbolicName() {
		return generatorSymbolicName;
	}
	
	public String getGeneratorVersionShort() {
		return generatorVersionShort;
	}
	
	public String getGeneratorVersionLong() {
		return generatorVersionLong;
	}
	
	public long getTimestamp() {
		return timestamp;
	}
	
	public String getException() {
		return exception;
	}
	
}

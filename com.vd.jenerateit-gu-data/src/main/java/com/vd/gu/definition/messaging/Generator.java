package com.vd.gu.definition.messaging;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.jenerateit.util.GensetParser;
import org.jenerateit.util.GensetParser.GenerationGroup;
import org.jenerateit.util.GensetParser.ModelAccess;
import org.jenerateit.util.GensetParser.TransformationStep;


public class Generator {
	
	public static class Option {
		private String name;
		private String value;
		private String description;
		private boolean readOnly;
		private Set<String> virtualProjects;

		protected Option() {
			
		}
		
		public Option(String name, String value, String description, boolean readOnly) {
			this.name = name;
			this.value = value;
			this.description = description;
			this.readOnly = readOnly;
		}

		public String getName() {
			return name;
		}

		public String getValue() {
			return value;
		}

		public String getDescription() {
			return description;
		}

		public boolean isReadOnly() {
			return readOnly;
		}
		
		public void setVirtualProjects(Set<String> virtualProjects) {
			this.virtualProjects = virtualProjects;
		}
		
		public Set<String> getVirtualProjects() {
			return virtualProjects;
		}
	}
	
//	public static String getOptionId(final TransformationStep step, String optionName) {
//		Deque<String> ids = new ArrayDeque<>();
//		TransformationStep currentStep = step;
//		do {
//			ids.addFirst(currentStep.getId());
//		} while ((currentStep = currentStep.getPreviousStep()) != null);
//		
//		return ids.stream().collect(Collectors.joining(":")) + ":" + optionName;
//	}

	private String name;
	private String symbolicName;
	private String versionShort;
	private String versionLong;
	private List<Option> options = null;
	
	protected Generator() {
		super();
	}

	public Generator(Generator generator) {
		this.name = generator.getName();
		this.symbolicName = generator.getSymbolicName();
		this.versionShort = generator.getVersionShort();
		this.versionLong = generator.getVersionLong();
		this.options = new ArrayList<>(generator.options);
	}
	
	public Generator(String name, String symbolicName, String versionShort, String versionLong, ModelAccess modelAccess) {
		this.name = name;
		this.symbolicName = symbolicName;
		this.versionShort = versionShort;
		this.versionLong = versionLong;
		this.options = new ArrayList<>();
		collectOptions(modelAccess);
	}
	
	public String getName() {
		return name;
	}
	
	public String getSymbolicName() {
		return symbolicName;
	}
	
	public String getVersionShort() {
		return versionShort;
	}
	
	public String getVersionLong() {
		return versionLong;
	}
	
	public Collection<Option> getOptions() {
		return options;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj instanceof Generator) {
			Generator other = (Generator) obj;
			return equals(other.getSymbolicName(), other.getVersionShort());
		}
		return false;
	}
	
	public boolean equals(String generatorId, String versionShort) {
		return this.symbolicName.equals(generatorId)
				&& this.versionShort.equals(versionShort);
	}
	
	@Override
	public int hashCode() {
		return this.symbolicName.hashCode();
	}
	
	private void collectOptions(TransformationStep step) {
		if (step.isDisabled()) {
			return;
		}
		if (step instanceof GenerationGroup) {
			GenerationGroup gg = (GenerationGroup) step;
			if (!gg.isActive()) {
				return;
			}
		}
		Set<String> virtualProjects = getVirtualProjects(step);
		if (step.getOptions() != null) {
			for (GensetParser.Option option : step.getOptions()) {
				Boolean readOnly = null;
				if (option.getVisibility().equalsIgnoreCase("public")) {
					readOnly = false;
				} else if (option.getVisibility().equalsIgnoreCase("protected")) {
					readOnly = true;
				}
				if (readOnly != null) {
					Option copiedOption = new Option(option.getName(), option.getValue(), option.getDescription(), readOnly);
					copiedOption.setVirtualProjects(virtualProjects);
					this.options.add(copiedOption);
				}
			}
		}
		
		if (step.getNextSteps() != null) {
			step.getNextSteps().forEach(s -> collectOptions(s));
		}
	}

	public static Set<String> getVirtualProjects(TransformationStep step) {
		Set<String> virtualProjects = new HashSet<>();
		getVirtualProjects(step, virtualProjects);
		return virtualProjects;
	}
	
	public static void getVirtualProjects(TransformationStep step, Set<String> virtualProjects) {
		if (step.isDisabled()) {
			return;
		}
		if (step instanceof GenerationGroup) {
			GenerationGroup gg = (GenerationGroup) step;
			if (!gg.isActive()) {
				return;
			}
			virtualProjects.add(gg.getProject());
		} else if (step.getNextSteps() != null){
			step.getNextSteps().forEach(s -> getVirtualProjects(s, virtualProjects));
		}
	}
}

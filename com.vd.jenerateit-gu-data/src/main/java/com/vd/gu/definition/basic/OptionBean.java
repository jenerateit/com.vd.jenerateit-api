package com.vd.gu.definition.basic;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * <table>
 * <tr><td>Generator</td><td>com.gs.vd.gen.java.retrofit.openapi:1.0</td></tr>
 * <tr><td>Element</td><td>Option</td></tr>
 * <tr><td>Type</td><td>OpenAPI Schema</td></tr>
 * <tr><td>Module</td><td>none</td></tr>
 * </table>
 */
@XmlRootElement(namespace="com.vd.gu.definition.basic")
public class OptionBean { // start of class

    private String name;
    
    private String value;
    
    private String virtualProject;
    
    /**
     * creates an instance of OptionBean
     */
    public OptionBean() {
        //DA-START:com.vd.gu.definition.basic.OptionBean:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.OptionBean:DA-ELSE
        //DA-END:com.vd.gu.definition.basic.OptionBean:DA-END
    }
    
    
    /**
     * getter for the field name
     *
     *
     *
     * @return
     */
    //DA-START:com.vd.gu.definition.basic.OptionBean.getName.annotations:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.OptionBean.getName.annotations:DA-ELSE
    @XmlElement(name="name")
    @JsonProperty(value="name")
    //DA-END:com.vd.gu.definition.basic.OptionBean.getName.annotations:DA-END
    public String getName() {
        //DA-START:com.vd.gu.definition.basic.OptionBean.getName.String:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.OptionBean.getName.String:DA-ELSE
        return this.name;
        //DA-END:com.vd.gu.definition.basic.OptionBean.getName.String:DA-END
    }
    
    /**
     * setter for the field name
     *
     *
     *
     * @param name  the name
     */
    //DA-START:com.vd.gu.definition.basic.OptionBean.setName.String.annotations:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.OptionBean.setName.String.annotations:DA-ELSE
    //DA-END:com.vd.gu.definition.basic.OptionBean.setName.String.annotations:DA-END
    public void setName(String name) {
        //DA-START:com.vd.gu.definition.basic.OptionBean.setName.String:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.OptionBean.setName.String:DA-ELSE
        this.name = name;
        //DA-END:com.vd.gu.definition.basic.OptionBean.setName.String:DA-END
    }
    
    /**
     * getter for the field value
     *
     *
     *
     * @return
     */
    //DA-START:com.vd.gu.definition.basic.OptionBean.getValue.annotations:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.OptionBean.getValue.annotations:DA-ELSE
    @XmlElement(name="value")
    @JsonProperty(value="value")
    //DA-END:com.vd.gu.definition.basic.OptionBean.getValue.annotations:DA-END
    public String getValue() {
        //DA-START:com.vd.gu.definition.basic.OptionBean.getValue.String:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.OptionBean.getValue.String:DA-ELSE
        return this.value;
        //DA-END:com.vd.gu.definition.basic.OptionBean.getValue.String:DA-END
    }
    
    /**
     * setter for the field value
     *
     *
     *
     * @param value  the value
     */
    //DA-START:com.vd.gu.definition.basic.OptionBean.setValue.String.annotations:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.OptionBean.setValue.String.annotations:DA-ELSE
    //DA-END:com.vd.gu.definition.basic.OptionBean.setValue.String.annotations:DA-END
    public void setValue(String value) {
        //DA-START:com.vd.gu.definition.basic.OptionBean.setValue.String:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.OptionBean.setValue.String:DA-ELSE
        this.value = value;
        //DA-END:com.vd.gu.definition.basic.OptionBean.setValue.String:DA-END
    }
    
    /**
     * getter for the field virtualProject
     *
     *
     *
     * @return
     */
    //DA-START:com.vd.gu.definition.basic.OptionBean.getVirtualProject.annotations:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.OptionBean.getVirtualProject.annotations:DA-ELSE
    @XmlElement(name="virtualProject")
    @JsonProperty(value="virtualProject")
    //DA-END:com.vd.gu.definition.basic.OptionBean.getVirtualProject.annotations:DA-END
    public String getVirtualProject() {
        //DA-START:com.vd.gu.definition.basic.OptionBean.getVirtualProject.String:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.OptionBean.getVirtualProject.String:DA-ELSE
        return this.virtualProject;
        //DA-END:com.vd.gu.definition.basic.OptionBean.getVirtualProject.String:DA-END
    }
    
    /**
     * setter for the field virtualProject
     *
     *
     *
     * @param virtualProject  the virtualProject
     */
    //DA-START:com.vd.gu.definition.basic.OptionBean.setVirtualProject.String.annotations:DA-START
    //DA-ELSE:com.vd.gu.definition.basic.OptionBean.setVirtualProject.String.annotations:DA-ELSE
    //DA-END:com.vd.gu.definition.basic.OptionBean.setVirtualProject.String.annotations:DA-END
    public void setVirtualProject(String virtualProject) {
        //DA-START:com.vd.gu.definition.basic.OptionBean.setVirtualProject.String:DA-START
        //DA-ELSE:com.vd.gu.definition.basic.OptionBean.setVirtualProject.String:DA-ELSE
        this.virtualProject = virtualProject;
        //DA-END:com.vd.gu.definition.basic.OptionBean.setVirtualProject.String:DA-END
    }
    
    //DA-START:com.vd.gu.definition.basic.OptionBean.additional.elements.in.type:DA-START
    public OptionBean(String name, String value) {
    	this.name = name;
    	this.value = value;
    	this.virtualProject = null;
    }
    
    public OptionBean(String name, String value, String virtualProject) {
    	this.name = name;
    	this.value = value;
    	this.virtualProject = virtualProject;
    }
    //DA-ELSE:com.vd.gu.definition.basic.OptionBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.vd.gu.definition.basic.OptionBean.additional.elements.in.type:DA-END
} // end of java type
package com.vd.gu;

import com.vd.gu.definition.basic.ErrorBean;


/**
 * error
 * exception type to be used as default, for cases where there is not an explicit exception type available
 * <table>
 * <tr><td>Generator</td><td>com.gs.vd.gen.java.retrofit.openapi:1.0</td></tr>
 * <tr><td>Element</td><td>default</td></tr>
 * <tr><td>Type</td><td>Response</td></tr>
 * <tr><td>Module</td><td>none</td></tr>
 * </table>
 */
public class TechnicalExceptionTransformListDefaultException extends RuntimeException { // start of class

    private static final long serialVersionUID = 1L;
    
    private ErrorBean responseCodeDefault;
    
    /**
     * creates an instance of TechnicalExceptionTransformListDefaultException
     */
    public TechnicalExceptionTransformListDefaultException() {
        //DA-START:com.vd.gu.TechnicalExceptionTransformListDefaultException:DA-START
        //DA-ELSE:com.vd.gu.TechnicalExceptionTransformListDefaultException:DA-ELSE
        super();
        //DA-END:com.vd.gu.TechnicalExceptionTransformListDefaultException:DA-END
    }
    
    /**
     * creates an instance of TechnicalExceptionTransformListDefaultException
     *
     * @param arg0  the arg0
     */
    public TechnicalExceptionTransformListDefaultException(String arg0) {
        //DA-START:com.vd.gu.TechnicalExceptionTransformListDefaultException.String:DA-START
        //DA-ELSE:com.vd.gu.TechnicalExceptionTransformListDefaultException.String:DA-ELSE
        super(arg0);
        //DA-END:com.vd.gu.TechnicalExceptionTransformListDefaultException.String:DA-END
    }
    
    /**
     * creates an instance of TechnicalExceptionTransformListDefaultException
     *
     * @param arg0  the arg0
     * @param arg1  the arg1
     */
    public TechnicalExceptionTransformListDefaultException(String arg0, Throwable arg1) {
        //DA-START:com.vd.gu.TechnicalExceptionTransformListDefaultException.String.Throwable:DA-START
        //DA-ELSE:com.vd.gu.TechnicalExceptionTransformListDefaultException.String.Throwable:DA-ELSE
        super(arg0, arg1);
        //DA-END:com.vd.gu.TechnicalExceptionTransformListDefaultException.String.Throwable:DA-END
    }
    
    /**
     * creates an instance of TechnicalExceptionTransformListDefaultException
     *
     * @param arg0  the arg0
     */
    public TechnicalExceptionTransformListDefaultException(Throwable arg0) {
        //DA-START:com.vd.gu.TechnicalExceptionTransformListDefaultException.Throwable:DA-START
        //DA-ELSE:com.vd.gu.TechnicalExceptionTransformListDefaultException.Throwable:DA-ELSE
        super(arg0);
        //DA-END:com.vd.gu.TechnicalExceptionTransformListDefaultException.Throwable:DA-END
    }
    
    /**
     * creates an instance of TechnicalExceptionTransformListDefaultException
     *
     * @param arg0  the arg0
     * @param arg1  the arg1
     * @param arg2  the arg2
     * @param arg3  the arg3
     */
    public TechnicalExceptionTransformListDefaultException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
        //DA-START:com.vd.gu.TechnicalExceptionTransformListDefaultException.String.Throwable.boolean.boolean:DA-START
        //DA-ELSE:com.vd.gu.TechnicalExceptionTransformListDefaultException.String.Throwable.boolean.boolean:DA-ELSE
        super(arg0, arg1, arg2, arg3);
        //DA-END:com.vd.gu.TechnicalExceptionTransformListDefaultException.String.Throwable.boolean.boolean:DA-END
    }
    
    
    /**
     * getter for the field responseCodeDefault
     *
     *
     *
     * @return
     */
    public ErrorBean getResponseCodeDefault() {
        //DA-START:com.vd.gu.TechnicalExceptionTransformListDefaultException.getResponseCodeDefault.ErrorBean:DA-START
        //DA-ELSE:com.vd.gu.TechnicalExceptionTransformListDefaultException.getResponseCodeDefault.ErrorBean:DA-ELSE
        return this.responseCodeDefault;
        //DA-END:com.vd.gu.TechnicalExceptionTransformListDefaultException.getResponseCodeDefault.ErrorBean:DA-END
    }
    
    /**
     * setter for the field responseCodeDefault
     *
     *
     *
     * @param responseCodeDefault  the responseCodeDefault
     */
    public void setResponseCodeDefault(ErrorBean responseCodeDefault) {
        //DA-START:com.vd.gu.TechnicalExceptionTransformListDefaultException.setResponseCodeDefault.ErrorBean:DA-START
        //DA-ELSE:com.vd.gu.TechnicalExceptionTransformListDefaultException.setResponseCodeDefault.ErrorBean:DA-ELSE
        this.responseCodeDefault = responseCodeDefault;
        //DA-END:com.vd.gu.TechnicalExceptionTransformListDefaultException.setResponseCodeDefault.ErrorBean:DA-END
    }
    
    //DA-START:com.vd.gu.TechnicalExceptionTransformListDefaultException.additional.elements.in.type:DA-START
    //DA-ELSE:com.vd.gu.TechnicalExceptionTransformListDefaultException.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.vd.gu.TechnicalExceptionTransformListDefaultException.additional.elements.in.type:DA-END
} // end of java type
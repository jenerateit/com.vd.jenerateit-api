/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package com.vd.communication.exception;

import java.net.URI;


/**
 * This Exception indicates that no data is available to successful finish the requested operation.
 * Use this Exception if you try to execute an operation but do not find all data you expected.
 * 
 * @author hrr
 * @since 1.3.0
 */
public class DataNotAvailableException extends LocationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1670331443395077422L;

	/**
	 * @see LocationException#LocationException(String, Throwable, URI)
	 */
	public DataNotAvailableException(final String message, final Throwable cause,
			final URI location) {
		super(message, cause, location);
	}

	/**
	 * @see LocationException#LocationException(String, URI)
	 */
	public DataNotAvailableException(final String message, final URI location) {
		super(message, location);
	}

	/**
	 * @see LocationException#LocationException(Throwable, URI)
	 */
	public DataNotAvailableException(final Throwable cause, final URI location) {
		super(cause, location);
	}

	
}

/*
 *  Virtual Developer - Code Generation the easy way
 *
 *  http://www.virtual-developer.com
 *
 *  Copyright:
 *    2007-2013 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    commercial license only
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package com.vd.communication.exception;

import org.jenerateit.exception.JenerateITException;

/**
 * @author hrr
 *
 */
public class CloudConnectorException extends JenerateITException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -874709967126418028L;
	
	private final Long errorId;
	private final int infos;
	private final int warnings;
	private final int errors;

	/**
	 * 
	 */
	public CloudConnectorException() {
		this(0, 0, 0, null);
	}

	/**
	 * @param errorId
	 */
	public CloudConnectorException(final Long errorId) {
		this(0, 0, 0, errorId);
	}

	/**
	 * @param message
	 * @param errorId
	 */
	public CloudConnectorException(final String message, final Long errorId) {
		this(message, 0, 0, 0, errorId);
	}

	/**
	 * 
	 * @param infos
	 * @param warnings
	 * @param errors
	 */
	public CloudConnectorException(final int infos, final int warnings, final int errors) {
		this(infos, warnings, errors, null);
	}
	
	/**
	 * 
	 * @param infos
	 * @param warnings
	 * @param errors
	 * @param errorId
	 */
	public CloudConnectorException(final int infos, final int warnings, final int errors, final Long errorId) {
		this(null, infos, warnings, errors, errorId);
	}
	
	/**
	 * @param message
	 * @param infos
	 * @param warnings
	 * @param errors
	 * @param errorId
	 */
	public CloudConnectorException(final String message, final int infos, final int warnings, final int errors, final Long errorId) {
		super(message);
		
		this.errorId = errorId;
		this.infos = infos;
		this.warnings = warnings;
		this.errors = errors;
	}

	/**
	 * Getter to access the errorId
	 * 
	 * @return
	 */
	public Long getErrorId() {
		return errorId;
	}

	/**
	 * @return the number of information messages
	 */
	public int getInfos() {
		return infos;
	}

	/**
	 * @return the number of warning messages
	 */
	public int getWarnings() {
		return warnings;
	}

	/**
	 * @return the number of error messages
	 */
	public int getErrors() {
		return errors;
	}

	
}

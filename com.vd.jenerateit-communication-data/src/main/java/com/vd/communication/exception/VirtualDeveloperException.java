/*
 *  Virtual Developer - Code Generation the easy way
 *
 *  http://www.virtual-developer.com
 *
 *  Copyright:
 *    2007-2013 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    commercial license only
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package com.vd.communication.exception;

import org.jenerateit.exception.JenerateITException;

/**
 * @author hrr
 * 
 * @deprecated Please use the {@link CloudConnectorException} or the {@link JenerateITException} instead
 */
public class VirtualDeveloperException extends JenerateITException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -874709967126418028L;

	/**
	 * @param message
	 */
	public VirtualDeveloperException(String message) {
		super(message);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public VirtualDeveloperException(String message, Throwable cause) {
		super(message, cause);
	}

}

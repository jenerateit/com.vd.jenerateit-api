package com.vd.communication.exception;

import java.net.URI;

/**
 * This (JenerateIT) Exception indicates that the previous transformation result (content) is not cached 
 * and needs to be resent by the client. 
 * This exception should only be used JenerateIT internally.
 * 
 * @author hrr
 * @since 1.3.0
 */
public final class TargetNotCachedException extends LocationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6912615971296810599L;

	/**
	 * @see LocationException#LocationException(String, Throwable, URI)
	 */
	public TargetNotCachedException(final String message, final Throwable cause,
			final URI location) {
		super(message, cause, location);
	}

	/**
	 * @see LocationException#LocationException(String, URI)
	 */
	public TargetNotCachedException(final String message, final URI location) {
		super(message, location);
	}

	/**
	 * @see LocationException#LocationException(Throwable, URI)
	 */
	public TargetNotCachedException(final Throwable cause, final URI location) {
		super(cause, location);
	}

	
}

/*
 *  Virtual Developer - Code Generation the easy way
 *
 *  http://www.virtual-developer.com
 *
 *  Copyright:
 *    2007-2013 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    commercial license only
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package com.vd.communication.exception;


/**
 * Exception to inform a virtual developer user the latest terms and conditions of a generator producer company is not accepted.
 *  
 * @author hrr
 * @since 1.4.0
 */
public class TermsAndConditionException extends CloudConnectorException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6485639124815422241L;

	/**
	 * Constructor
	 */
	public TermsAndConditionException() {
		super();
	}

	
}

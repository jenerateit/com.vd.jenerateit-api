/*
 *  Virtual Developer - Code Generation the easy way
 *
 *  http://www.virtual-developer.com
 *
 *  Copyright:
 *    2007-2013 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    commercial license only
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package com.vd.communication.exception;

import java.net.URI;

import org.jenerateit.util.StringTools;

/**
 * @author hrr
 *
 */
public abstract class LocationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7177274632956197079L;
	private final URI location;
	
	/**
	 * Constructor
	 * 
	 * @param message the user message
	 * @param location the reference identifier of the service this exception was thrown
	 */
	public LocationException(final String message, final URI location) {
		this(message, null, location);
	}

	/**
	 * Constructor
	 * 
	 * @param cause the root cause of the creation of this exception
	 * @param location the reference identifier of the service this exception was thrown
	 */
	public LocationException(final Throwable cause, final URI location) {
		this(cause != null ? cause.getMessage() : "location exception", cause, location);
	}

	/**
	 * Constructor
	 * 
	 * @param message the user message
	 * @param cause the root cause of the creation of this exception
	 * @param location the reference identifier of the service this exception was thrown
	 */
	public LocationException(final String message, final Throwable cause, final URI location) {
		super(message, cause);
		
		this.location = location;
	}

	/**
	 * Getter for the location information
	 * 
	 * @return the reference identifier of the service this exception was thrown
	 */
	public URI getLocation() {
		return location;
	}

	/**
	 * Prepare a message with attached location if available.
	 * 
	 * @return the message of this exception
	 * @see RuntimeException#getMessage()
	 */
	@Override
	public String getMessage() {
		if (location != null) {
			return new StringBuffer(super.getMessage()).append(StringTools.NEWLINE)
					.append("Location=").append(location.toASCIIString()).toString();
		}
		return super.getMessage();
	}

	
}

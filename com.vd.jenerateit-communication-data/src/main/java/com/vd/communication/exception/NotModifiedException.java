/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package com.vd.communication.exception;

import java.net.URI;


/**
 * This Exception indicates that no changes will occur after execute the requested operation.
 * Use this Exception if the requested operation did not change the status of a resource and the operation result.
 * 
 * @author hrr
 * @since 1.3.0
 */
public class NotModifiedException extends LocationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 517739157279838264L;

	/**
	 * @see LocationException#LocationException(String, Throwable, URI)
	 */
	public NotModifiedException(String message, Throwable cause, URI location) {
		super(message, cause, location);
	}

	/**
	 * @see LocationException#LocationException(String, URI)
	 */
	public NotModifiedException(final String message, final URI location) {
		super(message, location);
	}

	/**
	 * @see LocationException#LocationException(Throwable, URI)
	 */
	public NotModifiedException(final Throwable cause, final URI location) {
		super(cause, location);
	}
	
	
}

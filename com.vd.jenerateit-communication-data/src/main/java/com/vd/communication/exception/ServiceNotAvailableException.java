/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package com.vd.communication.exception;

import java.net.URI;



/**
 * This Exception indicates that the service - to call a operation on - is not available.
 * Use this Exception if can not find the request resource.
 * 
 * @author hrr
 * @since 1.3.0
 */
public class ServiceNotAvailableException extends LocationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1670331244495077422L;

	/**
	 * @see LocationException#LocationException(String, Throwable, URI)
	 */
	public ServiceNotAvailableException(final String message, final Throwable cause,
			final URI location) {
		super(message, cause, location);
	}

	/**
	 * @see LocationException#LocationException(String, URI)
	 */
	public ServiceNotAvailableException(final String message, final URI location) {
		super(message, location);
	}

	/**
	 * @see LocationException#LocationException(Throwable, URI)
	 */
	public ServiceNotAvailableException(final Throwable cause, final URI location) {
		super(cause, location);
	}

}

/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package com.vd.communication.exception;

import java.net.URI;


/**
 * This Exception indicates that no result after the called operation is available.
 * Use this Exception if you got an empty or null result which is not expected.
 * 
 * @author hrr
 * @since 1.3.0
 */
public class NoResultException extends LocationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1670331443395077422L;

	/**
	 * @see LocationException#LocationException(String, Throwable, URI)
	 */
	public NoResultException(final String message, final Throwable cause, final URI location) {
		super(message, cause, location);
	}

	/**
	 * @see LocationException#LocationException(String, URI)
	 */
	public NoResultException(final String message, final URI location) {
		super(message, location);
	}

	/**
	 * @see LocationException#LocationException(Throwable, URI)
	 */
	public NoResultException(final Throwable cause, final URI location) {
		super(cause, location);
	}
	
	
}

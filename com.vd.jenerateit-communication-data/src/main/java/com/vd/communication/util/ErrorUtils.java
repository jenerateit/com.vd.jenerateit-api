/*
 *  Virtual Developer - Code Generation the easy way
 *
 *  http://www.virtual-developer.com
 *
 *  Copyright:
 *    2007-2013 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    commercial license only
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package com.vd.communication.util;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.jenerateit.exception.JenerateITException;

import com.vd.communication.data.Error;
import com.vd.communication.exception.LocationException;
import com.vd.transformation.data.StackTraceElement;

/**
 * @author hrr
 *
 */
public class ErrorUtils {

	public static RuntimeException getException(final Error error) {
		return getException(error, null);
	}
	
	public static RuntimeException getException(final Error error, final URI location) {
		if (error == null) {
			throw new NullPointerException("An error object is necessary to create an Exception object");
		}
		
		try {
			Class<?> clazz = null;
			try {
				clazz = Class.forName(error.getType());
			} catch (Exception e) {
				clazz = JenerateITException.class;
			}
			RuntimeException t = null;
			try {
				/*if (LocationException.class.isAssignableFrom(clazz)) {
					Class<LocationException> lClazz = (Class<LocationException>) clazz;
					if (error.getRoot() != null) {
						t = lClazz.getConstructor(String.class, Throwable.class, URI.class).newInstance(error.getMessage(), getException(error.getRoot()), location);
					} else {
						t = lClazz.getConstructor(String.class, URI.class).newInstance(error.getMessage(), location);
					}

				} else*/ if (JenerateITException.class.isAssignableFrom(clazz)) {
					Class<JenerateITException> jClazz = (Class<JenerateITException>) clazz;
					if (error.getRoot() != null) {
						t = jClazz.getConstructor(String.class, Throwable.class).newInstance(error.getMessage(), getException(error.getRoot()));
					} else {
						t = jClazz.getConstructor(String.class).newInstance(error.getMessage());
					}
					
				} else if (RuntimeException.class.isAssignableFrom(clazz)) {
					Class<RuntimeException> rClazz = (Class<RuntimeException>) clazz;
					t = rClazz.getConstructor(String.class).newInstance(error.getMessage());
					
				} else {
					throw new NoSuchMethodException();
				}
			} catch (NoSuchMethodException e) {
				if (error.getRoot() != null) {
					t = new JenerateITException(error.getType() + ": " + error.getMessage(), getException(error.getRoot()));
				} else {
					t = new JenerateITException(error.getType() + ": " + error.getMessage());
				}
			}

			final List<java.lang.StackTraceElement> elements = new ArrayList<java.lang.StackTraceElement>();
			for (StackTraceElement ste : error.getStacks()) {
				elements.add(new java.lang.StackTraceElement(ste.getClazz(), ste.getMethod(), ste.getFile(), ste.getLine()));
			}
			t.setStackTrace(elements.toArray(new java.lang.StackTraceElement[elements.size()]));
			return t;

		} catch (RuntimeException e) {
			throw e;
		} catch (Throwable t) {
			throw new RuntimeException("Error while create an error object", t);
		}
	}
	

	/**
	 * Create an {@link Error} object from a Java exception.
	 * 
	 * @param t the exception
	 * @return the error object
	 */
	public static Error getError(final Throwable t) {
		return getError(t, null);
	}
	
	/**
	 * Create an {@link Error} object from a Java exception.
	 * 
	 * @param t the exception
	 * @param id the id of the Error (primary key in the virtual developer DB)
	 * @return the error object
	 */
	public static Error getError(final Throwable t, final Long id) {
		if (t == null) {
			throw new NullPointerException("A throwable object is necessary to create an Error object");
		}
		
		final Error error = new Error();
		error.setId(id);
		error.setType(t.getClass().getName());
		error.setMessage(t.getMessage());
		for (final java.lang.StackTraceElement ste : t.getStackTrace()) {
			// do not report internal stack trace, only user specific errors and their stack trace(s)
			if (ste.getClassName().startsWith("org.jenerateit.server.impl")) {
				break;
			}
			final StackTraceElement stack = new StackTraceElement();
			stack.setClazz(ste.getClassName());
			stack.setMethod(ste.getMethodName());
			stack.setLine(ste.getLineNumber());
			stack.setFile(ste.getFileName());
			error.getStacks().add(stack);
		}
		if (t.getCause() != null) {
			error.setRoot(getError(t.getCause()));
		}
		return error;
	}
}

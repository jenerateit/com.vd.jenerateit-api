/*
 * Copyright (c) 2012 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package org.jenerateit.modelconverter;

/**
 * @author hrr
 *
 */
public interface ModelConverterProviderI {

	/**
	 * Provides an implementation class for a {@link ModelConverterI} instance.
	 * 
	 * @return the class name of the model converter
	 */
	ModelConverterI getModelConverter();
}

/*
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit.modelconverter;


/**
 * Interface for model elements created by an {@link ModelConverterI}. 
 * All new created model elements should implement this interface if they provide an own icon.
 * This icon will be displayed within JenerateIT IDE with a GUI.
 *  
 * @author hrr
 *
 */
public interface IconProviderI {

	/**
	 * Method to provide JenerateIT a data buffer for an icon showing the meaning of an element.
	 * <p>
	 * <b>NOTE:</b><br>
	 * Since JenerateIT has a client server architecture the load of the image itself should be done on server side. 
	 * So the byte[] will be moved within the element to the client and can be used directly without load.
	 * </p>
	 * @return an icon resource
	 */
	byte[] getIconData();
}

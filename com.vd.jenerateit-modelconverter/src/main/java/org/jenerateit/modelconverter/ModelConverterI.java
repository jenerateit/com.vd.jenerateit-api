/*
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit.modelconverter;

import java.util.Collection;
import java.util.Set;

import org.jenerateit.util.MessageProviderI;

/**
 * Interface for a JenerateIT model to model converter.
 * 
 * @author hrr
 *
 */
public interface ModelConverterI extends MessageProviderI {

	/**
	 * Converts <i>raw</i> model objects (e.g. model elements from MagicDraw) into real POJOs.
	 * 
	 * @param rawElements a list of raw model element objects
	 * @param options the options to use while convert raw model elements into model elements (may be null)
	 * @return a set of converted meta model objects
	 * @throws ModelConverterException if the conversion fails
	 */
	Set<Object> convert(Collection<?> rawElements, ModelConverterOptions options) throws ModelConverterException;
 }

/**
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit.modelconverter;

import org.jenerateit.exception.JenerateITException;


/**
 * Exception class for model converter problems.
 * This {@link RuntimeException} has a field to put the model element into causing the problem.
 * 
 * @author hrr
 *
 */
public class ModelConverterException extends JenerateITException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7153062900572890555L;
	private final Object element;
	

	public ModelConverterException(final Throwable cause) {
		this(null, cause, null);
	}

	/**
	 * Constructor.
	 * 
	 * @param message the error message
	 */
	public ModelConverterException(final String message) {
		this(message, null, null);
	}

	/**
	 * Constructor
	 * 
	 * @param element the element causing this exception
	 */
	public ModelConverterException(final Object element) {
		this(null, null, element);
	}

	/**
	 * Constructor
	 * 
	 * @param message the error message
	 * @param element the element to convert
	 */
	public ModelConverterException(final String message, final Object element) {
		this(message, null, element);
	}
	
	/**
	 * Constructor
	 * 
	 * @param message the error message
	 * @param cause the exception caught while convert the given element 
	 */
	public ModelConverterException(final String message, final Throwable cause) {
		this(message, cause, null);
	}

	/**
	 * Constructor
	 * 
	 * @param cause the exception caught while convert the given element 
	 * @param element the element to convert
	 */
	public ModelConverterException(final Throwable cause, final Object element) {
		this(null, cause, element);
	}

	/**
	 * Constructor
	 * 
	 * @param message the error message
	 * @param cause the exception thrown while convert the given element 
	 * @param element the element to convert
	 */
	public ModelConverterException(String message, Throwable cause, final Object element) {
		super(message, cause);
		
		this.element = element;
	}

	/**
	 * Return the element that was converted this error occurred.
	 * 
	 * @return the element
	 */
	public Object getElement() {
		return element;
	}

//	/**
//	 * Returns the description of this exception
//	 * 
//	 * @return the description
//	 * @see java.lang.Throwable#getMessage()
//	 */
//	@Override
//	public String getMessage() {
//		StringBuffer sb = new StringBuffer();
//		if (super.getMessage() != null && this.element != null) {
//			sb.append("Error while model conversion '").append(super.getMessage()).append("' on model element: ").append(this.element.toString());
//		} else if (super.getMessage() != null) {
//			sb.append(super.getMessage());
//		}
//		if (getCause() != null && getCause().getMessage() != null) {
//			sb.append(", ").append(getCause().getMessage());
//		}
//		sb.append(this.element != null ? this.element.toString() : "<not available>").append("\n");
//		sb.append(super.getMessage());
//		return sb.toString();
//	}
//
//	
}

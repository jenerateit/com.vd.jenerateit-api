/**
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Collection of usefull String tools
 * 
 * @author $Author: hrr $
 * @version $Revision: 86 $
 */
public class StringTools {

	/** Instance of an empty string */
	public static final String EMPTY_STRING = "";

	/** 
	 * The newline character depending on the OS. 
	 * The system property <b>line.separator</b> is read.
	 * Default value is <b>\r\n</b>
	 * 
	 * @see System#getProperty(String, String) 
	 */
	public static final String NEWLINE = System.getProperty("line.separator", "\r\n");
	
	/**
	 * BOM bytes for the UTF-8 encoding
	 */
	public static final byte[] BOM_UTF8 = {(byte) 0xEF, (byte) 0xBB, (byte) 0xBF};
	
	/**
	 * BOM bytes for the UTF-16 encoding - big endian
	 */
	public static final byte[] BOM_UTF16_BIG_ENDIAN = {(byte) 0xFE, (byte) 0xFF};
	
	/**
	 * BOM bytes for the UTF-16 encoding - little endian
	 */
	public static final byte[] BOM_UTF16_LITTLE_ENDIAN = {(byte) 0xFF, (byte) 0xFE};
	
	/**
	 * BOM bytes for the UTF-32 encoding - big endian
	 */
	public static final byte[] BOM_UTF32_BIG_ENDIAN = {(byte) 0x00, (byte) 0x00, (byte) 0xFE, (byte) 0xFF};
	
	/**
	 * BOM bytes for the UTF-32 encoding - little endian
	 */
	public static final byte[] BOM_UTF32_LITTLE_ENDIAN = {(byte) 0xFF, (byte) 0xFE, (byte) 0x00, (byte) 0x00};
	
	/**
	 * Checks if the given String is not null and not empty.
	 * Note: Spaces are evaluated as normal character (no {@link String#trim() trim()} is run)
	 *  
	 * @param name the string to check
	 * @return true if the string is not empty or null otherwise false
	 */
	public static boolean isNotEmpty(String name) {
		return (name != null && name.length() > 0);
	}
	
	/**
	 * Checks if the given String is null or empty
	 * Note: Spaces are evaluated as normal character (no {@link String#trim() trim()} is run)
	 *  
	 * @param name the string to check
	 * @return true if the string is null or empty otherwise false
	 * @see #isNotEmpty(String)
	 */
	public static boolean isEmpty(String name) {
		return !isNotEmpty(name);
	}

	/**
	 * Checks if the given String is not null or empty and has at least one character
	 * Note: A {@link String#trim() trim()} operation is run on the given text
	 *  
	 * @param text the string to check
	 * @return true if the string is not null or empty and has at least one character otherwise false
	 * @see #isNotEmpty(String)
	 * @see String#trim()
	 */
	public static boolean isText(String text) {
		return (text != null && isNotEmpty(text.trim()));
	}
	
	/**
	 * Converts the first character into an upper case character
	 * 
	 * @param s the string to convert
	 * @return the converted string
	 */
	public static String firstUpperCase(String s) {
        if (isNotEmpty(s)) {
            char[] ca = s.toCharArray();
            ca[0] = Character.toUpperCase(ca[0]);
            return new String(ca);
        }
        return s;
    }
    
	/**
	 * Converts the first character into an lower case character
	 * 
	 * @param s the string to convert
	 * @return the converted string
	 */
	public static String firstLowerCase(String s) {
        if (isNotEmpty(s)) {
            char[] ca = s.toCharArray();
            ca[0] = Character.toLowerCase(ca[0]);
            return new String(ca);
        }
        return s;
    }

	/**
	 * Helper method to parse text segments for line breaks. 
	 * The text segment will be split into separate lines.
	 * 
	 * @see BufferedReader#readLine()
	 * @param s the text segment to parse
	 * @return an array of lines
	 */
	public static String[] getLines(String s) {
		if (isNotEmpty(s)) {
			List<String> lines = new ArrayList<>();
			BufferedReader br = new BufferedReader(new StringReader(s));
			String line = null;
			try {
				while ((line = br.readLine()) != null) {
					lines.add(line);
				}
				
			} catch (IOException e) {
				throw new IllegalArgumentException("The text to parse '" + s + "' has some invalid characters");
			}
			
			return lines.toArray(new String[lines.size()]);
		}
		return new String[] {};
	}

	/**
	 * Helper method to parse character sequences for line breaks. 
	 * The character sequence will be split into separate lines.
	 * 
	 * @see BufferedReader#readLine()
	 * @param s the character sequence to parse
	 * @return a list of lines or an empty list if no line is found
	 */
	public static List<CharSequence> getLines(CharSequence s) {
		final List<CharSequence> lines = new ArrayList<>();
		if (s != null && s.length() > 0) {
			final int length = s.length();
			int lastLineEnds = 0;
			int endBeforeNLCR = 0;
			for (int i = 0; i < length; i++) {
				char c = s.charAt(i);
				if (c == '\n') {
					endBeforeNLCR = i;
					if ((i + 1) < length && s.charAt(i + 1) == '\r') {
						i++;
					}
					lines.add(s.subSequence(lastLineEnds, endBeforeNLCR));
					lastLineEnds = i + 1;
				} else if (c == '\r') {
					endBeforeNLCR = i;
					if ((i + 1) < length && s.charAt(i + 1) == '\n') {
						i++;
					}
					lines.add(s.subSequence(lastLineEnds, endBeforeNLCR));
					lastLineEnds = i + 1;
				}
			} // for 
			if (lastLineEnds < length) {
				lines.add(s.subSequence(lastLineEnds, length));
			}
//			final BufferedReader br = new BufferedReader(new StringReader(s.toString()));
//			String line = null;
//			try {
//				while ((line = br.readLine()) != null) {
//					lines.add(line);
//				}
//				
//			} catch (IOException e) {
//				throw new IllegalArgumentException("The text to parse '" + s + "' has some invalid characters");
//			}
		}
		return lines;
	}
	
	/**
	 * Returns the number of leading bytes that are being used for
	 * the byte order mark (BOM).
	 * 
	 * @param content
	 * @return 0 when there is no BOM found
	 */
	public static int getNumberOfByteOrderMarkBytes(byte[] content) {
		if (hasByteOrderMark(content, Encoding.UTF_8, Endianess.ANY)) {
			return 3;
		} else if (hasByteOrderMark(content, Encoding.UTF_16, Endianess.BIG) ||
				hasByteOrderMark(content, Encoding.UTF_16, Endianess.LITTLE)) {
			return 2;
		} else if (hasByteOrderMark(content, Encoding.UTF_32, Endianess.BIG) ||
				hasByteOrderMark(content, Encoding.UTF_32, Endianess.LITTLE)) {
			return 4;
		}
		
		return 0;
	}
	
	/**
	 * Checks the given content to find out, whether it has a byte order mark (BOM).
	 * 
	 * <p>Note that calling this method checks for UTF-8, UTF-16 and UTF-32 BOM and
	 * also for LITTLE and BIG endian.
	 * 
	 * @param content
	 * @return
	 */
	public static boolean hasByteOrderMark(byte[] content) {
		return hasByteOrderMark(content, Encoding.UTF_8, Endianess.ANY) ||
				hasByteOrderMark(content, Encoding.UTF_16, Endianess.BIG) ||
				hasByteOrderMark(content, Encoding.UTF_16, Endianess.LITTLE) ||
				hasByteOrderMark(content, Encoding.UTF_32, Endianess.BIG) ||
				hasByteOrderMark(content, Encoding.UTF_32, Endianess.LITTLE);
	}
	
	/**
	 * In case the content has a byte order mark, this method returns the corresponding encoding.
	 * 
	 * @param content
	 * @return
	 */
	public static Encoding getByteOrderMarkEncoding(byte[] content) {
		if (hasByteOrderMark(content, Encoding.UTF_8, Endianess.ANY)) {
			return Encoding.UTF_8;
		} else if (hasByteOrderMark(content, Encoding.UTF_16, Endianess.BIG) ||
				hasByteOrderMark(content, Encoding.UTF_16, Endianess.LITTLE)) {
			return Encoding.UTF_16;
		} else if (hasByteOrderMark(content, Encoding.UTF_32, Endianess.BIG) ||
				hasByteOrderMark(content, Encoding.UTF_32, Endianess.LITTLE)) {
			return Encoding.UTF_32;
		}
		
		return null;
	}
	
	public static boolean hasByteOrderMark(byte[] content, Encoding encoding, Endianess endianess) {
		if (content != null && encoding != null) {
			switch (encoding) {
			case UTF_16:
				if (endianess != null && content.length >= 2) {
					switch (endianess) {
					case BIG:
						if (content[0] == BOM_UTF16_BIG_ENDIAN[0] &&
							content[1] == BOM_UTF16_BIG_ENDIAN[1]) {
							return true;
						}
						break;
					case LITTLE:
						if (content[0] == BOM_UTF16_LITTLE_ENDIAN[0] &&
							content[1] == BOM_UTF16_LITTLE_ENDIAN[1]) {
							return true;
						}
						break;
					default:
						break;
					}
				}
				break;
			case UTF_32:
				if (endianess != null && content.length >= 4) {
					switch (endianess) {
					case BIG:
						if (content[0] == BOM_UTF32_BIG_ENDIAN[0] &&
							content[1] == BOM_UTF32_BIG_ENDIAN[1] &&
							content[2] == BOM_UTF32_BIG_ENDIAN[2] &&
							content[3] == BOM_UTF32_BIG_ENDIAN[3]) {
							return true;
						}
						break;
					case LITTLE:
						if (content[0] == BOM_UTF32_LITTLE_ENDIAN[0] &&
							content[1] == BOM_UTF32_LITTLE_ENDIAN[1] &&
							content[2] == BOM_UTF32_LITTLE_ENDIAN[2] &&
							content[3] == BOM_UTF32_LITTLE_ENDIAN[3]) {
							return true;
						}
						break;
					default:
						break;
					}
				}
				break;
			case UTF_8:
				// This is independent from the endianess
				if (content.length >= 3) {
					if (content[0] == BOM_UTF8[0] &&
						content[1] == BOM_UTF8[1] &&
						content[2] == BOM_UTF8[2]) {
						return true;
					}
				}
				break;
			default:
				break;
			}
		}
		
		return false;
	}
	
	public enum Encoding {
		UTF_8,
		UTF_16,
		UTF_32,
		;
	}
	
	public enum Endianess {
		ANY,
		BIG,
		LITTLE,
		;
	}
}

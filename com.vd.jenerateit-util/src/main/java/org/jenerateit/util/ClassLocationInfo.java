/**
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.Iterator;

/**
 * Class to find the corresponding StackTraceElement out of an Exception
 *
 * @author $Author: hrr $
 * @version $Revision: 86 $
 */
public class ClassLocationInfo {

	/**
	 * Returns the StackTraceElement object found quite before the given class object.
	 * 
	 * @param t the throwable element to check
	 * @param clazz the class to look for in the stack trace
	 * @return the stackTraceElement if found or null
	 * @throws IllegalArgumentException if the Throwable or the clazz is null
	 */
	public static StackTraceElement getElementBeforeClass(Throwable t, Class<?> clazz) throws IllegalArgumentException {
		if (t == null) {
			throw new IllegalArgumentException("The Throwable object may not be null");
			
		} else if (clazz == null) {
			throw new IllegalArgumentException("The class object may not be null");
			
		} else {
			Iterator<StackTraceElement> i = Arrays.asList(t.getStackTrace()).iterator();
			boolean useNextOne = false;
			while (i.hasNext()) {
				StackTraceElement e = i.next();
				
				if (useNextOne) {
					return e;
					
				} else if (e.getClassName().compareTo(clazz.getName()) == 0) {
					useNextOne = true;
				}
			}
		}
		
		/**
		 * if not found write down a warning
		 */
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		t.printStackTrace(pw);
		return null;
	}

	/**
	 * Returns the StackTraceElement object found quite before the given class object.
	 * 
	 * @param clazz the class to look for in the stack trace
	 * @return the stackTraceElement if found or null
	 * @throws IllegalArgumentException if the Throwable or the clazz is null
	 * @since 1.5
	 */
	public static StackTraceElement getElementBeforeClass(Class<?> clazz) throws IllegalArgumentException {
		if (clazz == null) {
			throw new IllegalArgumentException("The class object may not be null");
			
		} else {
			boolean useNextOne = false;
			for (StackTraceElement e : Thread.currentThread().getStackTrace()) {

				if (useNextOne) {
					return e;
					
				} else if (e.getClassName().compareTo(clazz.getName()) == 0) {
					useNextOne = true;
				}
			}
		}
		
		/**
		 * if not found write down a warning
		 */
		StringBuffer sb = new StringBuffer();
		for (StackTraceElement e : Thread.currentThread().getStackTrace()) {
			sb.append(e.toString() + "\n");
		}
		return null;
	}
}

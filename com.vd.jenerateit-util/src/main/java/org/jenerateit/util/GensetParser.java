package org.jenerateit.util;

import java.io.InputStream;
import java.io.Serializable;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.jenerateit.exception.JenerateITException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class GensetParser {

	public static class Bundle {
		private final String id;
		private final String version;

		public Bundle(String id, String version) {
			this.id = id;
			this.version = version;
		}

		public String getId() {
			return id;
		}

		public String getVersion() {
			return version;
		}

		@Override
		public String toString() {
			return "{id: " + id + "; version: " + version + "}";
		}
	}

	public static class Option {

		private final String name;
		private final String value;
		private final String description;
		private final String visibility;

		public Option(String name, String value, String description, String visibility) {
			this.name = name;
			this.value = value;
			this.description = description;
			this.visibility = visibility;
		}

		public String getName() {
			return name;
		}

		public String getValue() {
			return value;
		}

		public String getDescription() {
			return description;
		}

		public String getVisibility() {
			return visibility;
		}

		@Override
		public String toString() {
			return "{name: " + name + "; value: " + value + "; description: " + description + "; visibility: "
					+ visibility + "}";
		}

	}

	public abstract static class TransformationStep {
		private final TransformationStep parent;
		private final String id;
		private final String version;
		private final Bundle bundle;
		private final boolean disabled;
		private List<Option> options = null;
		private URI uri;

		public TransformationStep(TransformationStep parent, String id, String version, Bundle bundle,
				boolean disabled) {
			this.parent = parent;
			this.id = id;
			this.version = version;
			this.bundle = bundle;
			this.disabled = disabled;
		}

		public TransformationStep getPreviousStep() {
			return parent;
		}

		public String getId() {
			return id;
		}

		public String getVersion() {
			return version;
		}

		public Bundle getBundle() {
			return bundle;
		}
		
		public boolean isDisabled() {
			return disabled;
		}

		public void setOptions(List<Option> options) {
			this.options = options;
		}

		public List<Option> getOptions() {
			return options;
		}

		public void setReference(URI uri) {
			this.uri = uri;
		}

		public URI getReference() {
			return uri;
		}

		public abstract List<TransformationStep> getNextSteps();

		public HashMap<String, Serializable> getMappedOptions() {
			HashMap<String, Serializable> ops = new HashMap<>();
			if (options != null) {
				for (Option option : options) {
					ops.put(option.getName(), option.getValue());
				}
			}
			return ops;
		}

		@Override
		public String toString() {
			String a = "id: " + id + "; version: " + version + "; bundle: " + bundle.toString() + "; disabled: "
					+ disabled;
			if (options != null) {
				a += "; options: " + options.toString();
			}
			return a;
		}
	}

	public static class ModelAccess extends TransformationStep {
		private final List<ModelConverter> modelConverters = new ArrayList<>();

		public ModelAccess(String id, String version, Bundle bundle, boolean disabled) {
			super(null, id, version, bundle, disabled);
		}

		public void addModelConverter(ModelConverter modelConverter) {
			this.modelConverters.add(modelConverter);
		}

		public List<ModelConverter> getModelConverters() {
			return modelConverters;
		}

		@Override
		public List<TransformationStep> getNextSteps() {
			List<TransformationStep> nextSteps = new ArrayList<>(modelConverters.size());
			nextSteps.addAll(modelConverters);
			return nextSteps;
		}

		@Override
		public String toString() {
			return "{" + super.toString() + "; modelConverters: " + listToString(modelConverters) + "}";
		}
	}

	public static class ModelConverter extends TransformationStep {
		private final List<GenerationGroup> generationGroups = new ArrayList<>();
		private final List<ModelConverter> modelConverters = new ArrayList<>();

		public ModelConverter(TransformationStep parent, String id, String version, Bundle bundle, boolean disabled) {
			super(parent, id, version, bundle, disabled);
		}

		public void addGenerationGroup(GenerationGroup generationGroup) {
			this.generationGroups.add(generationGroup);
		}

		public List<GenerationGroup> getGenerationGroups() {
			return generationGroups;
		}

		public void addModelConverter(ModelConverter modelConverter) {
			this.modelConverters.add(modelConverter);
		}

		public List<ModelConverter> getModelConverters() {
			return modelConverters;
		}

		@Override
		public List<TransformationStep> getNextSteps() {
			List<TransformationStep> nextSteps = new ArrayList<>(modelConverters.size() + generationGroups.size());
			nextSteps.addAll(modelConverters);
			nextSteps.addAll(generationGroups);
			return nextSteps;
		}

		@Override
		public String toString() {
			return "{" + super.toString() + "; generationGroups: " + listToString(generationGroups)
					+ "; modelConverters: " + listToString(modelConverters) + "}";
		}
	}

	public static class GenerationGroup extends TransformationStep {
		private final boolean active;
		private final String project;

		public GenerationGroup(TransformationStep parent, boolean active, String id, String version, boolean disabled,
				String project, Bundle bundle) {
			super(parent, id, version, bundle, disabled);
			this.active = active;
			this.project = project;
		}

		public boolean isActive() {
			return active;
		}

		public String getProject() {
			return project;
		}

		@Override
		public List<TransformationStep> getNextSteps() {
			return new ArrayList<>(0);
		}

		@Override
		public String toString() {
			return "{active: " + active + "; project: " + project + "; " + super.toString() + "}";
		}

	}

	private static String listToString(List<?> lists) {
		StringBuilder builder = new StringBuilder();
		builder.append("[");
		for (int i = 0; i < lists.size(); i++) {
			builder.append(lists.get(i).toString());
			if (i < (lists.size() - 1)) {
				builder.append(", ");
			}
		}
		builder.append("]");
		return builder.toString();
	}

	// public static void main(String[] args) throws FileNotFoundException {
	// File file = new File(
	// "/home/fred/gsoft/git/com.gs.gapp.devicedata.java/com.gs.gapp.devicedata.java.genset/VD-INF/virtualdeveloper.genset");
	// FileInputStream fis = new FileInputStream(file);
	// GensetParser parser = new GensetParser(fis);
	// System.out.println(parser.getModelAccess());
	// }

	private ModelAccess modelAccess;

	public GensetParser(InputStream inputStream) {
		Document document = null;
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			document = builder.parse(inputStream);
		} catch (Exception e) {
			throw new JenerateITException(e);
		}
		Element root = document.getDocumentElement();

		Node modelAccessNode = getModelAccessNode(root.getChildNodes());
		this.modelAccess = getModelAccess(modelAccessNode);
		// System.out.println("ModelAccess: " + modelAccess);

		Map<Node, ModelConverter> modelConverterMap = getRecursiveModelConverters(modelAccessNode.getChildNodes(),
				this.modelAccess, null);
		for (Node modelConverterNode : modelConverterMap.keySet()) {
			ModelConverter modelConverter = modelConverterMap.get(modelConverterNode);
			// System.out.println("ModelConverter: " + modelConverter);

			List<Node> generationGroupNodes = getGenerationGroupNodes(modelConverterNode.getChildNodes());

			for (Node generationGroupNode : generationGroupNodes) {
				GenerationGroup generationGroup = getGenerationGroup(generationGroupNode, modelConverter);

				// System.out.println("GenerationGroup: " + generationGroup);

				modelConverter.addGenerationGroup(generationGroup);
			}
		}

	}

	private Node getModelAccessNode(NodeList list) {
		Node modelAccessNode = null;
		for (int i = 0; i < list.getLength(); i++) {
			if (list.item(i).getNodeName().equals("ModelAccessNode")) {
				if (modelAccessNode != null) {
					throw new JenerateITException("More than one ModelAccessNode");
				}
				modelAccessNode = list.item(i);
			}
		}
		return modelAccessNode;
	}

	private ModelAccess getModelAccess(Node modelAccessNode) {
		String id = modelAccessNode.getAttributes().getNamedItem("id").getNodeValue();
		String version = modelAccessNode.getAttributes().getNamedItem("version").getNodeValue();
		if (id == null || version == null) {
			throw new JenerateITException("ModelAccess may not be null");
		}
		Bundle bundle = getBundleFromNodeList(modelAccessNode.getChildNodes(), id);
		Node disabledNode = modelAccessNode.getAttributes().getNamedItem("disabled");
		boolean disabled = disabledNode == null ? false : Boolean.parseBoolean(disabledNode.getNodeValue());
		ModelAccess modelAccess = new ModelAccess(id, version, bundle, disabled);
		modelAccess.setOptions(getOptionsFromNodeList(modelAccessNode.getChildNodes()));

		return modelAccess;
	}

	private List<Node> getModelConverterNodes(NodeList list) {
		List<Node> nodes = new ArrayList<>();
		for (int i = 0; i < list.getLength(); i++) {
			if (list.item(i).getNodeName().equals("ModelConverterNode")) {
				nodes.add(list.item(i));
			}
		}
		return nodes;
	}

	private ModelConverter getModelConverter(Node modelConverterNode, TransformationStep previousStep) {
		String id = modelConverterNode.getAttributes().getNamedItem("id").getNodeValue();
		String version = modelConverterNode.getAttributes().getNamedItem("version").getNodeValue();
		if (id == null || version == null) {
			throw new JenerateITException("ModelConverter may not be null");
		}
		Bundle bundle = getBundleFromNodeList(modelConverterNode.getChildNodes(), id);
		Node disabledNode = modelConverterNode.getAttributes().getNamedItem("disabled");
		boolean disabled = disabledNode == null ? false : Boolean.parseBoolean(disabledNode.getNodeValue());
		ModelConverter modelConverter = new ModelConverter(previousStep, id, version, bundle, disabled);
		modelConverter.setOptions(getOptionsFromNodeList(modelConverterNode.getChildNodes()));

		return modelConverter;
	}

	private Map<Node, ModelConverter> getRecursiveModelConverters(NodeList modelConverterNodeList,
			TransformationStep previousStep, Map<Node, ModelConverter> map) {
		if (map == null) {
			map = new HashMap<>();
		}

		for (Node modelConverterNode : getModelConverterNodes(modelConverterNodeList)) {
			ModelConverter modelConverter = getModelConverter(modelConverterNode, previousStep);
			map.put(modelConverterNode, modelConverter);
			if (previousStep instanceof ModelAccess) {
				ModelAccess modelAccess = (ModelAccess) previousStep;
				modelAccess.addModelConverter(modelConverter);
			} else if (previousStep instanceof ModelConverter) {
				((ModelConverter) previousStep).addModelConverter(modelConverter);
			}

			getRecursiveModelConverters(modelConverterNode.getChildNodes(), modelConverter, map);

		}

		return map;
	}

	private List<Node> getGenerationGroupNodes(NodeList list) {
		List<Node> nodes = new ArrayList<>();
		for (int i = 0; i < list.getLength(); i++) {
			if (list.item(i).getNodeName().equals("GenerationGroupNode")) {
				nodes.add(list.item(i));
			}
		}
		return nodes;
	}

	private GenerationGroup getGenerationGroup(Node generationGroupNode, ModelConverter previousStep) {
		boolean active = generationGroupNode.getAttributes().getNamedItem("active") != null
				? Boolean
						.parseBoolean(generationGroupNode.getAttributes().getNamedItem("active").getNodeValue())
						: false;
		String id = generationGroupNode.getAttributes().getNamedItem("id").getNodeValue();
		String version = generationGroupNode.getAttributes().getNamedItem("version").getNodeValue();
		String project = generationGroupNode.getAttributes().getNamedItem("project").getNodeValue();
		if (id == null || version == null | project == null) {
			throw new JenerateITException("GenerationGroup may not be null");
		}
		Bundle bundle = getBundleFromNodeList(generationGroupNode.getChildNodes(), id);
		Node disabledNode = generationGroupNode.getAttributes().getNamedItem("disabled");
		boolean disabled = disabledNode == null ? false : Boolean.parseBoolean(disabledNode.getNodeValue());
		GenerationGroup generationGroup = new GenerationGroup(previousStep, active, id, version, disabled, project,
				bundle);
		generationGroup.setOptions(getOptionsFromNodeList(generationGroupNode.getChildNodes()));

		return generationGroup;
	}

	private Bundle getBundleFromNodeList(NodeList nodeList, String id) {
		for (int i = 0; i < nodeList.getLength(); i++) {
			if (nodeList.item(i).getNodeName().equals("BundleNode")) {
				String bundleId = nodeList.item(i).getAttributes().getNamedItem("id").getNodeValue();
				String bundleValue = nodeList.item(i).getAttributes().getNamedItem("version").getNodeValue();
				if (bundleId == null || bundleValue == null) {
					throw new JenerateITException("BundleNode may not be null");
				}
				return new Bundle(bundleId, bundleValue);
			}
		}
		System.out.println("No BundleNode found for provider '" + id + "'");
		return null;
//		throw new JenerateITException("No BundleNode found for provider '" + id + "'");
	}

	private List<Option> getOptionsFromNodeList(NodeList nodeList) {
		List<Option> options = new ArrayList<>();
		for (int i = 0; i < nodeList.getLength(); i++) {
			if (nodeList.item(i).getNodeName().equals("OptionElement")) {
				Node optionNode = nodeList.item(i);
				Node nameItem = optionNode.getAttributes().getNamedItem("name");
				Node valueItem = optionNode.getAttributes().getNamedItem("value");
				Node descItem = optionNode.getAttributes().getNamedItem("description");
				Node visItem = optionNode.getAttributes().getNamedItem("visibility");
				
				String name = nameItem != null ? nameItem.getNodeValue() : null;
				String value = valueItem != null ? valueItem.getNodeValue() : null;
				String description = descItem != null ? descItem.getNodeValue() : null;
				String visibility = visItem != null ? visItem.getNodeValue() : null;

				if (name == null) {
					throw new JenerateITException("OptionElement may not be null");
				}
				options.add(new Option(name, value, description, visibility));
			}
		}
		return options.isEmpty() ? null : options;
	}

	public ModelAccess getModelAccess() {
		return modelAccess;
	}
}

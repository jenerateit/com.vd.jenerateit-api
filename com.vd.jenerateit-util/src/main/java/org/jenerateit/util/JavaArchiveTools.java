/**
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit.util;

import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.net.URL;
import java.security.CodeSource;
import java.security.ProtectionDomain;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.jar.Attributes;
import java.util.jar.Attributes.Name;
import java.util.jar.JarInputStream;

/**
 * Methods to handle java archive files
 * 
 * @author hrr
 */
public final class JavaArchiveTools {
	
	/**
	 * Returns a list of java archive files from the given directories/files
	 * 
	 * @param dirList the directories/files to look for the JAR files
	 * @throws FileNotFoundException if the directory dir is not valid
	 * @throws IllegalArgumentException if the given File is not a Directory
	 * @return a list of java archives
	 */
	public static List<File> getJavaArchiveFiles(List<File> dirList) throws IllegalArgumentException, FileNotFoundException {
		return getJavaArchiveFiles(dirList, false);
	}
	
	/**
	 * Returns a list of java archive files from the given directories/files and if useSubDir is true also
	 * of the sub directories.
	 * 
	 * @param dirList the directories/files to look for the JAR/ZIP files
	 * @param useSubDirs true if the subdirs should also be checked
	 * @throws FileNotFoundException if the directory dir is not valid
	 * @throws IllegalArgumentException if the given File is not a Directory
	 * @return a list of java archives
	 */
	public static List<File> getJavaArchiveFiles(List<File> dirList, boolean useSubDirs) throws IllegalArgumentException, FileNotFoundException {

		if (dirList != null && dirList.size() > 0) {
			Iterator<File> i = dirList.iterator();
			File dir = null;
			List<File> archiveFiles = new ArrayList<>();
			while (i.hasNext()) {
				dir = i.next();
				archiveFiles.addAll(getJavaArchiveFiles(dir, useSubDirs));
				
			}
			return archiveFiles;
			
		} else {
			throw new IllegalArgumentException("The list of directories should not be null or empty");
		}
	}

	/**
	 * Returns a list of java archive files from the given directories/files and if useSubDir is true also
	 * of the sub directories.
	 * 
	 * @param dirList the directories/files to look for the JAR/ZIP files
	 * @param useSubDirs true if the subdirs should also be checked
	 * @throws FileNotFoundException if the directory dir is not valid
	 * @throws IllegalArgumentException if the given File is not a Directory
	 * @return a list of java archives as URL collection
	 */
	public static Set<URL> getJavaArchiveURLs(List<File> dirList, boolean useSubDirs) throws IllegalArgumentException, FileNotFoundException {

		if (dirList != null && dirList.size() > 0) {
			Set<URL> URLs = new HashSet<>();
			for (File f : getJavaArchiveFiles(dirList, useSubDirs)) {
				try {
					URLs.add(f.toURI().normalize().toURL());
				} catch (Exception e) {
				}
			}
			return URLs;
			
		} else {
			throw new IllegalArgumentException("The list of directories/files should not be null or empty");
		}
	}

	/**
	 * Returns a list java archive files from the given directory (and subDirs if set).
	 * 
	 * @param dir the directory to look for jar and zip files
	 * @return a list of java archive files
	 * @throws FileNotFoundException if the directory can not be read
	 * @throws IllegalArgumentException if the directory is not valid 
	 */
	public static List<File> getJavaArchiveFiles(File dir) throws FileNotFoundException, IllegalArgumentException {
		return getJavaArchiveFiles(dir, false);
	}
	
	/**
	 * Returns a list java archive files from the given directory or file (and subDirs if set).
	 * 
	 * @param f the directory or file to look/check for jar and zip files
	 * @param useSubDirs true if the subdirs should also be checked
	 * @return a list of java archive files
	 * @throws FileNotFoundException if the directory can not be read
	 * @throws IllegalArgumentException if the directory is not valid 
	 */
	public static List<File> getJavaArchiveFiles(File f, boolean useSubDirs) throws FileNotFoundException, IllegalArgumentException {
		List<File> files = new ArrayList<>();
		if (f == null) {
			throw new IllegalArgumentException("The directory/file may not be null");

		} else if (!f.exists() ) {
			throw new FileNotFoundException("The directory/file '" + f + "' is not valid");

		} else if (f.isFile() && f.canRead() && checkForArchive(f.getName())) {
			files.add(f);
			
		} else if (f.isDirectory()) {
			
			files.addAll(Arrays.asList(f.listFiles(new FilenameFilter() {
				// the attribute 'dir' is ignored
				@Override
				public boolean accept(File dir, String name) {
					if (checkForArchive(name)) {
						return true;
					}
					return false;
				}})
			));
			
			if (useSubDirs) {
				Iterator<File> i = Arrays.asList(f.listFiles(new FileFilter() {
	
					@Override
					public boolean accept(File pathname) {
						if (pathname != null && pathname.exists() && pathname.isDirectory()) {
							return true;
						}
						return false;
					}})).iterator();
				
				File subDir = null;
				while (i.hasNext()) {
					subDir = i.next();
					files.addAll(getJavaArchiveFiles(subDir, useSubDirs));
				}
			}
		}
		return files;
	}

	/**
	 * Checks if the given path name is an archive.
	 * <ul>
	 * <li>the name is not null or empty</li>
	 * <li>the name ends with '.jar' or '.zip'
	 * </ul>
	 * 
	 * @param pathName the name of the archive
	 * @return true if the name fits to an archive name otherwise false
	 */
	private static boolean checkForArchive(String pathName) {
		return StringTools.isNotEmpty(pathName) && 
			(pathName.toLowerCase().endsWith(".jar") || pathName.toLowerCase().endsWith(".zip"));
	}
	
	/**
	 * Returns a list java archive files from the given directory/file (and subDirs if set).
	 * 
	 * @param f the directory/file to look for jar and zip files
	 * @param useSubDirs true if the subdirs should also be checked
	 * @return a list of URL pointing to java archive files
	 * @throws FileNotFoundException if the directory can not be read
	 * @throws IllegalArgumentException if the directory is not valid 
	 */
	public static Set<URL> getJavaArchiveURLs(File f, boolean useSubDirs) throws FileNotFoundException, IllegalArgumentException {
		return getJavaArchiveURLs(Arrays.asList(new File[] {f}), useSubDirs);
	}

	/**
	 * Try to locate the java archive (JAR file) of the given class. Returns null if the security 
	 * permissions are not given to access the jar file or if the class file 'entry' is not part of a
	 * jar file.
	 * 
	 * @param entry the class file to look it's jar file for
	 * @return the jar file location or null if not found
	 */
	public static URL getJavaArchiveLocation(Class<?> entry) {
		if (entry == null) {
			throw new NullPointerException("The entry class object may not be null");
			
		} else {
			try {
				ProtectionDomain pd = entry.getProtectionDomain();
				if (pd != null) {
					CodeSource cs = pd.getCodeSource();
					if (cs != null) {
						URL u = cs.getLocation();
						if (u != null && (u.getPath().toLowerCase().endsWith(".jar") || u.getPath().toLowerCase().endsWith(".zip"))) {
							return u;
							
						}
					}
				}
			} catch (Throwable t) {
			}
			return null;
		}
	}
	
	/**
	 * Returns the version information out of the MANIFEST information out of the given jar file.
	 * 
	 * @see Name#IMPLEMENTATION_VERSION
	 * @see Name#SPECIFICATION_VERSION
	 * @param location the URL to the jar file.
	 * @return the version information
	 */
	public static String getJavaArchiveVersion(URL location) {
		if (location == null) {
			throw new NullPointerException("The location may not be null");
			
		} else if (location.getPath() == null || 
				(!location.getPath().toLowerCase().endsWith(".jar") &&
				!location.getPath().toLowerCase().endsWith(".zip"))) {
			throw new IllegalArgumentException("The given location is not a jar file");
			
		} else {
			try (JarInputStream input = new JarInputStream(location.openStream())) {
				;//location.toURL().openStream());
				Attributes attributes = input.getManifest().getMainAttributes();
				if (attributes == null) {
					return null;
					
				} else if (attributes.containsKey(Attributes.Name.IMPLEMENTATION_VERSION)) {
					return attributes.getValue(Attributes.Name.IMPLEMENTATION_VERSION);
				
				} else if (attributes.containsKey(Attributes.Name.SPECIFICATION_VERSION)) {
					return attributes.getValue(Attributes.Name.SPECIFICATION_VERSION);
					
				}
			} catch (Throwable t) {
			} 
		}
		return null;
	}
	
	/**
	 * Separate the versions string into separate tokens. The '.' is expected to be the separator.
	 *  
	 * @param versions the complete version string
	 * @return the separated version string
	 */
	public static String[] getVersionDetails(String versions) {
		if (versions == null) {
			throw new NullPointerException("The version information may not be null");
			
		} else {
			StringTokenizer st = new StringTokenizer(versions, ".");
			Set<String> vs = new HashSet<>();
			while (st.hasMoreTokens()) {
				vs.add(st.nextToken());
			}
			return vs.toArray(new String[vs.size()]);
		}
	}
	
	/**
	 * Separate the versions string into separate version numbers. The '.' is expected to be the separator.
	 *  
	 * @param versions the complete version string
	 * @return the separated version numbers
	 */
	public static int[] getVersionNumbers(String versions) {
		if (versions == null) {
			throw new NullPointerException("The version information may not be null");
			
		} else {
			StringTokenizer st = new StringTokenizer(versions, ".");
			int vs[] = new int[st.countTokens()];
			for (int i = 0; i < vs.length; i++) {
				try {
					vs[i] = Integer.parseInt(st.nextToken());
				} catch (Throwable t) {
					vs[i] = -1;
				}
			}
			return vs;
			
		}
	}
}
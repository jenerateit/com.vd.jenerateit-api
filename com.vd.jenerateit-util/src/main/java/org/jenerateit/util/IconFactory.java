/**
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit.util;

import javax.swing.ImageIcon;

/**
 * Class to load icons out of a directory
 * 
 * @author $Author: hrr $
 * @version $Revision: 66 $
 */
public class IconFactory {

	/**
	 * Method to load Icons. Please note, the icons must be placed in a subpackage named
	 * 'icons' based on the given class.
	 *  
	 * @param clazz the class (package) to look for the icons {@link java.lang.Class#getResource(String)}
	 * @param name the name of the icon
	 * @return an ImageIcon
	 * @throws IllegalArgumentException if the name is not valid
	 * @throws NullPointerException if there is no resource (image) with the given name
	 */
	public static ImageIcon getImageIcon(Class<?> clazz, String name) 
		throws IllegalArgumentException, NullPointerException
	{
		if (StringTools.isNotEmpty(name)) {
			return new ImageIcon(clazz.getResource("icons/" + name));
		} else {
			throw new IllegalArgumentException("The name may not be null or empty");
		}
	}
}

/*
 * Copyright (c) 2013 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package org.jenerateit.util;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * @author hrr
 *
 */
public class UriTools {

	/**
	 * Check the URI for a correct syntax for JenerateIT.
	 * <p><b>NOTE:</b> In case of a not valid {@link URI} an Exception is thrown</p>
	 * 
	 * following steps are run:
	 * <ul>
	 * 	<li>use {@link URI#normalize()} method</li>
	 * 	<li>remove leading '/' and '.' ('..')</li>
	 * </ul>
	 * 
	 * 
	 * @see URI#normalize()
	 * @param uri the URI to check and may be modified
	 * @return a normalized relative URI
	 */
	public static URI checkAndModify(final URI uri) {
		if (uri == null) {
			throw new NullPointerException("The URI to check may not be null");
		}
		URI result = uri.normalize();
		final String path = result.getPath();
		for (int i = 0; path != null && i < path.length(); i++) {
			if (path.charAt(i) == '.'
					&& (i + 1) < path.length()
					&& (path.charAt(i + 1) == '.'
					|| path.charAt(i + 1) == '/')) {
				throw new IllegalArgumentException("The URI may not have dot(s) at the beginning of the path");
			} else if (path.charAt(i) == '~') {
				throw new IllegalArgumentException("The URI may not have a '~‘ at the beginning of the path");
			} else if (path.charAt(i) == '/') {
				continue;
			} else {
				if (i == 0) {
					// path is OK -> leave it as is
				} else {
					try {
						result = new URI(path.substring(i));
					} catch (URISyntaxException e) {
						// should never happen, since the path is normalized before
					}
				}
				break;
			}
		}
		
		return result;
	}

}

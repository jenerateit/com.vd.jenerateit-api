/*
 *  Virtual Developer - Code Generation the easy way
 *
 *  http://www.virtual-developer.com
 *
 *  Copyright:
 *    2007-2013 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    commercial license only
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit.util;

import java.util.List;

/**
 * Provides messages (info, wanring and error) during a JenerateIT runtime task.
 * 
 * @author hrr
 *
 */
public interface MessageProviderI {

	/**
	 * Provides all information messages produced by a JenerateIT runtime task
	 * 
	 * @return a list of information messages
	 */
	List<String> getInfos();


	/**
	 * Provides all warning messages produced by a JenerateIT runtime task
	 * 
	 * @return a list of warning messages
	 */
	List<String> getWarnings();


	/**
	 * Provides all error messages produced by a JenerateIT runtime task
	 * 
	 * @return a list of error messages
	 */
	List<String> getErrors();
}

/**
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit.util;

import java.awt.EventQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Class to start a new Swing Thread
 * 
 * @author hrr
 * @version $Revision: 23 $
 * 
 * @param <V> the result type
 */
public abstract class SwingWorker<V> implements Future<V>, Runnable {

    /**
     * Default executor. Executes each task in a new thread.
     */
    private static final Executor EXECUTOR = new Executor() {
        @Override
		public void execute(Runnable command) {
            new Thread(command).start();
        }
    };

    /** Executor instance. */
    private Executor executor;

    /** <tt>true</tt> if <tt>start</tt> method was called. */
    private boolean started;

    /** Creates new SwingWorker with default executor. */
    public SwingWorker() {
        this(EXECUTOR);
    }

    /**
     * Creates new SwingWorker with specified executor.
     * @param e executor for this worker
     */
    protected SwingWorker(Executor e) {
        setExecutor(e);
    }

    /**
     * Sets executor to be used when worker is started. 
     * @param e executor for this worker
     */
    public synchronized void setExecutor(Executor e) {
        executor = e;
    }

    /**
     * Returns executor to be used when worker is started. 
     * @return executor
     */
    public synchronized Executor getExecutor() {
        return executor;
    }

    /**
     * Submits this worker to executor for execution.
     */
    public synchronized void start() {
        if (!started) {
            executor.execute(this);
            started = true;
        }
    }

    /**
     * Calls the <tt>construct</tt> method to compute the
     * result, then invokes the <tt>finished</tt> method on
     * the event dispatch thread.
     */
    private final FutureTask<V> task =
        new FutureTask<V>(new Callable<V>() {
            @Override
			public V call() throws Exception {
                return construct();
            }
        }) {
            @Override
			protected void done() {
                EventQueue.invokeLater(new Runnable() {
                    @Override
					public void run() {
                        finished();
                    }
                });
            }
        };

    /**
     * Computes the value to be returned by the <tt>get</tt> method.
     * @return the computed result
     * @throws Exception in case an error occurs
     */
    protected abstract V construct() throws Exception;

    /**
     * Called on the event dispatching thread (not on the worker thread)
     * after the <tt>construct</tt> method has returned.
     */
    protected void finished() { }

    /* Runnable implementation. */

    /**
     * Runs this task. Called by executor.
     */
    @Override
	public void run() {
        task.run();
    }

    /* Future implementation. */

    /**
     * Attempts to cancel execution of this task. 
     * 
     * @param mayInterruptIfRunning flag to indicate if a running task should be interrupted
     * @return false if the task could not be cancelled, typically because it has already completed normally; true otherwise
     * @see java.util.concurrent.Future#cancel(boolean)
     * */
    @Override
	public boolean cancel(boolean mayInterruptIfRunning) {
        return task.cancel(mayInterruptIfRunning);
    }

    /**
     * Returns true if this task was cancelled before it completed normally.
     * 
     * @return true if task was cancelled before it completed
     * @see java.util.concurrent.Future#isCancelled()
     */
    @Override
	public boolean isCancelled() {
        return task.isCancelled();
    }

    /**
     * Returns true if this task completed.
     * 
     * @return true if this task completed.
     * @see java.util.concurrent.Future#isDone()
     */
    @Override
	public boolean isDone() {
        return task.isDone();
    }

    /**
     * Waits if necessary for this task to complete, and then returns its
     * result.
     * 
     * @return the computed result
     * @throws InterruptedException if the current thread was interrupted while waiting
     * @throws ExecutionException if the computation threw an exception
     * @throws CancellationException if the computation was cancelled
     * @see java.util.concurrent.Future#get()
     */
    @Override
	public V get() throws InterruptedException, ExecutionException, CancellationException {
        return task.get();
    }

    /**
     * Waits if necessary for at most the given time for this task to complete,
     * and then returns its result, if available.
     * 
     * @param timeout the maximum time to wait
     * @param unit the time unit of the timeout argument
     * @return the computed result
     * @throws InterruptedException if the current thread was interrupted while waiting
     * @throws ExecutionException if the computation threw an exception
     * @throws CancellationException if the computation was cancelled
     * @throws TimeoutException if the wait timed out
     * @see java.util.concurrent.Future#get(long, TimeUnit)
     */
    @Override
	public V get(long timeout, TimeUnit unit)
        throws InterruptedException, ExecutionException, TimeoutException, CancellationException {
        return task.get(timeout, unit);
    }
}
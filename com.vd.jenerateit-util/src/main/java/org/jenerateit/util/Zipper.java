package org.jenerateit.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.osgi.framework.Bundle;
import org.osgi.framework.wiring.BundleWiring;

/**
 * Creates an input stream for a given set of files. The files are going
 * to be loaded from the classpath that is defined by the given
 * bundle instance.
 * One possible use case for this class is to provided test models
 * as input for model access implementations.
 * 
 * @author mmt
 *
 */
public class Zipper {

	private final byte[] data;

	/**
	 * @param bundle
	 * @param fileNames
	 */
	public Zipper(Bundle bundle, String... fileNames) {
		this(bundle, 1024, fileNames);
	}
	
	/**
	 * @param classLoader
	 * @param fileNames
	 */
	public Zipper(ClassLoader classLoader, String... fileNames) {
		this(classLoader, 1024, fileNames);
	}
	
	/**
	 * @param bundle
	 * @param bufferLength
	 * @param fileNames
	 */
	public Zipper(Bundle bundle, int bufferLength, String... fileNames) {
		this(bundle.adapt(BundleWiring.class).getClassLoader(), bufferLength, fileNames);
	}
	
	/**
	 * @param classLoader
	 * @param bufferLength
	 * @param fileNames
	 */
	public Zipper(ClassLoader classLoader, int bufferLength, String... fileNames) {
		if (classLoader == null) throw new NullPointerException("parameter 'classLoader' must not be null");
		
		try (final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			final ZipOutputStream zipOutputStream = new ZipOutputStream(byteArrayOutputStream)) {

			if (fileNames != null) {
				byte[] bytes = new byte[bufferLength];
				for (String fileName : fileNames) {
					InputStream fileInputStream = classLoader.getResourceAsStream(fileName);
					if (fileInputStream == null) {
						throw new RuntimeException("resource '" + fileName + "' not found through class loader '" + classLoader + "'"); 
					}
					ZipEntry zipEntry = new ZipEntry(fileName);
					zipOutputStream.putNextEntry(zipEntry);
					int length;
					while ((length = fileInputStream.read(bytes)) >= 0) {
						zipOutputStream.write(bytes, 0, length);
					}
					zipOutputStream.closeEntry();
					fileInputStream.close();
				}
			}
			
			byteArrayOutputStream.flush();
			byte[] byteArray = byteArrayOutputStream.toByteArray();
			this.data = byteArray == null ? new byte[0] : byteArray;
			
		} catch (IOException ex) {
			ex.printStackTrace();
			throw new RuntimeException("I/O problems during reading of file for the following names:" + fileNames, ex);
		}
	}

	/**
	 * @return a new InputStream instance for every method invocation (the stream provides the zip file data)
	 */
	public InputStream getInputStream() {
		return new ByteArrayInputStream(this.data);
	}
}

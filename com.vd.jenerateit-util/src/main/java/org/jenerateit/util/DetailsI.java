/**
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit.util;

/**
 * Interface for details about an implementing object.
 * The details are a name and a (may be optional) description.
 * 
 * @author hrr
 *
 */
public interface DetailsI {

	/**
	 * Getter to request the name of the implementing object.
	 * 
	 * @return the name
	 */
	String getName();
	
	/**
	 * Getter to request a description about the implementing object.
	 * 
	 * @return the description
	 */
	String getDescription();
}

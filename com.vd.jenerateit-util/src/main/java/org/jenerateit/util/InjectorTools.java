/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package org.jenerateit.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

/**
 * @author hrr
 *
 */
public class InjectorTools {

	/**
	 * Set all annotated fields in the given object.
	 * 
	 * @param obj the object to set its fields
	 * @param annotation the annotation type to look for
	 * @param value the value to set in the field
	 * @throws IllegalArgumentException if the field type does not match
	 * @throws IllegalAccessException if the filed could not be accessed
	 */
	public static void injectFields(final Object obj, final Class<? extends Annotation> annotation, final Object value) 
			throws IllegalArgumentException, IllegalAccessException {
		if (obj == null) {
			throw new NullPointerException("The object to set its fields may not be null");
			
		} else if (annotation == null) {
			throw new NullPointerException("The annotation description may not be null");
			
		} else {
			Class<?> sc = obj.getClass();
			while (sc != null) {
				
				for (Field f : sc.getDeclaredFields()) {
										
					if (f.isAnnotationPresent(annotation)) {
						if (value == null) {
							// null should work always
						} else if (!f.getType().isInstance(value)) {
							throw new IllegalArgumentException("Found a field '" + f.getName() + "' of type '" + f.getType() + "' in class '" + 
									sc.getName() + "' with the annotation '" +	annotation.getSimpleName() + 
									"' but the type (" + value.getClass().getName() + 
									") does not match. Please check your implementation.");
						}
						f.setAccessible(true);
						f.set(obj, value);
					}
				}
				sc = sc.getSuperclass();
			}

		}
	}
	
}

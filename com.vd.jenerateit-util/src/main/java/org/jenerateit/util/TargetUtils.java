/*
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit.util;

import java.net.URI;
import java.net.URISyntaxException;

import org.jenerateit.exception.JenerateITException;


/**
 * @author hrr
 *
 */
public class TargetUtils {

    /**
     * <p>
     * Creates an error file URI out of a given target URI with the following rules:
     * </p>
     * 
     * <ul>
     * 	<li>/src/com/gs/gssp/DefaultTarget.java -&gt; DefaultTarget_error.java</li>
     * 	<li>/filelist -&gt; filelist_error</li>
     * </ul>
     * 
     * Some more text
     * 
     * @param targetURI the target URI which is used for normal operations
     * @return an error file URI with a special extension
     * @throws NullPointerException if the target URI is null
     * @throws JenerateITException if the target URI does not have a valid path segment
     * @throws URISyntaxException in case the URI can not be created
     */
    public static URI getTargetErrorURI(URI targetURI) 
    	throws NullPointerException, URISyntaxException {
    	if (targetURI == null) {
    		throw new NullPointerException("The target URI must not be null");
    		
    	} else if (!StringTools.isText(targetURI.getPath())) {
    		throw new JenerateITException("The target file '" + targetURI.toString() + "' does not have a valid path segment");
    		
    	} else {
    		return new URI(
    				targetURI.getScheme(),
    				targetURI.getUserInfo(),
    				targetURI.getHost(),
    				targetURI.getPort(),
    				getTargetErrorURI(targetURI.getPath()),
    				targetURI.getQuery(),
    				targetURI.getFragment());
    	}
    }
    
    private static final String ERROR_EXTENSION = "_error";
    
    /**
     * Creates an error URI out of a given target URI with the following rules:
     * <ul>
     * 	<li>DefaultTarget.java -&gt; DefaultTarget_error.java</li>
     * 	<li>filelist -&gt; filelist_error</li>
     * </ul>
     * 
     * Some more text
     * 
     * @param targetURI the target URI which is used for normal operations
     * @return an error URI with a special extension
     */
    public static String getTargetErrorURI(String targetURI) {
    	if (!StringTools.isText(targetURI)) {
    		throw new IllegalArgumentException("The target URI '" + targetURI + "' is not valid");
    		
    	} else if (targetURI.endsWith("/")) {
    		throw new JenerateITException("The target URI '" + targetURI + "' without a file information");
    		
    	} else {
    		StringBuffer result = new StringBuffer();

    		String s = targetURI;
	    	int pos = s.lastIndexOf("/");
	    	if (pos != -1) {
	    		result.append(s.substring(0, pos + 1));
	    		s = s.substring(pos + 1);
	    	}
	    	pos = s.lastIndexOf(".");
	    	if (pos != -1) {
	    		result.append(s.substring(0, pos)).append(ERROR_EXTENSION).append(s.substring(pos));
	    	} else {
	    		result.append(s).append(ERROR_EXTENSION);
	    	}
	    	return result.toString();
    	}
    }
    
    /**
     * Checks if the given URI is an error URI
     * 
     * @param targetURI the target URI to check
     * @return true if the target URI is an error URI otherwise false
     * @throws NullPointerException if the target URI is null
     * @throws JenerateITException if the target URI does not have a valid path segment
     */
    public static boolean isTargetErrorURI(URI targetURI) 
    	throws NullPointerException {
    	if (targetURI == null) {
    		throw new NullPointerException("The target URI must not be null");
    		
    	} else if (!StringTools.isText(targetURI.getPath())) {
    		throw new JenerateITException("The target file '" + targetURI.toString() + "' does not provide a path segment");
    		
    	} else {
    		
	    	return isTargetErrorURI(targetURI.getPath());
    	}
    }

    /**
     * Checks if the given target URI is an error file. Also a check for valid path is executed.
     * 
     * @param targetURI the target name to check
     * @return true if the target URI is an error URI otherwise false
     */
    public static boolean isTargetErrorURI(String targetURI) {
    	if (!StringTools.isText(targetURI)) {
    		throw new IllegalArgumentException("The target URI '" + targetURI + "' is not valid");
    		
    	} else if (targetURI.endsWith("/")) {
    		throw new JenerateITException("The target URI '" + targetURI + "' without a file information");

    	} else {
    		String s = getFilePart(targetURI);
    		return s.endsWith(ERROR_EXTENSION);
    	}
    }
    
    private static String getFilePart(String targetURI) {
		String s = targetURI;
    	int pos = s.lastIndexOf("/");
    	if (pos != -1) {
    		s = s.substring(pos + 1);
    	}
    	pos = s.lastIndexOf(".");
    	if (pos != -1) {
    		return s.substring(0, pos);
    	} else {
    		return s;
    	}
    }
    
}

/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package org.jenerateit.util;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.BitSet;
import java.util.StringTokenizer;

import org.osgi.framework.Version;




/**
 * Helper class to work with the URI coming from JenerateIT server.
 * This helper class has parsing methods and URI builder methods as well.
 * 
 * @author Heinz Rohmer
 *
 */
public final class UriParser {
	
	private static final char SEPARATOR = '/';
	public static final String MODELACCESS = "ma";
	public static final String MODELCONVERTER = "mc";
	public static final String TARGETPROJECT = "tp";
	public static final String VIRTUALDEVELOPER = "vd";
	public static final String GENERATIONGROUP = "gg";
	public static final String TARGET = "target";
	public static final String OSGI = "osgi";

	private static final int PRIME = 31;

	private int modelAccessId = Integer.MIN_VALUE;
	
	private int modelConverterId = Integer.MIN_VALUE;
	
	private int projectId = Integer.MIN_VALUE;
	
	private int virtualDeveloperId = Integer.MIN_VALUE;

	private int generationGroupId = Integer.MIN_VALUE;
	
	private URI targetPath = null;
	
	private long osgiBundleId = Long.MIN_VALUE;
	
	/**
	 * Constructor
	 */
	public UriParser() {
		super();
	}

	/**
	 * Construcotr parses the path element of the {@link URI} parameter
	 * 
	 * @param uri the URI with the path element to parse
	 */
	public UriParser(final URI uri) {
		this(uri.getPath());
	}
	
	/**
	 * Constructor, parses the given path
	 * 
	 * @param path the path to parse
	 */
	public UriParser(final String path) {
		final StringTokenizer st = new StringTokenizer(path, "/");
		String token = null;
		while (st.hasMoreTokens()) {
			token = st.nextToken();
			if (TARGETPROJECT.compareTo(token) == 0) {
				if (st.hasMoreTokens()) {
					projectId = Integer.parseInt(st.nextToken());
					continue;
				}
				throw new IllegalArgumentException("Found a target project URI '" + path + "' which is not valid");
			} else if (VIRTUALDEVELOPER.compareTo(token) == 0) {
				if (st.hasMoreTokens()) {
					virtualDeveloperId = Integer.parseInt(st.nextToken());
					continue;
				}
				throw new IllegalArgumentException("Found a generation group URI '" + path + "' which is not valid");
			} else if (GENERATIONGROUP.compareTo(token) == 0) {
				if (st.hasMoreTokens()) {
					generationGroupId = Integer.parseInt(st.nextToken());
					continue;
				}
				throw new IllegalArgumentException("Found a generation group URI '" + path + "' which is not valid");
			} else if (TARGET.compareTo(token) == 0) {
				if (st.hasMoreTokens()) {
					final StringBuilder sb = new StringBuilder(st.nextToken());
					while (st.hasMoreTokens()) {
						sb.append("/").append(st.nextToken());
					}
					try {
						targetPath = new URI(sb.toString());
					} catch (URISyntaxException e) {
						throw new RuntimeException("Error while prepare target path URI");
					}
					continue;
				}
				throw new IllegalArgumentException("Found a target URI '" + path + "' which is not valid");
			} else if (MODELACCESS.compareTo(token) == 0) {
				if (st.hasMoreTokens()) {
					modelAccessId = Integer.parseInt(st.nextToken());
					continue;
				}
				throw new IllegalArgumentException("Found an model access URI '" + path + "' with is not valid");
			} else if (MODELCONVERTER.compareTo(token) == 0) {
				if (st.hasMoreTokens()) {
					modelConverterId = Integer.parseInt(st.nextToken());
					continue;
				}
				throw new IllegalArgumentException("Found an model converter URI '" + path + "' with is not valid");
			} else if (OSGI.compareTo(token) == 0) {
				if (st.hasMoreTokens()) {
					osgiBundleId = Long.parseLong(st.nextToken());
					continue;
				}
				
			}
		}
	}

	/**
	 * Getter for the model access identifier
	 * 
	 * @return the modelAccessId
	 */
	public int getModelAccessId() {
		return modelAccessId;
	}

	/**
	 * Setter for the model access identifier
	 * 
	 * @param modelAccessId the model access identifier to set
	 * @return this parser object for fluent usage
	 */
	public UriParser setModelAccessId(int modelAccessId) {
		this.modelAccessId = modelAccessId;
		return this;
	}

	/**
	 * Getter for the model converter identifier
	 * 
	 * @return the modelConverterId the model converter identifier
	 */
	public int getModelConverterId() {
		return modelConverterId;
	}

	/**
	 * Setter for the model converter identifier
	 * 
	 * @param modelConverterId the model converter identifier to set
	 * @return this parser object for fluent usage
	 */
	public UriParser setModelConverterId(int modelConverterId) {
		this.modelConverterId = modelConverterId;
		return this;
	}

	/**
	 * Getter for the project identifier
	 * 
	 * @return the project identifier
	 */
	public int getProjectId() {
		return this.projectId;
	}

	/**
	 * The setter for the project identifier
	 * 
	 * @param id the project identifier
	 * @return this parser object for fluent usage
	 */
	public UriParser setProjectId(final int id) {
		this.projectId = id;
		return this;
	}
	
	/**
	 * Setter for the project identifier. The identifier is created
	 * by the given name
	 * 
	 * @param projectName the project name to set
	 * @return this parser object for fluent usage
	 */
	public UriParser setProjectName(String projectName) {
		if (!StringTools.isText(projectName)) {
			throw new IllegalArgumentException("The porject name is not valid");
		}
		this.projectId = projectName.trim().hashCode();
		return this;
	}

	/**
	 * Getter for the virtual developer identifier (generator identifier)
	 * 
	 * @return the virtual developer identifier
	 */
	public int getVirtualDeveloperId() {
		return virtualDeveloperId;
	}

	/**
	 * Setter for the virtual developer identifier (generator identifier)
	 * 
	 * @param virtualDeveloperId the virtual developer identifier to set
	 * @return this parser object for fluent usage
	 */
	public UriParser setVirtualDeveloperId(int virtualDeveloperId) {
		this.virtualDeveloperId = virtualDeveloperId;
		return this;
	}

	/**
	 * Setter for the virtual developer identifier (generator identifier). 
	 * The identifier will be created by the generator name and version (major and minor number)
	 * 
	 * @param name generator name
	 * @param version generator version
	 * @return this parser object for fluent usage
	 */
	public UriParser setVirtualDeveloper(final String name, final Version version) {
		if (!StringTools.isText(name)) {
			throw new IllegalArgumentException("The name of the virtual developer is not valid");
		} else if (version == null) {
			throw new NullPointerException("The version of the virtual developer may not be null");
		}
		int id = 1;
		id = PRIME * id + name.trim().hashCode();
		id = PRIME * id + version.getMajor();
		id = PRIME * id + version.getMinor();
		return setVirtualDeveloperId(id);
	}
	
	/**
	 * @deprecated Please use the method {@link #setModelAccess(URI, String, Version)} instead
	 * @param virtualDeveloper
	 * @param name
	 * @param version
	 * @param options
	 * @return this parser object for fluent usage
	 */
	@Deprecated
	public UriParser setModelAccess(final URI virtualDeveloper, final String name, final Version version, final int options) {
		return setModelAccess(virtualDeveloper, name, version);
	}
	
	/**
	 * Setter for the model access identifier.
	 * The identifier will be created by the virtual developer identifier, the name and version (major and minor number)
	 * 
	 * @param virtualDeveloper the virtual developer identifier
	 * @param name the model access name
	 * @param version the model access version
	 * @return this parser object for fluent usage
	 */
	public UriParser setModelAccess(final URI virtualDeveloper, final String name, final Version version) {
		if (virtualDeveloper == null) {
			throw new NullPointerException("The virtual developer URI may not be null");
		} else if (!StringTools.isText(name)) {
			throw new IllegalArgumentException("The name of the virtual developer is not valid");
		} else if (version == null) {
			throw new NullPointerException("The version of the virtual developer may not be null");
		}
		int id = 1;
		id = PRIME * id + virtualDeveloper.hashCode();
		id = PRIME * id + name.trim().hashCode();
		id = PRIME * id + version.getMajor();
		id = PRIME * id + version.getMinor();
		return setModelAccessId(id);
	}

	/**
	 * @deprecated  Please use the method {@link #setModelConverter(URI, String, Version)} instead
	 * @param previousStep
	 * @param name
	 * @param version
	 * @param options
	 * @return this parser object for fluent usage
	 */
	@Deprecated
	public UriParser setModelConverter(final URI previousStep, final String name, final Version version, final int options) {
		return setModelConverter(previousStep, name, version);
	}
	
	/**
	 * Setter for the model converter identifier.
	 * The identifier is created by the previous step identifier (model access identifier or model converter identifier),
	 * the name of the model converter and the version of the model converter
	 * 
	 * @param previousStep previous step identifier
	 * @param name the model converter name
	 * @param version the model converter version
	 * @return this parser object for fluent usage
	 */
	public UriParser setModelConverter(final URI previousStep, final String name, final Version version) {
		if (previousStep == null) {
			throw new NullPointerException("The previous step URI may not be null");
		} else if (!StringTools.isText(name)) {
			throw new IllegalArgumentException("The name of the virtual developer is not valid");
		} else if (version == null) {
			throw new NullPointerException("The version of the virtual developer may not be null");
		}
		int id = 1;
		id = PRIME * id + previousStep.hashCode();
		id = PRIME * id + name.trim().hashCode();
		id = PRIME * id + version.getMajor();
		id = PRIME * id + version.getMinor();
		return setModelConverterId(id);
	}

	/**
	 * @deprecated Please use the method {@link #setGenerationGroup(URI, String, Version)} instead
	 * @param modelConverter
	 * @param name
	 * @param version
	 * @param options
	 * @return
	 */
	@Deprecated
	public UriParser setGenerationGroup(final URI modelConverter, final String name, final Version version, final int options) {
		return setGenerationGroup(modelConverter, name, version);
	}
	
	/**
	 * 
	 * @param modelConverter
	 * @param name
	 * @param version
	 * @return this parser object for fluent usage
	 */
	public UriParser setGenerationGroup(final URI modelConverter, final String name, final Version version/*, final int options*/) {
		if (modelConverter == null) {
			throw new NullPointerException("The model converter URI may not be null");
		} else if (!StringTools.isText(name)) {
			throw new IllegalArgumentException("The name of the virtual developer is not valid");
		} else if (version == null) {
			throw new NullPointerException("The version of the virtual developer may not be null");
		}
		int id = 1;
		id = PRIME * id + modelConverter.hashCode();
		id = PRIME * id + name.trim().hashCode();
		id = PRIME * id + version.getMajor();
		id = PRIME * id + version.getMinor();
//		id = PRIME * id + options;
		return setGenerationGroupId(id);
	}

	/**
	 * Getter for the generation group identifier
	 * 
	 * @return the generation group identifier
	 */
	public int getGenerationGroupId() {
		return generationGroupId;
	}

	/**
	 * Setter for the generation group identifier
	 * 
	 * @param generationGroupId the generation group identifier to set
	 * @return this parser object for fluent usage
	 */
	public UriParser setGenerationGroupId(int generationGroupId) {
		this.generationGroupId = generationGroupId;
		return this;
	}

	/**
	 * Getter for the target path of the generated artifact
	 * 
	 * @return the target path
	 */
	public URI getTargetPath() {
		return targetPath;
	}

	/**
	 * Setter for the target path of the generated artifact
	 * 
	 * @param targetPath the target path to set
	 * @return this parser object for fluent usage
	 */
	public UriParser setTargetPath(URI targetPath) {
		this.targetPath = targetPath;
		return this;
	}

	
	/**
	 * @return the osgiBundleId
	 */
	public final long getOsgiBundleId() {
		return osgiBundleId;
	}

	/**
	 * @param osgiBundleId the osgiBundleId to set
	 */
	public final void setOsgiBundleId(long osgiBundleId) {
		this.osgiBundleId = osgiBundleId;
	}

	/**
	 * Getter to access the complete virtual developer path {@link URI} (generator path) starting with project name
	 *  
	 * @return the complete virtual developer path as {@link URI}
	 */
	public URI getVirtualDeveloperURI() {
		try {
			return new URI(new StringBuilder()
				.append(TARGETPROJECT).append(SEPARATOR).append(this.projectId).append(SEPARATOR)
				.append(VIRTUALDEVELOPER).append(SEPARATOR).append(this.virtualDeveloperId).append(SEPARATOR)
				.toString());
		} catch (URISyntaxException e) {
			throw new RuntimeException("Error while create URI" ,e);
		}
	}

	/**
	 * Getter to access the complete model access path {@link URI} starting with project name
	 *  
	 * @return the complete model access path as {@link URI}
	 */
	public URI getModelAccessURI() {
		try {
			return new URI(new StringBuilder()
				.append(TARGETPROJECT).append(SEPARATOR).append(this.projectId).append(SEPARATOR)
				.append(VIRTUALDEVELOPER).append(SEPARATOR).append(this.virtualDeveloperId).append(SEPARATOR)
				.append(MODELACCESS).append(SEPARATOR).append(this.modelAccessId).append(SEPARATOR)
				.toString());
		} catch (URISyntaxException e) {
			throw new RuntimeException("Error while create URI" ,e);
		}
	}
	
	/**
	 * Getter to access the complete model converter path {@link URI} starting with project name
	 *  
	 * @return the complete mocel converter path as {@link URI}
	 */
	public URI getModelConverterURI() {
		try {
			return new URI(new StringBuilder()
				.append(TARGETPROJECT).append(SEPARATOR).append(this.projectId).append(SEPARATOR)
				.append(VIRTUALDEVELOPER).append(SEPARATOR).append(this.virtualDeveloperId).append(SEPARATOR)
				.append(MODELCONVERTER).append(SEPARATOR).append(this.modelConverterId).append(SEPARATOR)
				.toString());
		} catch (URISyntaxException e) {
			throw new RuntimeException("Error while create URI" ,e);
		}
	}

	/**
	 * Getter to access the complete project path {@link URI} starting with project name
	 *  
	 * @return the complete project path as {@link URI}
	 */
	public URI getTargetProjectURI() {
		try {
			return new URI(new StringBuilder()
			.append(TARGETPROJECT).append(SEPARATOR).append(this.projectId).append(SEPARATOR).toString());
		} catch (URISyntaxException e) {
			throw new RuntimeException("Error while create URI" ,e);
		}
	}
	
	/**
	 * Getter to access the complete generation group path {@link URI} starting with project name
	 *  
	 * @return the complete generation group path as {@link URI}
	 */
	public URI getGenerationGroupURI() {
		try {
			return new URI(new StringBuilder()
			.append(TARGETPROJECT).append(SEPARATOR).append(this.projectId).append(SEPARATOR)
			.append(VIRTUALDEVELOPER).append(SEPARATOR).append(this.virtualDeveloperId).append(SEPARATOR)
			.append(GENERATIONGROUP).append(SEPARATOR).append(this.generationGroupId).append(SEPARATOR)
			.toString());
		} catch (URISyntaxException e) {
			throw new RuntimeException("Error while create URI" ,e);
		}
	}
	
	/**
	 * Getter to access the complete target path {@link URI} starting with project name
	 *  
	 * @return the complete target path as {@link URI}
	 */
	public URI getTargetPathURI() {
		try {
			return new URI(
				new StringBuilder()
					.append(TARGETPROJECT).append(SEPARATOR).append(this.projectId).append(SEPARATOR)
					.append(VIRTUALDEVELOPER).append(SEPARATOR).append(this.virtualDeveloperId).append(SEPARATOR)
					.append(GENERATIONGROUP).append(SEPARATOR).append(this.generationGroupId).append(SEPARATOR)
					.append(TARGET).append(SEPARATOR).toString())
				.resolve(this.targetPath);
		} catch (URISyntaxException e) {
			throw new RuntimeException("Error while create URI" ,e);
		}
	}
	
	/**
	 * Converter from {@link BitSet} to {@link String}
	 * 
	 * @param bitSet the bitstream to convert
	 * @return the string representation
	 */
	public static final String convert(final BitSet bitSet) {
		if (bitSet == null || bitSet.length() == 0) {
			return "0";
		}
		final StringBuilder sb = new StringBuilder(bitSet.length());
		for (int i = 0; i < bitSet.length(); i++) {
			sb.append(bitSet.get(i) ? "1" : "0");
		}
		return sb.toString();
	}

	/**
	 * Converter from {@link String} to {@link BitSet}
	 * 
	 * @param s the string to convert
	 * @return the bitstream 
	 */
	public static final BitSet convert(final String s) {
		final BitSet bitSet = new BitSet(s != null ? s.length() : 0);
		if (StringTools.isText(s)) {
			for (int i = 0; i < s.length(); i++) {
				bitSet.set(i, '1' == s.charAt(i));
			}
		}
		return bitSet;
	}
}

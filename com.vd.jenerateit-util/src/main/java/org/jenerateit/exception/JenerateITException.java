package org.jenerateit.exception;
/*
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */


/**
 * @author hrr
 *
 * Base class for all JenerateIT Exception. 
 */
public class JenerateITException extends RuntimeException {

	/**
	 * serial version UID.
	 */
	private static final long serialVersionUID = 2246961636310759397L;

	/**
	 * Constructor.
	 */
	public JenerateITException() {
		super();
	}

	/**
	 * Constructor.
	 * 
	 * @param message the error message
	 */
	public JenerateITException(String message) {
		super(message);
	}

	/**
	 * Constructor.
	 * 
	 * @param cause the exception this exception is based on 
	 */
	public JenerateITException(Throwable cause) {
		super(cause);
	}

	/**
	 * Constructor.
	 * 
	 * @param message the error message
	 * @param cause the exception this exception is based on 
	 */
	public JenerateITException(String message, Throwable cause) {
		super(message, cause);
	}

	public String getConcatenatedJenerateITExceptionMessages() {
		StringBuilder sb = new StringBuilder(this.getMessage());
		
		Throwable cause = this;
		while (cause != cause.getCause() && ((cause = cause.getCause()) instanceof JenerateITException)) {
			sb.append(" -> ").append(cause.getMessage());
		}
		
		return sb.toString();
	}
}

/**
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit.modelaccess;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Hashtable;

import org.junit.Assert;
import org.junit.Test;


public class ModelAccessOptionsTest {

	@Test
	public void testPutNull() {
		ModelAccessOptions mao = new ModelAccessOptions();
		mao.put("abc", null);
		
		Assert.assertNull(mao.get("abc"));
	}

	@Test
	public void testPutNullKey() {
		ModelAccessOptions mao = new ModelAccessOptions();
		mao.put(null, "abc");
		
		Assert.assertNotNull(mao.get(null));
		Assert.assertEquals("abc", mao.get(null));
	}
	
	@Test
	public void testHashCode() {
		ModelAccessOptions mao1 = new ModelAccessOptions();
		ModelAccessOptions mao2 = new ModelAccessOptions();
		ModelAccessOptions mao3 = new ModelAccessOptions();

		Assert.assertEquals(mao1.hashCode(), mao2.hashCode());
		Assert.assertEquals(mao1.hashCode(), mao3.hashCode());
		Assert.assertEquals(mao2.hashCode(), mao3.hashCode());

		mao1.put("clazz", Hashtable.class);
		mao1.put("number", new BigDecimal(23.4));
		mao1.put("string", "This is a short text");

		mao2.put("clazz", Hashtable.class);
		mao2.put("number", new BigDecimal(23.4));
		mao2.put("string", "This is a short text");

		mao3.put("clazz", Hashtable.class);
		mao3.put("number", new BigDecimal(23.3));
		mao3.put("string", "This is a short text");
		
		Assert.assertEquals(mao1.hashCode(), mao2.hashCode());
		Assert.assertFalse(mao1.hashCode() == mao3.hashCode());
		Assert.assertFalse(mao2.hashCode() == mao3.hashCode());
	}

	@Test
	public void testHashCodeSimple() {
		HashMap<String, Serializable> mao1 = new HashMap<String, Serializable>();
		mao1.put("11", "11");
		mao1.put("22", "22");
		HashMap<String, Serializable> mao2 = new HashMap<String, Serializable>();
		mao2.put("11", "11");
		mao2.put("33", "33");
		// Ups the hash code is equals :-(
		Assert.assertEquals(mao1.hashCode(), mao2.hashCode());
		Assert.assertFalse(mao1.equals(mao2));
	}
	
	@Test
	public void testEquals() {
		ModelAccessOptions mao1 = new ModelAccessOptions();		
		ModelAccessOptions mao2 = new ModelAccessOptions();
		ModelAccessOptions mao3 = new ModelAccessOptions();
		
		Assert.assertTrue(mao1.equals(mao2));
		Assert.assertTrue(mao2.equals(mao1));

		Assert.assertTrue(mao1.equals(mao3));
		Assert.assertTrue(mao3.equals(mao1));
		
		Assert.assertTrue(mao2.equals(mao3));
		Assert.assertTrue(mao3.equals(mao2));
		
		mao1.put("clazz", Hashtable.class);
		mao1.put("number", new BigDecimal(23.4));
		mao1.put("string", "This is a short text");

		mao2.put("clazz", Hashtable.class);
		mao2.put("number", new BigDecimal(23.4));
		mao2.put("string", "This is a short text");

		mao3.put("clazz", Hashtable.class);
		mao3.put("number", new BigDecimal(23.3));
		mao3.put("string", "This is a short text");
		
		Assert.assertTrue(mao1.equals(mao2));
		Assert.assertTrue(mao2.equals(mao1));
		
		Assert.assertFalse(mao1.equals(mao3));
		Assert.assertFalse(mao3.equals(mao1));

		Assert.assertFalse(mao2.equals(mao3));
		Assert.assertFalse(mao3.equals(mao2));
	}
	
	@Test
	public void testHashCodeWithValuesSameOrder() {
		ModelAccessOptions mao1 = new ModelAccessOptions();		
		ModelAccessOptions mao2 = new ModelAccessOptions();
		
		mao1.put("abc", new Long(10));
		mao1.put("def", new Integer(10));
		mao1.put("ghi", Assert.class);

		mao2.put("abc", new Long(10));
		mao2.put("def", new Integer(10));
		mao2.put("ghi", Assert.class);
		
		Assert.assertEquals(mao1.hashCode(), mao2.hashCode());
	}

	@Test
	public void testHashCodeWithValuesDifferentOrder() {
		ModelAccessOptions mao1 = new ModelAccessOptions();		
		ModelAccessOptions mao2 = new ModelAccessOptions();
		
		mao1.put("abc", new Long(10));
		mao1.put("def", new Integer(10));
		mao1.put("ghi", Assert.class);

		mao2.put("ghi", Assert.class);
		mao2.put("def", new Integer(10));
		mao2.put("abc", new Long(10));
		
		Assert.assertEquals(mao1.hashCode(), mao2.hashCode());
	}
}

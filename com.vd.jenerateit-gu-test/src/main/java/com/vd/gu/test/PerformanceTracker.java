package com.vd.gu.test;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class PerformanceTracker {
	
	private final ConcurrentHashMap<String, Interval> intervals = new ConcurrentHashMap<>();

	private final List<Interval> completedIntervals = new ArrayList<>();

	private final String name;

	public PerformanceTracker(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	/**
	 * @param key
	 * @return the previously existing interval for the given key (if there was one)
	 */
	public Interval start(String key) {
		Interval interval = null;

		interval = new Interval(key);
		Interval oldInterval = intervals.put(key, interval);

		return oldInterval;
	}

	/**
	 * @param key
	 * @return the interval for the given key
	 */
	public Interval end(String key) {
		Interval result = intervals.get(key);
		if (result != null) {
			result.end();
			completedIntervals.add(completedIntervals.size(), result);
		}

		return result;
	}

	/**
	 * @param key
	 * @return the interval for the given key
	 */
	public Interval get(String key) {
		return intervals.get(key);
	}

	/**
	 * @param key
	 * @return
	 */
	public Interval delete(String key) {
		Interval result = intervals.get(key);
		if (result != null) {
			intervals.remove(key);
		}
		return result;
	}

	/**
	 * @return
	 */
	public List<Interval> getIntervals() {
		List<Interval> result = new ArrayList<>();

		result.addAll(completedIntervals);
		Collections.sort(result);

		return result;
	}

	/**
	 * 
	 */
	public void clear() {
		intervals.clear();
		completedIntervals.clear();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("*** ").append(name).append(" ***");

		for (String key : intervals.keySet()) {
			sb.append(System.lineSeparator());
			Interval interval = intervals.get(key);
			sb.append(interval.toString());
		}

		return sb.toString();
	}

	/**
	 * @author marcu
	 *
	 */
	public static class Interval implements Comparable<Interval> {

		public static final String TIMESTAMP_FORMAT = "dd.MM.yyyy hh:mm:ss";
		public static final SimpleDateFormat timestampFormat = new SimpleDateFormat(TIMESTAMP_FORMAT);

		private final Date startTimestamp;
		private final String key;
		private final long startTime;
		private long endTime;
		private long diff;

		/**
		 * @param key
		 */
		private Interval(String key) {
			super();
			if (key == null || key.length() == 0) {
				throw new NullPointerException("key must not be null and must have length > 0");
			}
			this.key = key;
			this.startTime = System.nanoTime();
			this.startTimestamp = new Date();
		}

		public long getStartTime() {
			return startTime;
		}

		public long getEndTime() {
			return endTime;
		}

		public void end() {
			if (this.diff == 0) {
				this.endTime = System.nanoTime();
				this.diff = (long) ((endTime - startTime) * 0.000001);
			}
		}

		public String getKey() {
			return key;
		}

		public long getDiff() {
			return diff;
		}

		@Override
		public String toString() {
			return "Interval [key=" + key + ", diff=" + diff
					+ "]";
		}

		@Override
		public int compareTo(Interval other) {
			long diff = other.startTime - startTime;
			if (diff < 0)
				return -1;
			if (diff > 0)
				return 1;
			return 0;
		}

		public Date getStartTimestamp() {
			return startTimestamp;
		}

		public String getStartTimestampAsString() {
			return timestampFormat.format(startTimestamp);
		}

		public String getKeyPart(String splitRegex, int idx) {
			String[] keyParts = key.split(splitRegex);

			if (keyParts.length > idx) {
				return keyParts[idx].trim();
			}

			return "";
		}
	}
}

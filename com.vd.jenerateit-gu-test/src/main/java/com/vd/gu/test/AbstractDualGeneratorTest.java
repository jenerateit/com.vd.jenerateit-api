package com.vd.gu.test;

import static org.junit.Assert.fail;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vd.gu.TechnicalExceptionCreateGeneratorDefaultException;
import com.vd.gu.client.ResultForCreateGenerator;
import com.vd.gu.definition.MessageTypeEnum;
import com.vd.gu.definition.basic.FileBean;
import com.vd.gu.definition.basic.LoadResultBean;
import com.vd.gu.definition.basic.OptionBean;
import com.vd.gu.definition.basic.TargetContentBean;
import com.vd.gu.test.AbstractMultiModelGeneratorTest.InputFiles;
import com.vd.gu.test.GuClientTester.ComparisonResult;
import com.vd.gu.test.PerformanceTracker.Interval;

/**
 * <p>This class can be used as a parent class in order to use two different generators
 * generating for the same model or for two different models (different modeling tool)
 * that have semantically the same content.
 * 
 * <p>This class lets you write tests that use JUnit's {@link org.junit.runners.Parameterized} annotation.
 * You have to use the class {@link InputFiles} to provide parameters for a parameterized test. When
 * the very last file name that is provided this way ends with ".zip", then this file is assumed to
 * contain reference files for generated files. That ZIP file content is going to be compared to the
 * files that are going to be generated as a result of the test execution.
 *
 */
abstract public class AbstractDualGeneratorTest extends AbstractGensetTest {
	
	private static final @NotNull Logger LOG = LoggerFactory.getLogger(AbstractDualGeneratorTest.class);
	
    private InputFiles inputFiles1;
    private InputFiles inputFiles2;
    
	private ResultForCreateGenerator generator1;
	private ResultForCreateGenerator generator2;
    
    private String generatorId1;
    private String generatorId2;
    
    private String generatorVersion1;
    private String generatorVersion2;
    
    private final List<OptionBean> options1 = new ArrayList<>();
    private final List<OptionBean> options2 = new ArrayList<>();
    
    /**
	 * @param dualInputFiles nominates the input files for generator one and two
	 */
	public AbstractDualGeneratorTest(DualInputFiles dualInputFiles) {
		super();
		this.inputFiles1 = dualInputFiles.getInputFiles1();
		this.inputFiles2 = dualInputFiles.getInputFiles2();
    }
    
    /**
     * @return the model input files for the generator one
     */
    protected InputFiles getInputFiles1() {
		return inputFiles1;
	}
    
    /**
     * @return the model input files for the generator two
     */
    protected InputFiles getInputFiles2() {
		return inputFiles2;
	}
    
    /**
     * @return the version of the generator one
     */
    protected String getGeneratorVersion1() {
		if (generatorVersion1 != null) {
		    return generatorVersion1;
		} else if (getGenerator1UnderTest() != null) {
			return getGenerator1UnderTest().getVersion();
		}
		return null;
	}

	protected void setGeneratorVersion1(String generatorVersion1) {
		this.generatorVersion1 = generatorVersion1;
	}

	/**
	 * @return the version of the generator two
	 */
	protected String getGeneratorVersion2() {
		if (generatorVersion2 != null) {
		    return generatorVersion2;
		} else if (getGenerator2UnderTest() != null) {
			return getGenerator2UnderTest().getVersion();
		}
		return null;
	}

	protected void setGeneratorVersion2(String generatorVersion2) {
		this.generatorVersion2 = generatorVersion2;
	}

	/**
	 * @return
	 */
	protected ResultForCreateGenerator getGenerator1() {
		return generator1;
	}

	/**
	 * @return
	 */
	protected ResultForCreateGenerator getGenerator2() {
		return generator2;
	}

	/**
	 * @return the generator options for generator one
	 */
	protected List<OptionBean> getOptions1() {
		return options1;
	}

	/**
	 * @return the generator options for generator two
	 */
	protected List<OptionBean> getOptions2() {
		return options2;
	}

	protected void setGeneratorId1(String generatorId1) {
		this.generatorId1 = generatorId1;
	}

	protected void setGeneratorId2(String generatorId2) {
		this.generatorId2 = generatorId2;
	}

	/**
	 * @return the names of the model files for generator one
	 */
	protected String[] getModelFiles1() {
    	if (inputFiles1 != null) {
		    return inputFiles1.getModelFiles();
    	}
    	return null;
	}
    
    /**
     * @return the names of the model files for generator two
     */
    protected String[] getModelFiles2() {
    	if (inputFiles2 != null) {
		    return inputFiles2.getModelFiles();
    	}
    	return null;
	}
    
    /**
     * @return the generator id of generator one
     */
    protected String getGeneratorId1() {
		if (generatorId1 != null) {
		    return generatorId1;
		} else if (getGenerator1UnderTest() != null) {
			return getGenerator1UnderTest().getId();
		}
		return null;
	}
    
    /**
     * @return the generator id of generator two
     */
    protected String getGeneratorId2() {
		if (generatorId2 != null) {
		    return generatorId2;
		} else if (getGenerator2UnderTest() != null) {
			return getGenerator2UnderTest().getId();
		}
		return null;
	}
    
    /**
     * @return the generator descriptor of generator one
     */
    protected GeneratorUnderTestI getGenerator1UnderTest() {
		return null;
	}
    
    /**
     * @return the generator descriptor of generator two
     */
    protected GeneratorUnderTestI getGenerator2UnderTest() {
		return null;
	}
    
    @Before
	public void setUpGenerators() throws MalformedURLException {
		try {
		    this.generator1 = getClient().createGenerator(getGeneratorId1(), getGeneratorVersion1(), getOptions1());
		} catch (TechnicalExceptionCreateGeneratorDefaultException ex) {
			Optional<String> additionalErrorInformation = getAdditionalErrorInformation(ex);
			Assert.fail("failed to create generator 1 with ID '" + getGeneratorId1() + "' and version '" + getGeneratorVersion1() + "': " + ex.getMessage() + ", additional error information: " +
					(additionalErrorInformation.isPresent() ? additionalErrorInformation.get() : "NONE"));
		}
		
		try {
		    this.generator2 = getClient().createGenerator(getGeneratorId2(), getGeneratorVersion2(), getOptions2());
		} catch (TechnicalExceptionCreateGeneratorDefaultException ex) {
			Optional<String> additionalErrorInformation = getAdditionalErrorInformation(ex);
			Assert.fail("failed to create generator 2 with ID '" + getGeneratorId2() + "' and version '" + getGeneratorVersion2() + "': " + ex.getMessage() + ", additional error information: " +
					(additionalErrorInformation.isPresent() ? additionalErrorInformation.get() : "NONE"));
		}
	}
	
	@After
	public void tearDownGenerators() throws MalformedURLException {
		tearDownGenerator1();
		tearDownGenerator2();
	}
	
	/**
	 * With this method you can programmatically clean up generator one.
	 */
	protected void tearDownGenerator1() {
		if (this.getClient() != null) {
			if (this.generator1 != null) {
				this.getClient().deleteGenerator(generator1.getId());
				this.generator1 = null;
			}
		}
	}
	
	/**
	 * With this method you can programmatically clean up generator two.
	 */
	protected void tearDownGenerator2() {
		if (this.getClient() != null) {
			if (this.generator2 != null) {
				this.getClient().deleteGenerator(generator2.getId());
				this.generator2 = null;
			}
		}
	}
	
	/**
	 * <p>This is the main method to execute both generators, one after the other, and then compare their result.
	 * The main purpose of this method is to let a human being compare the generation results of generator one and two.
	 * 
	 * <p>Note that this method only returns normally, when both generations are completed successfully.
	 * In all other cases, the test will fail.
	 * 
	 * <p>You have to set the environment variable WRITE_TARGET_FILES to true to let the test run write the
	 * comparison results to the file system.
	 * 
	 * @param existingContent
	 * @return the comparison result after having successfully run both generators
	 */
	protected final ComparisonResult executeGeneration(Map<String,Map<String,FileBean>> existingContent, String baseDir) {
		// ------------ generator 1 --------------
		String performanceTrackingId1 = "load target list for " + getGenerator1UnderTest().getIdAndVersion();
		List<FileBean> inputFiles1 = GuClientTester.createInputFiles(this.getClass(), null, getModelFiles1());
		getPerformanceTracker().start(performanceTrackingId1);
		logGuConnectionDetails("executeGeneration(), about to call loadGenerator() for generator 1 " + getGenerator1().getId());
		LoadResultBean loadResult1 = getClient().loadGenerator(getGenerator1().getId(), inputFiles1);
		Interval interval1 = getPerformanceTracker().end(performanceTrackingId1);
		LOG.info("duration of loading the target list (" + loadResult1.getResults().stream().flatMap(resultBean -> resultBean.getReferences().stream()).count() + "): " + interval1.toString());
		
		if (GuClientTester.isLoadResultContainingErrors(loadResult1)) {
			fail("execution of generator 1 failed during loading of result list for generator '" + getGeneratorId1() + ":" + getGeneratorVersion1() + "':" +
		            GuClientTester.getLoadResultMessageText(loadResult1, MessageTypeEnum.ERROR) +
					System.lineSeparator() + "used model files: " + getModelFiles1());
		}
		
		GuClientTester.printMessages(loadResult1);
		
		performanceTrackingId1 = "generate targets for " + getGenerator1UnderTest().getIdAndVersion();
		getPerformanceTracker().start(performanceTrackingId1);
		Map<String,Map<String, TargetContentBean>> generatedTargetFiles1 =
				GuClientTester.getGeneratedTargetFiles(this.getClass(), getClient(), getGenerator1(), loadResult1, null);
		Interval interval1Write = getPerformanceTracker().end(performanceTrackingId1);
		LOG.info("duration of writing the targets (" + generatedTargetFiles1.values().stream().flatMap(map -> map.values().stream()).count() + "): " + interval1Write.toString());
		if (GuClientTester.getCountOfTargetErrors(generatedTargetFiles1) > 0) {
			GuClientTester.printMessagesForMappedTargets(generatedTargetFiles1);
			fail("execution of generator 1 resulted in " + GuClientTester.getCountOfTargetErrors(generatedTargetFiles1) + " targets generated with errors");
		}
		
		tearDownGenerator1();
		
		// ------------ generator 2 --------------
		String performanceTrackingId2 = "load target list for " + getGenerator2UnderTest().getIdAndVersion();
		List<FileBean> inputFiles2 = GuClientTester.createInputFiles(this.getClass(), null, getModelFiles2());
		getPerformanceTracker().start(performanceTrackingId2);
		logGuConnectionDetails("executeGeneration(), about to call loadGenerator() for generator 2 " + getGenerator2().getId());
		LoadResultBean loadResult2 = getClient().loadGenerator(getGenerator2().getId(), inputFiles2);
		Interval interval2 = getPerformanceTracker().end(performanceTrackingId2);
		LOG.info("duration of loading the target list (" + loadResult2.getResults().stream().flatMap(resultBean -> resultBean.getReferences().stream()).count() + "): " + interval2.toString());
		
		if (GuClientTester.isLoadResultContainingErrors(loadResult2)) {
			fail("execution of generator 2 failed during loading of result list for generator '" + getGeneratorId2() + ":" + getGeneratorVersion2() + "':" +
		            GuClientTester.getLoadResultMessageText(loadResult2, MessageTypeEnum.ERROR) +
					System.lineSeparator() + "used model files: " + getModelFiles2());
		}
		
		GuClientTester.printMessages(loadResult2);
		
		performanceTrackingId2 = "generate targets for " + getGenerator2UnderTest().getIdAndVersion();
		getPerformanceTracker().start(performanceTrackingId2);
		Map<String,Map<String, TargetContentBean>> generatedTargetFiles2 =
				GuClientTester.getGeneratedTargetFiles(this.getClass(), getClient(), getGenerator2(), loadResult2, null);
		Interval interval2Write = getPerformanceTracker().end(performanceTrackingId2);
		LOG.info("duration of writing the targets (" + generatedTargetFiles2.values().stream().flatMap(map -> map.values().stream()).count() + "): " + interval2Write.toString());
		if (GuClientTester.getCountOfTargetErrors(generatedTargetFiles2) > 0) {
			GuClientTester.printMessagesForMappedTargets(generatedTargetFiles2);
			fail("execution of generator 2 resulted in " + GuClientTester.getCountOfTargetErrors(generatedTargetFiles2) + " targets generated with errors");
		}
		
		tearDownGenerator2();
		
		// finally, compare the results and write those files that have different content (unless writeTargetFiles is false or null)
		ComparisonResult comparisonResult = GuClientTester.compareGenerationResults(
				generatedTargetFiles1.values().stream().flatMap(targetCollection -> targetCollection.values().stream()).collect(Collectors.toSet()),
				generatedTargetFiles2.values().stream().flatMap(targetCollection -> targetCollection.values().stream()).collect(Collectors.toSet()));
		
		if (getWriteTargetFiles() != null && getWriteTargetFiles().booleanValue()) {
			String effectiveBaseDir = baseDir == null ?
					GuClientTester.createTemporaryBaseDir()
					:
					baseDir;
			LOG.info("writing found differences to the directory '" + effectiveBaseDir + "'");
			comparisonResult.getDifferentResults().stream()
			    .forEach(targets -> {
				GuClientTester.writeTargetFile(effectiveBaseDir, targets.getResult1(), null, ".1-unstripped", false);
				GuClientTester.writeTargetFile(effectiveBaseDir, targets.getResult1(), null, ".1", true);
				GuClientTester.writeTargetFile(effectiveBaseDir, targets.getResult2(), null, ".2-unstripped", false);
				GuClientTester.writeTargetFile(effectiveBaseDir, targets.getResult2(), null, ".2", true);
			});
		}
		
		return comparisonResult;
	}
	
	/**
	 * <p>This class encapsulates two input models.
	 *
	 */
	public static class DualInputFiles {
		
		private final InputFiles inputFiles1;
		private final InputFiles inputFiles2;
		
		public DualInputFiles(InputFiles inputFiles1, InputFiles inputFiles2) {
		    super();
		    this.inputFiles1 = inputFiles1;
		    this.inputFiles2 = inputFiles2;
	    }
    
		protected InputFiles getInputFiles1() {
			return inputFiles1;
		}

		protected InputFiles getInputFiles2() {
			return inputFiles2;
		}
	}
}

	

package com.vd.gu.test;

import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringReader;
import java.net.FileNameMap;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.validation.constraints.NotNull;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.difflib.DiffUtils;
import com.github.difflib.patch.Patch;
import com.vd.gu.client.GUClientI;
import com.vd.gu.client.ResultForCreateGenerator;
import com.vd.gu.definition.MessageTypeEnum;
import com.vd.gu.definition.basic.FileBean;
import com.vd.gu.definition.basic.LoadResultBean;
import com.vd.gu.definition.basic.MessageBean;
import com.vd.gu.definition.basic.ResultBean;
import com.vd.gu.definition.basic.TargetContentBean;
import com.vd.gu.test.PerformanceTracker.Interval;

/**
 * <p>This class encapsulates functionality that is repeatedly needed
 * to write integration tests to test generator functionality.
 * It includes methods that trigger the transformation process.
 * The class only has static methods and fields.
 *
 */
public class GuClientTester {
	
	private static final @NotNull Logger LOG = LoggerFactory.getLogger(GuClientTester.class);
	
	private static final PerformanceTracker performanceTracker = new PerformanceTracker(GuClientTester.class.getSimpleName());
	
	public static final String REGEX_MULTILINE_JAVA_DOC = "\\/[*][*].*?[*]\\/";
	public static final Pattern PATTERN_REGEX_MULTILINE_JAVA_DOC = Pattern.compile(REGEX_MULTILINE_JAVA_DOC,
			Pattern.MULTILINE |
			Pattern.UNIX_LINES |
			Pattern.DOTALL |
			Pattern.CASE_INSENSITIVE);

	public static final String REGEX_MULTILINE_XML_DOC = "<!--.*?-->";
	public static final Pattern PATTERN_REGEX_MULTILINE_XML_DOC = Pattern.compile(REGEX_MULTILINE_XML_DOC,
			Pattern.MULTILINE |
			Pattern.UNIX_LINES |
			Pattern.DOTALL |
			Pattern.CASE_INSENSITIVE);
	
	/**
	 * <p>Read a file from the classpath and convert its
	 * content to a string. If the file cannot be found,
	 * {@link org.junit.Assert#fail} is being called, which makes
	 * the test fail. 
	 * 
	 * @param clazz
	 * @param fileName
	 * @return the file content that got read from the classpath
	 */
	public static String readFileFromClasspath(Class<?> clazz, String fileName) {
		byte[] fileContent = readFileAsByteArray(clazz, fileName);
		if (fileContent != null) {
		    return new String(fileContent);
		}
		return null;
	}
	
	/**
	 * <p>Read a file from the classpath and return it as a byte array.
	 * If the file cannot be found, {@link org.junit.Assert#fail} is being called,
	 * which makes the test fail.
	 * 
	 * @param clazz
	 * @param fileName
	 * @return the file content that got read from the classpath
	 */
	public static byte[] readFileAsByteArray(Class<?> clazz, String fileName) {
		byte[] result = null;
		URL resourceUrl = clazz.getClassLoader().getResource(fileName);
		if (resourceUrl != null) {
			try {
				result = IOUtils.toByteArray(resourceUrl);
			} catch (IOException ex) {
				ex.printStackTrace();
				fail("file '" + resourceUrl.getFile() + "' could not be read: " + ex.getMessage());
			}
		} else {
			fail("could not find a file with the name '" + fileName + "' by means of the classloader of class " + clazz.getName());
		}
		
		return result;
	}
	
	/**
	 * <p>Prepare an object that can be used as the input for
	 * code generation. This method reads the content from
	 * the classpath using the classloader that is associated with the passed class 'clazz'.
	 * 
	 * @param clazz
	 * @param baseDir optional, the directory on the classpath from where the content is read
	 * @param intermediatePath
	 * @param filename relative or absolute file name
	 * @return
	 * @throws IOException
	 */
	public static FileBean createFileBeanFromFile(Class<?> clazz, String baseDir, String intermediatePath, String filename) {
		FileBean result = null;
		
		String effectiveFilename = "";
		if (baseDir != null && baseDir.length() > 0) {
			effectiveFilename += baseDir;
		}
		
		if (intermediatePath != null && intermediatePath.length() > 0) {
			if (effectiveFilename != null && effectiveFilename.length() > 0) effectiveFilename += "/";
			effectiveFilename += intermediatePath;
		}
		
		if (filename != null && filename.length() > 0) {
			if (effectiveFilename != null && effectiveFilename.length() > 0) effectiveFilename += "/";
			effectiveFilename += filename;
		}
		
		File file = new File(effectiveFilename);
		
		byte[] fileContent = readFileAsByteArray(clazz, effectiveFilename);
			
		if (fileContent != null) {
			result = new FileBean();
			result.setPath(filename);
			result.setContent(fileContent);
    		result.setContentType(guessContentType(file.toPath()));
			result.setCharset("UTF-8");
		} else {
			fail("could not read file content for file '" + file.getPath() + "'");
		}
		
		return result;
	}
	
	/**
	 * <p>This method guesses a file's content type by using {@link java.nio.file.Files#probeContentType()}.
	 * 
	 * @param path
	 * @return
	 */
	private static String guessContentType(Path path) {
		String contentType = "application/text";
		try {
			Files.probeContentType(path);
		} catch (IOException ex) {/*intentionally ignore this since we still can successfully generate without knowing the content type*/}
		return contentType;
	}
	
	/**
	 * @param baseDir optional, the directory on the file system from where the content is read
	 * @param intermediatePath an optional intermediate path to be used to construct the full path
	 * @param filename relative path, including the file's name
	 * @return
	 */
	public static FileBean createFileBeanFromFile(String baseDir, String intermediatePath, String filename) {
        FileBean result = null;
		
		String effectiveFilename = "";
		if (baseDir != null && baseDir.length() > 0) {
			effectiveFilename += baseDir;
		}
		
		if (intermediatePath != null && intermediatePath.length() > 0) {
			if (effectiveFilename != null && effectiveFilename.length() > 0) effectiveFilename += "/";
			effectiveFilename += intermediatePath;
		}
		
		if (filename != null && filename.length() > 0) {
			if (effectiveFilename != null && effectiveFilename.length() > 0) effectiveFilename += "/";
			effectiveFilename += filename;
		}
		
		Path path = Paths.get(effectiveFilename);
		byte[] fileContent;
		try {
			fileContent = Files.readAllBytes(path);
			if (fileContent != null) {
				result = new FileBean();
				result.setPath(filename);
				result.setContent(fileContent);
				result.setContentType(guessContentType(path));
				result.setCharset("UTF-8");
			} else {
				fail("could not read file content for file '" + path + "'");
			}
		} catch (IOException ex) {
			ex.printStackTrace();
			fail("could not successfully read all bytes for the file '" + path + "' due to an IO exception: " + ex.getMessage());
		}
		
		return result;
	}
	
	/**
	 * <p>Prepare an object that can be used as the input for
	 * existing content for the file transformation.
	 * 
	 * @param path
	 * @param content
	 * @return
	 */
	public static FileBean createFileBean(String path, String content) {
		FileBean result = new FileBean();
		
		result = new FileBean();
		result.setPath(path);
		result.setContent(content.getBytes());
		result.setCharset("UTF-8");
		
		return result;
	}
	
	/**
	 * <p>Prepare a list of objects that can be used as the input for
	 * code generation.
	 * 
	 * @param clazz
	 * @param baseDir optional, the directory on the classpath from where the content is read
	 * @param fileNames
	 * @return
	 */
	public static List<FileBean> createInputFiles(Class<?> clazz, String baseDir, String... fileNames) {
		List<FileBean> result = new ArrayList<>();
		
		if (fileNames != null) {
			Stream.of(fileNames).forEach(filename -> result.add(createFileBeanFromFile(clazz, baseDir, null, filename)));
		}
		
		return result;
	}
	
	/**
	 * @param baseDir the directory on the file system from where the content is read
	 * @param fileNames
	 * @return
	 */
	public static List<FileBean> createInputFiles(@NotNull String baseDir, String... fileNames) {
		List<FileBean> result = new ArrayList<>();
		
		if (fileNames != null) {
			Stream.of(fileNames).forEach(filename -> result.add(createFileBeanFromFile(baseDir, null, filename)));
		}
		
		return result;
	}
	
	/**
	 * Check whether the load result includes error messages.
	 * 
	 * @param loadResult
	 * @return
	 */
	public static boolean isLoadResultContainingErrors(LoadResultBean loadResult) {
		if (loadResult.getMessages() != null) {
		    return loadResult.getMessages().stream().anyMatch(message -> message.getType() == MessageTypeEnum.ERROR);
		}

		return false;
	}
	
	/**
	 * <p>Note that the implementation uses the following Pattern flags to compile the regex:
	 * Pattern.MULTILINE | Pattern.UNIX_LINES | Pattern.DOTALL | Pattern.CASE_INSENSITIVE
	 * 
	 * @param loadResult
	 * @param regex
	 * @param messageType
	 * @return
	 */
	public static List<MessageBean> getMessages(LoadResultBean loadResult, String regex, MessageTypeEnum... messageTypes) {
		if (loadResult.getMessages() != null) {
			final Pattern pattern = Pattern.compile(regex == null ? "" : regex, Pattern.MULTILINE | Pattern.UNIX_LINES | Pattern.DOTALL | Pattern.CASE_INSENSITIVE);
		    return loadResult.getMessages().stream()
		    		.filter(message -> messageTypes == null || Arrays.asList(messageTypes).contains(message.getType()))
		    		.filter(message -> {
		    			if (regex == null) {
		    				return true;
		    			}
		    			Matcher matcher = pattern.matcher(message.getMessage());
		    			return matcher.matches();
		    		})
		    		.collect(Collectors.toList());
		}

		return Collections.emptyList();
	}
	
	/**
	 * @param loadResult
	 * @param regex
	 * @return
	 */
	public static List<MessageBean> getErrorMessages(LoadResultBean loadResult, String regex) {
		return getMessages(loadResult, regex, MessageTypeEnum.ERROR);
	}
	
	/**
	 * Check whether the load result includes warning messages.
	 * 
	 * @param loadResult
	 * @return
	 */
	public static boolean isLoadResultContainingWarnings(LoadResultBean loadResult) {
		if (loadResult.getMessages() != null) {
		    return loadResult.getMessages().stream().anyMatch(message -> message.getType() == MessageTypeEnum.WARNING);
		}

		return false;
	}
	
	/**
	 * @param loadResult
	 * @param regex
	 * @return
	 */
	public static List<MessageBean> getWarningMessages(LoadResultBean loadResult, String regex) {
		return getMessages(loadResult, regex, MessageTypeEnum.WARNING);
	}
	
	/**
	 * Check whether the load result includes info messages.
	 * 
	 * @param loadResult
	 * @return
	 */
	public static boolean isLoadResultContainingInfos(LoadResultBean loadResult) {
		if (loadResult.getMessages() != null) {
		    return loadResult.getMessages().stream().anyMatch(message -> message.getType() == MessageTypeEnum.INFO);
		}

		return false;
	}
	
	/**
	 * @param loadResult
	 * @param regex
	 * @return
	 */
	public static List<MessageBean> getInfoMessages(LoadResultBean loadResult, String regex) {
		return getMessages(loadResult, regex, MessageTypeEnum.INFO);
	}
	
	/**
	 * <p>Checks the load result for messages of a given type and constructs a combined message text
	 * in case there are messages found.
	 * 
	 * @param loadResult
	 * @param messageType
	 * @return
	 */
	public static Optional<String> getLoadResultMessageText(LoadResultBean loadResult, MessageTypeEnum messageType) {
		Optional<String> result = Optional.empty();
		if (loadResult.getMessages() != null) {
			StringBuilder messageText = new StringBuilder();
			loadResult.getMessages().stream()
				.filter(message -> message.getType() == messageType)
				.forEach(message -> {
					if (messageText.length() > 0) {
						messageText.append(System.lineSeparator()).append(System.lineSeparator());
					}
					messageText.append(message.getMessage());
				});
			if (messageText.length() > 0) {
				result = Optional.of(messageText.toString());
			}
		}
		
		return result;
	}
	
	/**
	 * <p>This method filters the given target contents to find those ones that match
	 * the given message types _and_ the given regex. Note that it is sufficient that
	 * the target has got one message that matches the regex in order to add it to the
	 * resulting map (as long as the message type also matches).
	 * 
	 * @param targetContents
	 * @param regex
	 * @param messageTypes
	 * @return
	 */
	public static Map<String,TargetContentBean> getTargetContentByMessage(Map<String,TargetContentBean> targetContents, String regex, MessageTypeEnum ...messageTypes) {
		final Map<String,TargetContentBean> result = new TreeMap<>();
		final Pattern pattern = Pattern.compile(regex == null ? "" : regex, Pattern.MULTILINE | Pattern.UNIX_LINES | Pattern.DOTALL | Pattern.CASE_INSENSITIVE);
		
		final List<MessageTypeEnum> messageTypesList;
		if (messageTypes == null) {
			messageTypesList = Collections.emptyList();
		} else {
			messageTypesList = Arrays.asList(messageTypes);
		}
		
		targetContents.entrySet().stream()
		    .filter(entry -> {
		    	if (messageTypes == null) return true;
		    	
		    	List<MessageTypeEnum> messageTypesOfTarget =
		    			entry.getValue().getMessages().stream().map(message -> message.getType()).collect(Collectors.toList());
		    	
		    	Set<MessageTypeEnum> intersection = messageTypesList.stream()
		    			  .distinct()
		    			  .filter(messageTypesOfTarget::contains)
		    			  .collect(Collectors.toSet());
		    	return !intersection.isEmpty();
		    })
		    .filter(entry -> {
		    	if (regex == null) return true;
		    	
		    	Set<MessageBean> relevantMessages = entry.getValue().getMessages().stream()
		    	    .filter(message -> pattern.matcher(message.getMessage()).matches())
		    	    .collect(Collectors.toSet());
		    	
		    	if (messageTypes == null) {
		    		return !relevantMessages.isEmpty();
		    	}
		    	
		    	List<MessageTypeEnum> messageTypesOfRelevantMessages =
		    			relevantMessages.stream().map(message -> message.getType()).collect(Collectors.toList());
		    	
		    	Set<MessageTypeEnum> intersection = messageTypesList.stream()
		    			  .distinct()
		    			  .filter(messageTypesOfRelevantMessages::contains)
		    			  .collect(Collectors.toSet());
		    	return !intersection.isEmpty();
		    })
		    .forEach(entry -> result.put(entry.getKey(), entry.getValue()));
		    
		return result;
	}
	
	/**
	 * Check whether a generated target file contains errors.
	 * 
	 * @param targetContent
	 * @return
	 */
	public static boolean isTargetContainingErrors(TargetContentBean targetContent) {
		if (targetContent.getMessages() != null) {
			return targetContent.getMessages().stream().anyMatch(message -> message.getType() == MessageTypeEnum.ERROR);
		}
		
		return false;
	}
	
	/**
	 * Check whether a collection of generated target files include at least one target that contains errors.
	 * 
	 * @param targetContents a map of target contents, the key is the target reference
	 * @return
	 */
	public static boolean isTargetContainingErrors(Map<String,TargetContentBean> targetContents) {
		return targetContents.entrySet().stream().anyMatch(targetContentEntry -> isTargetContainingErrors(targetContentEntry.getValue()));
	}
	
	/**
	 * Check whether a generated target file contains warnings.
	 * 
	 * @param targetContent
	 * @return
	 */
	public static boolean isTargetContainingWarnings(TargetContentBean targetContent) {
		if (targetContent.getMessages() != null) {
			return targetContent.getMessages().stream().anyMatch(message -> message.getType() == MessageTypeEnum.WARNING);
		}
		
		return false;
	}
	
	/**
	 * Check whether a generated target file contains errors.
	 * 
	 * @param targetContentPerVirtualProject
	 * @return
	 */
	public static long getCountOfTargetErrors(Map<String,Map<String, TargetContentBean>> targetContentPerVirtualProject) {
		return targetContentPerVirtualProject.values().stream()
			    .flatMap(targetCollection -> targetCollection.values().stream())
			    .filter(targetContent -> GuClientTester.isTargetContainingErrors(targetContent))
			    .count();
	}
	
	/**
	 * Check whether a collection of generated target files include at least one target that contains warnings.
	 * 
	 * @param targetContents
	 * @return
	 */
	public static boolean isTargetContainingWarnings(Map<String,TargetContentBean> targetContents) {
		return targetContents.entrySet().stream().anyMatch(targetContentEntry -> isTargetContainingWarnings(targetContentEntry.getValue()));
	}
	
	/**
	 * Check whether a generated target file contains infos.
	 * 
	 * @param targetContent
	 * @return
	 */
	public static boolean isTargetContainingInfos(TargetContentBean targetContent) {
		if (targetContent.getMessages() != null) {
			return targetContent.getMessages().stream().anyMatch(message -> message.getType() == MessageTypeEnum.INFO);
		}
		
		return false;
	}
	
	/**
	 * Check whether a collection of generated target files include at least one target that contains infos.
	 * 
	 * @param targetContents
	 * @return
	 */
	public static boolean isTargetContainingInfos(Map<String,TargetContentBean> targetContents) {
		return targetContents.entrySet().stream().anyMatch(targetContentEntry -> isTargetContainingInfos(targetContentEntry.getValue()));
	}
	
	/**
	 * <p>Checks a single target file for messages of a given type and constructs a combined message text
	 * in case there are messages found.
	 * 
	 * @param targetContent
	 * @param messageType
	 * @return
	 */
	public static Optional<String> getTargetContentMessageText(TargetContentBean targetContent, MessageTypeEnum messageType) {
		Optional<String> result = Optional.empty();
		if (targetContent.getMessages() != null) {
			StringBuilder messageText = new StringBuilder();
			targetContent.getMessages().stream()
				.filter(message -> message.getType() == messageType)
				.forEach(message -> {
					if (messageText.length() > 0) {
						messageText.append(System.lineSeparator()).append(System.lineSeparator());
					}
					messageText.append(message.getMessage());
				});
			if (messageText.length() > 0) {
				result = Optional.of(messageText.toString());
			}
		}
		
		return result;
	}
	
	/**
	 * <p>Compares two strings while ignoring any whitespaces. This method
	 * is useful when you want to check whether the generation result essentially
	 * has remained the same as before, except for some minor cosmetic things like
	 * for instance an additional empty line or blanks instead of tabs for indentation.
	 * 
	 * @param firstText
	 * @param secondText
	 * @return true if the two strings are identical with the exception of whitespace characters
	 */
	public static boolean isEqualIgnoringWhitespaces(String firstText, String secondText) {
		if (firstText == null && secondText == null) {
			return true;
		} else if (firstText == null && secondText != null || firstText != null && secondText == null) {
			return false;
		}
		
		return StringUtils.deleteWhitespace(firstText).equals(StringUtils.deleteWhitespace(secondText));
	}
	
	/**
	 * <p>Compares two files while ignoring line breaks. This method
	 * is useful when you have reference files that use linux style line breaks
	 * and compare them to files that are identical but use windows style line breaks.
	 * 
	 * @param firstFile
	 * @param secondFile
	 * @return
	 */
	public static boolean isEqualIgnoringLineBreaks(File firstFile, File secondFile) {
		if (firstFile == null && secondFile == null) {
			return true;
		} else if (firstFile == null && secondFile != null || firstFile != null && secondFile == null) {
			return false;
		}
		
		boolean result = false;
	    try {
	    	Reader reader1 = new BufferedReader(new FileReader(firstFile));
		    Reader reader2 = new BufferedReader(new FileReader(secondFile));
			result = IOUtils.contentEqualsIgnoreEOL(reader1, reader2);
		} catch (IOException e) {
			e.printStackTrace();
			fail("could not read at least one of the two files '" + firstFile.getPath() + "' and '" + secondFile.getPath() + "'");
		}
	    
	    return result;
	}
	
	/**
	 * <p>Compares two strings while ignoring line breaks. This method
	 * is useful when you have reference files that use linux style line breaks
	 * and compare them to files that are identical but use windows style line breaks.
	 * 
	 * @param firstContent
	 * @param secondContent
	 * @return
	 */
	public static boolean isEqualIgnoringLineBreaks(String firstContent, String secondContent) {
		if (firstContent == null && secondContent == null) {
			return true;
		} else if (firstContent == null && secondContent != null || firstContent != null && secondContent == null) {
			return false;
		}
		
		boolean result = false;
	    try {
	    	Reader reader1 = new StringReader(firstContent);
		    Reader reader2 = new StringReader(secondContent);
			result = IOUtils.contentEqualsIgnoreEOL(reader1, reader2);
		} catch (IOException ex) {
			ex.printStackTrace();
			fail("problems while comparing two strings for equality ingoring new lines: " + ex.getMessage());
		}
	    
	    return result;
	}
	
	/**
	 * <p>Compares a generated target file content with a reference file, while ignoring any whitespace characters.
	 * This method is useful when you want to check whether the generation result essentially
	 * has remained the same as before, except for some minor cosmetic things like
	 * for instance an additional empty line or blanks instead of tabs for indentation.
	 * 
	 * @param targetContent
	 * @param clazz
	 * @param referenceFile
	 * @return
	 */
	public static boolean isEqualIgnoringWhitespace(TargetContentBean targetContent, Class<?> clazz, String referenceFile) {
		String content = new String(targetContent.getFile().getContent());
		String referenceContent = readFileFromClasspath(clazz, referenceFile);
		return isEqualIgnoringWhitespaces(content, referenceContent);
	}
	
	/**
	 * <p>Compares two generated target files' content, while ignoring any whitespace characters.
	 * This method is useful when you want to answer one of the following questions:
	 * 
	 * <ul>
	 *     <li>Do old and new version of a generator lead to the same result?</li>
	 *     <li>Do two different generators that process a semantically identical model (but different modeling tool) lead to the same result?</li>
	 * </ul>
	 * 
	 * @param targetContent1
	 * @param targetContent2
	 * @return
	 */
	public static boolean isEqualIgnoringDocumentationAndWhitespace(TargetContentBean targetContent1, TargetContentBean targetContent2) {
		String content1 = new String(targetContent1.getFile().getContent());
		String content2 = new String(targetContent2.getFile().getContent());
		return isEqualIgnoringWhitespaces(stripDocumentation(content1), 
				                          stripDocumentation(content2));
	}
	
	/**
	 * @param targetContent1
	 * @param targetContent2
	 * @param stripDocumentation
	 * @return
	 */
	public static List<String> getDiff(TargetContentBean targetContent1, TargetContentBean targetContent2, boolean stripDocumentation) {
		if (targetContent1 != null && targetContent2 != null && isTextFile(targetContent1) && isTextFile(targetContent2)) {
			String content1 = new String(targetContent1.getFile().getContent());
			String content2 = new String(targetContent2.getFile().getContent());
            return getDiff(content1, content2, stripDocumentation);
		}
		
		return Collections.emptyList();
	}
	
	
	/**
	 * @param content1
	 * @param content2
	 * @return
	 */
	public static List<String> getDiff(String content1, String content2, boolean stripDocumentation) {
		final List<String> result = new ArrayList<>();
		if (content1 != null && content2 != null) {
			if (stripDocumentation) {
				content1 = stripDocumentation(content1);
				content2 = stripDocumentation(content2);
			}
			List<String> linesOfContent1 = Arrays.asList(content1.split("\n"));
			List<String> linesOfContent2 = Arrays.asList(content2.split("\n"));
			Patch<String> patch = DiffUtils.diff(linesOfContent1, linesOfContent2);
			patch.getDeltas().forEach(delta -> result.add(delta.toString()));
		}
		return result;
	}
	
	/**
	 * <p>Compares a generated target file content with a reference file.
	 * The comparison checks every single byte. So this method only returns
	 * true when the generated target file and the reference file are 100% identical.
	 * 
	 * @param targetContent
	 * @param clazz
	 * @param referenceFile
	 * @return
	 */
	public static boolean isEqual(TargetContentBean targetContent, Class<?> clazz, String referenceFile) {
		String content = new String(targetContent.getFile().getContent());
		String referenceContent = readFileFromClasspath(clazz, referenceFile);
		return content.equals(referenceContent);
	}
	
	/**
	 * @param targetContent
	 * @return
	 */
	public static String stripDocumentation(TargetContentBean targetContent) {
		String content = new String(targetContent.getFile().getContent());
		return stripDocumentation(content);
	}
	
	/**
	 * This method removes JavaDoc and XML documentation from the passed content.
	 * 
	 * @param content
	 * @return
	 */
	public static String stripDocumentation(String content) {
		content = PATTERN_REGEX_MULTILINE_JAVA_DOC.matcher(content).replaceAll("");
		content = PATTERN_REGEX_MULTILINE_XML_DOC.matcher(content).replaceAll("");
		return content;
	}
	
	/**
	 * <p>Calling this method counts the number of generated lines in a target file.
	 * This is a convenience method that you can for instance use to run a generation with
	 * different generator options or model options and then check whether the number of
	 * generated lines has remained the same. 
	 * 
	 * @param targetContent
	 * @return the number of newline characters found in the target content
	 */
	public static int getNumberOfLines(TargetContentBean targetContent) {
		String content = new String(targetContent.getFile().getContent());
		return getNumberOfLines(content);
	}
	
	/**
	 * Returns the number of newline characters that are found in a given string.
	 * 
	 * @param content
	 * @return
	 */
	public static int getNumberOfLines(String content) {
		if (content == null) return 0;
		return StringUtils.countMatches(content, "\n");
	}
	
	/**
	 * <p>Checks whether a generated target file doesn't have any content.
	 * This should normally never be the case and could be a sign of a bug
	 * in the generator or/and a bug in the Virtual Developer framework.
	 * 
	 * @param targetContent
	 * @return
	 */
	public static boolean isEmpty(TargetContentBean targetContent) {
		return targetContent.getFile().getContent() == null || targetContent.getFile().getContent().length == 0;
	}
	
	/**
	 * <p>Collects all target files that are empty.
	 * 
	 * @param targetContents
	 * @return
	 */
	public static Set<TargetContentBean> getEmptyContents(Map<String,TargetContentBean> targetContents) {
		if (!targetContents.isEmpty()) {
			return targetContents.entrySet().stream()
					.filter(targetContentEntry -> isEmpty(targetContentEntry.getValue()))
					.map(targetContentEntry -> targetContentEntry.getValue())
					.collect(Collectors.toSet());
		}
		return Collections.emptySet();
	}
	
	/**
	 * <p>Calling this method generates all files for the given load result.
	 * You can use the messageType parameter to filter the returned target contents.
	 * You will get all generated target files in return when you do not specify a messageType parameter at all.
	 * 
	 * @param clazz
	 * @param client
	 * @param generator
	 * @param loadResult
	 * @param existingContent
	 * @param messageType
	 * @return
	 */
	public static Map<String,Map<String, TargetContentBean>> getGeneratedTargetFiles(Class<?> clazz, GUClientI client, ResultForCreateGenerator generator, LoadResultBean loadResult,
			Map<String,Map<String, FileBean>> existingContent, MessageTypeEnum... messageType) {
		final Map<String,Map<String,TargetContentBean>> generatedTargetFilesPerVirtualProject = new HashMap<>();
		
		loadResult.getResults().forEach(result -> {
			result.getReferences().forEach(reference -> {
				TargetContentBean targetContent;
				FileBean existingContentFileBean = getExistingContent(existingContent, result.getVirtualProject(), ResultBean.getTargetPathFromReference(reference));
				targetContent = transform(client, generator, reference, existingContentFileBean);
		    	Map<String,TargetContentBean> generatedTargetFiles =
		    			generatedTargetFilesPerVirtualProject.computeIfAbsent(result.getVirtualProject(), key -> new TreeMap<>());
				
				if (messageType == null || messageType.length == 0) {
					generatedTargetFiles.put(reference, targetContent);
				} else if (Stream.of(messageType).anyMatch(type -> MessageTypeEnum.ERROR.equals(type)) && isTargetContainingErrors(targetContent)) {
					generatedTargetFiles.put(reference, targetContent);
				} else if (Stream.of(messageType).anyMatch(type -> MessageTypeEnum.WARNING.equals(type)) && isTargetContainingWarnings(targetContent)) {
					generatedTargetFiles.put(reference, targetContent);
				} else if (Stream.of(messageType).anyMatch(type -> MessageTypeEnum.INFO.equals(type)) && isTargetContainingInfos(targetContent)) {
					generatedTargetFiles.put(reference, targetContent);
				}
			});
		});
		
		return generatedTargetFilesPerVirtualProject;
	}

	/**
	 * This is the only method in {@link GuClientTester} that calls the generation client's transform method.
	 * 
	 * @param client
	 * @param generator
	 * @param reference
	 * @param existingContentFileBean
	 * @return
	 */
	private static TargetContentBean transform(GUClientI client, ResultForCreateGenerator generator, String reference,
			FileBean existingContentFileBean) {
		TargetContentBean targetContent;
		LOG.info("transforming file " + reference + " ...");
		if (existingContentFileBean != null) {
			targetContent = client.transformWithOldContent(generator.getId(), reference, existingContentFileBean);
		} else {
			targetContent = client.transform(generator.getId(), reference);
		}
		LOG.info("... file " + reference + " has been transformed");
		return targetContent;
	}
	
	/**
	 * <p>Calling this method transforms all target files for a given load result. After the
	 * transformation, the generated files are compared with files in the ZIP file. A set of target
	 * files is going to be returned where the content differ from the content in the respective
	 * file in the ZIP file.
	 * 
	 * <p>Note that {@link org.junit.Assert#fail} is getting called in case one
	 * of the target files is transformed with errors or when the ZIP file cannot be found
	 * or when a target file is not found in the ZIP file.
	 * 
	 * <p>In case there are differences found between a generated and a reference file, the generated
	 * file is going to be written to a temporary directory for further inspection.
	 * 
	 * @param clazz
	 * @param client
	 * @param generator
	 * @param zipFile
	 * @param loadResult
	 * @param existingContent
	 * @param stripDocumentation
	 * @return
	 */
	public static Map<String,Map<String, TargetContentBean>> generateAndGetModifiedTargetFiles(Class<?> clazz, GUClientI client, ResultForCreateGenerator generator,
			String zipFile, LoadResultBean loadResult, Map<String,Map<String, FileBean>> existingContent, boolean stripDocumentation) {
		final Map<String,Map<String, TargetContentBean>> modifiedTargetFilesPerVirtualProject = new HashMap<>();
		
		loadResult.getResults().forEach(result -> {
			Map<String,TargetContentBean> targetContents = new LinkedHashMap<>();
			result.getReferences().forEach(reference -> {
				FileBean existingContentFileBean = getExistingContent(existingContent, result.getVirtualProject(), ResultBean.getTargetPathFromReference(reference));
				TargetContentBean targetContent = transform(client, generator, reference, existingContentFileBean);
				if (isTargetContainingErrors(targetContent)) {
					fail("target file '" +
						(targetContent.getFile() != null ? targetContent.getFile().getPath() : reference) +
				        "' has been generated with errors: " +
				        getTargetContentMessageText(targetContent, MessageTypeEnum.ERROR).get());
				}
				
				if (targetContent.getFile() != null && !targetContent.isDidNotChanged()) {
					targetContents.put(reference, targetContent);
				}
			});

			Map<String, TargetContentBean> modifiedTargetFiles =
	    			modifiedTargetFilesPerVirtualProject.computeIfAbsent(result.getVirtualProject(), key -> new TreeMap<String, TargetContentBean>());
			
			Map<String, TargetContentBean> modifiedTargetContents = getModifiedTargetFiles(clazz, zipFile, result.getVirtualProject(), targetContents, stripDocumentation);
            modifiedTargetFiles.putAll(modifiedTargetContents);
		});
		
		return modifiedTargetFilesPerVirtualProject;
	}
	
	/**
	 * <p>Calling this method compares the generated files with files in the ZIP file. A set of target
	 * files is going to be returned where the content differ from the content in the respective
	 * file in the ZIP file.
	 * 
	 * <p>Note that {@link org.junit.Assert#fail} is getting called in case one
	 * of the target files is transformed with errors or when the ZIP file cannot be found
	 * or when a target file is not found in the ZIP file.
	 * 
	 * <p>In case there are differences found between a generated and a reference file, the generated
	 * file is going to be written to a temporary directory for further inspection.
	 * 
	 * @param clazz
	 * @param zipFile
	 * @param virtualProject
	 * @param targetContents
	 * @param stripDocumentation
	 * @return
	 */
	public static Map<String, TargetContentBean> getModifiedTargetFiles(Class<?> clazz, String zipFile, String virtualProject,
			Map<String,TargetContentBean> targetContents, boolean stripDocumentation) {
		
		Map<String, TargetContentBean> modifiedTargetFiles = new TreeMap<>();
		Map<String, String> zipContent = getZipContent(clazz, zipFile);
		final Map<String,TargetContentBean> targetContentsWithModifications = new TreeMap<String, TargetContentBean>();
		
		targetContents.entrySet()
		    .stream()
		    .filter(entry -> isTextFile(entry.getValue()))
		    .forEach(entry -> {
				String filename = virtualProject + "/" + entry.getValue().getFile().getPath();
				if (zipContent.containsKey(filename)) {
					String fileContentInZipFile = zipContent.get(filename);
					
					String firstContent = new String(entry.getValue().getFile().getContent());
					String secondContent = fileContentInZipFile;
					
					if (stripDocumentation) {
						firstContent = stripDocumentation(firstContent);
						secondContent = stripDocumentation(secondContent);
					}
					
					if (!isEqualIgnoringWhitespaces(firstContent, secondContent)) {
						modifiedTargetFiles.put(entry.getKey(), entry.getValue());
						targetContentsWithModifications.put(entry.getKey(), entry.getValue());
					}
				} else {
					fail("could not find file with name '" + filename + "' in ZIP file '" + zipFile + "', which means that a file that is expected to be generated in fact is not");
				}
			});
		
		if (!modifiedTargetFiles.isEmpty()) {
			// In case the generated files don't have the same content, we want the generated files to be written
			// to the file system. This makes it easier for the test developer to then find out the differences.
			// Note that only those files are written that actually differ from the reference files' content.
		    writeTargetFilesForVirtualProject(null, virtualProject, targetContentsWithModifications);
        }
		
		return modifiedTargetFiles;
	}
	
	
	/**
	 * <p>Read all files from the ZIP file and convert their content to a string.
	 * The returned map uses the file name as the key and the file content as the value.
	 * 
	 * <p>Note that {@link org.junit.Assert#fail} is getting called in case the ZIP
	 * file cannot be found or cannot be read successfully.
	 * 
	 * @param clazz
	 * @param zipFile
	 * @return
	 */
	public static Map<String,String> getZipContent(Class<?> clazz, String zipFile) {
		Map<String,String> zipContent = new LinkedHashMap<>();
		
		try {
			InputStream inputStream = clazz.getClassLoader().getResourceAsStream(zipFile);
			if (inputStream == null) {
				fail("could not find ZIP file '" + zipFile + "', which is required to execute the test");
			}
			final ZipInputStream zis = new ZipInputStream(inputStream);
			ZipEntry entry = null;
			while ((entry = zis.getNextEntry()) != null) {
				if (entry.isDirectory()) {
					// skip directories
				} else {
					StringBuilder sb = new StringBuilder();
					byte[] buffer = new byte[1024];
					int read = 0;
				    while ((read = zis.read(buffer, 0, 1024)) >= 0) {
				        sb.append(new String(buffer, 0, read));
				    }
					
					zipContent.put(entry.getName(), sb.toString());
				}
				
				zis.closeEntry();
			}
		} catch (IOException ex) {
            fail("cannot execute test since there are problems reading from ZIP file '" + zipFile + "': " + ex.getMessage());
		}
		
		return zipContent;
	}
	
	/**
	 * @param client
	 * @param generator
	 * @param baseDir
	 * @param loadResult
	 * @param existingContent
	 * @return
	 */
	public static Map<String,Map<String,String>> generateAndWriteTargetFiles(GUClientI client, ResultForCreateGenerator generator, String baseDir,
			LoadResultBean loadResult, Map<String,Map<String, FileBean>> existingContent) {
		return generateAndWriteTargetFiles(client, generator, baseDir, loadResult, existingContent, null);
	}
	
	/**
	 * Write generation results for all virtual projects to the file system.
	 * 
	 * <p>The purpose of this method is not to be called to test something but to have an easy means to create a set of
	 * files as the result of code generation. The created files can then be zipped and be used as reference files
	 * for the execution of regression tests.
	 * 
	 * <p>Note that calling this method includes the execution of transformation of target files.
	 * Also: When existing file content is passed and the transformation in the generation unit
	 * doesn't lead to changes of that content, there is no entry for the file in the returned map.
	 * 
	 * @param client
	 * @param generator
	 * @param baseDir the root directory for all written files, if null, a temporary directory will be created and used
	 * @param loadResult
	 * @param existingContent
	 * @param virtualProjectMapping
	 * @return a map of the written file content, the key is the relative path of the file, including the virtual project part
	 */
	public static Map<String,Map<String,String>> generateAndWriteTargetFiles(GUClientI client, ResultForCreateGenerator generator, String baseDir,
			LoadResultBean loadResult, Map<String,Map<String, FileBean>> existingContent, Map<String,String> virtualProjectMapping) {
		Map<String,Map<String,String>> generatedFileContent = new LinkedHashMap<>();
		
		if (baseDir == null || baseDir.length() == 0) {
			baseDir = createTemporaryBaseDir();
		}
		
		performanceTracker.start("generateAndWriteTargetFiles");
		
		final String effectiveBaseDir = baseDir;
		loadResult.getResults().stream()
		        .filter(result -> virtualProjectMapping == null || virtualProjectMapping.isEmpty() || virtualProjectMapping.containsKey(result.getVirtualProject()))
		        .forEach(result -> {
					final Map<String,TargetContentBean> targetContents = new LinkedHashMap<>();
					final Map<String,TargetContentBean> existingContentsDidNotChange = new LinkedHashMap<>();
					result.getReferences().stream()
					    .forEach(reference -> {
					    	FileBean existingContentFileBean = getExistingContent(existingContent, result.getVirtualProject(), ResultBean.getTargetPathFromReference(reference));
					    	TargetContentBean targetContent = transform(client, generator, reference, existingContentFileBean);
					    	if (isTargetContainingErrors(targetContent)) {
								fail("target file '" +
					    	        (targetContent.getFile() != null ? targetContent.getFile().getPath() : reference) +
					    	        "' has been generated with ERROR messages: " +
							        getTargetContentMessageText(targetContent, MessageTypeEnum.ERROR).get());
							} else if (isTargetContainingWarnings(targetContent)) {
								LOG.info("target file '" +
						    	        (targetContent.getFile() != null ? targetContent.getFile().getPath() : reference) +
						    	        "' has been generated with WARNING messages: " +
								        getTargetContentMessageText(targetContent, MessageTypeEnum.WARNING).get());
							} else if (isTargetContainingInfos(targetContent)) {
								LOG.info("target file '" +
						    	        (targetContent.getFile() != null ? targetContent.getFile().getPath() : reference) +
						    	        "' has been generated with INFO messages: " +
								        getTargetContentMessageText(targetContent, MessageTypeEnum.INFO).get());
							}
					    	
					    	if (targetContent.getFile() != null && !targetContent.isDidNotChanged()) {
								targetContents.put(reference, targetContent);
					    	} else {
					    		// --- Nonetheless put the existing content of the to-be-generated file in order to allow tests to check the content.
					    		targetContent.setFile(existingContentFileBean);
					    		String relativePath =  targetContent.getFile().getPath();
					    		targetContents.put(relativePath, targetContent);
					    		existingContentsDidNotChange.put(relativePath, targetContent);
					    		LOG.info("not writing the file '" + (targetContent.getFile() != null ? targetContent.getFile().getPath() : reference) +
					    				"' since it is not generated on the server or its content hasn't changed (this is the most likely case)");
					    	}
						});
					final Map<String, String> fileContentForResult = writeTargetFilesForVirtualProject(effectiveBaseDir, result.getVirtualProject(), targetContents, virtualProjectMapping);
					existingContentsDidNotChange.entrySet().stream()
					    .filter(entry -> !fileContentForResult.containsKey(entry.getKey()))
					    .forEach(entry -> fileContentForResult.put(entry.getKey(), new String(entry.getValue().getFile().getContent())));
					generatedFileContent.put(result.getVirtualProject(), fileContentForResult);
				});
		Interval intervalWrite = performanceTracker.end("generateAndWriteTargetFiles");
		LOG.info("duration of writing the targets (" + generatedFileContent.values().stream().flatMap(map -> map.values().stream()).count() + "): " + intervalWrite.toString());

		return generatedFileContent;
	}
	
	/**
	 * Generate target files for an already exising load result.
	 * 
	 * <p>The purpose of this method is to take a given load result and generate for it.
	 * When the parameter 'checkForTargetErrors' is true, the generation process will be interrupted
	 * with a {@link org.junit.Assert#fail() call as soon as there is one target found that comes with an ERROR message.
	 * 
	 * @param client
	 * @param generator
	 * @param loadResult
	 * @param existingContent
	 * @param checkForTargetErrors
	 * @return a map of the generated target file content, the key is the reference for the target
	 */
	public static Map<String,Map<String,TargetContentBean>> generateTargetFiles(GUClientI client, ResultForCreateGenerator generator, LoadResultBean loadResult,
			Map<String,Map<String, FileBean>> existingContent, boolean checkForTargetErrors) {
		Map<String,Map<String,TargetContentBean>> generatedFileContent = new LinkedHashMap<>();
		
		loadResult.getResults().stream()
		        .forEach(result -> {
					Map<String,TargetContentBean> targetContents = new LinkedHashMap<>();
					result.getReferences().stream()
					    .forEach(reference -> {
					    	FileBean existingContentFileBean = getExistingContent(existingContent, result.getVirtualProject(), ResultBean.getTargetPathFromReference(reference));
					    	TargetContentBean targetContent = transform(client, generator, reference, existingContentFileBean);
							if (checkForTargetErrors && isTargetContainingErrors(targetContent)) {
								fail("target file '" +
									(targetContent.getFile() != null ? targetContent.getFile().getPath() : reference) +
									"' has been generated with errors: " +
							        getTargetContentMessageText(targetContent, MessageTypeEnum.ERROR).get());
							}
							
							targetContents.put(reference, targetContent);
						});
					generatedFileContent.put(result.getVirtualProject(), targetContents);
				});

		return generatedFileContent;
	}
	
	/**
	 * @param baseDir the root directory for all written files, if null, a temporary directory will be created and used
	 * @param targetContents existing file content, this parameter is optional
	 * @return a map of the written file content, the key is the virtual project, the value is a result of calling {@link #writeTargetFilesForVirtualProject(String, String, Map)} 
	 */
	public static Map<String,Map<String,String>> writeTargetFiles(String baseDir, Map<String,Map<String,TargetContentBean>> targetContents) {
		Map<String,Map<String,String>> generatedFileContent = new LinkedHashMap<>();
		
		if (baseDir == null || baseDir.length() == 0) {
			baseDir = createTemporaryBaseDir();
		}
		final String effectiveBaseDir = baseDir;
		
		targetContents.entrySet().stream().forEach(entry -> {
			Map<String, String> writtenFileContent = writeTargetFilesForVirtualProject(effectiveBaseDir, entry.getKey(), targetContents.get(entry.getKey()));
			generatedFileContent.put(entry.getKey(), writtenFileContent);
		});
		
		return generatedFileContent;
	}
	
	/**
	 * @param baseDir
	 * @param virtualProject
	 * @param targetContents
	 * @return
	 */
	public static Map<String,String> writeTargetFilesForVirtualProject(String baseDir, String virtualProject, Map<String,TargetContentBean> targetContents) {
	    return writeTargetFilesForVirtualProject(baseDir, virtualProject, targetContents, null);
	}
	
	/**
	 * Write generation results for a single virtual project to the file system.
	 * 
	 * <p>The purpose of this method is not to be called to test something but to have an easy means to create a set of
	 * files as the result of code generation. The created files can then be zipped and be used as reference files
	 * for the execution of regression tests.
	 * 
	 * @param baseDir the root directory for all written files, if null, a temporary directory will be created and used
	 * @param virtualProject
	 * @param targetContents
	 * @param virtualProjectMapping
	 * @return a map of the written file content, the key is the relative path of the file, excluding the virtual project part
	 */
	public static Map<String,String> writeTargetFilesForVirtualProject(String baseDir, String virtualProject, Map<String,TargetContentBean> targetContents,
			Map<String,String> virtualProjectMapping) {
		
		final Map<String,String> result = new LinkedHashMap<>();
		if (baseDir == null || baseDir.length() == 0) {
			baseDir = createTemporaryBaseDir();
		}
		
		final String effectiveBaseDir = baseDir;
		Path effectiveBaseDirPath = Paths.get(effectiveBaseDir);
		try {
			if (!Files.exists(effectiveBaseDirPath)) {
			    Files.createDirectories(effectiveBaseDirPath);
			}
		} catch (IOException ex) {
			ex.printStackTrace();
			fail("could not create the directory '" + effectiveBaseDir + "': " + ex.getMessage());
		}
		
		final String directoryForVirtualProject;
    	if (virtualProjectMapping != null && !virtualProjectMapping.isEmpty() && virtualProjectMapping.containsKey(virtualProject)) {
    		directoryForVirtualProject = virtualProjectMapping.get(virtualProject);
    	} else {
    		directoryForVirtualProject = virtualProject;
    	}
    	String directoryForFiles = effectiveBaseDir + "/" + directoryForVirtualProject;
    	LOG.info("writing files for virtual project '" + virtualProject + "' to directory '" + directoryForFiles +  "'");
		targetContents.values()
		    .stream()
		    .filter(targetContent -> targetContent.getFile() != null && !targetContent.isDidNotChanged())
		    .forEach(targetContent -> {
		    	writeTargetFile(directoryForFiles, targetContent, result, null, false);
		    });
		
		return result;
	}
	
	/**
	 * @param baseDir
	 * @param targetContent
	 * @param result
	 * @param postfixForDiff
	 */
	public static void writeTargetFile(String baseDir, TargetContentBean targetContent, final Map<String,String> result, String postfixForDiff, boolean stripDocumentation) {
		String relativePath =  targetContent.getFile().getPath();
		String filename = baseDir + "/" + relativePath;

		if (postfixForDiff != null) {
			filename = filename + postfixForDiff;
		}
		
		if (isTextFile(targetContent)) {
			FileWriter fileWriter;
			try {
				Path path = Paths.get(filename);
				Files.createDirectories(path.getParent());
				fileWriter = new FileWriter(filename);
				PrintWriter printWriter = new PrintWriter(fileWriter);
				String content = new String(targetContent.getFile().getContent());
				if (result != null) {
				    result.put(relativePath, content);
				}
				
				if (stripDocumentation) {
					content = stripDocumentation(content);
				}
				
			    printWriter.print(content);
			    printWriter.close();
			} catch (IOException ex) {
				ex.printStackTrace();
				fail("could not write the generated textual file '" + filename + "': " + ex.getMessage());
			}
    	} else {
    		// write binary file
    		Path path = Paths.get(filename);
    		try {
    			if (!Files.exists(path)) {
	    			Files.createDirectories(path.getParent());
	    			Files.createFile(path);
    			}
    		    Files.write(path, targetContent.getFile().getContent());
	        } catch (IOException ex) {
	        	ex.printStackTrace();
	        	fail("could not write the generated binary file '" + filename + "': " + ex.getMessage());
	        }
    	}
	}
	
	/**
	 * <p>Calling this method creates a temporary directory. The name of
	 * the temporary directory always starts with "gu-test-" in order to make
	 * it easy to find them on the filesystem.
	 * 
	 * <p>Note that this method calls {@link org.junit.Assert#fail} in case the
	 * temporary directory cannot be created.
	 * 
	 * @return
	 */
	public static String createTemporaryBaseDir() {
	    try {
	    	return Files.createTempDirectory("gu-test-").toFile().getAbsolutePath();
		} catch (IOException ex) {
			ex.printStackTrace();
			fail("cannot write generated files since the temporary directory could not be created: " + ex.getMessage());
		}
	    
	    return null;
	}
	
	/**
	 * Checks whether the given target file is a textual file (in comparison to a binary file).
	 * 
	 * @param targetContent
	 * @return
	 */
	public static boolean isTextFile(TargetContentBean targetContent) {
		// TODO replace this guess by means of more sensible API calls once the API allows for this (mmt 07-Jan-2022)
		if (targetContent.getDeveloperAreaCount() > 0) {
			return true;
		} else if (targetContent.getFile() == null) {
			return false;
		}
		
		return isTextFile(targetContent.getFile().getPath());
	}
	
	/**
	 * @param path
	 * @return
	 */
	public static boolean isTextFile(String path) {
		// Note that the following logic presumes that for all to-be-generated binary files,
		// the API calls return a proper content type.
		FileNameMap fileNameMap = URLConnection.getFileNameMap();
		String contentType = fileNameMap.getContentTypeFor(path);
		return contentType == null || 
				contentType.contains("text") ||
				contentType.contains("txt") ||
				contentType.contains("xml") ||
				contentType.contains("html") ||
				contentType.contains("css") ||
				contentType.contains("json") ||
				contentType.contains("yml") ||
				contentType.contains("yaml");
	}
	
	/**
	 * <p>Calling this method gives you a human readable text including all file names
	 * of the given target files. This method is meant to be used to provide readable
	 * output for the result of tests.
	 * 
	 * <p>Note that the resulting file names include the name of the virtual project.
	 * 
	 * @param targetContents
	 * @param separator
	 * @return
	 */
	public static String getFileNames(Map<String,Map<String, TargetContentBean>> targetContents, String separator) {
		StringBuilder sb = new StringBuilder();
		final String effectiveSeparator = separator == null ? "" : separator;
		
		if (targetContents != null) {
			targetContents.entrySet().forEach(targetContentEntry -> {
				if (sb.length() > 0) sb.append(effectiveSeparator);
				    targetContentEntry.getValue().entrySet().forEach(mappedTargetContent -> {
				    	if (mappedTargetContent.getValue().getFile() != null) {
						    sb.append(targetContentEntry.getKey() + "/" + mappedTargetContent.getValue().getFile().getPath());
				    	} else {
				    		sb.append(mappedTargetContent.getKey());
				    	}
				    });
			});
		}
		
		return sb.toString();
	}
	
	/**
	 * @param targets
	 * @param relativeNameOnly set this to true to get the file name only, without its directory
	 * @param sorted when this is set to true, the list of returned file names is sorted alphabetically (case insensitive ordering)
	 * @return
	 */
	public static List<String> getFileNamesAsList(final Set<TargetContentBean> targets, final boolean relativeNameOnly, final boolean sorted) {
		final ArrayList<String> fileNames = new ArrayList<>();
		targets.stream()
		    .filter(target -> target.getFile() != null)
		    .forEach(target -> {
		    	if (relativeNameOnly) {
		    		if (target.getFile().getPath().contains("/")) {
		    			fileNames.add(target.getFile().getPath().substring(target.getFile().getPath().lastIndexOf("/")+1));
		    		} else {
		    			fileNames.add(target.getFile().getPath());
		    		}
		    	} else {
		    		fileNames.add(target.getFile().getPath());
		    	}
		    });
		if (sorted) {
			Collections.sort(fileNames, String.CASE_INSENSITIVE_ORDER);
		}
		return fileNames;
	}
	
	/**
	 * @param targets
	 * @param relativeNameOnly
	 * @param sorted
	 * @return
	 */
	public static List<String> getFileNamesAsList(final Map<String,Set<TargetContentBean>> targets, final boolean relativeNameOnly, final boolean sorted) {
		final ArrayList<String> allFileNames = new ArrayList<>();
		targets.entrySet().stream().forEach(entry -> {
			List<String> fileNames = getFileNamesAsList(entry.getValue(), relativeNameOnly, sorted);
			if (!relativeNameOnly) {
				// Here we prepend the name of the virtual project since we do have this information here. The similar
				// method that doesn't take a target map but a target set doesn't do this since it doesn't have this information available.
				fileNames.stream().map(fileName -> entry.getKey() + "/" + fileName).forEach(fileName -> allFileNames.add(fileName));
			} else {
				allFileNames.addAll(fileNames);
			}
		});
		if (sorted) {
			Collections.sort(allFileNames, String.CASE_INSENSITIVE_ORDER);
		}
		return allFileNames;
	}
	
	/**
	 * @param targets
	 * @return
	 */
	public static long getNumberOfGeneratedMappedTargets(Map<String,Map<String,TargetContentBean>> targets) {
		return targets.values().stream().flatMap(m -> m.values().stream()).count();
	}
	
	/**
	 * @param targets
	 * @return
	 */
	public static long getNumberOfGeneratedTargets(Map<String,Set<TargetContentBean>> targets) {
		return targets.values().stream().flatMap(coll -> coll.stream()).count();
	}
	
	
	
	/**
	 * 
	 * @param text
	 * @param regex
	 * @return
	 * @see <a href="https://docs.oracle.com/javase/8/docs/api/java/util/regex/Pattern.html">Java 8 Pattern JavaDoc</a>
	 */
	public static boolean checkRegexMatch(String text, String regex) {
		Pattern pattern = Pattern.compile(regex == null ?
				"" : regex,
				Pattern.MULTILINE |
				Pattern.UNIX_LINES |
				Pattern.DOTALL |
				Pattern.CASE_INSENSITIVE);
		
		return pattern.matcher(text).matches();
	}
	
	/**
	 * <p>Checks a complete set of target files to find matching ones. If regexForPath and regexForContent
	 * are both specified, the two conditions are combined with the logical operator AND.
	 * 
	 * @param targets
	 * @param regexForPath
	 * @param regexForContent
	 * @return the matching target files, an empty set if there is no match
	 */
	public static Set<TargetContentBean> filterTargetsForMappedTargets(Map<String, TargetContentBean> targets, String regexForPath, String regexForContent) {
		final Pattern patternForPath = Pattern.compile(regexForPath == null ?
				"" : regexForPath, Pattern.MULTILINE | Pattern.UNIX_LINES | Pattern.DOTALL | Pattern.CASE_INSENSITIVE);
		final Pattern patternForContent = Pattern.compile(regexForContent == null ?
				"" : regexForContent, Pattern.MULTILINE | Pattern.UNIX_LINES | Pattern.DOTALL | Pattern.CASE_INSENSITIVE);
		
		if (targets != null) {
			Set<TargetContentBean> result = targets.values().stream()
				.filter(target -> target.getFile() != null)
			    .filter(target -> {
			        boolean matching = (regexForPath == null || patternForPath.matcher(target.getFile().getPath()).matches())
		                                &&
	                                   (regexForContent == null || patternForContent.matcher(new String(target.getFile().getContent())).matches());
			        return matching;
			    })
			    .collect(Collectors.toSet());
			return result;
		}
		
		return Collections.emptySet();
	}
	
	/**
	 * <p>Checks a complete set of target files to find matching ones. If regexForPath and regexForContent
	 * are both specified, the two conditions are combined with the logical operator AND.
	 * 
	 * @param targets
	 * @param regexForPath
	 * @param regexForContent
	 * @return the matching target files per virtual project, an empty map if there is no match
	 */
	public static Map<String,Set<TargetContentBean>> filterTargets(Map<String,Map<String, TargetContentBean>> targets, String regexForPath, String regexForContent) {
		Map<String,Set<TargetContentBean>> result = new HashMap<>();
		
		if (targets != null) {
			targets.entrySet().stream().forEach(entry -> {
				Set<TargetContentBean> filteredTargets = filterTargetsForMappedTargets(entry.getValue(), regexForPath, regexForContent);
				if (!filteredTargets.isEmpty()) {
					Set<TargetContentBean> filteredTargetsForOneVirtualProject =
			    			result.computeIfAbsent(entry.getKey(), key -> new LinkedHashSet<TargetContentBean>());
					filteredTargetsForOneVirtualProject.addAll(filteredTargets);
				}
			});
		}
		
		return result;
	}
	
	/**
	 * @param existingContent
	 * @param virtualProject
	 * @param relativePath
	 * @return
	 */
	public static FileBean getExistingContent(Map<String,Map<String, FileBean>> existingContent, @NotNull String virtualProject, @NotNull String relativePath) {
		if (existingContent == null) return null;
		
		if (existingContent.containsKey(virtualProject)) {
			return existingContent.get(virtualProject).get(relativePath);
		}
		
		return null;
	}
	
	/**
	 * @param generatedFileContent
	 * @return
	 */
	public static Map<String,Map<String,FileBean>> createExistingContent(Map<String, Map<String, String>> generatedFileContent) {
		if (generatedFileContent == null) return Collections.emptyMap();
		
		final Map<String,Map<String,FileBean>> result = new LinkedHashMap<>();
		
		generatedFileContent.entrySet().stream().forEach(entry -> {
			String virtualProject = entry.getKey();
			final Map<String, FileBean> map;
			if (result.containsKey(virtualProject)) {
		        map = result.get(virtualProject);
			} else {
				map = new LinkedHashMap<>();
				result.put(virtualProject, map);
			}
			
			entry.getValue().entrySet().stream().forEach(contentEntry -> {
				FileBean fileBean = createFileBean(contentEntry.getKey(), contentEntry.getValue());
				map.put(fileBean.getPath(), fileBean);
			});
		});
		
		return result;
	}
	
	/**
	 * <p>This method reads some files from the classpath in order to use them as an already
	 * existing file content to be sent along while executing the file transformation.
	 * 
	 * <p>If you need to send very many files to the generation unit, you are better off
	 * using the method {@link #createExistingContent(Class, String, String)}, which reads
	 * the existing content from a zip file. This way the number of files you need to store
	 * in your code repository is kept low.
	 * 
	 * @param clazz needed for resource loading
	 * @param baseDir a base directory where the files are located
	 * @param existingFiles a map with all file names per virtual project
	 * @return a map where the key is a virtual project and the value holds a map where key is file name and value is the file content
	 */
	public static Map<String,Map<String,FileBean>> createExistingContent(Class<?> clazz, @NotNull String baseDir, Map<String, List<String>> existingFiles) {
		if (existingFiles == null) return Collections.emptyMap();
		
		final Map<String,Map<String,FileBean>> result = new LinkedHashMap<>();
		
		existingFiles.entrySet().stream().forEach(entry -> {
			String virtualProject = entry.getKey();
			final Map<String, FileBean> map;
			if (result.containsKey(virtualProject)) {
		        map = result.get(virtualProject);
			} else {
				map = new LinkedHashMap<>();
				result.put(virtualProject, map);
			}
			
			
			entry.getValue().stream().forEach(filePath -> {
				FileBean fileBean = createFileBeanFromFile(clazz, baseDir, virtualProject, filePath);
				map.put(filePath, fileBean);
			});
		});
		
		return result;
	}
	
	/**
	 * <p>This method reads some files from a zip file in order to use them as an already
	 * existing file content to be sent along while executing the file transformation.
	 * 
	 * <p>If you need to send only a few files to the generation unit, you might find it more convenient
	 * to use the method {@link #createExistingContent(Class, String, Map)}, which reads
	 * the existing content individual files on your classpath. This way you can directly work
	 * with those files while developing tests instead of having to fiddle around with zip tools.
	 * 
	 * @param clazz needed for resource loading
	 * @param baseDir an optional base directory where the zip file is located
	 * @param zipFile the name of the zip file that holds the existing code to be sent to the file transformation
	 * @return a map where the key is a virtual project and the value holds a map where key is file name and value is the file content
	 */
	public static Map<String,Map<String,FileBean>> createExistingContent(Class<?> clazz, String baseDir, String zipFile) {
		if (zipFile == null) return Collections.emptyMap();
		
		final Map<String,Map<String,FileBean>> result = new LinkedHashMap<>();
		String effectiveZipFile = baseDir == null || baseDir.length() == 0 ?
				zipFile : baseDir + "/" + zipFile;
		Map<String, String> zipContent = getZipContent(clazz, effectiveZipFile);
		
		zipContent.entrySet().stream()
			.forEach(entry -> {
				String filenameIncludingVirtualProject = entry.getKey();
				String[] filenameSegments = filenameIncludingVirtualProject.split("/", 2);
				String virtualProject = filenameSegments[0];
				String filename = filenameSegments[1];
				String content = entry.getValue();
				FileBean fileBean = createFileBean(filename, content);
				
				final Map<String, FileBean> map;
				if (result.containsKey(virtualProject)) {
			        map = result.get(virtualProject);
				} else {
					map = new LinkedHashMap<>();
					result.put(virtualProject, map);
				}
				
				map.put(filename, fileBean);
			});
		
		return result;
	}
	
	
	/**
	 * @param loadResult
	 * @return a map where the key is a virtual project name and the value is the collection of file names in that virtual project
	 */
	public static Map<String,List<String>> createFilenameMap(LoadResultBean loadResult) {
		if (loadResult == null) return Collections.emptyMap();
		
		final Map<String,List<String>> filenameMap = new LinkedHashMap<>();
		
		loadResult.getResults().forEach(result -> {
			
			final List<String> filenames = new ArrayList<>();
			filenameMap.put(result.getVirtualProject(), filenames);
			
			result.getReferences().forEach(reference -> {
				String relativePath = ResultBean.getTargetPathFromReference(reference);
				filenames.add(relativePath);
			});
		});
		
		return filenameMap;
	}
	
	/**
	 * <p>This method performs an elaborate comparison of the two passed collections of generation results.
	 * The comparison result provides information about targets that are found in both collections (identical paths)
	 * and also whether such target pairs have identical content (ignoring whitespace characters and also generator information).
	 * 
	 * <p>In addition to this, the comparison result includes the information about targets that are in the first result but
	 * not in the second and vice versa.
	 * 
	 * <p>The typical use case for this method is to compare the results of two different versions of the same generator or
	 * to compare the results of two different generators that are supposed to generate the same output but for
	 * different model formats.
	 * 
	 * @param result1
	 * @param result2
	 * @return
	 */
	public static ComparisonResult compareGenerationResults(final Set<TargetContentBean> result1,
			final Set<TargetContentBean> result2) {
		ComparisonResult result = new ComparisonResult();
		
		result1.stream()
		    .forEach(target1 -> {
		    	Optional<TargetContentBean> match = findMatchingTarget(target1, result2);
		    	if (match.isPresent()) {
		    		result.addTargets(new Targets(target1, match.get()));
		    	} else {
		    		result.addTargets(new Targets(target1, null));
		    	}
		    });
		
		result2.stream()
		    .forEach(target2 -> {
		    	Optional<TargetContentBean> match = findMatchingTarget(target2, result1);
		    	if (!match.isPresent()) {
		    		result.addTargets(new Targets(null, target2));
		    	}
		    });
		
		return result;
	}
	
	/**
	 * <p>Returns an optional that contains an object in case the passed target's path is found in the
	 * collection of targets.
	 * 
	 * @param target
	 * @param targets
	 * @return
	 */
	private static Optional<TargetContentBean> findMatchingTarget(final TargetContentBean target, final Set<TargetContentBean> targets) {
		if (target.getFile() == null) return null;
		
		Optional<TargetContentBean> result = targets.stream()
		    .filter(aTarget -> aTarget.getFile() != null)
		    .filter(aTarget -> aTarget.getFile().getPath().equals(target.getFile().getPath()))
		    .findFirst();
		return result;
		
	}
	
	/**
	 * <p>This method simply prints out the messages that are part of the result of loading the target list.
	 * It doesn't check anything but helps during development of tests and also when something
	 * goes wrong and you want to print additional information to analyze failed tests.
	 * 
	 * @param loadResult
	 */
	public static void printMessages(LoadResultBean loadResult) {
		if (loadResult.getMessages().isEmpty()) {
			System.out.println("The load result doesn't include any messages.");
		} else {
			System.out.println("The load result includes " + loadResult.getMessages().size() + " messages.");
			printMessages(loadResult.getMessages());
		}
	}
	
	/**
	 * <p>This method simply prints out the messages that are part of the result of generating targets.
	 * It doesn't check anything but helps during development of tests and also when something
	 * goes wrong and you want to print additional information to analyze failed tests.
	 * 
	 * @param targets
	 */
	public static void printMessages(Map<String,TargetContentBean> targets) {
		if (targets == null || targets.isEmpty()) {
			System.out.println("no targets to check for messages to be displayed.");
		} else {
			System.out.println("checking " + targets.size() + " targets for messages");
			targets.entrySet().stream()
			    .forEach(entry -> {
			    	TargetContentBean target = entry.getValue();
					if (!target.getMessages().isEmpty()) {
						System.out.println("Message for target '" +
					        entry.getKey() + "'");
						printMessages(target.getMessages());
					}
				});
		}
	}
	
	/**
	 * <p>This method simply prints out the messages that are part of the result of generating targets.
	 * It doesn't check anything but helps during development of tests and also when something
	 * goes wrong and you want to print additional information to analyze failed tests.
	 * 
	 * @param targets
	 */
	public static void printMessagesPerVritualProject(Map<String,Map<String, TargetContentBean>> targets) {
		if (targets == null || targets.isEmpty()) {
			System.out.println("no targets to check for messages to be displayed");
		} else {
			targets.entrySet().stream().forEach(entry -> {
				System.out.println("messages for targets of virtual project '" + entry.getKey() + "'");
				printMessages(entry.getValue());
			});
		}
	}
	
	/**
	 * <p>This method simply prints out the messages that are part of the result of generating targets.
	 * It doesn't check anything but helps during development of tests and also when something
	 * goes wrong and you want to print additional information to analyze failed tests.
	 * 
	 * @param targets
	 */
	public static void printMessagesForMappedTargets(Map<String,Map<String,TargetContentBean>> targets) {
		if (targets == null || targets.isEmpty()) {
			System.out.println("no targets to check for messages to be displayed");
		} else {
			targets.entrySet().stream().forEach(entry -> {
				Collection<TargetContentBean> targetsPerVirtualProject = entry.getValue().values();
				if (!targetsPerVirtualProject.isEmpty()) {
					System.out.println("messages for targets of virtual project '" + entry.getKey() + "'");
					printMessages(entry.getValue());
				}
			});
		}
	}
	
	/**
	 * @param messages
	 */
	private static void printMessages(List<MessageBean> messages) {
		messages.stream().forEach(message -> printMessage(message));
	}
	
	/**
	 * @param message
	 */
	private static void printMessage(MessageBean message) {
		System.out.println();
		System.out.println(message.getType() + ":" + message.getMessage());
	}
	
	/**
	 * <p>Instances of this class hold a pair of instances of {@link TargetContentBean} that are
	 * related to each other. Only if both targets have the same path and file name, result1 and result2
	 * are set. In all other cases, either result1 or result2 is null.
	 *
	 */
	public static class Targets implements Comparable<Targets> {
		public enum Status {
			IDENTICAL,
			FIRST_RESULT_MISSING,
			SECOND_RESULT_MISSING,
			DIFFERENT,
			;
		}
		private final TargetContentBean result1;
		private final TargetContentBean result2;
		private final Status status;
		private final List<String> diffs;
		
		public Targets(TargetContentBean result1, TargetContentBean result2) {
			super();
			this.result1 = result1;
			this.result2 = result2;
			
			if (result1 == null && result2 != null) {
				this.status = Status.FIRST_RESULT_MISSING;
				this.diffs = Collections.emptyList();
			} else if (result1 != null && result2 == null) {
				this.status = Status.SECOND_RESULT_MISSING;
				this.diffs = Collections.emptyList();
			} else if (result1 != null && result2 != null) {
				if (GuClientTester.isEqualIgnoringDocumentationAndWhitespace(result1, result2)) {
					this.status = Status.IDENTICAL;
					this.diffs = Collections.emptyList();
				} else {
					// What's the difference?
					this.status = Status.DIFFERENT;
					this.diffs = GuClientTester.getDiff(result1, result2, true);
				}
			} else {
				this.status = null;
				this.diffs = Collections.emptyList();
				fail("To create a 'Targets' object, at least one of the two parameters 'result1' and 'result2' has to be non-null.");
			}
		}

		public Status getStatus() {
			return status;
		}

		public List<String> getDiffs() {
			return diffs;
		}

		public TargetContentBean getResult1() {
			return result1;
		}

		public TargetContentBean getResult2() {
			return result2;
		}

		@Override
		public String toString() {
			return "Targets [result1=" + 
		        (result1 == null ? "null" : result1.getFile().getPath()) + ", result2=" +
			    (result2 == null ? "null" : result2.getFile().getPath()) + ", status=" + status + "]";
		}

		@Override
		public int compareTo(Targets targets) {
			if (targets == this) return 0;
			
			if (result1 != null && result2 != null &&
					targets.result1 != null && targets.result2 != null) {
				return result1.getFile().getPath().compareTo(targets.result1.getFile().getPath());
			} else if (result1 != null && result2 == null &&
					targets.result1 != null && targets.result2 == null) {
				return result1.getFile().getPath().compareTo(targets.result1.getFile().getPath());
			} else if (result1 == null && result2 != null &&
					targets.result1 == null && targets.result2 != null) {
				return result2.getFile().getPath().compareTo(targets.result2.getFile().getPath());
			} else if (result1 == null && result2 != null &&
					targets.result1 != null && targets.result2 == null) {
				return result2.getFile().getPath().compareTo(targets.result1.getFile().getPath());
			} else if (result1 != null && result2 == null &&
					targets.result1 == null && targets.result2 != null) {
				return result1.getFile().getPath().compareTo(targets.result2.getFile().getPath());
			}
			
 			return 0;
		}
	}
	
	/**
	 * <p>Instances of this class hold the complete result of the comparison of
	 * two generations with two different generators.
	 *
	 */
	public static class ComparisonResult {
		
		private final List<Targets> targets = new ArrayList<>();
		
		private final List<TargetContentBean> targets1 = new ArrayList<>();
		
		private final List<TargetContentBean> targets2 = new ArrayList<>();
		
		protected List<TargetContentBean> getTargets1() {
			return targets1;
		}

		protected List<TargetContentBean> getTargets2() {
			return targets2;
		}

		public void addTargets(Targets targets) {
			if (targets != null) {
			    this.targets.add(targets);
			    if (targets.getResult1() != null) {
			    	targets1.add(targets.getResult1());
			    }
			    if (targets.getResult2() != null) {
			    	targets2.add(targets.getResult2());
			    }
			}
		}
		
		public List<Targets> getTargets() {
			return Collections.unmodifiableList(targets);
		}
		
		public long getCountOfFirstResultMissing() {
			return this.targets.stream()
			    .filter(targets -> targets.getStatus() == Targets.Status.FIRST_RESULT_MISSING)
			    .count();
		}
		
		public List<Targets> getFirstResultMissing() {
			return this.targets.stream()
				    .filter(targets -> targets.getStatus() == Targets.Status.FIRST_RESULT_MISSING)
				    .sorted()
				    .collect(Collectors.toList());
		}
		
        public long getCountOfSecondResultMissing() {
        	return this.targets.stream()
    			    .filter(targets -> targets.getStatus() == Targets.Status.SECOND_RESULT_MISSING)
    			    .count();
		}
        
        public List<Targets> getSecondResultMissing() {
        	return this.targets.stream()
    			    .filter(targets -> targets.getStatus() == Targets.Status.SECOND_RESULT_MISSING)
    			    .sorted()
    			    .collect(Collectors.toList());
		}
        
        public long getCountOfDifferentResults() {
        	return this.targets.stream()
    			    .filter(targets -> targets.getStatus() == Targets.Status.DIFFERENT)
    			    .count();
        }
        
        public List<Targets> getDifferentResults() {
        	return this.targets.stream()
    			    .filter(targets -> targets.getStatus() == Targets.Status.DIFFERENT)
    			    .sorted()
    			    .collect(Collectors.toList());
        }
        
        public long getCountOfIdenticalResults() {
        	return this.targets.stream()
    			    .filter(targets -> targets.getStatus() == Targets.Status.IDENTICAL)
    			    .count();
        }
        
        public List<Targets> getIdenticalResults() {
        	return this.targets.stream()
    			    .filter(targets -> targets.getStatus() == Targets.Status.IDENTICAL)
    			    .sorted()
    			    .collect(Collectors.toList());
        }
        
        public List<Targets> getGeneratedByBoth() {
        	return this.targets.stream()
				    .filter(targets -> targets.getStatus() == Targets.Status.IDENTICAL ||
				        targets.getStatus() == Targets.Status.DIFFERENT)
				    .sorted()
				    .collect(Collectors.toList());
        }
        
        public List<Targets> getGeneratedByOnlyOne() {
        	return this.targets.stream()
				    .filter(targets -> targets.getStatus() == Targets.Status.FIRST_RESULT_MISSING ||
				        targets.getStatus() == Targets.Status.SECOND_RESULT_MISSING)
				    .sorted()
				    .collect(Collectors.toList());
        }
	}

	/**
	 * @param baseDir the directory on the file system from where the content is read
	 * @param loadResult
	 * @param virtualProjectMapping
	 * @return
	 */
	public static Map<String, Map<String, FileBean>> readExistingContent(@NotNull String baseDir, LoadResultBean loadResult,
			Map<String, String> virtualProjectMapping) {
		Map<String, Map<String, FileBean>> existingContent = new LinkedHashMap<>();
		
		loadResult.getResults().stream().forEach(result -> {
			result.getReferences().forEach(reference -> {
				String virtualProject = result.getVirtualProject();
				String relativePathOfTarget = ResultBean.getTargetPathFromReference(reference);
				String pathForVirtualProject = virtualProject;
				if (virtualProjectMapping != null && virtualProjectMapping.containsKey(virtualProject) ) {
					pathForVirtualProject = virtualProjectMapping.get(virtualProject);
				}
				String fullPath = baseDir + "/" + pathForVirtualProject + "/" + relativePathOfTarget;
				
    			Map<String, FileBean> existingContentPerVirtualProject =
    					existingContent.computeIfAbsent(virtualProject, key -> new LinkedHashMap<String, FileBean>());
    			
    			// --- finally, read existing file content ... note that a might well be not (yet) existing
    			if (isTextFile(fullPath)) {
	    			Path existingFile = Paths.get(fullPath);
	    			if (Files.exists(existingFile)) {
		    			byte[] contentAsByteArray;
						try {
							contentAsByteArray = Files.readAllBytes(existingFile);
							if (contentAsByteArray != null) {
			    				FileBean fileBean = new FileBean();
			    				fileBean.setCharset("UTF-8");
			    				fileBean.setContent(contentAsByteArray);
			    				fileBean.setPath(relativePathOfTarget);
			    				existingContentPerVirtualProject.put(relativePathOfTarget, fileBean);
			    			}
						} catch (IOException ex) {
							ex.printStackTrace();
							fail("could not read file '" + existingFile.toString() + "' due to an IOException: " + ex.getMessage());
						}
	    			}
    			}
			});
		});
		return existingContent;
	}
	
	/**
	 * This method checks the given content for duplicate lines.
	 * 
	 * <p>Empty lines are going to be ignored. You can use
	 * the passed array of ignored lines to exclude certain content
	 * from being checked. This is useful when you already are aware that
	 * the content includes duplicate content and you consider this to be valid.
	 * A typical example for the ignorance: Generated Java code will include several
	 * lines with closing curly braces.
	 * 
	 * @param content
	 * @param ignoredLinesArray
	 * @return
	 */
	public static Optional<String> findDuplicateLines(final String content, final String ...ignoredLinesArray) {
		if (content == null) {
			return Optional.empty();
		}
			
		String[] lines = content.split(System.lineSeparator());
		final StringBuilder duplicateLines = new StringBuilder();
		final Set<String> existingLines = new HashSet<>();
		final Set<String> ignoredLines;
		if (ignoredLinesArray == null) {
			ignoredLines = Collections.emptySet();
		} else {
			ignoredLines = new HashSet<>();
			Stream.of(ignoredLinesArray)
			.filter(ignoreLine -> ignoreLine != null)
			.forEach(ignoredLine -> ignoredLines.add(ignoredLine.toLowerCase()));
		}
		
		Stream.of(lines)
		    .filter(line -> line == null || line.trim().isEmpty())
		    .map(line -> line.trim())
		    .filter(line -> ignoredLines.isEmpty() || !ignoredLines.contains(line.toLowerCase()))
		    .forEach(line -> {
		    	if (existingLines.contains(line)) {
		    		if (duplicateLines.length() > 0) duplicateLines.append(System.lineSeparator());
		    		duplicateLines.append(line);
		    	} else {
		    		existingLines.add(line.toLowerCase());
		    	}
		    });
		
		if (duplicateLines.length() == 0) {
			return Optional.empty();
		} else {
			return Optional.of(duplicateLines.toString());
		}
	}
}

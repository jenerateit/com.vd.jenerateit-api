package com.vd.gu.test;

import static org.junit.Assert.fail;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

import javax.validation.constraints.NotNull;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vd.gu.TechnicalExceptionCreateGeneratorDefaultException;
import com.vd.gu.TechnicalExceptionLoadGeneratorDefaultException;
import com.vd.gu.client.ResultForCreateGenerator;
import com.vd.gu.definition.MessageTypeEnum;
import com.vd.gu.definition.basic.FileBean;
import com.vd.gu.definition.basic.LoadResultBean;
import com.vd.gu.definition.basic.OptionBean;
import com.vd.gu.definition.basic.TargetContentBean;
import com.vd.gu.test.PerformanceTracker.Interval;

/**
 * <p>This class encapsulates the configuration and creation of a generation client for a specific generator.
 * You can create tests that inherit from this class and set system properties in an instance initialization block.
 * These settings  provide the configuration for the generation client.
 * 
 * <p>The following keys can be used to set the properties:
 * 
 * <ul>
 *     <li>GU_HOSTNAME
 *     <li>GU_ID
 *     <li>GU_PORT
 *     <li>LOG_WEBSERVICE_CALLS
 *     <li>WRITE_TARGET_FILES
 * </ul>
 * 
 *
 */
abstract public class AbstractGeneratorTest extends AbstractGensetTest {
	
	private static final @NotNull Logger LOG = LoggerFactory.getLogger(AbstractGensetTest.class);
	
	private ResultForCreateGenerator generator;
	
    private String guGeneratorId;
    
    private String guGeneratorVersion;
    
    private final List<OptionBean> options = new ArrayList<>();
    
    public AbstractGeneratorTest() {
		super();
    }
	
	public AbstractGeneratorTest(OptionBean ...options) {
		super();
		if (options != null) {
			this.options.addAll(Arrays.asList(options));
		}
    }
	
	public AbstractGeneratorTest(String ...options) {
		super();
		if (options != null) {
			this.options.addAll(convertToOptionBeans(Arrays.asList(options)));
		}
    }
	
	/**
	 * @return
	 */
	protected List<OptionBean> getAdditionalOptions() {
		return Collections.emptyList();
	}
    
	protected ResultForCreateGenerator getGenerator() {
		return generator;
	}

	protected String getGuGeneratorId() {
		if (guGeneratorId != null) {
		    return guGeneratorId;
		} else if (getGeneratorUnderTest() != null) {
			return getGeneratorUnderTest().getId();
		}
		return null;
	}

	protected void setGuGeneratorId(String guGeneratorId) {
		this.guGeneratorId = guGeneratorId;
	}

	protected String getGuGeneratorVersion() {
		if (guGeneratorVersion != null) {
		    return guGeneratorVersion;
		} else if (getGeneratorUnderTest() != null) {
			return getGeneratorUnderTest().getVersion();
		}
		return null;
	}
	
	protected void setGuGeneratorVersion(String guGeneratorVersion) {
		this.guGeneratorVersion = guGeneratorVersion;
	}
	
	protected String getGuGeneratorIdAndVersion() {
		return getGuGeneratorId() + ":" + getGuGeneratorVersion();
	}

	@NotNull
	protected List<OptionBean> getOptions() {
		return options;
	}
	
	protected GeneratorUnderTestI getGeneratorUnderTest() {
		return null;
	}
	
	/**
	 * Search for an option in the text executions used options.
	 * A case insensitive comparison is being used to find
	 * a matching option.
	 * 
	 * @param optionName
	 * @return
	 */
	public Optional<OptionBean> getOption(String optionName) {
		return this.getOptions()
				.stream()
				.filter(option -> optionName.toLowerCase().equals(option.getName().toLowerCase()))
		        .findFirst();
	}

	@Before
	public void setUpGenerator() throws MalformedURLException {
		
		try {
			ArrayList<OptionBean> allOptions = new ArrayList<>();
			allOptions.addAll(options);
			allOptions.addAll(this.getAdditionalOptions());
		    this.generator = getClient().createGenerator(getGuGeneratorId(), getGuGeneratorVersion(), allOptions);
		} catch (TechnicalExceptionCreateGeneratorDefaultException ex) {
			Optional<String> additionalErrorInformation = getAdditionalErrorInformation(ex);
			Assert.fail("failed to create generator with ID '" + getGuGeneratorId() + "' and version '" + getGuGeneratorVersion() + "': " + ex.getMessage() + ", additional error information: " +
		       (additionalErrorInformation.isPresent() ? additionalErrorInformation.get() : "NONE"));
		}
	}
	
	@After
	public void tearDownGenerator() throws MalformedURLException {
		if (this.getClient() != null && this.generator != null) {
			this.getClient().deleteGenerator(generator.getId());
		}
	}
	
	/**
	 * @return all model files serving as the input for the code generation
	 */
	abstract protected String[] getModelFiles();
	
	/**
	 * @param separator
	 * @return all model file names as one text
	 */
	protected String getModelFileNames(String separator) {
		final StringBuilder result = new StringBuilder();
		String[] modelFiles = getModelFiles();
		if (modelFiles != null) {
			Stream.of(modelFiles).forEach(filename -> {
				if (result.length() > 0) result.append(separator);
				result.append(filename);
			});
		}
		
		return result.toString();
	}
	
	/**
	 * <p>You can optionally overwrite this method an let it return
	 * the name of a ZIP file that contains reference target files.
	 * 
	 * <p>When you use several different generator parameters for the same input model, it
	 * is a good idea to let the implementation return different ZIP file names
	 * dependent on what is in the result of a call to {@link AbstractGeneratorText#getOptions}.
	 * 
	 * @return
	 */
	protected String getZippedReferenceTargetFiles() {
		return null;
	}
		
	/**
	 * <p>This method executes a complete test for a given set of input files.
	 * The test applies generator options that are provided by means of {@link org.junit.runners.Parameterized}.
	 * This way you can run several tests with the same input models but with different generator parameters.
	 * 
	 * If you want to do more than just checking whether the generation fails or whether a given set of reference
	 * output is different from the actually generated targets, you'll have to call {@link #executeGeneration} instead.
	 * 
	 * @param stripGeneratorInfo
	 * @return
	 */
	protected final void executeGenerationWithChecks(boolean stripGeneratorInfo) {
		List<FileBean> inputFiles = GuClientTester.createInputFiles(this.getClass(), null, getModelFiles());
		
		LoadResultBean loadResult = null;
		
		try {
			String performanceTrackerId = "load target list for " + getGeneratorUnderTest().getIdAndVersion();
			getPerformanceTracker().start(performanceTrackerId);
			logGuConnectionDetails("executeGenerationWithChecks(), about to call loadGenerator() for generator " + getGenerator().getId());
		    loadResult = getClient().loadGenerator(getGenerator().getId(), inputFiles);
		    Interval interval = getPerformanceTracker().end(performanceTrackerId);
		    LOG.info("duration of loading the target list (" + loadResult.getResults().stream().flatMap(resultBean -> resultBean.getReferences().stream()).count() + "): " + interval.toString());
		} catch (TechnicalExceptionLoadGeneratorDefaultException ex) {
			ex.printStackTrace();
			if (ex.getResponseCodeDefault().getMessage().contains("404")) {
				fail("could not load the target file list for the generator '" + this.getGuGeneratorIdAndVersion() + "' since the information in the model file(s) " +
			        getModelFileNames(System.lineSeparator()) + " is not sufficient to load the model (model not found).");
			} else {
				fail("could not load the target file list for the generator '" + this.getGuGeneratorIdAndVersion() + "' and the model file(s) " + getModelFileNames(System.lineSeparator()) +
						", reason: " + ex.getResponseCodeDefault().getMessage());
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			fail("could not load the target file list for the generator '" + this.getGuGeneratorIdAndVersion() + "' since an exception has occurred:" +
			        ex.getMessage() + ", model file(s) used: " + getModelFileNames(System.lineSeparator()));
		}
		
		if (GuClientTester.isLoadResultContainingErrors(loadResult)) {
			Optional<String> messageText = GuClientTester.getLoadResultMessageText(loadResult, MessageTypeEnum.ERROR);
			fail("execution of generator failed during loading of result list: " +
		            (messageText.isPresent() ? messageText.get() : "[no message available]") +
					System.lineSeparator() + "used model files: " + getModelFileNames(System.lineSeparator()));
		}
		
		GuClientTester.printMessages(loadResult);
		
		String performanceTrackerId = "generate files for " + getGeneratorUnderTest().getIdAndVersion();
		getPerformanceTracker().start(performanceTrackerId);
		if (getZippedReferenceTargetFiles() != null && getZippedReferenceTargetFiles().length() > 0) {
			Map<String,Map<String, TargetContentBean>> modifiedTargetFiles =
					GuClientTester.generateAndGetModifiedTargetFiles(this.getClass(), getClient(), getGenerator(),
							getZippedReferenceTargetFiles(), loadResult, null, stripGeneratorInfo);
			long numberOfModifiedTargets = modifiedTargetFiles.values().stream().flatMap(targetMap -> targetMap.values().stream()).count();
			if (numberOfModifiedTargets > 0) {
			    fail("there are " + numberOfModifiedTargets +
						" generated files not having the same content as the zipped reference files: " +
						GuClientTester.getFileNames(modifiedTargetFiles, ","));
			}
		} else {
			Map<String,Map<String, TargetContentBean>> generatedTargetFilesWithErrors =
					GuClientTester.getGeneratedTargetFiles(this.getClass(), getClient(), getGenerator(), loadResult, null, MessageTypeEnum.ERROR);
			long numberOfTargetsWithErrors = generatedTargetFilesWithErrors.values().stream().flatMap(targetMap -> targetMap.values().stream()).count();
			if (numberOfTargetsWithErrors > 0) {
				GuClientTester.printMessagesForMappedTargets(generatedTargetFilesWithErrors);
				fail("there are " + numberOfTargetsWithErrors +
						" files that got generated with errors: " +	GuClientTester.getFileNames(generatedTargetFilesWithErrors, ","));
			}
		}
		Interval interval = getPerformanceTracker().end(performanceTrackerId);
		LOG.info("duration of writing the targets (" + loadResult.getResults().stream().flatMap(resultBean -> resultBean.getReferences().stream()).count() + "): " + interval.toString());
	}
	
	/**
	 * @param baseDir
	 * @param virtualProjectMapping
	 * @return the generated file content
	 */
	protected final Map<String,Map<String,String>> executeGenerationWithExistingContent(String modelDir, String baseDir, Map<String,String> virtualProjectMapping) {
		LoadResultBean loadResult = executeLoadForModelFromFilesystem(modelDir != null ? modelDir : baseDir);
		
		GuClientTester.printMessages(loadResult);
//		List<MessageBean> warningMessages = GuClientTester.getWarningMessages(loadResult, null);
//		if (!warningMessages.isEmpty()) {
//			LOG.info("--- WARNING MESSAGES ---");
//			warningMessages.stream()
//				.forEach(warningMessage -> LOG.info(warningMessage.getMessage()));
//		}
//		
//		List<MessageBean> infoMessages = GuClientTester.getInfoMessages(loadResult, null);
//		if (!infoMessages.isEmpty()) {
//			LOG.info("--- INFO MESSAGES ---");
//			infoMessages.stream()
//				.forEach(infoMessage -> LOG.info(infoMessage.getMessage()));
//		}
		
		Map<String, Map<String, FileBean>> existingContent =
				GuClientTester.readExistingContent(baseDir, loadResult, virtualProjectMapping);
		
		return GuClientTester.generateAndWriteTargetFiles(getClient(), getGenerator(), baseDir, loadResult, existingContent , virtualProjectMapping);
	}
	
	/**
	 * @param existingContent
	 * @return
	 */
	protected final Map<String,Map<String, TargetContentBean>> executeGeneration(Map<String,Map<String,FileBean>> existingContent) {
		String performanceTrackerId = "load target list for " + getGeneratorUnderTest().getIdAndVersion();
		getPerformanceTracker().start(performanceTrackerId);
		List<FileBean> inputFiles = GuClientTester.createInputFiles(this.getClass(), null, getModelFiles());
		logGuConnectionDetails("executeGeneration(), about to call loadGenerator() for generator " + getGenerator().getId());
		LoadResultBean loadResult = getClient().loadGenerator(getGenerator().getId(), inputFiles);
		Interval interval = getPerformanceTracker().end(performanceTrackerId);
		LOG.info("duration of loading the target list (" + loadResult.getResults().stream().flatMap(resultBean -> resultBean.getReferences().stream()).count() + "): " + interval.toString());
		
		if (GuClientTester.isLoadResultContainingErrors(loadResult)) {
			Optional<String> messageText = GuClientTester.getLoadResultMessageText(loadResult, MessageTypeEnum.ERROR);
			fail("execution of generator failed during loading of result list:" + (messageText.isPresent() ? messageText.get() : "[no message available]") +
					System.lineSeparator() + "used model files: " + getModelFileNames(System.lineSeparator()));
		}
		
		GuClientTester.printMessages(loadResult);
		
		performanceTrackerId = "generate files for " + getGeneratorUnderTest().getIdAndVersion();
		getPerformanceTracker().start(performanceTrackerId);
		Map<String, Map<String, TargetContentBean>> result = GuClientTester.getGeneratedTargetFiles(this.getClass(), getClient(), getGenerator(), loadResult, null);
		Interval intervalWrite = getPerformanceTracker().end(performanceTrackerId);
		LOG.info("duration of writing the targets (" + result.values().stream().flatMap(map -> map.values().stream()).count() + "): " + intervalWrite.toString());
		return result;
	}
	
	/**
	 * @param baseDir optional
	 * @return
	 */
	protected final LoadResultBean executeLoadForModelFromClasspath(String baseDir) {
		return executeLoadForModelFromClasspath(baseDir, true);
	}
	
	/**
	 * @param baseDir optional
	 * @param checkForLoadErrors
	 * @return
	 */
	protected final LoadResultBean executeLoadForModelFromClasspath(String baseDir, boolean checkForLoadErrors) {
		List<FileBean> inputFiles = GuClientTester.createInputFiles(this.getClass(), baseDir, getModelFiles());
		return executeLoad(inputFiles, checkForLoadErrors);
	}
	
    /**
     * @param baseDir optional
     * @return
     */
    protected final LoadResultBean executeLoadForModelFromFilesystem(String baseDir) {
    	return executeLoadForModelFromFilesystem(baseDir, true);
	}
    
    /**
     * @param baseDir optional
     * @param checkForLoadErrors
     * @return
     */
    protected final LoadResultBean executeLoadForModelFromFilesystem(String baseDir, boolean checkForLoadErrors) {
    	List<FileBean> inputFiles = GuClientTester.createInputFiles(baseDir, getModelFiles());
		return executeLoad(inputFiles, checkForLoadErrors);
	}
    
    /**
	 * Call this method from a child class to load the target file list for the given input model files.
	 * 
	 * @param inputFiles
	 * @return
	 */
	protected final LoadResultBean executeLoad(List<FileBean> inputFiles) {
		return executeLoad(inputFiles, true);
	}

	/**
	 * Call this method from a child class to load the target file list for the given input model files.
	 * 
	 * @param inputFiles
	 * @param checkForLoadErrors
	 * @return
	 */
	protected final LoadResultBean executeLoad(List<FileBean> inputFiles, boolean checkForLoadErrors) {
		LoadResultBean loadResult = null;
		try {
			String performanceTrackerId = "load target list for " + getGeneratorUnderTest().getIdAndVersion();
			getPerformanceTracker().start(performanceTrackerId);
			logGuConnectionDetails("executeLoad(), about to call loadGenerator() for generator " + getGenerator().getId());
			loadResult = getClient().loadGenerator(getGenerator().getId(), inputFiles);
			Interval interval = getPerformanceTracker().end(performanceTrackerId);
			LOG.info("duration of loading the target list (" + loadResult.getResults().stream().flatMap(resultBean -> resultBean.getReferences().stream()).count() + "): " + interval.toString());
			
			if (checkForLoadErrors && GuClientTester.isLoadResultContainingErrors(loadResult)) {
				Optional<String> messageText = GuClientTester.getLoadResultMessageText(loadResult, MessageTypeEnum.ERROR);
				fail("execution of generator failed during loading of result list: " +
			            (messageText.isPresent() ? messageText.get() : "[no message available]") +
						System.lineSeparator() + "used model files: " + getModelFileNames(System.lineSeparator()));
			}
			
			GuClientTester.printMessages(loadResult);
		} catch (TechnicalExceptionLoadGeneratorDefaultException ex) {
			ex.printStackTrace();
			if (ex.getResponseCodeDefault().getMessage().contains("404")) {
				fail("could not load the target file list for the generator '" + this.getGuGeneratorIdAndVersion() + "' since the information in the model file(s) " +
			        getModelFileNames(System.lineSeparator()) + " is not sufficient to load the model (model not found).");
			} else {
				fail("could not load the target file list for the generator '" + this.getGuGeneratorIdAndVersion() + "' and the model file(s) " + getModelFileNames(System.lineSeparator()) +
						", reason: " + ex.getResponseCodeDefault().getMessage());
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			fail("could not load the target file list for the generator '" + this.getGuGeneratorIdAndVersion() + "' since an exception has occurred:" +
			        ex.getMessage() + ", model file(s) used: " + getModelFileNames(System.lineSeparator()));
		}
		
		return loadResult;
	}
}

package com.vd.gu.test;

/**
 * Create enums that hold information about input models for generation tests
 * and let the enums implement this interface.
 *
 */
public interface ModelProvider {
	
	String getName();
	
	ModelType getModelType();
	
	ModelFileLocation getModelFileLocation();

	String[] getModelFiles();
	
	public enum ModelType {
		GAPP,
		MODELER,
		OPENAPI,
		EXCEL,
		WSDL,
		XSD,
		XML,
		PREDEF,
		;
	}
	
	public enum ModelFileLocation {
		CLASSPTH,
		/**
		 * Use this model file location when you want to use input model information
		 * that is found in a target project, stored in a code repository (GitHub, Bitbucket, ...).
		 */
		FILESYSTEM,
		;
	}
}

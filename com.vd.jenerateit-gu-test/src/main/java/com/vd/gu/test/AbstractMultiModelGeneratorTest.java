package com.vd.gu.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * <p>This class can be used as a parent class in order to provide generation tests
 * without having to write a lot of code. The only thing you have to provide is
 * a set of names of input files for the generation. The test will fail when one
 * of the following is true:
 * <ul>
 *     <li> the load result includes at least one error (bug or validation failure in model access or model converters)
 *     <li> one of the generated target files has been generated with at least one error
 *     <li> one of the input files cannot be successfully read
 *     <li> when you provide a ZIP file with reference files to be compared to the generated files and at least one of them doesn't match (see further down for more details)
 * </ul>
 * 
 * <p>This class lets you write tests that use JUnit's {@link org.junit.runners.Parameterized} annotation.
 * You have to use the class {@link InputFiles} to provide parameters for a parameterized test. When
 * the very last file name that is provided this way ends with ".zip", then this file is assumed to
 * contain reference files for generated files. That ZIP file content is going to be compared to the
 * files that are going to be generated as a result of the test execution.
 *
 */
abstract public class AbstractMultiModelGeneratorTest extends AbstractGeneratorTest {
	
    private InputFiles inputFiles;
    
    private String zippedReferenceTargetFiles;
    
    protected InputFiles getInputFiles() {
		return inputFiles;
	}
    
    protected String[] getModelFiles() {
    	if (inputFiles != null) {
		    return inputFiles.getModelFiles();
    	}
    	return null;
	}
    
	protected String getZippedReferenceTargetFiles() {
		return zippedReferenceTargetFiles;
	}

	public AbstractMultiModelGeneratorTest() {
		super();
    }

	public AbstractMultiModelGeneratorTest(InputFiles inputFiles) {
		super();
    	this.inputFiles = inputFiles;
    	
    	if (inputFiles.getModelFiles().length > 1 && inputFiles.getModelFiles()[inputFiles.getModelFiles().length-1].endsWith(".zip")) {
    		zippedReferenceTargetFiles = inputFiles.getModelFiles()[inputFiles.getModelFiles().length-1];
    		
    		List<String> list = new ArrayList<>(Arrays.asList(inputFiles.getModelFiles()));
    		list.remove(zippedReferenceTargetFiles);
    		this.inputFiles.modelFiles = list.toArray(new String[0]);
    	}
    }
	
	/**
	 * <p>This class encapsulates a list of file names. The corresponding files
	 * make up for the input files for the generator under test. There is one exception:
	 * If the last given file name ends with ".zip", that file is supposed to contain
	 * reference target files that are going to be compared to the files that are
	 * generated as a result of the test execution.
	 * 
	 *
	 */
	public static class InputFiles {
		private String modelName;
		private String[] modelFiles;
		
		public InputFiles(String modelName, String... modelFiles) {
		    super();
		    this.modelName = modelName;
		    if (modelFiles != null) {
    	        this.modelFiles = modelFiles;
		    }
	    }
		
		public InputFiles(ModelProvider modelProvider) {
			this(modelProvider.getName(), modelProvider.getModelFiles());
		}
    
        public String getModelName() {
			return modelName;
		}

		public String[] getModelFiles() {
		    return modelFiles;
	    }

		@Override
		public String toString() {
			String prefix = modelName == null ? "" : modelName + " "; 
			return prefix + Arrays.toString(modelFiles).replace(",", System.lineSeparator());
		}
	}
}

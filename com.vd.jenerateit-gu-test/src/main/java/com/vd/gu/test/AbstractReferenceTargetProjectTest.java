package com.vd.gu.test;

import java.util.Map;

import com.vd.gu.definition.basic.OptionBean;

public abstract class AbstractReferenceTargetProjectTest extends AbstractGeneratorTest {

	public AbstractReferenceTargetProjectTest() {
		super();
	}

	public AbstractReferenceTargetProjectTest(OptionBean... options) {
		super(options);
	}

	public AbstractReferenceTargetProjectTest(String... options) {
		super(options);
	}
	
	/**
	 * @return
	 */
	protected String getModelDir() {
		return getBaseDir();
	}
	
	/**
	 * @return
	 */
	abstract protected String getBaseDir();
	
	/**
	 * @return
	 */
	abstract protected Map<String, String> getVirtualProjectMappings();
	
	/**
	 * @return
	 */
	protected final Map<String,Map<String,String>> executeGenerationWithExistingContent() {
		return executeGenerationWithExistingContent(getModelDir(), getBaseDir(), getVirtualProjectMappings());
	}
}

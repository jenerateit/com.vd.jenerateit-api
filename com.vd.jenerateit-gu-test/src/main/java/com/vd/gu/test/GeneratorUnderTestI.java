package com.vd.gu.test;

public interface GeneratorUnderTestI {

	String getId();
	
	String getVersion();
	
	/**
	 * 
	 */
	default String getIdAndVersion() {
		return getId() + ":" + getVersion();
	}
}

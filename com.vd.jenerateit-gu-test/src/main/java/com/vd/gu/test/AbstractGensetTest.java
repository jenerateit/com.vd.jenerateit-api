package com.vd.gu.test;

import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import org.eclipse.microprofile.config.Config;
import org.eclipse.microprofile.config.ConfigProvider;
import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vd.gu.TechnicalExceptionCreateGeneratorDefaultException;
import com.vd.gu.client.GUClientI;
import com.vd.gu.client.impl.GUClient;
import com.vd.gu.client.impl.GUClientConfiguration;
import com.vd.gu.client.impl.GUClientConfiguration.Logging;
import com.vd.gu.definition.basic.OptionBean;

import okhttp3.logging.HttpLoggingInterceptor;
import okhttp3.logging.HttpLoggingInterceptor.Level;

/**
 * <p>This class encapsulates the configuration and creation of a generation client for a specific generator.
 * You can create tests that inherit from this class and set system properties in an instance initialization block.
 * These settings  provide the configuration for the generation client.
 * 
 * <p>The following keys can be used to set the properties:
 * 
 * <ul>
 *     <li>GU_HOSTNAME
 *     <li>GU_ID
 *     <li>GU_PORT
 *     <li>LOG_WEBSERVICE_CALLS
 *     <li>WRITE_TARGET_FILES
 * </ul>
 * 
 *
 */
abstract public class AbstractGensetTest {
	
	private static final @NotNull Logger LOG = LoggerFactory.getLogger(AbstractGensetTest.class);
	
	/**
	 * <p>Create a list of option beans based on the textual input.
	 * The textual options in the list parameter are split by the
	 * character '|' with the following meaning of its parts:
	 * 
	 * <p>[option key]|[option value]|[virtual project]
	 * 
	 * <p>The virtual project part is optional.
	 * 
	 * @param options
	 * @return
	 */
	public static final List<OptionBean> convertToOptionBeans(List<String> options) {
		final List<OptionBean> result = new ArrayList<>();
		if (options != null) {
			options.stream()
			    .forEach(option -> {
			    	OptionBean optionBean = null;
			    	String[] optionParts = option.split("[|]");
			    	if (optionParts.length == 2) {
			    		optionBean = new OptionBean(optionParts[0].trim(), optionParts[1].trim());
			    	} else if (optionParts.length == 3) {
			    		optionBean = new OptionBean(optionParts[0].trim(), optionParts[1].trim(), optionParts[2].trim());
			    	}
			    	
			    	if (optionBean != null) {
			    		result.add(optionBean);
			    	}
			    });
        }
		return result;
	}
	
	private final PerformanceTracker performanceTracker = new PerformanceTracker(this.getClass().getSimpleName());
    
    private GUClientI client;
	
	private String protocol = "http";
    
    private String guHostname;
    
    private String guId;
    
    private int guPort;
    
    private Integer timeoutConnect = 60000;
    private Integer timeoutRead    = 60000;
    private Integer timeoutWrite   = 60000;
    
    private Boolean logWebserviceCalls;
    
    private Boolean writeTargetFiles;
    
    {
        Config config = ConfigProvider.getConfig();
        guHostname = null;
        try {
			guHostname = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {/*intentionally ignore this*/}
	    
        
	    Optional<String> hostname = config.getOptionalValue("GU_HOSTNAME", String.class);
	    if (hostname.isPresent()) {
	    	guHostname = hostname.get();
	    }
	    
	    Optional<String> id = config.getOptionalValue("GU_ID", String.class);
	    guId = id.isPresent() ? id.get() : "id";
	    
	    Optional<Integer> port = config.getOptionalValue("GU_PORT", Integer.class);
	    guPort = port.isPresent() ? port.get() : 8080;
	    
	    Optional<Boolean> logWebserviceCalls = config.getOptionalValue("LOG_WEBSERVICE_CALLS", Boolean.class);
	    this.logWebserviceCalls = logWebserviceCalls.isPresent() ? logWebserviceCalls.get() : Boolean.FALSE;
	    
	    Optional<Boolean> writeTargetFiles = config.getOptionalValue("WRITE_TARGET_FILES", Boolean.class);
	    this.writeTargetFiles = writeTargetFiles.isPresent() ? writeTargetFiles.get() : Boolean.FALSE;
	}
    
    public AbstractGensetTest() {
		super();
    }
	
	/**
	 * @return a helper object to make simple performance measurements
	 */
	protected PerformanceTracker getPerformanceTracker() {
		return performanceTracker;
	}

	protected GUClientI getClient() {
		return client;
	}
	
	protected String getProtocol() {
		return protocol;
	}
	
	protected void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	@NotNull
	protected String getGuHostname() {
		return guHostname;
	}

	@NotNull
	protected String getGuId() {
		return guId;
	}

	@NotNull
	protected int getGuPort() {
		return guPort;
	}

	@NotNull
	protected Integer getTimeoutConnect() {
		return timeoutConnect;
	}

	@NotNull
	protected Integer getTimeoutRead() {
		return timeoutRead;	
	}

	@NotNull
	protected Integer getTimeoutWrite() {
		return timeoutWrite;
	}

	@NotNull
	protected Boolean getLogWebserviceCalls() {
		return logWebserviceCalls;
	}
	
	@NotNull
	protected Boolean getWriteTargetFiles() {
		return writeTargetFiles;
	}
	
	@Before
	public void setUpClient() throws MalformedURLException {
		GUClientConfiguration configuration = new GUClientConfiguration(new URL(getConnectionUrl()),
				getTimeoutConnect(), getTimeoutRead(), getTimeoutWrite(), "application/json");
		
		if (logWebserviceCalls) {
			HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
			loggingInterceptor.setLevel(Level.BASIC);
			configuration.addInterceptors(loggingInterceptor);
		} else {
			configuration.setLogging(new Logging() {
				@Override
				public void log(String arg0) {}
			});
		}
		this.client = new GUClient(configuration);
	}
	
	protected String getConnectionUrl() {
		return getProtocol() +  "://" + getGuHostname() + ":" + getGuPort() + "/gu/" + getGuId() + "/";
	}
	
	/**
	 * log the connection URL and the used timeouts
	 */
	protected void logGuConnectionDetails(String optionalLogMessage) {
		if (optionalLogMessage != null && !optionalLogMessage.isEmpty()) {
		    LOG.info(optionalLogMessage);
		}
		LOG.info("connection details to access the GU: " + getConnectionUrl());
		LOG.info("timeouts used: connect=" + getTimeoutConnect() + ", read=" + getTimeoutRead() + ", write=" + getTimeoutWrite());
	}
	
	protected Optional<String> getAdditionalErrorInformation(TechnicalExceptionCreateGeneratorDefaultException ex) {
		if (ex != null && ex.getResponseCodeDefault() != null && ex.getResponseCodeDefault().getMessage() != null) {
			String errorText = ex.getResponseCodeDefault().getCode().name() + ": " + ex.getResponseCodeDefault().getMessage();
			return Optional.of(errorText);
		}
		return Optional.empty();
	}
}

package com.vd.gu.test;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Test;

import com.vd.gu.definition.basic.OptionBean;
import com.vd.gu.definition.basic.TargetContentBean;

/**
 * <p>This class can be used as a parent class in order to provide generation tests
 * without having to write a lot of code. The only thing you have to provide is
 * the names of input files for the generation by implementing {@link getModelFiles}.
 * This class doesn't contain any executable test method.
 * However, it contains a method {@link AbstractMultiOptionsGeneratorTest#generate}, that you can overwrite and add
 * a {@link Test} annotation to it. Calling super() then executes the complete generation
 * of the given input model and checks the results for any error. More precisely,
 * that test will fail when one of the following is true:
 * 
 * <ul>
 *     <li> the load result includes at least one error (bug or validation failure in model access or model converters)
 *     <li> one of the generated target files has been generated with at least one error
 *     <li> one of the input files cannot be successfully read
 *     <li> when you provide a ZIP file with reference files to be compared to the generated files and at least one of them doesn't match (see further down for more details)
 * </ul>
 * 
 * <p>This class lets you write tests that use JUnit's {@link org.junit.runners.Parameterized} annotation.
 * You can use this feature to provide a set of generator options. One generator options is provided
 * by means of a string that contains the option name, value and the name of virtual project. The
 * parts of an option are separated by the | character, for example: "target.dir|src|Java Classes"
 * provides an option value "src" for the parameter "target.dir". The option will be applied
 * for the generation group that writes files for the virutal project "Java Classes".
 * 
 * <p>Finally, when you overwrite the method {@link #getZippedReferenceTargetFiles} you
 * can let it return the name of ZIP file that contains reference target files for generated files.
 * That ZIP file content is going to be compared to the files that are going to be generated as a result
 * of the test execution in the {@link #executeGeneration} method.
 *
 */
abstract public class AbstractMultiOptionsGeneratorTest extends AbstractGeneratorTest {
	
	private final GeneratorOptions generatorOptions;
	
	public AbstractMultiOptionsGeneratorTest() {
		super();
		this.generatorOptions = null;
    }

	protected AbstractMultiOptionsGeneratorTest(OptionBean ...options) {
		super(options);
		this.generatorOptions = null;
    }
	
	protected AbstractMultiOptionsGeneratorTest(String ...options) {
		super(options);
		this.generatorOptions = null;
    }
	
	protected AbstractMultiOptionsGeneratorTest(GeneratorOptions generatorOptions) {
		super(generatorOptions.getOptions());
		this.generatorOptions = generatorOptions;
    }
	
	public GeneratorOptions getGeneratorOptions() {
		return generatorOptions;
	}

	/**
	 * Overwrite this method in order to do checks on the generated targets (files),
	 * e.g. checking for the actual content or for WARNING or INFO messages.
	 * 
	 * @param targets
	 */
	protected void onGeneratedTargetFiles(Set<TargetContentBean> targets) {
		
	}
	
	/**
	 * <p>This class encapsulates a list of generator options.
	 *
	 */
	public static class GeneratorOptions {
		
		private String scenarioName;
		private OptionBean[] options;
		
		public GeneratorOptions(String scenarioName, OptionBean ...options) {
		    super();
		    this.scenarioName = scenarioName;
		    if (options != null) {
    	        this.options = options;
		    }
	    }
    
        public String getScenarioName() {
			return scenarioName;
		}

		public OptionBean[] getOptions() {
		    return options;
	    }

		@Override
		public String toString() {
			String prefix = scenarioName == null ? "" : scenarioName + ": ";
			return prefix + Stream.of(options).map(option -> getOptionAsString(option))
			                    .collect(Collectors.joining(","));
		}
		
		public String getOptionAsString(OptionBean option) {
			String result = option.getName() + "|" + option.getValue();
			if (option.getVirtualProject() != null && option.getVirtualProject().length() > 0) {
				result = result + "|" + option.getVirtualProject();
			}
			return result;
		}
	}
}

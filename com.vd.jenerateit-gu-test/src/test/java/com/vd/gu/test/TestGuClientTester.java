package com.vd.gu.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class TestGuClientTester {
	
	private static final String javaTwoJavaDocs = "start\n/**\n"
			+ "* This is a simple JavaDoc.\n"
			+ "*/\nend"
			+ "\n .... \n"
			+ "start2\n/**\n"
			+ "* This is a simple JavaDoc.\n"
			+ "*/\nend2";
	
	private static final String xmlXmlDoc = "start\n<!--\n"
			+ "This is a simple XML Doc"
			+ "-->\nend"
			+ "\n .... \n"
			+ "start2\n<!--\n"
			+ "This is a simple XML Doc"
			+ "-->\nend2";
	
	private static final String xmlXmlSingleLineDoc = "start\n<!-- This is a simple XML Doc -->\nend";
	
	private static final String javaGeneratorInfo = "/**\n"
			+ "* <table>\n"
			+ "* <tr><td>Generator</td><td>com.gs.vd.modeler.generators.capability:1.0</td></tr>\n"
			+ "* <tr><td>Element</td><td><a href=\"https://gs.virtual-developer.com/modeler/modeler/elementview/elementview.xhtml?elementPk=3426\">IDS HighQ Connector Application</a></td></tr>\n"
			+ "* <tr><td>Type</td><td>application-ui (Product)</td></tr>\n"
			+ "* <tr><td>Module</td><td><a href=\"https://gs.virtual-developer.com/modeler/modeler/moduledetailsview/moduledetailsview.xhtml?modulePk=3403\">IDS HighQ Connector Application (0-SNAPSHOT)</a></td></tr>\n"
			+ "* </table>\n"
			+ "*/";
	
	private static final String xmlGeneratorInfo = "<!--  displays a list of accounts -->\n"
			+ "<!--   -->\n"
			+ "<!--  -->\n"
			+ "<!-- Generator: com.gs.gapp.vd-capability:1.11 -->\n"
			+ "<!-- Model: name=AccountListView, type=view, module=UiAccount -->";
	
	@Test
	public void testJavaDocStrippingForJavaFiles() {
		
		System.out.println("content before:" + System.lineSeparator() + javaGeneratorInfo);
		String strippedContent = GuClientTester.stripDocumentation(javaGeneratorInfo);
		System.out.println("content after:" + System.lineSeparator() + strippedContent);
		assertTrue("The number of lines hasn't come down to 0, original text:" +
		    GuClientTester.getNumberOfLines(javaGeneratorInfo) + ", stripped text:" + GuClientTester.getNumberOfLines(strippedContent),
		    0 == GuClientTester.getNumberOfLines(strippedContent));
	}

	@Test
	public void testGeneratorInfoStrippingForXmlFiles() {
		
		System.out.println("content before:" + System.lineSeparator() + xmlGeneratorInfo);
		String strippedContent = GuClientTester.stripDocumentation(xmlGeneratorInfo);
		System.out.println("content after:" + System.lineSeparator() + strippedContent);
		assertTrue("The number of characters hasn't come down to 4 (newlines), original text:" +
			    xmlGeneratorInfo.length() + ", stripped text:" + strippedContent.length(),
			    strippedContent.length() == 4);
	}
	
	@Test
	public void testStripJavaDoc() {
		System.out.println("content before:" + System.lineSeparator() + javaTwoJavaDocs);
		String strippedContent = GuClientTester.stripDocumentation(javaTwoJavaDocs);
		System.out.println("content after:" + System.lineSeparator() + strippedContent);
		
		assertTrue("The stripped JavaDoc still has JavDoc characters in it:" +
				javaTwoJavaDocs + ", stripped text:" + strippedContent,
			    !strippedContent.contains("*") && !strippedContent.contains("/"));
		
		assertTrue("The stripping of the JavaDoc has removed to much:" +
				javaTwoJavaDocs + ", stripped text:" + strippedContent,
			    strippedContent.contains("start") && strippedContent.contains("end") && strippedContent.contains("...."));
	}
	
	@Test
	public void testStripXmlDoc() {
		System.out.println("content before:" + System.lineSeparator() + xmlXmlDoc);
		String strippedContent = GuClientTester.stripDocumentation(xmlXmlDoc);
		System.out.println("content after:" + System.lineSeparator() + strippedContent);
		
		assertTrue("The stripped XML documentation still has documentation characters in it:" +
				xmlXmlDoc + ", stripped text:" + strippedContent,
			    !strippedContent.contains("<!--") && !strippedContent.contains("-->"));
		
		assertTrue("The stripping of the XML documentation has removed to much:" +
				xmlXmlDoc + ", stripped text:" + strippedContent,
			    strippedContent.contains("start") && strippedContent.contains("end") && strippedContent.contains("...."));
		
		System.out.println("content before:" + System.lineSeparator() + xmlXmlSingleLineDoc);
		strippedContent = GuClientTester.stripDocumentation(xmlXmlSingleLineDoc);
		System.out.println("content after:" + System.lineSeparator() + strippedContent);
		
		assertTrue("The stripped XML documentation still has documentation characters in it:" +
				xmlXmlSingleLineDoc + ", stripped text:" + strippedContent,
			    !strippedContent.contains("<!--") && !strippedContent.contains("-->"));
		
		assertTrue("The stripping of the XML documentation has removed to much:" +
				xmlXmlSingleLineDoc + ", stripped text:" + strippedContent,
			    strippedContent.contains("start") && strippedContent.contains("end"));
	}
}

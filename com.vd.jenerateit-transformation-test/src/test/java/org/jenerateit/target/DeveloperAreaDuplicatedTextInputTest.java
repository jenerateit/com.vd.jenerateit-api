package org.jenerateit.target;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.IsNull.nullValue;
import static org.hamcrest.core.StringContains.containsString;
import static org.hamcrest.core.StringStartsWith.startsWith;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.lang.ref.SoftReference;
import java.util.Queue;
import java.util.Set;
import java.util.SortedSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.jenerateit.annotation.GenerationGroup;
import org.jenerateit.annotation.ModelElement;
import org.jenerateit.exception.JenerateITException;
import org.jenerateit.generationgroup.GenerationGroupConfigI;
import org.jenerateit.generationgroup.GenerationGroupI;
import org.jenerateit.generationgroup.GenerationGroupOptions;
import org.jenerateit.project.TargetProjectI;
import org.jenerateit.transformation.TransformationTarget;
import org.jenerateit.util.InjectorTools;
import org.jenerateit.writer.WriterI;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.vd.transformation.data.TransformationRequest;

/**
 * special test class to test the comment end is null
 * @author hrr
 *
 */
public class DeveloperAreaDuplicatedTextInputTest {

	private final TransformationTarget tt = new TransformationTarget();
	private final GenerationGroupI gg = new GenerationGroupI() {
		
		@Override
		public Class<? extends WriterI> getWriterClass(Object element, TargetI<?> target) {
			return null;
		}
		
		@Override
		public Class<? extends WriterI> getWriterClass(Object element, Class<? extends TargetI<?>> targetClass) {
			return SimpleTextTextWriter.class;
		}
		
		@Override
		public String getName() {
			return "test";
		}
		
		@Override
		public String getDescription() {
			return "Just a test";
		}
		
		@Override
		public boolean isGenerationProtocol() {
			// set to true to avoid calling getProject()
			return true;
		}
		
		@Override
		public int compareTo(GenerationGroupI o) {
			return 0;
		}
		
		private final ConcurrentHashMap<Class<? extends WriterI>, Queue<SoftReference<WriterI>>> writerCache
			= new ConcurrentHashMap<>();
		@Override
		public ConcurrentMap<Class<? extends WriterI>, Queue<SoftReference<WriterI>>> getWriterCache() {
			return writerCache;
		}
		
		@Override
		public String getVersion() {
			return "0.0.0";
		}
		
		@Override
		public TargetProjectI getTargetProject() {
			return null;
		}
		
		@Override
		public GenerationGroupOptions getOptions() {
			return new GenerationGroupOptions();
		}
		
		@Override
		public Serializable getOption(String key) {
			return null;
		}
		
		@Override
		public GenerationGroupConfigI getConfig() {
			return null;
		}
		
		@Override
		public SortedSet<TargetI<? extends TargetDocumentI>> findTargets(Set<? extends Object> metaModels,
				Class<? extends TargetI<? extends TargetDocumentI>> targetFilter, Class<? extends WriterI> writerFilter) {
			return null;
		}
	};
	
	
	private TestTextTarget target = null;
	
	@Before
	public void before() {
		gg.getWriterCache().clear();
		this.target = new TestTextTarget();
		try {
			InjectorTools.injectFields(this.target, GenerationGroup.class, this.gg);
			InjectorTools.injectFields(this.target, ModelElement.class, new Object());
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
			fail("Error while prepare generation group within target");
		}
		this.tt.setCurrentTarget(target);
	}

	@After
	public void after() {
		this.target = null;
		this.tt.setCurrentTarget(null);
	}
	
	@Test
	public void duplicatedDeveloperArea() {
		try (final InputStream input = new ByteArrayInputStream(new StringBuilder()
				.append("# just a test").append("\n")
				.append("#DA-START:test.text.content:DA-START").append("\n")
				.append("# this is my comment test").append("\n")
				.append("#DA-ELSE:test.text.content:DA-ELSE").append("\n")
				.append("# this is a comment test").append("\n")
				.append("#DA-END:test.text.content:DA-END").append("\n")
				.append("#DA-START:test.text.content:DA-START").append("\n")
				.append("# this is my comment test").append("\n")
				.append("#DA-ELSE:test.text.content:DA-ELSE").append("\n")
				.append("# this is a comment test").append("\n")
				.append("#DA-END:test.text.content:DA-END").append("\n")
				.toString().getBytes())) {
			final TransformationRequest request = new TransformationRequest();
			request.setModified(true);
			request.setNew(false);
			target.transform(request, input);
			fail("Exception expected");

		} catch (TargetException e) {
			assertThat(e.getMessage(), startsWith("While parsing the modified content from the file sent by the user, a problem with a developer area occurred: Found a new developer area with ID 'test.text.content' at position"));
			assertThat(e.getMessage(), containsString(", but there is already a developer area with the same ID at position "));
			assertThat(target.getErrors(), not(nullValue()));
			assertThat(target.getErrors().isEmpty(), is(Boolean.TRUE));
		} catch (Exception e) {
			fail("Unexpected Exception: " + e.getClass() + "(" + JenerateITException.class + ":" 
					+ e.getMessage() + ")");
		}
	}
}

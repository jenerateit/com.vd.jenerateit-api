package org.jenerateit.target;

import java.util.Arrays;
import java.util.SortedSet;
import java.util.TreeSet;


public class SimpleTextTextTargetDocument extends AbstractTextTargetDocument {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6159313610453487828L;

	public final static TargetSection SIMPLE = new TargetSection("simple", 10);
	
	private static final SortedSet<TargetSection> SECTIONS = new TreeSet<TargetSection>(
			Arrays.asList(new  TargetSection[] {SIMPLE }));
	
	public SimpleTextTextTargetDocument() {
		super();
	}

	public SortedSet<TargetSection> getTargetSections() {
		return SECTIONS;
	}

	@Override
	public CharSequence getCommentStart() {
		return "#";
	}

	@Override
	public CharSequence getCommentEnd() {
		return null;
	}

	@Override
	public char getPrefixChar() {
		return ' ';
	}

}

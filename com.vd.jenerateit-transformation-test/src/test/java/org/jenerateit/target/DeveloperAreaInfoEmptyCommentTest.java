/*
 *  Virtual Developer - Code Generation the easy way
 *
 *  http://www.virtual-developer.com
 *
 *  Copyright:
 *    2007-2013 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    commercial license only
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit.target;

import java.util.Arrays;
import java.util.Collection;
import java.util.SortedSet;
import java.util.TreeSet;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 * @author hrr
 *
 */
@RunWith(Parameterized.class)
public class DeveloperAreaInfoEmptyCommentTest {

	public static final TargetSection SECTION_1 = new TargetSection("1", 1);
	public static final TargetSection SECTION_2 = new TargetSection("2", 2, 4);
	public static final TargetSection SECTION_3 = new TargetSection("3", 3, 4);
	public static final TargetSection SECTION_4 = new TargetSection("4", 4);
	/**
	 * 
	 * @author hrr
	 *
	 */
	protected class TestTargetDocument extends AbstractTextTargetDocument {

		/**
		 * 
		 */
		private static final long serialVersionUID = -5066609279222487366L;

		public TestTargetDocument() {
			super();
		}

		private SortedSet<TargetSection> sections = null;
		
		public SortedSet<TargetSection> getTargetSections() {
			if (this.sections == null) {
				this.sections = new TreeSet<TargetSection>(Arrays.asList(new TargetSection[] {
						SECTION_4,
						SECTION_3,
						SECTION_2,
						SECTION_1
				}));
			}
			return sections;
		}

		public String getCommentEnd() {
			return "";
		}

		public String getCommentStart() {
			return "#";
		}

		public char getPrefixChar() {
			return ' ';
		}

	}

	private final String id;
	private final TestTargetDocument document = new TestTargetDocument();

	/**
	 * 
	 */
	public DeveloperAreaInfoEmptyCommentTest(String id) {
		this.id = id;
	}
	
	@Parameterized.Parameters
	public static Collection<String[]> getIds() {
		return Arrays.asList(new String[][] {
				{"abc"}, // 0
				{"abc.def"},
				{"abc.1"},
				{"abc.1 "},
				{"abc_def_ghi_jkl"},
				{"abc.def.ghi.jkl"},
				{"abc.def.GHI_JKL.1"},
				{"abc.def\tGHI_JKL.1"},
				{" abc"},
				{"abc "},
				{"a b c"},
				{"abc"},
				{"abc_"},
				{"_abc"},
				{"_abc_"}, // 15
				{"abc_def_123"},
				{"abc.def"},
				{"abc_._def"},
				{"abc.def.123_"},
				{".abc.def.ghi"}, // 20
				{"abc.def.ghi."},
				{".abc.def.ghi."},
				{"abc-def-ghi"},
				{"java.lang.String[]"},
				{"java.util.Set<Integer>"},
				{"java.util.Map<java.lang.Integer, ? extends java.util.List>"},
		});
	}

	@Test
	public void testId() {
		this.document.checkDeveloperAreaId(this.id);
	}
}

package org.jenerateit.target;

import java.net.URI;
import java.net.URISyntaxException;

import org.jenerateit.exception.JenerateITException;



/**
 *
 * @author hrr
 */
public class TestTextTarget extends AbstractTextTarget<SimpleTextTextTargetDocument> {


	public String getDescription() {
		return "test text target";
	}
	
	
	/* (non-Javadoc)
	 * @see com.gs.jenerateit.target.TargetI#getTargetURI()
	 */
	@Override
	public URI getTargetURI() {
		try {
			return new URI("test/test.txt");
		} catch (URISyntaxException e) {
			throw new JenerateITException("Error while create Target URI", e);
		}
	}

}

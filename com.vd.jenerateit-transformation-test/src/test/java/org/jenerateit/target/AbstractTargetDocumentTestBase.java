package org.jenerateit.target;

import java.util.Arrays;
import java.util.SortedSet;
import java.util.TreeSet;


public class AbstractTargetDocumentTestBase {

	public static final TargetSection SECTION_1 = new TargetSection("1", 1);
	public static final TargetSection SECTION_2 = new TargetSection("2", 2, 4);
	public static final TargetSection SECTION_3 = new TargetSection("3", 3, 4);
	public static final TargetSection SECTION_4 = new TargetSection("4", 4);

	/**
	 * 
	 * @author hrr
	 *
	 */
	protected class TestTargetDocument extends AbstractTextTargetDocument {

		/**
		 * 
		 */
		private static final long serialVersionUID = -5066609279222487366L;

		public TestTargetDocument() {
			super();
		}

		private SortedSet<TargetSection> sections = null;
		
		public SortedSet<TargetSection> getTargetSections() {
			if (this.sections == null) {
				this.sections = new TreeSet<TargetSection>(Arrays.asList(new TargetSection[] {
						SECTION_4,
						SECTION_3,
						SECTION_2,
						SECTION_1
				}));
			}
			return sections;
		}

		public String getCommentEnd() {
			return null;
		}

		public String getCommentStart() {
			return "#";
		}

		public char getPrefixChar() {
			return ' ';
		}

	}

	public static final TargetSection DEFAULT_SECTION = new TargetSection("default", 1);

	protected class DefaultTargetDocument extends AbstractTextTargetDocument {

		/**
		 * 
		 */
		private static final long serialVersionUID = 7206281298950078271L;

		public DefaultTargetDocument() {
			super();
		}

		private SortedSet<TargetSection> sections = null;

		public String getCommentEnd() {
			// TODO Auto-generated method stub
			return null;
		}

		public String getCommentStart() {
			// TODO Auto-generated method stub
			return null;
		}

		public char getPrefixChar() {
			return 0;
		}

		public SortedSet<TargetSection> getTargetSections() {
			if (this.sections == null) {
				this.sections = new TreeSet<TargetSection>(Arrays.asList(new TargetSection[] {DEFAULT_SECTION}));
			}
			return sections;
		} 
		
	}
}

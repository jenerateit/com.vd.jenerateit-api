package org.jenerateit.target;

import java.util.Map;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.writer.AbstractTextWriter;
import org.jenerateit.writer.WriterException;

public class DifferentNewlineTextWriter extends AbstractTextWriter {

	@ModelElement
	private Object modelElement;
	
	@Override
	public void transform(TargetSection ts) throws WriterException {
		if (String.class.isInstance(modelElement)) {
			wNL("First line with system newline");
			w("Second line with MacOS newline\rThird line with UNIX newline\nFourth line with Window newline\r\n");
			wNL("Last line with system newline");
		} else if (Map.class.isInstance(modelElement)) {
			wNL("First line with system newline");
			bDA("test.text.one");
			w("Second line with MacOS newline\rThird line with UNIX newline\nFourth line with Window newline\r\n");
			eDA();
			wNL("Last line with system newline");			
		}
	}

}

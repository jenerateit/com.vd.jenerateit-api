package org.jenerateit.target;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.util.StringTools;
import org.jenerateit.writer.AbstractTextWriter;
import org.jenerateit.writer.WriterException;

public class SimpleXmlTextWriterWithDuplicatedDeveloperAreas extends AbstractTextWriter {

	@ModelElement
	private Object modelElement;
	
	@Override
	public void transform(TargetSection ts) throws WriterException {
		write(ts, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>", StringTools.NEWLINE);
		write(ts, "<root>", StringTools.NEWLINE);
		indent();
			write(ts, "<element>", StringTools.NEWLINE);
			
			bDA("test.xml.content");
			write(ts, 
					getTextTransformationTarget().getNewTargetDocument().getCommentStart(), 
					" this is a comment test ", 
					getTextTransformationTarget().getNewTargetDocument().getCommentEnd());
			eDA();
			bDA("test.xml.content");
			write(ts, 
					getTextTransformationTarget().getNewTargetDocument().getCommentStart(), 
					" this is a test that should not be written since the da is duplicated ", 
					getTextTransformationTarget().getNewTargetDocument().getCommentEnd());
			eDA();
			write(ts, "</element>", StringTools.NEWLINE);
		outdent();
		write(ts, "<root>", StringTools.NEWLINE);


	}

}

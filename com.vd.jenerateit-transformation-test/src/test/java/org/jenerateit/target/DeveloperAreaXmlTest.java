package org.jenerateit.target;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.Map;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.util.InjectorTools;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.vd.transformation.data.TransformationRequest;


public class DeveloperAreaXmlTest extends DeveloperAreaTestBase {

	@Override
	@Before
	public void before() {
		super.before();
	}

	@Override
	@After
	public void after() {
		super.after();
	}
	
	@Test
	public void simpleXml() {
		try {
			InjectorTools.injectFields(this.target, ModelElement.class, new Object());
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
			fail("Error while prepare model element within target");
		}
		final TransformationRequest request = new TransformationRequest();
		request.setModified(false);
		request.setNew(true);
		target.transform(request, null);
		
		assertThat(target.getNewTargetDocument().toString(), 
				is("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<root>\n    <element>\n    </element>\n<root>\n"));
	}
	
	@Test
	public void simpleMultilineXml() {
		try {
			final Map<String, String> props = new LinkedHashMap<>();
			props.put("key.1", "value.1");
			props.put("key.2", "value.2");
			props.put("key.3", "value.3");
			InjectorTools.injectFields(this.target, ModelElement.class, props);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
			fail("Error while prepare model element within target");
		}
		final TransformationRequest request = new TransformationRequest();
		request.setModified(false);
		request.setNew(true);
		target.transform(request, null);
		
		assertThat(target.getNewTargetDocument().toString(), 
				is("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<root>\n    <element>\n    <!--DA-START:test.xml.content:DA-START-->\n    <!--DA-ELSE:test.xml.content:DA-ELSE-->\n    <!--key.1 = value.1\n    key.2 = value.2\n    key.3 = value.3\n    -->\n    <!--DA-END:test.xml.content:DA-END-->\n    </element>\n<root>\n"));
	}
	
	@Test
	public void emptyDeveloperAreaXml() {
		try {
			InjectorTools.injectFields(this.target, ModelElement.class, new String());
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
			fail("Error while prepare model element within target");
		}

		try (final InputStream input = new ByteArrayInputStream(new StringBuilder()
				.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>").append("\n")
				.append("<root>").append("\n")
				.append("    <element>").append("\n")
				.append("    <!--DA-START:test.xml.content:DA-START-->").append("\n")
				.append("    <!--DA-ELSE:test.xml.content:DA-ELSE-->").append("\n")
				.append("    <!-- this is a comment test -->").append("\n")
				.append("    <!--DA-END:test.xml.content:DA-END-->").append("\n")
				.append("    </element>").append("\n")
				.append("<root>").append("\n")
				.toString().getBytes())) {
			final TransformationRequest request = new TransformationRequest();
			request.setModified(false);
			request.setNew(false);
			target.transform(request, input);
		} catch (IOException e) {
			e.printStackTrace();
			fail("Error while load previous document");
		}
		
		assertThat(target.getNewTargetDocument().toString(), 
				is("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<root>\n    <element>\n    <!--DA-START:test.xml.content:DA-START-->\n    <!--DA-ELSE:test.xml.content:DA-ELSE-->\n    <!-- this is a comment test -->\n    <!--DA-END:test.xml.content:DA-END-->\n    </element>\n<root>\n"));
	}

	@Test
	public void emptyDeveloperAreaWithNewlineXml() {
		try {
			InjectorTools.injectFields(this.target, ModelElement.class, new Long(0));
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
			fail("Error while prepare model element within target");
		}

		try (final InputStream input = new ByteArrayInputStream(new StringBuilder()
				.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>").append("\n")
				.append("<root>").append("\n")
				.append("    <element>").append("\n")
				.append("    <!--DA-START:test.xml.content:DA-START-->").append("\n")
				.append("    <!--DA-ELSE:test.xml.content:DA-ELSE-->").append("\n")
				.append("    <!-- this is a comment test -->").append("\n")
				.append("    <!--DA-END:test.xml.content:DA-END-->").append("\n")
				.append("    </element>").append("\n")
				.append("<root>").append("\n")
				.toString().getBytes())) {
			final TransformationRequest request = new TransformationRequest();
			request.setModified(false);
			request.setNew(false);
			target.transform(request, input);
		} catch (IOException e) {
			e.printStackTrace();
			fail("Error while load previous document");
		}
		
		assertThat(target.getNewTargetDocument().toString(), 
				is("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<root>\n    <element>\n    <!--DA-START:test.xml.content:DA-START-->\n    <!--DA-ELSE:test.xml.content:DA-ELSE-->\n    <!-- this is a comment test -->\n    <!--DA-END:test.xml.content:DA-END-->\n    </element>\n<root>\n"));
	}

	@Test
	public void overwrittenDeveloperAreaWithNewlineXml() {
		try {
			InjectorTools.injectFields(this.target, ModelElement.class, new Long(0));
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
			fail("Error while prepare model element within target");
		}

		try (final InputStream input = new ByteArrayInputStream(new StringBuilder()
				.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>").append("\n")
				.append("<root>").append("\n")
				.append("    <element>").append("\n")
				.append("    <!--DA-START:test.xml.content:DA-START-->").append("\n")
				.append("    <!-- this is my comment test -->").append("\n")
				.append("    <!--DA-ELSE:test.xml.content:DA-ELSE-->").append("\n")
				.append("    <!-- this is a comment test -->").append("\n")
				.append("    <!--DA-END:test.xml.content:DA-END-->").append("\n")
				.append("    </element>").append("\n")
				.append("<root>").append("\n")
				.toString().getBytes())) {
			final TransformationRequest request = new TransformationRequest();
			request.setModified(false);
			request.setNew(false);
			target.transform(request, input);
		} catch (IOException e) {
			e.printStackTrace();
			fail("Error while load previous document");
		}
		
		assertThat(target.getNewTargetDocument().toString(), 
				is("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<root>\n    <element>\n    <!--DA-START:test.xml.content:DA-START-->\n    <!-- this is my comment test -->\n    <!--DA-ELSE:test.xml.content:DA-ELSE-->\n    <!-- this is a comment test -->\n    <!--DA-END:test.xml.content:DA-END-->\n    </element>\n<root>\n"));
	}

	@Test
	public void overwrittenDeveloperAreaXml() {
		try {
			final Map<String, String> props = new LinkedHashMap<>();
			props.put("key.1", "value.1");
			props.put("key.2", "value.2");
			props.put("key.3", "value.3");
			InjectorTools.injectFields(this.target, ModelElement.class, props);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
			fail("Error while prepare model element within target");
		}

		try (final InputStream input = new ByteArrayInputStream(new StringBuilder()
				.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>").append("\n")
				.append("<root>").append("\n")
				.append("    <element>").append("\n")
				.append("    <!--DA-START:test.xml.content:DA-START-->").append("\n")
				.append("    <!-- this is my comment test -->").append("\n")
				.append("    <!--DA-ELSE:test.xml.content:DA-ELSE-->").append("\n")
				.append("    <!-- this is a comment test -->").append("\n")
				.append("    <!--DA-END:test.xml.content:DA-END-->").append("\n")
				.append("    </element>").append("\n")
				.append("<root>").append("\n")
				.toString().getBytes())) {
			final TransformationRequest request = new TransformationRequest();
			request.setModified(false);
			request.setNew(false);
			target.transform(request, input);
		} catch (IOException e) {
			e.printStackTrace();
			fail("Error while load previous document");
		}
		
		assertThat(target.getNewTargetDocument().toString(), 
				is("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<root>\n    <element>\n    <!--DA-START:test.xml.content:DA-START-->\n    <!-- this is my comment test -->\n    <!--DA-ELSE:test.xml.content:DA-ELSE-->\n    <!--key.1 = value.1-->\n    <!--key.2 = value.2-->\n    <!--key.3 = value.3-->\n    <!---->\n    <!--DA-END:test.xml.content:DA-END-->\n    </element>\n<root>\n"));
	}

	@Test
	public void overwrittenDeveloperAreaXmlWithSpaces() {
		try {
			final Map<String, String> props = new LinkedHashMap<>();
			props.put("key.1", "value.1");
			props.put("key.2", "value.2");
			props.put("key.3", "value.3");
			InjectorTools.injectFields(this.target, ModelElement.class, props);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
			fail("Error while prepare model element within target");
		}

		try (final InputStream input = new ByteArrayInputStream(new StringBuilder()
				.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>").append("\n")
				.append("<root>").append("\n")
				.append("    <element>").append("\n")
				.append("    <!-- DA-START:test.xml.content:DA-START -->").append("\n")
				.append("    <!-- this is my comment test -->").append("\n")
				.append("    <!-- DA-ELSE:test.xml.content:DA-ELSE -->").append("\n")
				.append("    <!-- this is a comment test -->").append("\n")
				.append("    <!-- DA-END:test.xml.content:DA-END -->").append("\n")
				.append("    </element>").append("\n")
				.append("<root>").append("\n")
				.toString().getBytes())) {
			final TransformationRequest request = new TransformationRequest();
			request.setModified(false);
			request.setNew(false);
			target.transform(request, input);
		} catch (IOException e) {
			e.printStackTrace();
			fail("Error while load previous document");
		}
		
		assertThat(target.getNewTargetDocument().toString(), 
				is("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<root>\n    <element>\n    <!--DA-START:test.xml.content:DA-START-->\n    <!-- this is my comment test -->\n    <!--DA-ELSE:test.xml.content:DA-ELSE-->\n    <!--key.1 = value.1-->\n    <!--key.2 = value.2-->\n    <!--key.3 = value.3-->\n    <!---->\n    <!--DA-END:test.xml.content:DA-END-->\n    </element>\n<root>\n"));
	}

}

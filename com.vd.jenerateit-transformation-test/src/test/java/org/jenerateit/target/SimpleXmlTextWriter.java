package org.jenerateit.target;

import java.util.Map;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.util.StringTools;
import org.jenerateit.writer.AbstractTextWriter;
import org.jenerateit.writer.WriterException;

public class SimpleXmlTextWriter extends AbstractTextWriter {

	@ModelElement
	private Object modelElement;
	
	@Override
	public void transform(TargetSection ts) throws WriterException {
		write(ts, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>", StringTools.NEWLINE);
		write(ts, "<root>", StringTools.NEWLINE);
		indent();
			write(ts, "<element>", StringTools.NEWLINE);
			
			if (Map.class.isInstance(modelElement)) {
				final Map<?,?> props = Map.class.cast(modelElement);
				bDA("test.xml.content");
				write(ts, 
						getTextTransformationTarget().getCommentStart()); 
				for (final Object key : props.keySet()) {
					write(ts, key.toString(), " = ", props.get(key.toString()).toString(), StringTools.NEWLINE);
				}
				write(ts,
						getTextTransformationTarget().getCommentEnd(),
						StringTools.NEWLINE);
				eDA();
			} else if (String.class.isInstance(modelElement)) {
				bDA("test.xml.content");
				write(ts, 
						getTextTransformationTarget().getCommentStart(), 
						" this is a comment test ", 
						getTextTransformationTarget().getCommentEnd());
				eDA();
			} else if (Long.class.isInstance(modelElement)) {
				bDA("test.xml.content");
				write(ts, 
						getTextTransformationTarget().getCommentStart(), 
						" this is a comment test ", 
						getTextTransformationTarget().getCommentEnd(),
						StringTools.NEWLINE);
				eDA();
			}
			
			write(ts, "</element>", StringTools.NEWLINE);
		outdent();
		write(ts, "<root>", StringTools.NEWLINE);


	}

}

package org.jenerateit.target;

import java.util.Map;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.util.StringTools;
import org.jenerateit.writer.AbstractTextWriter;
import org.jenerateit.writer.WriterException;

public class SimpleTextTextWriter extends AbstractTextWriter {

	@ModelElement
	private Object modelElement;
	
	@Override
	public void transform(TargetSection ts) throws WriterException {
		write(ts, "# test text target file", StringTools.NEWLINE);
			
		if (Map.class.isInstance(modelElement)) {
			final Map<?,?> props = Map.class.cast(modelElement);
			bDA("test.text.content");
			for (final Object key : props.keySet()) {
				write(ts, getTextTransformationTarget().getNewTargetDocument().getCommentStart(), " "); 
				write(ts, key.toString(), " = ", props.get(key.toString()).toString());
				write(ts,
						getTextTransformationTarget().getNewTargetDocument().getCommentEnd(),
						StringTools.NEWLINE);
			}
			eDA();
		} else if (String.class.isInstance(modelElement)) {
			bDA("test.text.content");
			write(ts, 
					getTextTransformationTarget().getNewTargetDocument().getCommentStart(), 
					" this is a comment test ", 
					getTextTransformationTarget().getNewTargetDocument().getCommentEnd());
			eDA();
		} else if (Long.class.isInstance(modelElement)) {
			bDA("test.text.content");
			write(ts, 
					getTextTransformationTarget().getNewTargetDocument().getCommentStart(), 
					" this is a comment test ", 
					getTextTransformationTarget().getNewTargetDocument().getCommentEnd(),
					StringTools.NEWLINE);
			eDA();
		}
		
	}

}

package org.jenerateit.target;

import java.net.URI;
import java.net.URISyntaxException;

import org.jenerateit.exception.JenerateITException;



/**
 *
 * @author hrr
 */
public class TestXmlTarget extends AbstractTextTarget<SimpleXmlTextTargetDocument> {


	public String getDescription() {
		return "test XML target";
	}
	
	
	/* (non-Javadoc)
	 * @see com.gs.jenerateit.target.TargetI#getTargetURI()
	 */
	@Override
	public URI getTargetURI() {
		try {
			return new URI("test/test.xml");
		} catch (URISyntaxException e) {
			throw new JenerateITException("Error while create Target URI", e);
		}
	}

}

package org.jenerateit.target;

import java.util.Arrays;
import java.util.SortedSet;
import java.util.TreeSet;


public class SimpleTargetDocument extends AbstractTargetDocument {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6159313610843487828L;

	public final static TargetSection SIMPLE = new TargetSection("simple", 10);
	
	private static final SortedSet<TargetSection> SECTIONS = new TreeSet<TargetSection>(
			Arrays.asList(new  TargetSection[] {SIMPLE }));
	
	public SimpleTargetDocument() {
		super();
	}

	public SortedSet<TargetSection> getTargetSections() {
		return SECTIONS;
	}

}

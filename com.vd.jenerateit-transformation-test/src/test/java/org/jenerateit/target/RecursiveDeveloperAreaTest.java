 package org.jenerateit.target;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.lang.ref.SoftReference;
import java.util.Queue;
import java.util.Set;
import java.util.SortedSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.jenerateit.annotation.GenerationGroup;
import org.jenerateit.annotation.ModelElement;
import org.jenerateit.generationgroup.GenerationGroupConfigI;
import org.jenerateit.generationgroup.GenerationGroupI;
import org.jenerateit.generationgroup.GenerationGroupOptions;
import org.jenerateit.project.TargetProjectI;
import org.jenerateit.transformation.TransformationTarget;
import org.jenerateit.util.InjectorTools;
import org.jenerateit.util.StringTools;
import org.jenerateit.writer.WriterI;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.vd.transformation.data.TransformationRequest;

public class RecursiveDeveloperAreaTest {

	private final TransformationTarget tt = new TransformationTarget();
	private final GenerationGroupI gg = new GenerationGroupI() {
		
		@Override
		public Class<? extends WriterI> getWriterClass(Object element, TargetI<?> target) {
			return null;
		}
		
		@Override
		public Class<? extends WriterI> getWriterClass(Object element, Class<? extends TargetI<?>> targetClass) {
			return SimpleXmlTextWriterWithRecursiveDeveloperAreas.class;
		}
		
		@Override
		public String getName() {
			return "test";
		}
		
		@Override
		public String getDescription() {
			return "Just a test";
		}
		
		@Override
		public boolean isGenerationProtocol() {
			// set to true to avoid calling getProject()
			return true;
		}
		
		@Override
		public int compareTo(GenerationGroupI o) {
			// TODO Auto-generated method stub
			return 0;
		}
		
		private final ConcurrentHashMap<Class<? extends WriterI>, Queue<SoftReference<WriterI>>> writerCache
			= new ConcurrentHashMap<>();
		@Override
		public ConcurrentMap<Class<? extends WriterI>, Queue<SoftReference<WriterI>>> getWriterCache() {
			return writerCache;
		}
		
		@Override
		public String getVersion() {
			return "0.0.0";
		}
		
		@Override
		public TargetProjectI getTargetProject() {
			return null;
		}
		
		@Override
		public GenerationGroupOptions getOptions() {
			return new GenerationGroupOptions();
		}
		
		@Override
		public Serializable getOption(String key) {
			return null;
		}
		
		@Override
		public GenerationGroupConfigI getConfig() {
			return null;
		}
		
		@Override
		public SortedSet<TargetI<? extends TargetDocumentI>> findTargets(Set<? extends Object> metaModels,
				Class<? extends TargetI<? extends TargetDocumentI>> targetFilter, Class<? extends WriterI> writerFilter) {
			return null;
		}
	};
	
	
	private TestXmlTarget target = null;
	
	@Before
	public void before() {
		gg.getWriterCache().clear();
		this.target = new TestXmlTarget();
		try {
			InjectorTools.injectFields(this.target, GenerationGroup.class, this.gg);
			InjectorTools.injectFields(this.target, ModelElement.class, new Object());
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
			fail("Error while prepare generation group within target");
		}
		this.tt.setCurrentTarget(target);
	}

	@After
	public void after() {
		this.target = null;
		this.tt.setCurrentTarget(null);
	}
	
	@Test
	public void emptyDeveloperAreaXml() {
		String doc = new StringBuilder()
				.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>").append(StringTools.NEWLINE)
				.append("<root>").append(StringTools.NEWLINE)
				.append("    <element>").append(StringTools.NEWLINE)
				.append("    <!--DA-START:test.xml.element.content:DA-START-->").append(StringTools.NEWLINE)
				.append("    <!--DA-ELSE:test.xml.element.content:DA-ELSE-->").append(StringTools.NEWLINE)
				.append("        <more-element>").append(StringTools.NEWLINE)
				.append("        <!--DA-START:test.xml.element.moreelement.content:DA-START-->").append(StringTools.NEWLINE)
				.append("        <!--DA-ELSE:test.xml.element.moreelement.content:DA-ELSE-->").append(StringTools.NEWLINE)
				.append("            <two-more-element>").append(StringTools.NEWLINE)
				.append("            <!--DA-START:test.xml.element.moreelement.twomoreelement.content:DA-START-->").append(StringTools.NEWLINE)
				.append("            <!--DA-ELSE:test.xml.element.moreelement.twomoreelement.content:DA-ELSE-->").append(StringTools.NEWLINE)
				.append("                <three-more-element>").append(StringTools.NEWLINE)
				.append("                <!--DA-START:test.xml.element.moreelement.twomoreelement.threemoreelement.content:DA-START-->").append(StringTools.NEWLINE)
				.append("                <!--DA-ELSE:test.xml.element.moreelement.twomoreelement.threemoreelement.content:DA-ELSE-->").append(StringTools.NEWLINE)
				.append("                    42").append(StringTools.NEWLINE)
				.append("                <!--DA-END:test.xml.element.moreelement.twomoreelement.threemoreelement.content:DA-END-->").append(StringTools.NEWLINE)
				.append("                </three-more-element>").append(StringTools.NEWLINE)
				.append("            <!--DA-END:test.xml.element.moreelement.twomoreelement.content:DA-END-->").append(StringTools.NEWLINE)
				.append("            </two-more-element>").append(StringTools.NEWLINE)
				.append("        <!--DA-END:test.xml.element.moreelement.content:DA-END-->").append(StringTools.NEWLINE)
				.append("        </more-element>").append(StringTools.NEWLINE)
				.append("    <!--DA-END:test.xml.element.content:DA-END-->").append(StringTools.NEWLINE)
				.append("    </element>").append(StringTools.NEWLINE)
				.append("<root>").append(StringTools.NEWLINE)
				.toString();
		
		
		try (final InputStream input = new ByteArrayInputStream(doc.getBytes())) {
			final TransformationRequest request = new TransformationRequest();
			request.setModified(false);
			request.setNew(false);
			target.transform(request, input);
		} catch (IOException e) {
			e.printStackTrace();
			fail("Error while load previous document");
		}
		
		assertThat(target.getNewTargetDocument().toString(), is(doc));
	}
	
	@Test
	public void changedInnerDeveloperAreaXml() {
		String doc = new StringBuilder()
				.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>").append(StringTools.NEWLINE)
				.append("<root>").append(StringTools.NEWLINE)
				.append("    <element>").append(StringTools.NEWLINE)
				.append("    <!--DA-START:test.xml.element.content:DA-START-->").append(StringTools.NEWLINE)
				.append("    <!--DA-ELSE:test.xml.element.content:DA-ELSE-->").append(StringTools.NEWLINE)
				.append("        <more-element>").append(StringTools.NEWLINE)
				.append("        <!--DA-START:test.xml.element.moreelement.content:DA-START-->").append(StringTools.NEWLINE)
				.append("        <!--DA-ELSE:test.xml.element.moreelement.content:DA-ELSE-->").append(StringTools.NEWLINE)
				.append("            <two-more-element>").append(StringTools.NEWLINE)
				.append("            <!--DA-START:test.xml.element.moreelement.twomoreelement.content:DA-START-->").append(StringTools.NEWLINE)
				.append("            <!--DA-ELSE:test.xml.element.moreelement.twomoreelement.content:DA-ELSE-->").append(StringTools.NEWLINE)
				.append("                <three-more-element>").append(StringTools.NEWLINE)
				.append("                <!--DA-START:test.xml.element.moreelement.twomoreelement.threemoreelement.content:DA-START-->").append(StringTools.NEWLINE)
				.append("                    21").append(StringTools.NEWLINE)
				.append("                    22").append(StringTools.NEWLINE)
				.append("                    23").append(StringTools.NEWLINE)
				.append("                    24").append(StringTools.NEWLINE)
				.append("                <!--DA-ELSE:test.xml.element.moreelement.twomoreelement.threemoreelement.content:DA-ELSE-->").append(StringTools.NEWLINE)
				.append("                    42").append(StringTools.NEWLINE)
				.append("                <!--DA-END:test.xml.element.moreelement.twomoreelement.threemoreelement.content:DA-END-->").append(StringTools.NEWLINE)
				.append("                </three-more-element>").append(StringTools.NEWLINE)
				.append("            <!--DA-END:test.xml.element.moreelement.twomoreelement.content:DA-END-->").append(StringTools.NEWLINE)
				.append("            </two-more-element>").append(StringTools.NEWLINE)
				.append("        <!--DA-END:test.xml.element.moreelement.content:DA-END-->").append(StringTools.NEWLINE)
				.append("        </more-element>").append(StringTools.NEWLINE)
				.append("    <!--DA-END:test.xml.element.content:DA-END-->").append(StringTools.NEWLINE)
				.append("    </element>").append(StringTools.NEWLINE)
				.append("<root>").append(StringTools.NEWLINE)
				.toString();

		String shouldDoc = new StringBuilder()
				.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>").append(StringTools.NEWLINE)
				.append("<root>").append(StringTools.NEWLINE)
				.append("    <element>").append(StringTools.NEWLINE)
				.append("    <!--DA-START:test.xml.element.content:DA-START-->").append(StringTools.NEWLINE)
				.append("    <!--DA-ELSE:test.xml.element.content:DA-ELSE-->").append(StringTools.NEWLINE)
				.append("        <more-element>").append(StringTools.NEWLINE)
				.append("        <!--DA-START:test.xml.element.moreelement.content:DA-START-->").append(StringTools.NEWLINE)
				.append("        <!--DA-ELSE:test.xml.element.moreelement.content:DA-ELSE-->").append(StringTools.NEWLINE)
				.append("            <two-more-element>").append(StringTools.NEWLINE)
				.append("            <!--DA-START:test.xml.element.moreelement.twomoreelement.content:DA-START-->").append(StringTools.NEWLINE)
				.append("            <!--DA-ELSE:test.xml.element.moreelement.twomoreelement.content:DA-ELSE-->").append(StringTools.NEWLINE)
				.append("                <three-more-element>").append(StringTools.NEWLINE)
				.append("                <!--DA-START:test.xml.element.moreelement.twomoreelement.threemoreelement.content:DA-START-->").append(StringTools.NEWLINE)
				.append("                    21").append(StringTools.NEWLINE)
				.append("                    22").append(StringTools.NEWLINE)
				.append("                    23").append(StringTools.NEWLINE)
				.append("                    24").append(StringTools.NEWLINE)
				.append("                <!--DA-ELSE:test.xml.element.moreelement.twomoreelement.threemoreelement.content:DA-ELSE-->").append(StringTools.NEWLINE)
				.append("                    <!--42-->").append(StringTools.NEWLINE)
				.append("                <!--DA-END:test.xml.element.moreelement.twomoreelement.threemoreelement.content:DA-END-->").append(StringTools.NEWLINE)
				.append("                </three-more-element>").append(StringTools.NEWLINE)
				.append("            <!--DA-END:test.xml.element.moreelement.twomoreelement.content:DA-END-->").append(StringTools.NEWLINE)
				.append("            </two-more-element>").append(StringTools.NEWLINE)
				.append("        <!--DA-END:test.xml.element.moreelement.content:DA-END-->").append(StringTools.NEWLINE)
				.append("        </more-element>").append(StringTools.NEWLINE)
				.append("    <!--DA-END:test.xml.element.content:DA-END-->").append(StringTools.NEWLINE)
				.append("    </element>").append(StringTools.NEWLINE)
				.append("<root>").append(StringTools.NEWLINE)
				.toString();
		
		try (final InputStream input = new ByteArrayInputStream(doc.getBytes())) {
			final TransformationRequest request = new TransformationRequest();
			request.setModified(false);
			request.setNew(false);
			target.transform(request, input);
		} catch (IOException e) {
			e.printStackTrace();
			fail("Error while load previous document");
		}
		
		assertThat(target.getNewTargetDocument().toString(), is(shouldDoc));
	}
	
	@Test
	public void changedOuterDeveloperAreaXml() {
		String doc = new StringBuilder()
				.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>").append(StringTools.NEWLINE)
				.append("<root>").append(StringTools.NEWLINE)
				.append("    <element>").append(StringTools.NEWLINE)
				.append("    <!--DA-START:test.xml.element.content:DA-START-->").append(StringTools.NEWLINE)
				.append("        42").append(StringTools.NEWLINE)
				.append("        43").append(StringTools.NEWLINE)
				.append("        44").append(StringTools.NEWLINE)
				.append("    <!--DA-ELSE:test.xml.element.content:DA-ELSE-->").append(StringTools.NEWLINE)
				.append("        <more-element>").append(StringTools.NEWLINE)
				.append("        <!--DA-START:test.xml.element.moreelement.content:DA-START-->").append(StringTools.NEWLINE)
				.append("        <!--DA-ELSE:test.xml.element.moreelement.content:DA-ELSE-->").append(StringTools.NEWLINE)
				.append("            <two-more-element>").append(StringTools.NEWLINE)
				.append("            <!--DA-START:test.xml.element.moreelement.twomoreelement.content:DA-START-->").append(StringTools.NEWLINE)
				.append("            <!--DA-ELSE:test.xml.element.moreelement.twomoreelement.content:DA-ELSE-->").append(StringTools.NEWLINE)
				.append("                <!--<three-more-element>-->").append(StringTools.NEWLINE)
				.append("                <!--DA-START:test.xml.element.moreelement.twomoreelement.threemoreelement.content:DA-START-->").append(StringTools.NEWLINE)
				.append("                    21").append(StringTools.NEWLINE)
				.append("                    21").append(StringTools.NEWLINE)
				.append("                    23").append(StringTools.NEWLINE)
				.append("                <!--DA-ELSE:test.xml.element.moreelement.twomoreelement.threemoreelement.content:DA-ELSE-->").append(StringTools.NEWLINE)
				.append("                    42").append(StringTools.NEWLINE)
				.append("                <!--DA-END:test.xml.element.moreelement.twomoreelement.threemoreelement.content:DA-END-->").append(StringTools.NEWLINE)
				.append("                </three-more-element>").append(StringTools.NEWLINE)
				.append("            <!--DA-END:test.xml.element.moreelement.twomoreelement.content:DA-END-->").append(StringTools.NEWLINE)
				.append("            </two-more-element>").append(StringTools.NEWLINE)
				.append("        <!--DA-END:test.xml.element.moreelement.content:DA-END-->").append(StringTools.NEWLINE)
				.append("        </more-element>").append(StringTools.NEWLINE)
				.append("    <!--DA-END:test.xml.element.content:DA-END-->").append(StringTools.NEWLINE)
				.append("    </element>").append(StringTools.NEWLINE)
				.append("<root>").append(StringTools.NEWLINE)
				.toString();
		String shouldDoc = new StringBuilder()
				.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>").append(StringTools.NEWLINE)
				.append("<root>").append(StringTools.NEWLINE)
				.append("    <element>").append(StringTools.NEWLINE)
				.append("    <!--DA-START:test.xml.element.content:DA-START-->").append(StringTools.NEWLINE)
				.append("        42").append(StringTools.NEWLINE)
				.append("        43").append(StringTools.NEWLINE)
				.append("        44").append(StringTools.NEWLINE)
				.append("    <!--DA-ELSE:test.xml.element.content:DA-ELSE-->").append(StringTools.NEWLINE)
				.append("        <!--<more-element>-->").append(StringTools.NEWLINE)
				.append("        <!--DA-START:test.xml.element.moreelement.content:DA-START-->").append(StringTools.NEWLINE)
				.append("        <!--DA-ELSE:test.xml.element.moreelement.content:DA-ELSE-->").append(StringTools.NEWLINE)
				.append("            <!--<two-more-element>-->").append(StringTools.NEWLINE)
				.append("            <!--DA-START:test.xml.element.moreelement.twomoreelement.content:DA-START-->").append(StringTools.NEWLINE)
				.append("            <!--DA-ELSE:test.xml.element.moreelement.twomoreelement.content:DA-ELSE-->").append(StringTools.NEWLINE)
				.append("                <!--<three-more-element>-->").append(StringTools.NEWLINE)
				.append("                <!--DA-START:test.xml.element.moreelement.twomoreelement.threemoreelement.content:DA-START-->").append(StringTools.NEWLINE)
				.append("<!--                    21-->").append(StringTools.NEWLINE)
				.append("<!--                    21-->").append(StringTools.NEWLINE)
				.append("<!--                    23-->").append(StringTools.NEWLINE)
				.append("                <!--DA-ELSE:test.xml.element.moreelement.twomoreelement.threemoreelement.content:DA-ELSE-->").append(StringTools.NEWLINE)
				.append("                    <!--42-->").append(StringTools.NEWLINE)
				.append("                <!--DA-END:test.xml.element.moreelement.twomoreelement.threemoreelement.content:DA-END-->").append(StringTools.NEWLINE)
				.append("                <!--</three-more-element>-->").append(StringTools.NEWLINE)
				.append("            <!--DA-END:test.xml.element.moreelement.twomoreelement.content:DA-END-->").append(StringTools.NEWLINE)
				.append("            <!--</two-more-element>-->").append(StringTools.NEWLINE)
				.append("        <!--DA-END:test.xml.element.moreelement.content:DA-END-->").append(StringTools.NEWLINE)
				.append("        <!--</more-element>-->").append(StringTools.NEWLINE)
				.append("    <!--DA-END:test.xml.element.content:DA-END-->").append(StringTools.NEWLINE)
				.append("    </element>").append(StringTools.NEWLINE)
				.append("<root>").append(StringTools.NEWLINE)
				.toString();
		
		
		try (final InputStream input = new ByteArrayInputStream(doc.getBytes())) {
			final TransformationRequest request = new TransformationRequest();
			request.setModified(false);
			request.setNew(false);
			target.transform(request, input);
		} catch (IOException e) {
			e.printStackTrace();
			fail("Error while load previous document");
		}
		
		assertThat(target.getNewTargetDocument().toString(), is(shouldDoc));
	}
	

	@Test
	public void changedOuterDeveloperAreaBackXml() {
		String doc = new StringBuilder()
				.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>").append(StringTools.NEWLINE)
				.append("<root>").append(StringTools.NEWLINE)
				.append("    <element>").append(StringTools.NEWLINE)
				.append("    <!--DA-START:test.xml.element.content:DA-START-->").append(StringTools.NEWLINE)
				.append("    <!--DA-ELSE:test.xml.element.content:DA-ELSE-->").append(StringTools.NEWLINE)
				.append("        <!--<more-element>-->").append(StringTools.NEWLINE)
				.append("        <!--DA-START:test.xml.element.moreelement.content:DA-START-->").append(StringTools.NEWLINE)
				.append("        <!--DA-ELSE:test.xml.element.moreelement.content:DA-ELSE-->").append(StringTools.NEWLINE)
				.append("            <!--<two-more-element>-->").append(StringTools.NEWLINE)
				.append("            <!--DA-START:test.xml.element.moreelement.twomoreelement.content:DA-START-->").append(StringTools.NEWLINE)
				.append("            <!--DA-ELSE:test.xml.element.moreelement.twomoreelement.content:DA-ELSE-->").append(StringTools.NEWLINE)
				.append("                <!--<three-more-element>-->").append(StringTools.NEWLINE)
				.append("                <!--DA-START:test.xml.element.moreelement.twomoreelement.threemoreelement.content:DA-START-->").append(StringTools.NEWLINE)
				.append("<!--                    21-->").append(StringTools.NEWLINE)
				.append("<!--                    22-->").append(StringTools.NEWLINE)
				.append("<!--                    23-->").append(StringTools.NEWLINE)
				.append("                <!--DA-ELSE:test.xml.element.moreelement.twomoreelement.threemoreelement.content:DA-ELSE-->").append(StringTools.NEWLINE)
				.append("                    <!--42-->").append(StringTools.NEWLINE)
				.append("                <!--DA-END:test.xml.element.moreelement.twomoreelement.threemoreelement.content:DA-END-->").append(StringTools.NEWLINE)
				.append("                <!--</three-more-element>-->").append(StringTools.NEWLINE)
				.append("            <!--DA-END:test.xml.element.moreelement.twomoreelement.content:DA-END-->").append(StringTools.NEWLINE)
				.append("            <!--</two-more-element>-->").append(StringTools.NEWLINE)
				.append("        <!--DA-END:test.xml.element.moreelement.content:DA-END-->").append(StringTools.NEWLINE)
				.append("        <!--</more-element>-->").append(StringTools.NEWLINE)
				.append("    <!--DA-END:test.xml.element.content:DA-END-->").append(StringTools.NEWLINE)
				.append("    </element>").append(StringTools.NEWLINE)
				.append("<root>").append(StringTools.NEWLINE)
				.toString();

		String shouldDoc = new StringBuilder()
				.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>").append(StringTools.NEWLINE)
				.append("<root>").append(StringTools.NEWLINE)
				.append("    <element>").append(StringTools.NEWLINE)
				.append("    <!--DA-START:test.xml.element.content:DA-START-->").append(StringTools.NEWLINE)
				.append("    <!--DA-ELSE:test.xml.element.content:DA-ELSE-->").append(StringTools.NEWLINE)
				.append("        <more-element>").append(StringTools.NEWLINE)
				.append("        <!--DA-START:test.xml.element.moreelement.content:DA-START-->").append(StringTools.NEWLINE)
				.append("        <!--DA-ELSE:test.xml.element.moreelement.content:DA-ELSE-->").append(StringTools.NEWLINE)
				.append("            <two-more-element>").append(StringTools.NEWLINE)
				.append("            <!--DA-START:test.xml.element.moreelement.twomoreelement.content:DA-START-->").append(StringTools.NEWLINE)
				.append("            <!--DA-ELSE:test.xml.element.moreelement.twomoreelement.content:DA-ELSE-->").append(StringTools.NEWLINE)
				.append("                <three-more-element>").append(StringTools.NEWLINE)
				.append("                <!--DA-START:test.xml.element.moreelement.twomoreelement.threemoreelement.content:DA-START-->").append(StringTools.NEWLINE)
				.append("<!--                    21-->").append(StringTools.NEWLINE)
				.append("<!--                    22-->").append(StringTools.NEWLINE)
				.append("<!--                    23-->").append(StringTools.NEWLINE)
				.append("                <!--DA-ELSE:test.xml.element.moreelement.twomoreelement.threemoreelement.content:DA-ELSE-->").append(StringTools.NEWLINE)
				.append("                    <!--42-->").append(StringTools.NEWLINE)
				.append("                <!--DA-END:test.xml.element.moreelement.twomoreelement.threemoreelement.content:DA-END-->").append(StringTools.NEWLINE)
				.append("                </three-more-element>").append(StringTools.NEWLINE)
				.append("            <!--DA-END:test.xml.element.moreelement.twomoreelement.content:DA-END-->").append(StringTools.NEWLINE)
				.append("            </two-more-element>").append(StringTools.NEWLINE)
				.append("        <!--DA-END:test.xml.element.moreelement.content:DA-END-->").append(StringTools.NEWLINE)
				.append("        </more-element>").append(StringTools.NEWLINE)
				.append("    <!--DA-END:test.xml.element.content:DA-END-->").append(StringTools.NEWLINE)
				.append("    </element>").append(StringTools.NEWLINE)
				.append("<root>").append(StringTools.NEWLINE)
				.toString();
		
		
		try (final InputStream input = new ByteArrayInputStream(doc.getBytes())) {
			final TransformationRequest request = new TransformationRequest();
			request.setModified(false);
			request.setNew(false);
			target.transform(request, input);
		} catch (IOException e) {
			e.printStackTrace();
			fail("Error while load previous document");
		}
		
		assertThat(target.getNewTargetDocument().toString(), is(shouldDoc));
	}
	
	@Test
	public void changedMiddelDeveloperAreaXml() {
		String doc = new StringBuilder()
				.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>").append(StringTools.NEWLINE)
				.append("<root>").append(StringTools.NEWLINE)
				.append("    <element>").append(StringTools.NEWLINE)
				.append("    <!--DA-START:test.xml.element.content:DA-START-->").append(StringTools.NEWLINE)
				.append("    <!--DA-ELSE:test.xml.element.content:DA-ELSE-->").append(StringTools.NEWLINE)
				.append("        <more-element>").append(StringTools.NEWLINE)
				.append("        <!--DA-START:test.xml.element.moreelement.content:DA-START-->").append(StringTools.NEWLINE)
				.append("            <tag/>").append(StringTools.NEWLINE)
				.append("            <tagi/>").append(StringTools.NEWLINE)
				.append("            <tagy/>").append(StringTools.NEWLINE)
				.append("        <!--DA-ELSE:test.xml.element.moreelement.content:DA-ELSE-->").append(StringTools.NEWLINE)
				.append("            <two-more-element>").append(StringTools.NEWLINE)
				.append("            <!--DA-START:test.xml.element.moreelement.twomoreelement.content:DA-START-->").append(StringTools.NEWLINE)
				.append("            <!--DA-ELSE:test.xml.element.moreelement.twomoreelement.content:DA-ELSE-->").append(StringTools.NEWLINE)
				.append("                <three-more-element>").append(StringTools.NEWLINE)
				.append("                <!--DA-START:test.xml.element.moreelement.twomoreelement.threemoreelement.content:DA-START-->").append(StringTools.NEWLINE)
				.append("                    21").append(StringTools.NEWLINE)
				.append("                <!--DA-ELSE:test.xml.element.moreelement.twomoreelement.threemoreelement.content:DA-ELSE-->").append(StringTools.NEWLINE)
				.append("                    <!--42-->").append(StringTools.NEWLINE)
				.append("                <!--DA-END:test.xml.element.moreelement.twomoreelement.threemoreelement.content:DA-END-->").append(StringTools.NEWLINE)
				.append("                </three-more-element>").append(StringTools.NEWLINE)
				.append("            <!--DA-END:test.xml.element.moreelement.twomoreelement.content:DA-END-->").append(StringTools.NEWLINE)
				.append("            </two-more-element>").append(StringTools.NEWLINE)
				.append("        <!--DA-END:test.xml.element.moreelement.content:DA-END-->").append(StringTools.NEWLINE)
				.append("        </more-element>").append(StringTools.NEWLINE)
				.append("    <!--DA-END:test.xml.element.content:DA-END-->").append(StringTools.NEWLINE)
				.append("    </element>").append(StringTools.NEWLINE)
				.append("<root>").append(StringTools.NEWLINE)
				.toString();

		String shouldDoc = new StringBuilder()
				.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>").append(StringTools.NEWLINE)
				.append("<root>").append(StringTools.NEWLINE)
				.append("    <element>").append(StringTools.NEWLINE)
				.append("    <!--DA-START:test.xml.element.content:DA-START-->").append(StringTools.NEWLINE)
				.append("    <!--DA-ELSE:test.xml.element.content:DA-ELSE-->").append(StringTools.NEWLINE)
				.append("        <more-element>").append(StringTools.NEWLINE)
				.append("        <!--DA-START:test.xml.element.moreelement.content:DA-START-->").append(StringTools.NEWLINE)
				.append("            <tag/>").append(StringTools.NEWLINE)
				.append("            <tagi/>").append(StringTools.NEWLINE)
				.append("            <tagy/>").append(StringTools.NEWLINE)
				.append("        <!--DA-ELSE:test.xml.element.moreelement.content:DA-ELSE-->").append(StringTools.NEWLINE)
				.append("            <!--<two-more-element>-->").append(StringTools.NEWLINE)
				.append("            <!--DA-START:test.xml.element.moreelement.twomoreelement.content:DA-START-->").append(StringTools.NEWLINE)
				.append("            <!--DA-ELSE:test.xml.element.moreelement.twomoreelement.content:DA-ELSE-->").append(StringTools.NEWLINE)
				.append("                <!--<three-more-element>-->").append(StringTools.NEWLINE)
				.append("                <!--DA-START:test.xml.element.moreelement.twomoreelement.threemoreelement.content:DA-START-->").append(StringTools.NEWLINE)
				.append("<!--                    21-->").append(StringTools.NEWLINE)
				.append("                <!--DA-ELSE:test.xml.element.moreelement.twomoreelement.threemoreelement.content:DA-ELSE-->").append(StringTools.NEWLINE)
				.append("                    <!--42-->").append(StringTools.NEWLINE)
				.append("                <!--DA-END:test.xml.element.moreelement.twomoreelement.threemoreelement.content:DA-END-->").append(StringTools.NEWLINE)
				.append("                <!--</three-more-element>-->").append(StringTools.NEWLINE)
				.append("            <!--DA-END:test.xml.element.moreelement.twomoreelement.content:DA-END-->").append(StringTools.NEWLINE)
				.append("            <!--</two-more-element>-->").append(StringTools.NEWLINE)
				.append("        <!--DA-END:test.xml.element.moreelement.content:DA-END-->").append(StringTools.NEWLINE)
				.append("        </more-element>").append(StringTools.NEWLINE)
				.append("    <!--DA-END:test.xml.element.content:DA-END-->").append(StringTools.NEWLINE)
				.append("    </element>").append(StringTools.NEWLINE)
				.append("<root>").append(StringTools.NEWLINE)
				.toString();
		
		try (final InputStream input = new ByteArrayInputStream(doc.getBytes())) {
			final TransformationRequest request = new TransformationRequest();
			request.setModified(false);
			request.setNew(false);
			target.transform(request, input);
		} catch (IOException e) {
			e.printStackTrace();
			fail("Error while load previous document");
		}
		
		assertThat(target.getNewTargetDocument().toString(), is(shouldDoc));
	}
	
	@Test
	public void testWriterFail() {
		for (SimpleXmlTextWriterWithRecursiveDeveloperAreas.Fails fail : SimpleXmlTextWriterWithRecursiveDeveloperAreas.Fails.values()) {
			before();
			try {
				InjectorTools.injectFields(this.target, ModelElement.class, fail);
			} catch (IllegalArgumentException | IllegalAccessException e) {
				Assert.fail("could not inject model element");
			}

			final TransformationRequest request = new TransformationRequest();
			request.setModified(false);
			request.setNew(true);
			try {
				target.transform(request, null);
				Assert.fail(fail.name() + ": no exception was thrown in transform");
			} catch (Exception e) {
				Assert.assertEquals("the wrong exception type was thrown", fail.getExceptionClazz(), e.getClass());
				Assert.assertEquals("the wrong expetion message was thrown", fail.getMsg(), e.getMessage());
			}
			
			after();
		}
	}
	
}

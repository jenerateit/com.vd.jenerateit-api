package org.jenerateit.target;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.IsNull.nullValue;
import static org.hamcrest.core.StringContains.containsString;
import static org.hamcrest.core.StringEndsWith.endsWith;
import static org.hamcrest.core.StringStartsWith.startsWith;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.exception.JenerateITException;
import org.jenerateit.util.InjectorTools;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.vd.transformation.data.TransformationRequest;

public class DeveloperAreaProblemTest extends DeveloperAreaTestBase {

	@Override
	@Before
	public void before() {
		super.before();
		try {
			InjectorTools.injectFields(this.target, ModelElement.class, new Object());
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
			fail("Error while prepare model element within target");
		}
	}

	@Override
	@After
	public void after() {
		super.after();
	}

	

	@Test
	public void recursiveDeveloperArea() {
		try (final InputStream input = new ByteArrayInputStream(new StringBuilder()
				.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>").append("\n")
				.append("<root>").append("\n")
				.append("    <element>").append("\n")
				.append("    <!--DA-START:test.xml.content:DA-START-->").append("\n")
				.append("    <!--DA-START:test.xml.content.recursive:DA-START-->").append("\n")
				.append("    <!-- this is my comment test -->").append("\n")
				.append("    <!--DA-ELSE:test.xml.content:DA-ELSE-->").append("\n")
				.append("    <!-- this is a comment test -->").append("\n")
				.append("    <!--DA-END:test.xml.content:DA-END-->").append("\n")
				.append("    </element>").append("\n")
				.append("<root>").append("\n")
				.toString().getBytes())) {
			final TransformationRequest request = new TransformationRequest();
			request.setModified(true);
			request.setNew(false);
			target.transform(request, input);
			fail("Exception expected");

		} catch (TargetException e) {
			assertThat(e.getMessage(), is("While parsing the modified content from the file sent by the user, a problem with a developer area occurred: The developer area else path with ID 'test.xml.content' at postiont 199 in line 7 does not fit to developer area start with ID 'test.xml.content.recursive' at position 106"));
//			assertThat(e.getMessage(), containsString(", but there is already a developer area open with ID 'test.xml.content' at position "));
			assertThat(target.getErrors(), not(nullValue()));
			assertThat(target.getErrors().isEmpty(), is(Boolean.TRUE));
		} catch (Exception e) {
			fail("Unexpected Exception: " + e.getClass() + "(" + JenerateITException.class + ":" 
					+ e.getMessage() + ")");
		}
	}
	
	@Test
	public void recursiveNotModifiedFlgDeveloperArea() {
		try (final InputStream input = new ByteArrayInputStream(new StringBuilder()
				.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>").append("\n")
				.append("<root>").append("\n")
				.append("    <element>").append("\n")
				.append("    <!--DA-START:test.xml.content:DA-START-->").append("\n")
				.append("    <!--DA-START:test.xml.content.recursive:DA-START-->").append("\n")
				.append("    <!-- this is my comment test -->").append("\n")
				.append("    <!--DA-ELSE:test.xml.content:DA-ELSE-->").append("\n")
				.append("    <!-- this is a comment test -->").append("\n")
				.append("    <!--DA-END:test.xml.content:DA-END-->").append("\n")
				.append("    </element>").append("\n")
				.append("<root>").append("\n")
				.toString().getBytes())) {
			final TransformationRequest request = new TransformationRequest();
			request.setModified(false);
			request.setNew(false);
			target.transform(request, input);
			fail("Exception expected");

		} catch (TargetException e) {
			assertThat(e.getMessage(), is("While parsing the content from the file sent by the user, a problem with a developer area occurred: The developer area else path with ID 'test.xml.content' at postiont 199 in line 7 does not fit to developer area start with ID 'test.xml.content.recursive' at position 106"));
//			assertThat(e.getMessage(), containsString(", but there is already a developer area open with ID 'test.xml.content' at position "));
			assertThat(target.getErrors(), not(nullValue()));
			assertThat(target.getErrors().isEmpty(), is(Boolean.TRUE));
		} catch (Exception e) {
			fail("Unexpected Exception: " + e.getClass() + "(" + JenerateITException.class + ":" 
					+ e.getMessage() + ")");
		}
	}
	
	@Test
	public void noStartDeveloperArea() {
		try (final InputStream input = new ByteArrayInputStream(new StringBuilder()
				.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>").append("\n")
				.append("<root>").append("\n")
				.append("    <element>").append("\n")
				.append("    <!-- this is my comment test -->").append("\n")
				.append("    <!--DA-ELSE:test.xml.content:DA-ELSE-->").append("\n")
				.append("    <!-- this is a comment test -->").append("\n")
				.append("    <!--DA-END:test.xml.content:DA-END-->").append("\n")
				.append("    </element>").append("\n")
				.append("<root>").append("\n")
				.toString().getBytes())) {
			final TransformationRequest request = new TransformationRequest();
			request.setModified(false);
			request.setNew(false);
			target.transform(request, input);
			fail("Exception expected");

		} catch (TargetException e) {
			assertThat(e.getMessage(), startsWith("While parsing the content from the file sent by the user, a problem with a developer area occurred: Found a developer area else path with ID 'test.xml.content' at position "));
			assertThat(e.getMessage(), containsString(", but there is no developer area with this ID started"));
			assertThat(target.getErrors(), not(nullValue()));
			assertThat(target.getErrors().isEmpty(), is(Boolean.TRUE));
		} catch (Exception e) {
			fail("Unexpected Exception: " + e.getClass() + "(" + JenerateITException.class + ":" 
					+ e.getMessage() + ")");
		}
	}
	
	@Test
	public void duplicatedElseDeveloperArea() {
		try (final InputStream input = new ByteArrayInputStream(new StringBuilder()
				.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>").append("\n")
				.append("<root>").append("\n")
				.append("    <element>").append("\n")
				.append("    <!--DA-START:test.xml.content:DA-START-->").append("\n")
				.append("    <!-- this is my comment test -->").append("\n")
				.append("    <!--DA-ELSE:test.xml.content:DA-ELSE-->").append("\n")
				.append("    <!--DA-ELSE:test.xml.content:DA-ELSE-->").append("\n")
				.append("    <!-- this is a comment test -->").append("\n")
				.append("    <!--DA-END:test.xml.content:DA-END-->").append("\n")
				.append("    </element>").append("\n")
				.append("<root>").append("\n")
				.toString().getBytes())) {
			final TransformationRequest request = new TransformationRequest();
			request.setModified(false);
			request.setNew(false);
			target.transform(request, input);
			fail("Exception expected");

		} catch (TargetException e) {
			assertThat(e.getMessage(), startsWith("While parsing the content from the file sent by the user, a problem with a developer area occurred: Found a developer area else path with ID 'test.xml.content' at position"));
			assertThat(e.getMessage(), containsString(", but there is already a developer area else path at position "));
			assertThat(target.getErrors(), not(nullValue()));
			assertThat(target.getErrors().isEmpty(), is(Boolean.TRUE));
		} catch (Exception e) {
			fail("Unexpected Exception: " + e.getClass() + "(" + JenerateITException.class + ":" 
					+ e.getMessage() + ")");
		}
	}
	
	@Test
	public void wrongElseDeveloperArea() {
		try (final InputStream input = new ByteArrayInputStream(new StringBuilder()
				.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>").append("\n")
				.append("<root>").append("\n")
				.append("    <element>").append("\n")
				.append("    <!--DA-START:test.xml.content:DA-START-->").append("\n")
				.append("    <!-- this is my comment test -->").append("\n")
				.append("    <!--DA-ELSE:test.xml.content.wrong:DA-ELSE-->").append("\n")
				.append("    <!-- this is a comment test -->").append("\n")
				.append("    <!--DA-END:test.xml.content:DA-END-->").append("\n")
				.append("    </element>").append("\n")
				.append("<root>").append("\n")
				.toString().getBytes())) {
			final TransformationRequest request = new TransformationRequest();
			request.setModified(false);
			request.setNew(false);
			target.transform(request, input);
			fail("Exception expected");

		} catch (TargetException e) {
			assertThat(e.getMessage(), startsWith("While parsing the content from the file sent by the user, a problem with a developer area occurred: The developer area else path with ID 'test.xml.content.wrong' at postiont"));
			assertThat(e.getMessage(), containsString(" does not fit to developer area start with ID 'test.xml.content' at position "));
			assertThat(target.getErrors(), not(nullValue()));
			assertThat(target.getErrors().isEmpty(), is(Boolean.TRUE));
		} catch (Exception e) {
			fail("Unexpected Exception: " + e.getClass() + "(" + JenerateITException.class + ":" 
					+ e.getMessage() + ")");
		}
	}
	
	@Test
	public void onlyEndDeveloperArea() {
		try (final InputStream input = new ByteArrayInputStream(new StringBuilder()
				.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>").append("\n")
				.append("<root>").append("\n")
				.append("    <element>").append("\n")
				.append("    <!--DA-END:test.xml.content:DA-END-->").append("\n")
				.append("    </element>").append("\n")
				.append("<root>").append("\n")
				.toString().getBytes())) {
			final TransformationRequest request = new TransformationRequest();
			request.setModified(false);
			request.setNew(false);
			target.transform(request, input);
			fail("Exception expected");

		} catch (TargetException e) {
			assertThat(e.getMessage(), startsWith("While parsing the content from the file sent by the user, a problem with a developer area occurred: Found a developer area end path with ID 'test.xml.content' at position "));
			assertThat(e.getMessage(), endsWith(", but there is no developer area started"));
			assertThat(target.getErrors(), not(nullValue()));
			assertThat(target.getErrors().isEmpty(), is(Boolean.TRUE));
		} catch (Exception e) {
			fail("Unexpected Exception: " + e.getClass() + "(" + JenerateITException.class + ":" 
					+ e.getMessage() + ")");
		}
	}
	
	@Test
	public void endWithoutElseDeveloperArea() {
		try (final InputStream input = new ByteArrayInputStream(new StringBuilder()
				.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>").append("\n")
				.append("<root>").append("\n")
				.append("    <element>").append("\n")
				.append("    <!--DA-START:test.xml.content:DA-START-->").append("\n")
				.append("    <!-- this is my comment test -->").append("\n")
				.append("    <!-- this is a comment test -->").append("\n")
				.append("    <!--DA-END:test.xml.content:DA-END-->").append("\n")
				.append("    </element>").append("\n")
				.append("<root>").append("\n")
				.toString().getBytes())) {
			final TransformationRequest request = new TransformationRequest();
			request.setModified(false);
			request.setNew(false);
			target.transform(request, input);
			fail("Exception expected");

		} catch (TargetException e) {
			assertThat(e.getMessage(), startsWith("While parsing the content from the file sent by the user, a problem with a developer area occurred: Found a developer area end path with ID 'test.xml.content' at position "));
			assertThat(e.getMessage(), endsWith(", but there is no developer area else path"));
			assertThat(target.getErrors(), not(nullValue()));
			assertThat(target.getErrors().isEmpty(), is(Boolean.TRUE));
		} catch (Exception e) {
			fail("Unexpected Exception: " + e.getClass() + "(" + JenerateITException.class + ":" 
					+ e.getMessage() + ")");
		}
	}
	
	@Test
	public void wrongEndDeveloperArea() {
		try (final InputStream input = new ByteArrayInputStream(new StringBuilder()
				.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>").append("\n")
				.append("<root>").append("\n")
				.append("    <element>").append("\n")
				.append("    <!--DA-START:test.xml.content:DA-START-->").append("\n")
				.append("    <!-- this is my comment test -->").append("\n")
				.append("    <!--DA-ELSE:test.xml.content:DA-ELSE-->").append("\n")
				.append("    <!-- this is a comment test -->").append("\n")
				.append("    <!--DA-END:test.xml.content.wrong:DA-END-->").append("\n")
				.append("    </element>").append("\n")
				.append("<root>").append("\n")
				.toString().getBytes())) {
			final TransformationRequest request = new TransformationRequest();
			request.setModified(false);
			request.setNew(false);
			target.transform(request, input);
			fail("Exception expected");

		} catch (TargetException e) {
			assertThat(e.getMessage(), startsWith("While parsing the content from the file sent by the user, a problem with a developer area occurred: The developer area end path with ID 'test.xml.content.wrong' at postiont "));
			assertThat(e.getMessage(), containsString(" does not fit to developer area start with ID 'test.xml.content' at position "));
			assertThat(target.getErrors(), not(nullValue()));
			assertThat(target.getErrors().isEmpty(), is(Boolean.TRUE));
		} catch (Exception e) {
			fail("Unexpected Exception: " + e.getClass() + "(" + JenerateITException.class + ":" 
					+ e.getMessage() + ")");
		}
	}
	
	@Test
	public void duplicatedDeveloperArea() {
		try (final InputStream input = new ByteArrayInputStream(new StringBuilder()
				.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>").append("\n")
				.append("<root>").append("\n")
				.append("    <element>").append("\n")
				.append("    <!--DA-START:test.xml.content:DA-START-->").append("\n")
				.append("    <!-- this is my comment test -->").append("\n")
				.append("    <!--DA-ELSE:test.xml.content:DA-ELSE-->").append("\n")
				.append("    <!-- this is a comment test -->").append("\n")
				.append("    <!--DA-END:test.xml.content:DA-END-->").append("\n")
				.append("    <!--DA-START:test.xml.content:DA-START-->").append("\n")
				.append("    <!-- this is my comment test -->").append("\n")
				.append("    <!--DA-ELSE:test.xml.content:DA-ELSE-->").append("\n")
				.append("    <!-- this is a comment test -->").append("\n")
				.append("    <!--DA-END:test.xml.content:DA-END-->").append("\n")
				.append("    </element>").append("\n")
				.append("<root>").append("\n")
				.toString().getBytes())) {
			final TransformationRequest request = new TransformationRequest();
			request.setModified(false);
			request.setNew(false);
			target.transform(request, input);
			fail("Exception expected");

		} catch (TargetException e) {
			assertThat(e.getMessage(), startsWith("While parsing the content from the file sent by the user, a problem with a developer area occurred: Found a new developer area with ID 'test.xml.content' at position "));
			assertThat(e.getMessage(), containsString(", but there is already a developer area with the same ID at position "));
			assertThat(target.getErrors(), not(nullValue()));
			assertThat(target.getErrors().isEmpty(), is(Boolean.TRUE));
		} catch (Exception e) {
			fail("Unexpected Exception: " + e.getClass() + "(" + JenerateITException.class + ":" 
					+ e.getMessage() + ")");
		}
	}
	
	
}

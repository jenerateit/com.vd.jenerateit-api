package org.jenerateit.target;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.hamcrest.core.IsEqual.equalTo;

import java.util.Arrays;
import java.util.Collections;
import java.util.SortedSet;
import java.util.TreeSet;

import org.junit.Test;

public class TextTargetDocumentTest {

	private static class NoCommentStartTextTargetDocument extends AbstractTextTargetDocument {

		/**
		 * 
		 */
		private static final long serialVersionUID = -3106452633555260190L;
		private static final SortedSet<TargetSection> SECTIONS = Collections.unmodifiableSortedSet(new TreeSet<>(Arrays.asList(new TargetSection[] {
				new TargetSection("one", 1)
		})));
		
		@Override
		public CharSequence getCommentStart() {
			return null;
		}

		@Override
		public CharSequence getCommentEnd() {
			return null;
		}

		@Override
		public char getPrefixChar() {
			return ' ';
		}

		@Override
		public SortedSet<TargetSection> getTargetSections() {
			return SECTIONS;
		}
		
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void noCommentStart() {
		new NoCommentStartTextTargetDocument();
	}

	@Test
	public void withCommentStart() {
		AbstractTextTargetDocument doc = new NoCommentStartTextTargetDocument() {

			/**
			 * 
			 */
			private static final long serialVersionUID = -8293362487308157130L;

			@Override
			public CharSequence getCommentStart() {
				return "#";
			}
			
		};
		assertThat("Comment start is null", doc.getCommentStart(), notNullValue());
		assertThat("Comment end is invalid", doc.getCommentEnd(), nullValue());
		assertThat("Comment start is invalid", doc.getCommentStart().toString(), equalTo("#"));
	}
}

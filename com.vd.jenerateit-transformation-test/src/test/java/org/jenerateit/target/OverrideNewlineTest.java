package org.jenerateit.target;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.Queue;
import java.util.Set;
import java.util.SortedSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.jenerateit.annotation.GenerationGroup;
import org.jenerateit.annotation.ModelElement;
import org.jenerateit.exception.JenerateITException;
import org.jenerateit.generationgroup.GenerationGroupConfigI;
import org.jenerateit.generationgroup.GenerationGroupI;
import org.jenerateit.generationgroup.GenerationGroupOptions;
import org.jenerateit.project.TargetProjectI;
import org.jenerateit.transformation.TransformationTarget;
import org.jenerateit.util.InjectorTools;
import org.jenerateit.util.StringTools;
import org.jenerateit.writer.WriterI;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.vd.transformation.data.TransformationRequest;

public class OverrideNewlineTest {

	private static final String OVERRIDEN_NEWLINE = new String("\r");
	private final TransformationTarget tt = new TransformationTarget();
	private final GenerationGroupI gg = new GenerationGroupI() {
		
		@Override
		public Class<? extends WriterI> getWriterClass(Object element, TargetI<?> target) {
			return null;
		}
		
		@Override
		public Class<? extends WriterI> getWriterClass(Object element, Class<? extends TargetI<?>> targetClass) {
			return DifferentNewlineTextWriter.class;
		}
		
		@Override
		public String getName() {
			return "test";
		}
		
		@Override
		public String getDescription() {
			return "Just a test";
		}
		
		@Override
		public boolean isGenerationProtocol() {
			// set to true to avoid calling getProject()
			return true;
		}
		
		@Override
		public int compareTo(GenerationGroupI o) {
			return 0;
		}
		
		private final ConcurrentHashMap<Class<? extends WriterI>, Queue<SoftReference<WriterI>>> writerCache
			= new ConcurrentHashMap<>();
		@Override
		public ConcurrentMap<Class<? extends WriterI>, Queue<SoftReference<WriterI>>> getWriterCache() {
			return writerCache;
		}
		
		@Override
		public String getVersion() {
			return "0.0.0";
		}
		
		@Override
		public TargetProjectI getTargetProject() {
			return null;
		}
		
		@Override
		public GenerationGroupOptions getOptions() {
			return new GenerationGroupOptions();
		}
		
		@Override
		public Serializable getOption(String key) {
			return null;
		}
		
		@Override
		public GenerationGroupConfigI getConfig() {
			return null;
		}
		
		@Override
		public SortedSet<TargetI<? extends TargetDocumentI>> findTargets(Set<? extends Object> metaModels,
				Class<? extends TargetI<? extends TargetDocumentI>> targetFilter, Class<? extends WriterI> writerFilter) {
			return null;
		}
	};
	
	
	private TestTextTarget target = null;
	
	@Before
	public void before() {
		gg.getWriterCache().clear();
		this.target = new TestTextTarget() {

			@Override
			public CharSequence getLineBreakChars() {
				return OVERRIDEN_NEWLINE;
			}
			
		};
		try {
			InjectorTools.injectFields(this.target, GenerationGroup.class, this.gg);
			InjectorTools.injectFields(this.target, ModelElement.class, new String());
		} catch (IllegalArgumentException | IllegalAccessException e) {
			fail("Error while prepare generation group within target");
		}
		this.tt.setCurrentTarget(target);
	}

	@After
	public void after() {
		this.target = null;
		this.tt.setCurrentTarget(null);
	}
	
	@Test
	public void testNoDA() {
		final StringBuilder sb = new StringBuilder()
				.append("First line with system newline").append(OVERRIDEN_NEWLINE)
				.append("Second line with MacOS newline").append(OVERRIDEN_NEWLINE)
				.append("Third line with UNIX newline").append(OVERRIDEN_NEWLINE)
				.append("Fourth line with Window newline").append(OVERRIDEN_NEWLINE)
				.append("Last line with system newline").append(OVERRIDEN_NEWLINE);

		try {
			final TransformationRequest request = new TransformationRequest();
			request.setModified(false);
			request.setNew(true);
			target.transform(request, null);

//			final byte[] doc = target.getNewTargetDocument().toByte();
//			final byte[] ref = sb.toString().getBytes();
			assertThat(target.getNewTargetDocument().toString(), 
					equalTo(sb.toString()));
		} catch (Exception e) {
			fail("Unexpected Exception: " + e.getClass() + "(" + JenerateITException.class + ":" 
					+ e.getMessage() + ")");
		}
	}
	
	@Test
	public void testEmptyDA() {
		try {
			InjectorTools.injectFields(this.target, ModelElement.class, new HashMap<String, Object>());
		} catch (IllegalArgumentException | IllegalAccessException e) {
			fail("Error while prepare generation group within target");
		}
		final StringBuilder sb = new StringBuilder()
				.append("First line with system newline").append(OVERRIDEN_NEWLINE)
				.append("#DA-START:test.text.one:DA-START").append(OVERRIDEN_NEWLINE)
				.append("#DA-ELSE:test.text.one:DA-ELSE").append(OVERRIDEN_NEWLINE)
				.append("Second line with MacOS newline").append(OVERRIDEN_NEWLINE)
				.append("Third line with UNIX newline").append(OVERRIDEN_NEWLINE)
				.append("Fourth line with Window newline").append(OVERRIDEN_NEWLINE)
				.append("#DA-END:test.text.one:DA-END").append(OVERRIDEN_NEWLINE)
				.append("Last line with system newline").append(OVERRIDEN_NEWLINE);

		try {
			final TransformationRequest request = new TransformationRequest();
			request.setModified(false);
			request.setNew(true);
			target.transform(request, null);

//			final byte[] doc = target.getNewTargetDocument().toByte();
//			final byte[] ref = sb.toString().getBytes();
			assertThat(target.getNewTargetDocument().toString(), 
					equalTo(sb.toString()));
		} catch (Exception e) {
			fail("Unexpected Exception: " + e.getClass() + "(" + JenerateITException.class + ":" 
					+ e.getMessage() + ")");
		}
	}

	@Test
	public void testModifiedDA() {
		try {
			InjectorTools.injectFields(this.target, ModelElement.class, new HashMap<String, Object>());
		} catch (IllegalArgumentException | IllegalAccessException e) {
			fail("Error while prepare generation group within target");
		}
		final StringBuilder sb = new StringBuilder()
				.append("First line with system newline").append(OVERRIDEN_NEWLINE)
				.append("#DA-START:test.text.one:DA-START").append(OVERRIDEN_NEWLINE)
				.append("Second line with MacOS newline").append(OVERRIDEN_NEWLINE)
				.append("Third line with UNIX newline").append(OVERRIDEN_NEWLINE)
				.append("Fourth line with Window newline").append(OVERRIDEN_NEWLINE)
				.append("#DA-ELSE:test.text.one:DA-ELSE").append(OVERRIDEN_NEWLINE)
				.append("#Second line with MacOS newline").append(OVERRIDEN_NEWLINE)
				.append("#Third line with UNIX newline").append(OVERRIDEN_NEWLINE)
				.append("#Fourth line with Window newline").append(OVERRIDEN_NEWLINE)
				.append("#DA-END:test.text.one:DA-END").append(OVERRIDEN_NEWLINE)
				.append("Last line with system newline").append(OVERRIDEN_NEWLINE);

		try (final InputStream in = new ByteArrayInputStream(new StringBuilder()
				.append("First line with system newline").append(StringTools.NEWLINE)
				.append("#DA-START:test.text.one:DA-START").append(StringTools.NEWLINE)
				.append("Second line with MacOS newline\r")
				.append("Third line with UNIX newline\n")
				.append("Fourth line with Window newline\r\n")
				.append("#DA-ELSE:test.text.one:DA-ELSE").append(StringTools.NEWLINE)
				.append("#Second line with MacOS newline").append(StringTools.NEWLINE)
				.append("#Third line with UNIX newline").append(StringTools.NEWLINE)
				.append("#Fourth line with Window newline").append(StringTools.NEWLINE)
				.append("#DA-END:test.text.one:DA-END").append(StringTools.NEWLINE)
				.append("Last line with system newline").append(StringTools.NEWLINE)
				.toString().getBytes())) {
			final TransformationRequest request = new TransformationRequest();
			request.setModified(true);
			request.setNew(false);
			target.transform(request, in);

			final byte[] doc = target.getNewTargetDocument().toByte();
			// F   i    r    s    t        l    i    n    e        w    i    t    h        s    y    s    t    e    m        n    e    w    l    i    n    e    \n
			//[70, 105, 114, 115, 116, 32, 108, 105, 110, 101, 32, 119, 105, 116, 104, 32, 115, 121, 115, 116, 101, 109, 32, 110, 101, 119, 108, 105, 110, 101, 10, 
			// #   D   A   -   S   T   A   R   T   :   t    e    s    t    .   t    e    x    t    .   o    n    e    :   D   A   -   S   T   A   R   T   \n
			// 35, 68, 65, 45, 83, 84, 65, 82, 84, 58, 116, 101, 115, 116, 46, 116, 101, 120, 116, 46, 111, 110, 101, 58, 68, 65, 45, 83, 84, 65, 82, 84, 10,
			// S   e    c   o    n    d        l    i    n    e        w    i    t    h        M   a   c   O   S       n    e    w    l    i    n    e    ?\r?
			// 83, 101, 99, 111, 110, 100, 32, 108, 105, 110, 101, 32, 119, 105, 116, 104, 32, 77, 97, 99, 79, 83, 32, 110, 101, 119, 108, 105, 110, 101, 10,
			// Third line with UNIX newline\n
			// 84, 104, 105, 114, 100, 32, 108, 105, 110, 101, 32, 119, 105, 116, 104, 32, 85, 78, 73, 88, 32, 110, 101, 119, 108, 105, 110, 101, 10, 
			// 70, 111, 117, 114, 116, 104, 32, 108, 105, 110, 101, 32, 119, 105, 116, 104, 32, 87, 105, 110, 100, 111, 119, 32, 110, 101, 119, 108, 105, 110, 101, 10, 
			// 35, 68, 65, 45, 69, 76, 83, 69, 58, 116, 101, 115, 116, 46, 116, 101, 120, 116, 46, 111, 110, 101, 58, 68, 65, 45, 69, 76, 83, 69, 10, 
			// 35, 83, 101, 99, 111, 110, 100, 32, 108, 105, 110, 101, 32, 119, 105, 116, 104, 32, 77, 97, 99, 79, 83, 32, 110, 101, 119, 108, 105, 110, 101, 110, 117, 108, 108, 10, 
			// 35, 84, 104, 105, 114, 100, 32, 108, 105, 110, 101, 32, 119, 105, 116, 104, 32, 85, 78, 73, 88, 32, 110, 101, 119, 108, 105, 110, 101, 110, 117, 108, 108, 10, 
			// #   F   o    u    r    t    h        l    i    n    e        w    i    t    h        W   i    n    d    o    w        n    e    w    l    i    n    e    n    u    l    l
			// 35, 70, 111, 117, 114, 116, 104, 32, 108, 105, 110, 101, 32, 119, 105, 116, 104, 32, 87, 105, 110, 100, 111, 119, 32, 110, 101, 119, 108, 105, 110, 101, 110, 117, 108, 108, 10, 
			// 35, 68, 65, 45, 69, 78, 68, 58, 116, 101, 115, 116, 46, 116, 101, 120, 116, 46, 111, 110, 101, 58, 68, 65, 45, 69, 78, 68, 10, 
			// 76, 97, 115, 116, 32, 108, 105, 110, 101, 32, 119, 105, 116, 104, 32, 115, 121, 115, 116, 101, 109, 32, 110, 101, 119, 108, 105, 110, 101, 10]
			final byte[] ref = sb.toString().getBytes();
			//[70, 105, 114, 115, 116, 32, 108, 105, 110, 101, 32, 119, 105, 116, 104, 32, 115, 121, 115, 116, 101, 109, 32, 110, 101, 119, 108, 105, 110, 101, 10, 
			// 35, 68, 65, 45, 83, 84, 65, 82, 84, 58, 116, 101, 115, 116, 46, 116, 101, 120, 116, 46, 111, 110, 101, 58, 68, 65, 45, 83, 84, 65, 82, 84, 10, 
			// 83, 101, 99, 111, 110, 100, 32, 108, 105, 110, 101, 32, 119, 105, 116, 104, 32, 77, 97, 99, 79, 83, 32, 110, 101, 119, 108, 105, 110, 101, 10, 
			// 84, 104, 105, 114, 100, 32, 108, 105, 110, 101, 32, 119, 105, 116, 104, 32, 85, 78, 73, 88, 32, 110, 101, 119, 108, 105, 110, 101, 10, 
			// 70, 111, 117, 114, 116, 104, 32, 108, 105, 110, 101, 32, 119, 105, 116, 104, 32, 87, 105, 110, 100, 111, 119, 32, 110, 101, 119, 108, 105, 110, 101, 10, 
			// 35, 68, 65, 45, 69, 76, 83, 69, 58, 116, 101, 115, 116, 46, 116, 101, 120, 116, 46, 111, 110, 101, 58, 68, 65, 45, 69, 76, 83, 69, 10, 
			// 35, 83, 101, 99, 111, 110, 100, 32, 108, 105, 110, 101, 32, 119, 105, 116, 104, 32, 77, 97, 99, 79, 83, 32, 110, 101, 119, 108, 105, 110, 101, 10, 
			// 35, 84, 104, 105, 114, 100, 32, 108, 105, 110, 101, 32, 119, 105, 116, 104, 32, 85, 78, 73, 88, 32, 110, 101, 119, 108, 105, 110, 101, 10, 
			// 35, 70, 111, 117, 114, 116, 104, 32, 108, 105, 110, 101, 32, 119, 105, 116, 104, 32, 87, 105, 110, 100, 111, 119, 32, 110, 101, 119, 108, 105, 110, 101, 10, 
			// 35, 68, 65, 45, 69, 78, 68, 58, 116, 101, 115, 116, 46, 116, 101, 120, 116, 46, 111, 110, 101, 58, 68, 65, 45, 69, 78, 68, 10, 
			// 76, 97, 115, 116, 32, 108, 105, 110, 101, 32, 119, 105, 116, 104, 32, 115, 121, 115, 116, 101, 109, 32, 110, 101, 119, 108, 105, 110, 101, 10]
			assertThat(doc,	equalTo(ref));
			assertThat(target.getNewTargetDocument().toString(), 
					equalTo(sb.toString()));
		} catch (Exception e) {
			fail("Unexpected Exception: " + e.getClass() + "(" + JenerateITException.class + ":" 
					+ e.getMessage() + ")");
		}
	}
}

package org.jenerateit.target;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.exception.JenerateITException;
import org.jenerateit.util.StringTools;
import org.jenerateit.writer.AbstractTextWriter;
import org.jenerateit.writer.WriterException;
import org.junit.Assert;

public class SimpleXmlTextWriterWithRecursiveDeveloperAreas extends AbstractTextWriter {

	public enum Fails {
//		DO_NOT_END_DA(DeveloperAreaException.class, "Found a developer area with ID '1' which is never closed"),
		END_TO_MUCH_DA(TargetException.class, "There is no developer area to end"),
		END_DA(TargetException.class, "There is no developer area to end"),
		OPEN_DA_SAME_ID(DeveloperAreaException.class, "Found a new developer area with ID '1' at position 1 in line 7, but there is already a developer area with the same ID at position 53"),
		GET_DA_ID(TargetException.class, "There is no developer area to end"),
//		ONLY_BEGIN_DA(DeveloperAreaException.class, "Found a developer area with ID '99' which is never closed");
		
		;
		
		private final Class<? extends Exception> clazz;
		private final String msg;
		
		private Fails(Class<? extends Exception> clazz, String msg) {
			this.clazz = clazz;
			this.msg = msg;
		}
		
		public Class<? extends Exception> getExceptionClazz() {
			return clazz;
		}
		
		public String getMsg() {
			return msg;
		}
	}
	
	@ModelElement
	private Object modelElement;
	
	@Override
	public void transform(TargetSection ts) throws WriterException {
		if (Fails.class.isInstance(modelElement)) {
			transformFails(ts);
		} else {
			transformGood(ts);
		}
	}

	private void transformGood(TargetSection ts) {
		write(ts, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>", StringTools.NEWLINE);
		write(ts, "<root>", StringTools.NEWLINE);
		indent();
			w("<element>", StringTools.NEWLINE);
			bDA("test.xml.element.content");
			indent();
				w("<more-element>", StringTools.NEWLINE);
				bDA("test.xml.element.moreelement.content");
				indent();
					w("<two-more-element>", StringTools.NEWLINE);
					bDA("test.xml.element.moreelement.twomoreelement.content");
					indent();
						w("<three-more-element>", StringTools.NEWLINE);
						bDA("test.xml.element.moreelement.twomoreelement.threemoreelement.content");
						indent();
							w("42", StringTools.NEWLINE);
						outdent();
						eDA();
						w("</three-more-element>", StringTools.NEWLINE);
					outdent();
					eDA();
					w("</two-more-element>", StringTools.NEWLINE);
				outdent();
				eDA();
				w("</more-element>", StringTools.NEWLINE);
			outdent();
			eDA();
			w("</element>", StringTools.NEWLINE);
		outdent();
		w("<root>", StringTools.NEWLINE);
	}

	private void transformFails(TargetSection ts) {
		Fails fail = Fails.class.cast(modelElement);
		switch (fail) {
//		case DO_NOT_END_DA:
//			transformDoNotEndDa(ts);
//			break;
		case END_TO_MUCH_DA:
			transformEndToMuchDa(ts);
			break;
		case END_DA:
			transformEndDa(ts);
			break;
		case OPEN_DA_SAME_ID:
			transformOpenDaSameId(ts);
			break;
		case GET_DA_ID:
			transformGetDaId(ts);
			break;
//		case ONLY_BEGIN_DA:
//			transformOlyBeginDa(ts);
//			break;
		default:
			throw new JenerateITException("unhandled enum entry");
		}
	}

	private void transformOlyBeginDa(TargetSection ts) {
		bDA("1");
		w("<tag>");
		bDA("2");
		w("<tag>");
		bDA("3");
		w("<tag>");
		bDA("4");
		eDA();
	}
	
	
	private void transformDoNotEndDa(TargetSection ts) {
		bDA("1");
		bDA("2");
		w("<tag>");
		eDA();
		bDA("3");
		eDA();
	}
	
	private void transformEndToMuchDa(TargetSection ts) {
		bDA("1");
		bDA("2");
		w("<tag>");
		eDA();
		bDA("3");
		eDA();
		eDA();
		w("<tug>");
		eDA();
		w("<tig>");
	}
	
	private void transformEndDa(TargetSection ts) {
		eDA();
	}
	
	private void transformOpenDaSameId(TargetSection ts) {
		bDA("1");
		bDA("1");
		eDA();
		eDA();
	}
	
	private void transformGetDaId(TargetSection ts) {
		final int size = 100;
		for (int i = 0; i < size; i++) {
			bDA(Integer.toString(i));
		}
		Assert.assertEquals("the developer area id is wrong", "99", getTextTransformationTarget().getCurrentDeveloperAreaId());
		bDA("x");
		Assert.assertEquals("the developer area id is wrong", "x", getTextTransformationTarget().getCurrentDeveloperAreaId());
		eDA();
		Assert.assertEquals("the developer area id is wrong", "99", getTextTransformationTarget().getCurrentDeveloperAreaId());
		String[] currentDeveloperAreaIds = getTextTransformationTarget().getCurrentDeveloperAreaIds();
		Assert.assertEquals("the developer area id is wrong", "99", currentDeveloperAreaIds[0]);
		Assert.assertEquals("the stack of the current developer ids is of wrong size", size, currentDeveloperAreaIds.length);
		int developerAreaId = size;
		for (String id : currentDeveloperAreaIds) {
			developerAreaId--;
			Assert.assertEquals("the developer area id is wrong", Integer.toString(developerAreaId), id);
			eDA();
		}
		eDA();
	}
}

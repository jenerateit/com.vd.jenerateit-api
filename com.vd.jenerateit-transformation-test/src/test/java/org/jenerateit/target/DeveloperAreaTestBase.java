package org.jenerateit.target;

import static org.junit.Assert.fail;

import java.io.Serializable;
import java.lang.ref.SoftReference;
import java.util.Queue;
import java.util.Set;
import java.util.SortedSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.jenerateit.annotation.GenerationGroup;
import org.jenerateit.generationgroup.GenerationGroupConfigI;
import org.jenerateit.generationgroup.GenerationGroupI;
import org.jenerateit.generationgroup.GenerationGroupOptions;
import org.jenerateit.project.TargetProjectI;
import org.jenerateit.transformation.TransformationTarget;
import org.jenerateit.util.InjectorTools;
import org.jenerateit.writer.WriterI;

public abstract class DeveloperAreaTestBase {

	protected final TransformationTarget tt = new TransformationTarget();
	protected final GenerationGroupI gg = new GenerationGroupI() {
		
		@Override
		public Class<? extends WriterI> getWriterClass(Object element, TargetI<?> target) {
			return null;
		}
		
		@Override
		public Class<? extends WriterI> getWriterClass(Object element, Class<? extends TargetI<?>> targetClass) {
			return SimpleXmlTextWriter.class;
		}
		
		@Override
		public String getName() {
			return "test";
		}
		
		@Override
		public String getDescription() {
			return "Just a test";
		}
		
		@Override
		public boolean isGenerationProtocol() {
			// set to true to avoid calling getProject()
			return true;
		}
		
		@Override
		public int compareTo(GenerationGroupI o) {
			// TODO Auto-generated method stub
			return 0;
		}
		
		private final ConcurrentHashMap<Class<? extends WriterI>, Queue<SoftReference<WriterI>>> writerCache
			= new ConcurrentHashMap<>();
		@Override
		public ConcurrentMap<Class<? extends WriterI>, Queue<SoftReference<WriterI>>> getWriterCache() {
			return writerCache;
		}
		
		@Override
		public String getVersion() {
			return "0.0.0";
		}
		
		@Override
		public TargetProjectI getTargetProject() {
			return null;
		}
		
		@Override
		public GenerationGroupOptions getOptions() {
			return new GenerationGroupOptions();
		}
		
		@Override
		public Serializable getOption(String key) {
			return null;
		}
		
		@Override
		public GenerationGroupConfigI getConfig() {
			return null;
		}
		
		@Override
		public SortedSet<TargetI<? extends TargetDocumentI>> findTargets(Set<? extends Object> metaModels,
				Class<? extends TargetI<? extends TargetDocumentI>> targetFilter, Class<? extends WriterI> writerFilter) {
			return null;
		}
	};
	
	
	protected TestXmlTarget target = null;
	
	protected void before() {
		gg.getWriterCache().clear();
		this.target = new TestXmlTarget();
		try {
			InjectorTools.injectFields(this.target, GenerationGroup.class, this.gg);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
			fail("Error while prepare generation group within target");
		}
		this.tt.setCurrentTarget(target);
	}

	protected void after() {
		this.target = null;
		this.tt.setCurrentTarget(null);
	}
}

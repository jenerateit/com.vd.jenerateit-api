package org.jenerateit.target;

import org.jenerateit.exception.JenerateITException;
import org.jenerateit.target.DeveloperAreaInfoI.DeveloperAreaStatus;
import org.jenerateit.transformation.TransformationTarget;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class DeveloperAreaInfoTest extends AbstractTargetDocumentTestBase {

	private TestTargetDocument document = null;
	private final TransformationTarget tt = new TransformationTarget();
	private final TestTarget target = new TestTarget() {

		/* (non-Javadoc)
		 * @see org.jenerateit.target.AbstractTarget#isGenerationProtocol()
		 */
		@Override
		public boolean isGenerationProtocol() {
			return false;
		}
		
	};
	
	@Before
	public void before() {
		this.tt.setCurrentTarget(target);
		document = new TestTargetDocument();
		document.addText(SECTION_1, "# header comment\n"); // 17
		document.addText(SECTION_1, "# LA-START:abc:LA-START\n"); // 24
		document.addText(SECTION_1, "the user code\n"); // 14		
		document.addText(SECTION_1, "# LA-ELSE:abc:LA-ELSE\n"); // 22
		document.addText(SECTION_1, "# the generated code\n"); // 21		
		document.addText(SECTION_1, "# LA-END:abc:LA-END\n"); // 20
		document.addText(SECTION_1, "going on\n"); // 9		
	}
	
	@Test
	public void constructor() {
		this.document.checkDeveloperAreaId("abc");
		DeveloperAreaInfoI dai = this.document.new DeveloperAreaInfo("abc", 17, 118, 41, 55);
		Assert.assertEquals(dai.getStart(), 17);
		Assert.assertEquals(dai.getEnd(), 118);
		Assert.assertEquals(dai.getId(), "abc");
		Assert.assertEquals(dai.getUserContent(), "the user code\n");
		Assert.assertEquals(dai.getText(), "# LA-START:abc:LA-START\nthe user code\n# LA-ELSE:abc:LA-ELSE\n# the generated code\n# LA-END:abc:LA-END\n");
		Assert.assertEquals(dai.getStatus(), DeveloperAreaStatus.LOCKED);
	}

	@Test(expected = JenerateITException.class)
	public void constructorIdNull() {
		this.document.new DeveloperAreaInfo(null, 17, 118, 41, 55);
	}

	@Test(expected = JenerateITException.class)
	public void constructorIdEmpty() {
		this.document.new DeveloperAreaInfo("", 17, 118, 41, 55);
	}

	@Test(expected = JenerateITException.class)
	public void constructorIdSpace() {
		this.document.new DeveloperAreaInfo(" ", 17, 118, 41, 55);
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructorInvalidEnd() {
		this.document.new DeveloperAreaInfo("abc", 17, 56, 41, 55);
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructorInvalidStart() {
		this.document.new DeveloperAreaInfo("abc", -1, 118, 41, 55);
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructorEndContentStart() {
		this.document.new DeveloperAreaInfo("abc", 17, 118, 36, 55);
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructorContentStartContentEnd() {
		this.document.new DeveloperAreaInfo("abc", 17, 118, 41, 40);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void constructorContentEndEnd() {
		this.document.new DeveloperAreaInfo("abc", 17, 70, 41, 55);
	}
	
	@Test
	public void equals() {
		DeveloperAreaInfoI dai1 = this.document.new DeveloperAreaInfo("abc", 17, 118, 41, 55);

		DeveloperAreaInfoI dai2 = this.document.new DeveloperAreaInfo("abc", 17, 118, 41, 55);
		Assert.assertTrue("Both objects needs to be the same", dai1.equals(dai2));
		dai2 = this.document.new DeveloperAreaInfo("ABC", 17, 118, 41, 55);
		Assert.assertTrue("Both objects needs to be the same", dai1.equals(dai2));
		
		dai2 = this.document.new DeveloperAreaInfo("def", 17, 118, 41, 55);
		Assert.assertFalse("the developer area 'def' and 'abc' should not be equals", dai1.equals(dai2));
	}
	
	@Test
	public void hashCodeTest() {
		DeveloperAreaInfoI dai1 = this.document.new DeveloperAreaInfo("abc", 17, 118, 41, 55);

		DeveloperAreaInfoI dai2 = this.document.new DeveloperAreaInfo("abc", 17, 118, 41, 55);
		Assert.assertTrue("Both hash codes needs to be the same", dai1.hashCode() == dai2.hashCode());
		dai2 = this.document.new DeveloperAreaInfo("ABC", 17, 118, 41, 55);
		Assert.assertTrue("Both hash codes needs to be the same", dai1.hashCode() == dai2.hashCode());
		
		dai2 = this.document.new DeveloperAreaInfo("def", 17, 118, 41, 55);
		Assert.assertFalse("the developer area 'def' and 'abc' should have different hash codes", dai1.hashCode() == dai2.hashCode());
	}

//	@DataProvider(name = "good_pattern")
//	public Object[][] createGoodPatterData() {
//		return new Object[][] {
//				{ "abc" },
//				{ "abc.def" },
//				{ "abc.1" },
//				{ "abc_def_ghi_jkl" },
//				{ "abc.def.ghi.jkl" },
//				{"abc.def.GHI_JKL.1"},
//				{"abc"},
//				{"abc_"},
//				{"_abc"},
//				{"_abc_"},
//				{"abc_def_123"},
//				{"abc.def"},
//				{"abc_._def"},
//				{"abc.def.123_"}
//		};
//	}
//	@Test(dataProvider = "good_pattern")
//	public void goodPattern(String p) {
//		Assert.assertTrue(DeveloperAreaInfo.checkDeveloperAreaId(p), "'" + p + "' does not match");
//	}
//
//	@DataProvider(name = "bad_pattern")
//	public Object[][] createBadPatternData() {
//		return new Object[][] {
//				{ "" },
//				{ "  " },
//				{ "abc def ghi" },
//				{"annotations-class-ABasicClass"},
//				{" abc"},
//				{"abc "},
//				{" abc "},
//				{"abc def"},
//				{null},
//				{StringTools.EMPTY_STRING},
//				{"  "},
//				{"\t"},
//				{"abc_.def "},
//				{"abc.def abc.def"}
//		};
//	}
//	
//	@Test(dataProvider = "bad_pattern")
//	public void badPattern(String p) {
//		Assert.assertFalse(DeveloperAreaInfo.checkDeveloperAreaId(p), "'" + p + "' does match");
//	}
}

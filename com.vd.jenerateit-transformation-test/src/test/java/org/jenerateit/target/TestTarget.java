package org.jenerateit.target;

import java.net.URI;
import java.net.URISyntaxException;

import org.jenerateit.exception.JenerateITException;



/**
 *
 * @author hrr
 */
public class TestTarget extends AbstractTarget<SimpleTargetDocument> {


	public String getDescription() {
		return "test target";
	}
	
	
	/* (non-Javadoc)
	 * @see com.gs.jenerateit.target.TargetI#getTargetURI()
	 */
	@Override
	public URI getTargetURI() {
		try {
			return new URI("test/test.txt");
		} catch (URISyntaxException e) {
			throw new JenerateITException("Error while create Target URI", e);
		}
	}

//	public static TargetI<?> getTestTarget() {
//		
//		JenerateITI jenerateit = new JenerateITImpl();
//		
//		LoadJob lj = new LoadJob(
//				TestTarget.class, 
//				new HashSet<Serializable>(Arrays.asList(new Serializable[] {new TestMetaModelElement(1L, "test", "testspace")})), 
//				new TestGenerationGroup("test", "0.0.1", new TargetProjectImpl("test", new JenerateITImpl(), true)));
//		
//		LoadJobResult ljr = lj.call();
//		Assert.assertNotNull(ljr);
//		Assert.assertNotNull(ljr.)
//		TargetI<?> target = new TestTarget(
//				, 
//				, null);
//		
//		return target;
//	}
}

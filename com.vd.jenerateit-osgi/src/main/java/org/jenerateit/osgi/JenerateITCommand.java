/*
 *  Virtual Developer - Code Generation the easy way
 *
 *  http://www.virtual-developer.com
 *
 *  Copyright:
 *    2007-2013 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    commercial license only
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit.osgi;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * @author hrr
 *
 */
public abstract class JenerateITCommand {

	/**
	 * 
	 */
	public JenerateITCommand() {
		super();
	}

	/**
	 * Return the scope this commands are based on.
	 * 
	 * @return
	 */
	protected abstract String getScope();
	
	/**
	 * Calculate the properties needed to register this command.
	 * 
	 * @return
	 */
	public Dictionary<String, Object> getProperties() {
		final Dictionary<String, Object> properties = new Hashtable<String, Object>();
		properties.put("osgi.command.scope", getScope());
		
		final List<String> commands = new ArrayList<String>();
		for (final Method m : getCommands()) {
			commands.add(m.getName());
		}
		properties.put("osgi.command.function", commands.toArray(new String[commands.size()]));
		return properties;
	}
	
	private SortedSet<Method> getCommands() {
		final SortedSet<Method> commands = new TreeSet<Method>(new Comparator<Method>() {

			@Override
			public int compare(Method m1, Method m2) {
				int comp = m1.getName().compareTo(m2.getName());
				if (comp == 0) {
					comp = m1.getParameterTypes().length - m2.getParameterTypes().length;
				}
				return comp;
			}
		});
		for (final Method m : this.getClass().getMethods()) {
			if (Modifier.isPublic(m.getModifiers()) &&
					Void.TYPE.isAssignableFrom(m.getReturnType()) &&
					m.getParameterTypes().length < 2 &&
					m.getAnnotation(GogoCommand.class) != null) {
				commands.add(m);
			}
		}
		return commands;
	}
	
	@GogoCommand("print this help message")
	public void help() {
		System.out.println(getScope() + ":");
		for (final Method m : getCommands()) {
			System.out.print("  - ");
			System.out.print(m.getName());
			if (m.getParameterTypes().length > 0) {
				System.out.print(" <");
				System.out.print(m.getParameterTypes()[0].getName());
				System.out.print(">");
			}
			System.out.print(": ");
			System.out.println(GogoCommand.class.cast(m.getAnnotation(GogoCommand.class)).value());
		}
	}
}

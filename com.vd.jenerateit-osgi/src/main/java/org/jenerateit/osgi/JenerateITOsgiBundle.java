/*
 * Copyright (c) 2013 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package org.jenerateit.osgi;

import java.util.Map;

import org.osgi.service.component.ComponentContext;

/**
 * @author hrr
 *
 */
public abstract class JenerateITOsgiBundle {

	/**
	 * Constructor
	 */
	public JenerateITOsgiBundle() {
		
	}

	/**
	 * Startup methods for OSGI Components.
	 * 
	 * @param context the context of this component
	 * @param properties the properties if available
	 */
	public abstract void startup(final ComponentContext context, final Map<String, Object> properties);
	
	/**
	 * Shutdown methods for OSGI Components.
	 * 
	 * @param context the context of this component
	 */
	public abstract void shutdown(final ComponentContext context);
}

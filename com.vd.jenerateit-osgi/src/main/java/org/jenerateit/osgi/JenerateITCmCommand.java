/*
 *  Virtual Developer - Code Generation the easy way
 *
 *  http://www.virtual-developer.com
 *
 *  Copyright:
 *    2007-2013 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    commercial license only
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit.osgi;

import java.io.IOException;
import java.util.Dictionary;
import java.util.Hashtable;

import org.osgi.service.cm.Configuration;

/**
 * @author hrr
 *
 */
public abstract class JenerateITCmCommand extends JenerateITCommand {

	private final Configuration configuration;

	/**
	 * @param configuration the configuration object to set values to
	 */
	public JenerateITCmCommand(final Configuration configuration) {
		super();
		
		if (configuration == null) {
			throw new NullPointerException("The ConfigAdmin configuration object may not be null");
		}
		this.configuration = configuration;
	}

	/**
	 * Write the given value and key into the configuration and update them accordingly.
	 * 
	 * @param key
	 * @param value
	 * @throws IOException
	 */
	protected void update(final String key, final Object value) throws IOException {
		if (key == null) {
			throw new NullPointerException("The property key may not be null");
		}
		Dictionary<String, Object> props = configuration.getProperties();
		if (props == null) {
			props = new Hashtable<String, Object>();
			props.put(key, value);
		} else {
			props.put(key, value);
		}
		configuration.update(props);
	}
	
	/**
	 * 
	 * @param key
	 * @return
	 */
	protected Object get(final String key) {
		final Dictionary<String, Object> props = configuration.getProperties();
		return props != null ? props.get(key) : null;
	}
	
}

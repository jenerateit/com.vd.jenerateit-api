package org.jenerateit.osgi;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.jenerateit.util.StringTools;
import org.osgi.service.log.LogService;

/**
 * Provide OSGI {@link LogService logging} to child classes
 * 
 * @author hrr
 *
 */
public abstract class JenerateITOsgiLoggerBundle extends JenerateITOsgiBundle {

	protected LogService logger = null;
	
	public JenerateITOsgiLoggerBundle() {
		super();
	}

	public void addLogService(final LogService log) {
		logger = log;
		info("LogService is attached");
	}

	public void removeLogService(final LogService log) {
		info("LogService is dettached");
		logger = null;
	}

	//
	// ----------------------------------------------------------------------------------
	//
	
	
	protected void debug(final String msg) {
		debug(msg, null);
	}
	
	protected void debug(final String msg, final Throwable t) {
		log(LogService.LOG_DEBUG, msg, t);
	}
	
	protected void warning(final String msg) {
		warning(msg, null);
	}
	
	protected void warning(final String msg, final Throwable t) {
		log(LogService.LOG_WARNING, msg, t);
	}
	
	protected void info(final String msg) {
		info(msg, null);
	}
	
	protected void info(final String msg, final Throwable t) {
		log(LogService.LOG_INFO, msg, t);
	}
	
	protected void error(final String msg) {
		error(msg, null);
	}
	
	protected void error(final String msg, final Throwable t) {
		log(LogService.LOG_ERROR, msg, t);
	}
	
	private void log(final int level, final String msg, final Throwable t) {
		if (this.logger != null) {
			if (t != null) {
				this.logger.log(level, msg, t);
			} else {
				this.logger.log(level, msg);
			}
			
		} else {
			final StringWriter sw = new StringWriter();
			sw.append(msg).append(StringTools.NEWLINE);
			if (t != null) {
				t.printStackTrace(new PrintWriter(sw));
			}
			
			if (level == LogService.LOG_ERROR) {
				System.err.println(sw.toString());
			} else {
				System.out.println(sw.toString());
			}
		}
	}
}

/*
 *  Virtual Developer - Code Generation the easy way
 *
 *  http://www.virtual-developer.com
 *
 *  Copyright:
 *    2007-2013 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    commercial license only
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit.osgi;

import java.io.IOException;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Map;

import org.osgi.framework.Constants;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;
import org.osgi.service.component.ComponentContext;

/**
 * Provide the JenerateIT OSGI command infratructure to child classes
 * 
 * @author hrr
 *
 */
public abstract class JenerateITOsgiCmBundle extends JenerateITOsgiLoggerBundle implements ManagedService {

	private ServiceRegistration<?> managedServiceRegistration = null;
	private ConfigurationAdmin configAdmin = null;
	/**
	 * The config admin configuration. This instance will be created while startup this bundle.
	 */
	protected Configuration configuration = null;

	/**
	 * 
	 */
	public JenerateITOsgiCmBundle() {
		super();
	}

	/**
	 * Returns the default properties for the implementing bundle
	 * 
	 * @return a dictionary with the default 
	 */
	protected abstract Dictionary<String, Object> getDefaults();
	
	/**
	 * Returns the 'service.pid' value.
	 * 
	 * @return
	 */
	protected abstract String getServicePid();
	
	/**
	 * Event method if the bundles configuration changed. Clients need to implement this method. 
	 * All related OSGI stuff is handled by this parent class.
	 * 
	 * @param properties
	 */
	protected abstract void configurationUpdated(Dictionary<String, ?> properties) throws ConfigurationException;

	/* (non-Javadoc)
	 * @see org.jenerateit.osgi.JenerateITOsgiBundle#startup(org.osgi.service.component.ComponentContext, java.util.Map)
	 */
	@Override
	public void startup(ComponentContext context, Map<String, Object> properties) {
		try {
			final Dictionary<String, Object> defaults = getDefaults();
			/*
			 * if the 'service.pid' is missing, add it for registering the ManagedService
			 */
			if (defaults.get(Constants.SERVICE_PID) == null) {
				defaults.put(Constants.SERVICE_PID, getServicePid());
			}
			this.configuration = this.configAdmin.getConfiguration(getServicePid());
			final Dictionary<String, Object> configProperties = configuration.getProperties();
			if (configProperties == null) {
				configuration.update(defaults);
			} else {
				final Enumeration<String> defaultIterator = defaults.keys();
				while (defaultIterator.hasMoreElements()) {
					final String key = defaultIterator.nextElement();
					if (Constants.SERVICE_PID.compareTo(key) == 0) {
						continue;
					} else if (configProperties.get(key) == null) {
						configProperties.put(key, defaults.get(key));
					}
				}
				configuration.update();
			}
			
			managedServiceRegistration = context.getBundleContext().registerService(ManagedService.class, this, defaults);
		} catch (IOException e) {
			throw new RuntimeException("Error while regsiter commands", e);
		}
	}

	/* (non-Javadoc)
	 * @see org.jenerateit.osgi.JenerateITOsgiBundle#shutdown(org.osgi.service.component.ComponentContext)
	 */
	@Override
	public void shutdown(ComponentContext context) {
		this.configuration = null;

		if (managedServiceRegistration != null) {
			managedServiceRegistration.unregister();
			managedServiceRegistration = null;
		}
		
	}

	
	/* (non-Javadoc)
	 * @see org.osgi.service.cm.ManagedService#updated(java.util.Dictionary)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public final void updated(Dictionary<String, ?> properties)
			throws ConfigurationException {
		if (properties == null) {
			if (managedServiceRegistration != null) {
				managedServiceRegistration.setProperties(getDefaults());
			}
		} else {
			configurationUpdated(properties);
			if (properties.get(Constants.SERVICE_PID) == null) {
				((Dictionary<String, Object>)properties).put(Constants.SERVICE_PID, getServicePid());
			}
			if (managedServiceRegistration != null) {
				managedServiceRegistration.setProperties(properties);
			}
		}
	}

	public void addConfigurationAdmin(final ConfigurationAdmin configAdmin) {
		this.configAdmin = configAdmin;
	}
	
	public void removeConfigurationAdmin(final ConfigurationAdmin configAdmin) {
		this.configAdmin = null;
	}
	
}

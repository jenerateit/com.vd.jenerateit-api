package com.vd.gu.client.impl;

import com.vd.gu.client.impl.GUClientConfiguration.Logging;


/**
 * <table>
 * <tr><td>Generator</td><td>com.gs.vd.gen.java.retrofit.openapi:1.0</td></tr>
 * <tr><td>Element</td><td>GU</td></tr>
 * <tr><td>Type</td><td>Collection of OpenAPI3 models</td></tr>
 * <tr><td>Module</td><td>none</td></tr>
 * </table>
 */
class GUClientLoggingInterceptorSettings { // start of class

    /**
     * the rest client object that holds the main logic and makes use of retrofit interfaces
     */
    private final GUClient restClient;
    
    private GUClientLoggingInterceptorSettings.Level level = Level.BASIC;
    
    /**
     * creates an instance of GUClientLoggingInterceptorSettings
     *
     * @param restClient  the restClient
     */
    public GUClientLoggingInterceptorSettings(GUClient restClient) {
        //DA-START:com.vd.gu.client.impl.GUClientLoggingInterceptorSettings.GUClient:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClientLoggingInterceptorSettings.GUClient:DA-ELSE
        this.restClient = restClient;
        //DA-END:com.vd.gu.client.impl.GUClientLoggingInterceptorSettings.GUClient:DA-END
    }
    
    
    /**
     *
     * @return
     */
    public GUClientLoggingInterceptorSettings.Level getLogLevel() {
        //DA-START:com.vd.gu.client.impl.GUClientLoggingInterceptorSettings.getLogLevel.Level:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClientLoggingInterceptorSettings.getLogLevel.Level:DA-ELSE
        return this.level;
        //DA-END:com.vd.gu.client.impl.GUClientLoggingInterceptorSettings.getLogLevel.Level:DA-END
    }
    
    /**
     *
     * @param level  the level
     * @return
     */
    public void setLogLevel(GUClientLoggingInterceptorSettings.Level level) {
        //DA-START:com.vd.gu.client.impl.GUClientLoggingInterceptorSettings.setLogLevel.Level.void:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClientLoggingInterceptorSettings.setLogLevel.Level.void:DA-ELSE
        this.level = level;
        //DA-END:com.vd.gu.client.impl.GUClientLoggingInterceptorSettings.setLogLevel.Level.void:DA-END
    }
    
    /**
     *
     * @param message  the message
     * @return
     */
    public void log(String message) {
        //DA-START:com.vd.gu.client.impl.GUClientLoggingInterceptorSettings.log.String.void:DA-START
    	Logging logging = this.restClient.getConfiguration().getLogging();
    	if (logging != null) {
    		logging.log(message);
    	} else {
    		System.out.println(message);
    	}
        //DA-ELSE:com.vd.gu.client.impl.GUClientLoggingInterceptorSettings.log.String.void:DA-ELSE
        //System.out.println(message);
        //DA-END:com.vd.gu.client.impl.GUClientLoggingInterceptorSettings.log.String.void:DA-END
    }
    
    
    
    /**
     * <table>
     * <tr><td>Element</td><td>Level</td></tr>
     * <tr><td>Type</td><td>JavaEnumeration</td></tr>
     * <tr><td>Module</td><td>none</td></tr>
     * </table>
     */
    public enum Level { // start of class
        
        NONE,
        
        BASIC,
        
        HEADERS,
        ;
        
        
        
        
        //DA-START:com.vd.gu.client.impl.GUClientLoggingInterceptorSettings.Level.additional.elements.in.type:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClientLoggingInterceptorSettings.Level.additional.elements.in.type:DA-ELSE
        // add any additional element in here that you need in your Java type but is not generated 
        //DA-END:com.vd.gu.client.impl.GUClientLoggingInterceptorSettings.Level.additional.elements.in.type:DA-END
    } // end of java type
    
    //DA-START:com.vd.gu.client.impl.GUClientLoggingInterceptorSettings.additional.elements.in.type:DA-START
    //DA-ELSE:com.vd.gu.client.impl.GUClientLoggingInterceptorSettings.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.vd.gu.client.impl.GUClientLoggingInterceptorSettings.additional.elements.in.type:DA-END
} // end of java type
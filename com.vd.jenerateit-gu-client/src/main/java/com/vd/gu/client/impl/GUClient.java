package com.vd.gu.client.impl;

import com.vd.gu.client.GUClientI;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;
import java.util.concurrent.TimeUnit;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vd.gu.client.ResultForCreateGenerator;
import java.util.Map;
import com.vd.gu.TechnicalExceptionCreateGeneratorDefaultException;
import java.lang.annotation.Annotation;
import retrofit2.Converter;
import com.vd.gu.definition.basic.ErrorBean;
import retrofit2.Call;
import retrofit2.Response;
import okhttp3.ResponseBody;
import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import retrofit2.Callback;
import com.vd.gu.TechnicalExceptionDeleteGeneratorDefaultException;
import com.vd.gu.definition.basic.LoadResultBean;
import java.util.List;
import com.vd.gu.definition.basic.FileBean;
import com.vd.gu.TechnicalExceptionLoadGeneratorDefaultException;
import com.vd.gu.definition.basic.TargetContentBean;
import com.vd.gu.TechnicalExceptionTransformDefaultException;
import com.vd.gu.TechnicalExceptionTransformWithOldContentDefaultException;
import com.vd.gu.TechnicalExceptionTransformListDefaultException;
import com.vd.gu.TechnicalExceptionTransformListWithOldContentDefaultException;
import java.util.Set;
import java.util.HashSet;
import com.vd.gu.definition.basic.OptionBean;
import java.util.LinkedHashMap;
import java.util.ArrayList;
import com.vd.gu.definition.basic.LoadErrorBean;


/**
 * <table>
 * <tr><td>Generator</td><td>com.gs.vd.gen.java.retrofit.openapi:1.0</td></tr>
 * <tr><td>Element</td><td>GU</td></tr>
 * <tr><td>Type</td><td>Collection of OpenAPI3 models</td></tr>
 * <tr><td>Module</td><td>none</td></tr>
 * </table>
 */
public class GUClient implements GUClientI { // start of class

    /**
     * holds data that is required to access a service (host, port, base path, ...)
     */
    private final GUClientConfiguration configuration;
    
    /**
     * Factory for calls, which can be used to send HTTP requests and read their responses. @see http://square.github.io/okhttp/3.x/okhttp/
     */
    private OkHttpClient okHttpClient;
    
    /**
     * Retrofit adapts a Java interface to HTTP calls by using annotations on the declared methods to define how requests are made.
     * Create instances using the builder and pass your interface to create(java.lang.Class<T>) to generate an implementation.
     */
    private Retrofit retrofit;
    
    /**
     * An implementation of a Java interface that has Retrofit annotations on its methods.
     */
    private GURetrofitI serviceGU;
    
    /**
     * creates an instance of GUClient
     *
     * @param configuration  the configuration
     */
    public GUClient(GUClientConfiguration configuration) {
        //DA-START:com.vd.gu.client.impl.GUClient.GUClientConfiguration:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClient.GUClientConfiguration:DA-ELSE
        this.configuration = configuration;
        if (configuration == null) throw new NullPointerException("parameter 'configuration' must not be null");
        
        // --- init configuration objects
        GUClientApplicationInterceptorSettings applicationInterceptorSettings = new GUClientApplicationInterceptorSettings(this);
        GUClientNetworkInterceptorSettings networkInterceptorSettings = new GUClientNetworkInterceptorSettings(this);
        GUClientLoggingInterceptorSettings loggingInterceptorSettings = new GUClientLoggingInterceptorSettings(this);
        
        // --- propagate general configuration that got passed through the REST client
        // - add the media type that has to be used for de-/serialization, which is going to be used in the network interceptor
        LinkedHashMap<String, List<String>> httpHeaders = networkInterceptorSettings.getHttpHeaders();
        ArrayList<String> acceptHeaders = new ArrayList<>();
        if (configuration.getMediaType() == null) {
            acceptHeaders.add("application/json");  // this is the default data format
        } else {
            acceptHeaders.add(configuration.getMediaType());
        }
        httpHeaders.put("Accept", acceptHeaders);
        
        // --- create the OkHttp builder
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
            .addInterceptor(new GUClientApplicationInterceptor(applicationInterceptorSettings))
            .addNetworkInterceptor(new GUClientNetworkInterceptor(networkInterceptorSettings))
            .addNetworkInterceptor(new GUClientLoggingInterceptor(loggingInterceptorSettings))
            .connectTimeout(configuration.getTimeoutConnect(), TimeUnit.MILLISECONDS)
            .writeTimeout(configuration.getTimeoutWrite(), TimeUnit.MILLISECONDS)
            .readTimeout(configuration.getTimeoutRead(), TimeUnit.MILLISECONDS);
        
        // --- call callback method to allow for customization of the builder before building the OkHttp client
        configuration.customizeOkHttpClientBuilder(builder);
        configuration.getInterceptors().forEach(i -> builder.addInterceptor(i));
        okHttpClient = builder.build();
        
        Retrofit.Builder retrofitBuilder = new Retrofit.Builder();
        retrofitBuilder.baseUrl(configuration.getUrl().toString());
        getConverterFactories().forEach(factory -> retrofitBuilder.addConverterFactory(factory));
        retrofitBuilder.client(okHttpClient);
        retrofit = retrofitBuilder.build();
        // finally, creating the service objects 
        serviceGU = retrofit.create(GURetrofitI.class);
        //DA-END:com.vd.gu.client.impl.GUClient.GUClientConfiguration:DA-END
    }
    
    
    /**
     * setup a new generator
     *
     * @param generatorId generator id
     * @param generatorVersion generator version
     * @param request optional array of options for the generator
     * @return
     * @throws TechnicalExceptionCreateGeneratorDefaultException which is a runtime exception
     */
    @Override
    public ResultForCreateGenerator createGenerator(String generatorId, String generatorVersion, List<OptionBean> request) throws TechnicalExceptionCreateGeneratorDefaultException {
        //DA-START:com.vd.gu.client.impl.GUClient.createGenerator.String.String.List.ResultForCreateGenerator:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClient.createGenerator.String.String.List.ResultForCreateGenerator:DA-ELSE
        Call<ResponseBody> call = serviceGU.createGenerator(generatorId, generatorVersion, request);
        Response<ResponseBody> response = null;
        try {
            response = call.execute();
            if (response.isSuccessful()) {
        	    return new ResultForCreateGenerator(response.headers().get("Location"));
            } else {
                {
                    TechnicalExceptionCreateGeneratorDefaultException e = new TechnicalExceptionCreateGeneratorDefaultException("status:" + response.code() + ", message:" + response.message());
                    Converter<ResponseBody, ErrorBean> responseBodyConverter = retrofit.responseBodyConverter(ErrorBean.class, new Annotation[0]);
                ErrorBean exceptionData = responseBodyConverter.convert(response.errorBody());
                e.setResponseCodeDefault(exceptionData);
                    throw e;
                }
            }
        } catch (ConnectException e) {
           	throw new RuntimeException(e.getMessage(), e);
        } catch (SocketTimeoutException e) {
            throw new RuntimeException("connection took to long, timeout", e);
        } catch (IOException e) {
           	throw new RuntimeException("IOException: " + (response != null ? "http status code: " + response.code() + " " + response.message() + ": " : "") + e.getMessage(), e);
        }
        //DA-END:com.vd.gu.client.impl.GUClient.createGenerator.String.String.List.ResultForCreateGenerator:DA-END
    }
    
    /**
     * setup a new generator
     *
     * @param generatorId
     * @param generatorVersion
     * @param request
     * @param callback
     */
    @Override
    public void createGeneratorWithCallback(String generatorId, String generatorVersion, List<OptionBean> request, GUClientI.CreateGeneratorCallback callback) {
        //DA-START:com.vd.gu.client.impl.GUClient.createGeneratorWithCallback.String.String.List.CreateGeneratorCallback:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClient.createGeneratorWithCallback.String.String.List.CreateGeneratorCallback:DA-ELSE
        Call<ResponseBody> call = serviceGU.createGenerator(generatorId, generatorVersion, request);
        call.enqueue(new Callback<ResponseBody>() {
        
        	@Override
        	public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                	    callback.onSuccess(new ResultForCreateGenerator(response.headers().get("Location")));
                    } else {
                        {
                            TechnicalExceptionCreateGeneratorDefaultException e = new TechnicalExceptionCreateGeneratorDefaultException("status:" + response.code() + ", message:" + response.message());
                            Converter<ResponseBody, ErrorBean> responseBodyConverter = retrofit.responseBodyConverter(ErrorBean.class, new Annotation[0]);
                        ErrorBean exceptionData = responseBodyConverter.convert(response.errorBody());
                        e.setResponseCodeDefault(exceptionData);
                            throw e;
                        }
                    }
                } catch (Exception e) {
                    callback.onFailure(new IOException((response != null ? "http status code: " + response.code() + " " + response.message() + ": " : "") + e.getMessage(), e));
                }
            }
        
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                callback.onFailure(throwable);
            }
        });
        //DA-END:com.vd.gu.client.impl.GUClient.createGeneratorWithCallback.String.String.List.CreateGeneratorCallback:DA-END
    }
    
    /**
     * remove the generator
     *
     * @param id the id of the previously loaded generator
     * @throws TechnicalExceptionDeleteGeneratorDefaultException which is a runtime exception
     */
    @Override
    public void deleteGenerator(String id) throws TechnicalExceptionDeleteGeneratorDefaultException {
        //DA-START:com.vd.gu.client.impl.GUClient.deleteGenerator.String:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClient.deleteGenerator.String:DA-ELSE
        Call<ResponseBody> call = serviceGU.deleteGenerator(id);
        Response<ResponseBody> response = null;
        try {
            response = call.execute();
            if (response.isSuccessful()) {
        	    return ;
            } else {
                {
                    TechnicalExceptionDeleteGeneratorDefaultException e = new TechnicalExceptionDeleteGeneratorDefaultException("status:" + response.code() + ", message:" + response.message());
                    Converter<ResponseBody, ErrorBean> responseBodyConverter = retrofit.responseBodyConverter(ErrorBean.class, new Annotation[0]);
                ErrorBean exceptionData = responseBodyConverter.convert(response.errorBody());
                e.setResponseCodeDefault(exceptionData);
                    throw e;
                }
            }
        } catch (ConnectException e) {
           	throw new RuntimeException(e.getMessage(), e);
        } catch (SocketTimeoutException e) {
            throw new RuntimeException("connection took to long, timeout", e);
        } catch (IOException e) {
           	throw new RuntimeException("IOException: " + (response != null ? "http status code: " + response.code() + " " + response.message() + ": " : "") + e.getMessage(), e);
        }
        //DA-END:com.vd.gu.client.impl.GUClient.deleteGenerator.String:DA-END
    }
    
    /**
     * remove the generator
     *
     * @param id
     * @param callback
     */
    @Override
    public void deleteGeneratorWithCallback(String id, GUClientI.DeleteGeneratorCallback callback) {
        //DA-START:com.vd.gu.client.impl.GUClient.deleteGeneratorWithCallback.String.DeleteGeneratorCallback:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClient.deleteGeneratorWithCallback.String.DeleteGeneratorCallback:DA-ELSE
        Call<ResponseBody> call = serviceGU.deleteGenerator(id);
        call.enqueue(new Callback<ResponseBody>() {
        
        	@Override
        	public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                	    callback.onSuccess();
                    } else {
                        {
                            TechnicalExceptionDeleteGeneratorDefaultException e = new TechnicalExceptionDeleteGeneratorDefaultException("status:" + response.code() + ", message:" + response.message());
                            Converter<ResponseBody, ErrorBean> responseBodyConverter = retrofit.responseBodyConverter(ErrorBean.class, new Annotation[0]);
                        ErrorBean exceptionData = responseBodyConverter.convert(response.errorBody());
                        e.setResponseCodeDefault(exceptionData);
                            throw e;
                        }
                    }
                } catch (Exception e) {
                    callback.onFailure(new IOException((response != null ? "http status code: " + response.code() + " " + response.message() + ": " : "") + e.getMessage(), e));
                }
            }
        
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                callback.onFailure(throwable);
            }
        });
        //DA-END:com.vd.gu.client.impl.GUClient.deleteGeneratorWithCallback.String.DeleteGeneratorCallback:DA-END
    }
    
    /**
     * load the generator with a model
     *
     * @param id the id of the previously loaded generator
     * @param request the model files
     * @return created
     * @throws TechnicalExceptionLoadGeneratorDefaultException which is a runtime exception
     */
    @Override
    public LoadResultBean loadGenerator(String id, List<FileBean> request) throws TechnicalExceptionLoadGeneratorDefaultException {
        //DA-START:com.vd.gu.client.impl.GUClient.loadGenerator.String.List.LoadResultBean:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClient.loadGenerator.String.List.LoadResultBean:DA-ELSE
        Call<LoadResultBean> call = serviceGU.loadGenerator(id, request);
        Response<LoadResultBean> response = null;
        try {
            response = call.execute();
            if (response.isSuccessful()) {
        	    return response.body();
            } else {
                {
                    TechnicalExceptionLoadGeneratorDefaultException e = new TechnicalExceptionLoadGeneratorDefaultException("status:" + response.code() + ", message:" + response.message());
                    Converter<ResponseBody, LoadErrorBean> responseBodyConverter = retrofit.responseBodyConverter(LoadErrorBean.class, new Annotation[0]);
                LoadErrorBean exceptionData = responseBodyConverter.convert(response.errorBody());
                e.setResponseCodeDefault(exceptionData);
                    throw e;
                }
            }
        } catch (ConnectException e) {
           	throw new RuntimeException(e.getMessage(), e);
        } catch (SocketTimeoutException e) {
            throw new RuntimeException("connection took to long, timeout", e);
        } catch (IOException e) {
           	throw new RuntimeException("IOException: " + (response != null ? "http status code: " + response.code() + " " + response.message() + ": " : "") + e.getMessage(), e);
        }
        //DA-END:com.vd.gu.client.impl.GUClient.loadGenerator.String.List.LoadResultBean:DA-END
    }
    
    /**
     * load the generator with a model
     *
     * @param id
     * @param request
     * @param callback
     */
    @Override
    public void loadGeneratorWithCallback(String id, List<FileBean> request, GUClientI.LoadGeneratorCallback callback) {
        //DA-START:com.vd.gu.client.impl.GUClient.loadGeneratorWithCallback.String.List.LoadGeneratorCallback:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClient.loadGeneratorWithCallback.String.List.LoadGeneratorCallback:DA-ELSE
        Call<LoadResultBean> call = serviceGU.loadGenerator(id, request);
        call.enqueue(new Callback<LoadResultBean>() {
        
        	@Override
        	public void onResponse(Call<LoadResultBean> call, Response<LoadResultBean> response) {
                try {
                    if (response.isSuccessful()) {
                	    callback.onSuccess(response.body());
                    } else {
                        {
                            TechnicalExceptionLoadGeneratorDefaultException e = new TechnicalExceptionLoadGeneratorDefaultException("status:" + response.code() + ", message:" + response.message());
                            Converter<ResponseBody, LoadErrorBean> responseBodyConverter = retrofit.responseBodyConverter(LoadErrorBean.class, new Annotation[0]);
                        LoadErrorBean exceptionData = responseBodyConverter.convert(response.errorBody());
                        e.setResponseCodeDefault(exceptionData);
                            throw e;
                        }
                    }
                } catch (Exception e) {
                    callback.onFailure(new IOException((response != null ? "http status code: " + response.code() + " " + response.message() + ": " : "") + e.getMessage(), e));
                }
            }
        
            @Override
            public void onFailure(Call<LoadResultBean> call, Throwable throwable) {
                callback.onFailure(throwable);
            }
        });
        //DA-END:com.vd.gu.client.impl.GUClient.loadGeneratorWithCallback.String.List.LoadGeneratorCallback:DA-END
    }
    
    /**
     * transform a file
     *
     * @param id the id of the previously loaded generator
     * @param reference the reference of the content
     * @return ok
     * @throws TechnicalExceptionTransformDefaultException which is a runtime exception
     */
    @Override
    public TargetContentBean transform(String id, String reference) throws TechnicalExceptionTransformDefaultException {
        //DA-START:com.vd.gu.client.impl.GUClient.transform.String.String.TargetContentBean:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClient.transform.String.String.TargetContentBean:DA-ELSE
        Call<TargetContentBean> call = serviceGU.transform(id, reference);
        Response<TargetContentBean> response = null;
        try {
            response = call.execute();
            if (response.isSuccessful()) {
        	    return response.body();
            } else {
                {
                    TechnicalExceptionTransformDefaultException e = new TechnicalExceptionTransformDefaultException("status:" + response.code() + ", message:" + response.message());
                    Converter<ResponseBody, ErrorBean> responseBodyConverter = retrofit.responseBodyConverter(ErrorBean.class, new Annotation[0]);
                ErrorBean exceptionData = responseBodyConverter.convert(response.errorBody());
                e.setResponseCodeDefault(exceptionData);
                    throw e;
                }
            }
        } catch (ConnectException e) {
           	throw new RuntimeException(e.getMessage(), e);
        } catch (SocketTimeoutException e) {
            throw new RuntimeException("connection took to long, timeout", e);
        } catch (IOException e) {
           	throw new RuntimeException("IOException: " + (response != null ? "http status code: " + response.code() + " " + response.message() + ": " : "") + e.getMessage(), e);
        }
        //DA-END:com.vd.gu.client.impl.GUClient.transform.String.String.TargetContentBean:DA-END
    }
    
    /**
     * transform a file
     *
     * @param id
     * @param reference
     * @param callback
     */
    @Override
    public void transformWithCallback(String id, String reference, GUClientI.TransformCallback callback) {
        //DA-START:com.vd.gu.client.impl.GUClient.transformWithCallback.String.String.TransformCallback:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClient.transformWithCallback.String.String.TransformCallback:DA-ELSE
        Call<TargetContentBean> call = serviceGU.transform(id, reference);
        call.enqueue(new Callback<TargetContentBean>() {
        
        	@Override
        	public void onResponse(Call<TargetContentBean> call, Response<TargetContentBean> response) {
                try {
                    if (response.isSuccessful()) {
                	    callback.onSuccess(response.body());
                    } else {
                        {
                            TechnicalExceptionTransformDefaultException e = new TechnicalExceptionTransformDefaultException("status:" + response.code() + ", message:" + response.message());
                            Converter<ResponseBody, ErrorBean> responseBodyConverter = retrofit.responseBodyConverter(ErrorBean.class, new Annotation[0]);
                        ErrorBean exceptionData = responseBodyConverter.convert(response.errorBody());
                        e.setResponseCodeDefault(exceptionData);
                            throw e;
                        }
                    }
                } catch (Exception e) {
                    callback.onFailure(new IOException((response != null ? "http status code: " + response.code() + " " + response.message() + ": " : "") + e.getMessage(), e));
                }
            }
        
            @Override
            public void onFailure(Call<TargetContentBean> call, Throwable throwable) {
                callback.onFailure(throwable);
            }
        });
        //DA-END:com.vd.gu.client.impl.GUClient.transformWithCallback.String.String.TransformCallback:DA-END
    }
    
    /**
     *
     * @param id
     * @param reference
     * @return
     */
    @Override
    public Map<String, List<String>> transformHead(String id, String reference) {
        //DA-START:com.vd.gu.client.impl.GUClient.transformHead.String.String.Map:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClient.transformHead.String.String.Map:DA-ELSE
        Call<Void> call = serviceGU.transformHead(id, reference);
        Response<Void> response = null;
        try {
            response = call.execute();
            if (response.isSuccessful()) {
        	    return response.headers().toMultimap();
            } else {
                throw new IOException("response was not successfull");
            }
        } catch (ConnectException e) {
           	throw new RuntimeException(e.getMessage(), e);
        } catch (SocketTimeoutException e) {
            throw new RuntimeException("connection took to long, timeout", e);
        } catch (IOException e) {
           	throw new RuntimeException("IOException: " + (response != null ? "http status code: " + response.code() + " " + response.message() + ": " : "") + e.getMessage(), e);
        }
        //DA-END:com.vd.gu.client.impl.GUClient.transformHead.String.String.Map:DA-END
    }
    
    /**
     *
     * @param id
     * @param reference
     * @param callback
     */
    @Override
    public void transformHeadWithCallback(String id, String reference, GUClientI.TransformHeadCallback callback) {
        //DA-START:com.vd.gu.client.impl.GUClient.transformHeadWithCallback.String.String.TransformHeadCallback:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClient.transformHeadWithCallback.String.String.TransformHeadCallback:DA-ELSE
        Call<Void> call = serviceGU.transformHead(id, reference);
        call.enqueue(new Callback<Void>() {
        
        	@Override
        	public void onResponse(Call<Void> call, Response<Void> response) {
                try {
                    if (response.isSuccessful()) {
                	    callback.onSuccess(response.headers().toMultimap());
                    } else {
                        throw new IOException("response was not successfull");
                    }
                } catch (Exception e) {
                    callback.onFailure(new IOException((response != null ? "http status code: " + response.code() + " " + response.message() + ": " : "") + e.getMessage(), e));
                }
            }
        
            @Override
            public void onFailure(Call<Void> call, Throwable throwable) {
                callback.onFailure(throwable);
            }
        });
        //DA-END:com.vd.gu.client.impl.GUClient.transformHeadWithCallback.String.String.TransformHeadCallback:DA-END
    }
    
    /**
     * transform a file with old content
     *
     * @param id the id of the previously loaded generator
     * @param reference the reference of the content
     * @param request the old content
     * @return ok
     * @throws TechnicalExceptionTransformWithOldContentDefaultException which is a runtime exception
     */
    @Override
    public TargetContentBean transformWithOldContent(String id, String reference, FileBean request) throws TechnicalExceptionTransformWithOldContentDefaultException {
        //DA-START:com.vd.gu.client.impl.GUClient.transformWithOldContent.String.String.FileBean.TargetContentBean:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClient.transformWithOldContent.String.String.FileBean.TargetContentBean:DA-ELSE
        Call<TargetContentBean> call = serviceGU.transformWithOldContent(id, reference, request);
        Response<TargetContentBean> response = null;
        try {
            response = call.execute();
            if (response.isSuccessful()) {
        	    return response.body();
            } else {
                {
                    TechnicalExceptionTransformWithOldContentDefaultException e = new TechnicalExceptionTransformWithOldContentDefaultException("status:" + response.code() + ", message:" + response.message());
                    Converter<ResponseBody, ErrorBean> responseBodyConverter = retrofit.responseBodyConverter(ErrorBean.class, new Annotation[0]);
                ErrorBean exceptionData = responseBodyConverter.convert(response.errorBody());
                e.setResponseCodeDefault(exceptionData);
                    throw e;
                }
            }
        } catch (ConnectException e) {
           	throw new RuntimeException(e.getMessage(), e);
        } catch (SocketTimeoutException e) {
            throw new RuntimeException("connection took to long, timeout", e);
        } catch (IOException e) {
           	throw new RuntimeException("IOException: " + (response != null ? "http status code: " + response.code() + " " + response.message() + ": " : "") + e.getMessage(), e);
        }
        //DA-END:com.vd.gu.client.impl.GUClient.transformWithOldContent.String.String.FileBean.TargetContentBean:DA-END
    }
    
    /**
     * transform a file with old content
     *
     * @param id
     * @param reference
     * @param request
     * @param callback
     */
    @Override
    public void transformWithOldContentWithCallback(String id, String reference, FileBean request, GUClientI.TransformWithOldContentCallback callback) {
        //DA-START:com.vd.gu.client.impl.GUClient.transformWithOldContentWithCallback.String.String.FileBean.TransformWithOldContentCallback:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClient.transformWithOldContentWithCallback.String.String.FileBean.TransformWithOldContentCallback:DA-ELSE
        Call<TargetContentBean> call = serviceGU.transformWithOldContent(id, reference, request);
        call.enqueue(new Callback<TargetContentBean>() {
        
        	@Override
        	public void onResponse(Call<TargetContentBean> call, Response<TargetContentBean> response) {
                try {
                    if (response.isSuccessful()) {
                	    callback.onSuccess(response.body());
                    } else {
                        {
                            TechnicalExceptionTransformWithOldContentDefaultException e = new TechnicalExceptionTransformWithOldContentDefaultException("status:" + response.code() + ", message:" + response.message());
                            Converter<ResponseBody, ErrorBean> responseBodyConverter = retrofit.responseBodyConverter(ErrorBean.class, new Annotation[0]);
                        ErrorBean exceptionData = responseBodyConverter.convert(response.errorBody());
                        e.setResponseCodeDefault(exceptionData);
                            throw e;
                        }
                    }
                } catch (Exception e) {
                    callback.onFailure(new IOException((response != null ? "http status code: " + response.code() + " " + response.message() + ": " : "") + e.getMessage(), e));
                }
            }
        
            @Override
            public void onFailure(Call<TargetContentBean> call, Throwable throwable) {
                callback.onFailure(throwable);
            }
        });
        //DA-END:com.vd.gu.client.impl.GUClient.transformWithOldContentWithCallback.String.String.FileBean.TransformWithOldContentCallback:DA-END
    }
    
    /**
     * transform a list of files
     *
     * @param id the id of the previously loaded generator
     * @param request a list of references to load
     * @return ok
     * @throws TechnicalExceptionTransformListDefaultException which is a runtime exception
     */
    @Override
    public Map<String, TargetContentBean> transformList(String id, List<String> request) throws TechnicalExceptionTransformListDefaultException {
        //DA-START:com.vd.gu.client.impl.GUClient.transformList.String.List.Map:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClient.transformList.String.List.Map:DA-ELSE
        Call<Map<String, TargetContentBean>> call = serviceGU.transformList(id, request);
        Response<Map<String, TargetContentBean>> response = null;
        try {
            response = call.execute();
            if (response.isSuccessful()) {
        	    return response.body();
            } else {
                {
                    TechnicalExceptionTransformListDefaultException e = new TechnicalExceptionTransformListDefaultException("status:" + response.code() + ", message:" + response.message());
                    Converter<ResponseBody, ErrorBean> responseBodyConverter = retrofit.responseBodyConverter(ErrorBean.class, new Annotation[0]);
                ErrorBean exceptionData = responseBodyConverter.convert(response.errorBody());
                e.setResponseCodeDefault(exceptionData);
                    throw e;
                }
            }
        } catch (ConnectException e) {
           	throw new RuntimeException(e.getMessage(), e);
        } catch (SocketTimeoutException e) {
            throw new RuntimeException("connection took to long, timeout", e);
        } catch (IOException e) {
           	throw new RuntimeException("IOException: " + (response != null ? "http status code: " + response.code() + " " + response.message() + ": " : "") + e.getMessage(), e);
        }
        //DA-END:com.vd.gu.client.impl.GUClient.transformList.String.List.Map:DA-END
    }
    
    /**
     * transform a list of files
     *
     * @param id
     * @param request
     * @param callback
     */
    @Override
    public void transformListWithCallback(String id, List<String> request, GUClientI.TransformListCallback callback) {
        //DA-START:com.vd.gu.client.impl.GUClient.transformListWithCallback.String.List.TransformListCallback:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClient.transformListWithCallback.String.List.TransformListCallback:DA-ELSE
        Call<Map<String, TargetContentBean>> call = serviceGU.transformList(id, request);
        call.enqueue(new Callback<Map<String, TargetContentBean>>() {
        
        	@Override
        	public void onResponse(Call<Map<String, TargetContentBean>> call, Response<Map<String, TargetContentBean>> response) {
                try {
                    if (response.isSuccessful()) {
                	    callback.onSuccess(response.body());
                    } else {
                        {
                            TechnicalExceptionTransformListDefaultException e = new TechnicalExceptionTransformListDefaultException("status:" + response.code() + ", message:" + response.message());
                            Converter<ResponseBody, ErrorBean> responseBodyConverter = retrofit.responseBodyConverter(ErrorBean.class, new Annotation[0]);
                        ErrorBean exceptionData = responseBodyConverter.convert(response.errorBody());
                        e.setResponseCodeDefault(exceptionData);
                            throw e;
                        }
                    }
                } catch (Exception e) {
                    callback.onFailure(new IOException((response != null ? "http status code: " + response.code() + " " + response.message() + ": " : "") + e.getMessage(), e));
                }
            }
        
            @Override
            public void onFailure(Call<Map<String, TargetContentBean>> call, Throwable throwable) {
                callback.onFailure(throwable);
            }
        });
        //DA-END:com.vd.gu.client.impl.GUClient.transformListWithCallback.String.List.TransformListCallback:DA-END
    }
    
    /**
     * transform a list of files with old content
     *
     * @param id the id of the previously loaded generator
     * @param request a key value map with the reference and the old content to load
     * @return ok
     * @throws TechnicalExceptionTransformListWithOldContentDefaultException which is a runtime exception
     */
    @Override
    public Map<String, TargetContentBean> transformListWithOldContent(String id, Map<String, FileBean> request) throws TechnicalExceptionTransformListWithOldContentDefaultException {
        //DA-START:com.vd.gu.client.impl.GUClient.transformListWithOldContent.String.Map.Map:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClient.transformListWithOldContent.String.Map.Map:DA-ELSE
        Call<Map<String, TargetContentBean>> call = serviceGU.transformListWithOldContent(id, request);
        Response<Map<String, TargetContentBean>> response = null;
        try {
            response = call.execute();
            if (response.isSuccessful()) {
        	    return response.body();
            } else {
                {
                    TechnicalExceptionTransformListWithOldContentDefaultException e = new TechnicalExceptionTransformListWithOldContentDefaultException("status:" + response.code() + ", message:" + response.message());
                    Converter<ResponseBody, ErrorBean> responseBodyConverter = retrofit.responseBodyConverter(ErrorBean.class, new Annotation[0]);
                ErrorBean exceptionData = responseBodyConverter.convert(response.errorBody());
                e.setResponseCodeDefault(exceptionData);
                    throw e;
                }
            }
        } catch (ConnectException e) {
           	throw new RuntimeException(e.getMessage(), e);
        } catch (SocketTimeoutException e) {
            throw new RuntimeException("connection took to long, timeout", e);
        } catch (IOException e) {
           	throw new RuntimeException("IOException: " + (response != null ? "http status code: " + response.code() + " " + response.message() + ": " : "") + e.getMessage(), e);
        }
        //DA-END:com.vd.gu.client.impl.GUClient.transformListWithOldContent.String.Map.Map:DA-END
    }
    
    /**
     * transform a list of files with old content
     *
     * @param id
     * @param request
     * @param callback
     */
    @Override
    public void transformListWithOldContentWithCallback(String id, Map<String, FileBean> request, GUClientI.TransformListWithOldContentCallback callback) {
        //DA-START:com.vd.gu.client.impl.GUClient.transformListWithOldContentWithCallback.String.Map.TransformListWithOldContentCallback:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClient.transformListWithOldContentWithCallback.String.Map.TransformListWithOldContentCallback:DA-ELSE
        Call<Map<String, TargetContentBean>> call = serviceGU.transformListWithOldContent(id, request);
        call.enqueue(new Callback<Map<String, TargetContentBean>>() {
        
        	@Override
        	public void onResponse(Call<Map<String, TargetContentBean>> call, Response<Map<String, TargetContentBean>> response) {
                try {
                    if (response.isSuccessful()) {
                	    callback.onSuccess(response.body());
                    } else {
                        {
                            TechnicalExceptionTransformListWithOldContentDefaultException e = new TechnicalExceptionTransformListWithOldContentDefaultException("status:" + response.code() + ", message:" + response.message());
                            Converter<ResponseBody, ErrorBean> responseBodyConverter = retrofit.responseBodyConverter(ErrorBean.class, new Annotation[0]);
                        ErrorBean exceptionData = responseBodyConverter.convert(response.errorBody());
                        e.setResponseCodeDefault(exceptionData);
                            throw e;
                        }
                    }
                } catch (Exception e) {
                    callback.onFailure(new IOException((response != null ? "http status code: " + response.code() + " " + response.message() + ": " : "") + e.getMessage(), e));
                }
            }
        
            @Override
            public void onFailure(Call<Map<String, TargetContentBean>> call, Throwable throwable) {
                callback.onFailure(throwable);
            }
        });
        //DA-END:com.vd.gu.client.impl.GUClient.transformListWithOldContentWithCallback.String.Map.TransformListWithOldContentCallback:DA-END
    }
    
    /**
     * creates an ObjectMapper instance every time this method is called
     * within this method you can parameterize the object mapper
     *
     * @return
     */
    private ObjectMapper getJacksonObjectMapper() {
        //DA-START:com.vd.gu.client.impl.GUClient.getJacksonObjectMapper.ObjectMapper:DA-START
    	ObjectMapper result = new ObjectMapper();
    	result.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return result;
        //DA-ELSE:com.vd.gu.client.impl.GUClient.getJacksonObjectMapper.ObjectMapper:DA-ELSE
        //ObjectMapper result = new ObjectMapper();
        //return result;
        //DA-END:com.vd.gu.client.impl.GUClient.getJacksonObjectMapper.ObjectMapper:DA-END
    }
    
    
    /**
     * returns a set of converter factories
     *
     * @return
     */
    private Set<Converter.Factory> getConverterFactories() {
        //DA-START:com.vd.gu.client.impl.GUClient.getConverterFactories.Set:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClient.getConverterFactories.Set:DA-ELSE
        Set<Converter.Factory> factories = new HashSet<>();
        Converter.Factory primitiveConverterFactory = getPrimitiveConverterFactory();
        if (primitiveConverterFactory != null) {
        	factories.add(primitiveConverterFactory);
        }
        factories.add(JacksonConverterFactory.create(getJacksonObjectMapper()));
        
        return factories;
        //DA-END:com.vd.gu.client.impl.GUClient.getConverterFactories.Set:DA-END
    }
    
    /**
     * creates a converter factory for primitve types
     *
     * @return
     */
    private Converter.Factory getPrimitiveConverterFactory() {
        //DA-START:com.vd.gu.client.impl.GUClient.getPrimitiveConverterFactory.Factory:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClient.getPrimitiveConverterFactory.Factory:DA-ELSE
        return null;
        //DA-END:com.vd.gu.client.impl.GUClient.getPrimitiveConverterFactory.Factory:DA-END
    }
    
    //DA-START:com.vd.gu.client.impl.GUClient.additional.elements.in.type:DA-START
    public GUClientConfiguration getConfiguration() {
		return configuration;
	}
    //DA-ELSE:com.vd.gu.client.impl.GUClient.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.vd.gu.client.impl.GUClient.additional.elements.in.type:DA-END
} // end of java type
package com.vd.gu.client.impl;

import okhttp3.Interceptor;
import okhttp3.Response;
import java.io.IOException;


/**
 * <table>
 * <tr><td>Generator</td><td>com.gs.vd.gen.java.retrofit.openapi:1.0</td></tr>
 * <tr><td>Element</td><td>GU</td></tr>
 * <tr><td>Type</td><td>Collection of OpenAPI3 models</td></tr>
 * <tr><td>Module</td><td>none</td></tr>
 * </table>
 */
class GUClientApplicationInterceptor extends GUClientInterceptor implements Interceptor { // start of class

    /**
     * settings that are needed for the logic in the interceptor's intercept() method
     */
    private final GUClientApplicationInterceptorSettings settings;
    
    /**
     * creates an instance of GUClientApplicationInterceptor
     *
     * @param settings  the settings
     */
    public GUClientApplicationInterceptor(GUClientApplicationInterceptorSettings settings) {
        //DA-START:com.vd.gu.client.impl.GUClientApplicationInterceptor.GUClientApplicationInterceptorSettings:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClientApplicationInterceptor.GUClientApplicationInterceptorSettings:DA-ELSE
        super();
        this.settings = settings;
        //DA-END:com.vd.gu.client.impl.GUClientApplicationInterceptor.GUClientApplicationInterceptorSettings:DA-END
    }
    
    
    /**
     * getter for the field settings
     *
     * settings that are needed for the logic in the interceptor's intercept() method
     *
     * @return
     */
    public GUClientApplicationInterceptorSettings getSettings() {
        //DA-START:com.vd.gu.client.impl.GUClientApplicationInterceptor.getSettings.GUClientApplicationInterceptorSettings:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClientApplicationInterceptor.getSettings.GUClientApplicationInterceptorSettings:DA-ELSE
        return this.settings;
        //DA-END:com.vd.gu.client.impl.GUClientApplicationInterceptor.getSettings.GUClientApplicationInterceptorSettings:DA-END
    }
    
    /**
     *
     * @param chain  the chain
     * @return
     * @throws IOException which is a checked exception
     */
    @Override
    public Response intercept(Interceptor.Chain chain) throws IOException {
        //DA-START:com.vd.gu.client.impl.GUClientApplicationInterceptor.intercept.Chain.Response:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClientApplicationInterceptor.intercept.Chain.Response:DA-ELSE
        okhttp3.Request request = chain.request();
        Response response = chain.proceed(request);
        return response;
        //DA-END:com.vd.gu.client.impl.GUClientApplicationInterceptor.intercept.Chain.Response:DA-END
    }
    
    //DA-START:com.vd.gu.client.impl.GUClientApplicationInterceptor.additional.elements.in.type:DA-START
    //DA-ELSE:com.vd.gu.client.impl.GUClientApplicationInterceptor.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.vd.gu.client.impl.GUClientApplicationInterceptor.additional.elements.in.type:DA-END
} // end of java type
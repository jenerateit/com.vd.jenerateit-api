package com.vd.gu.client.impl;

import retrofit2.http.POST;
import retrofit2.http.Headers;
import retrofit2.Call;
import okhttp3.ResponseBody;
import retrofit2.http.Path;
import java.util.Map;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import com.vd.gu.definition.basic.LoadResultBean;
import java.util.List;
import com.vd.gu.definition.basic.FileBean;
import retrofit2.http.GET;
import com.vd.gu.definition.basic.TargetContentBean;
import retrofit2.http.PUT;
import retrofit2.http.HEAD;
import com.vd.gu.definition.basic.OptionBean;


/**
 * GU
 *
 * Version: 1.0.0
 *
 * Generate Code on a Virtual Developer Genration Unit
 * <table>
 * <tr><td>Generator</td><td>com.gs.vd.gen.java.retrofit.openapi:1.0</td></tr>
 * <tr><td>Element</td><td>GU</td></tr>
 * <tr><td>Type</td><td>OpenAPI3 model</td></tr>
 * <tr><td>Module</td><td>none</td></tr>
 * </table>
 */
public interface GURetrofitI { // start of class

    
    /**
     * setup a new generator
     *
     * @param generatorId generator id
     * @param generatorVersion generator version
     * @param request optional array of options for the generator
     * @return
     */
    //DA-START:com.vd.gu.client.impl.GURetrofitI.createGenerator.String.String.List.annotations:DA-START
    //DA-ELSE:com.vd.gu.client.impl.GURetrofitI.createGenerator.String.String.List.annotations:DA-ELSE
    @POST(value="v1/generator/{generatorId}/{generatorVersion}")
    //DA-END:com.vd.gu.client.impl.GURetrofitI.createGenerator.String.String.List.annotations:DA-END
    Call<ResponseBody> createGenerator(@Path(value="generatorId") String generatorId, @Path(value="generatorVersion") String generatorVersion, @Body List<OptionBean> request);
    
    /**
     * remove the generator
     *
     * @param id the id of the previously loaded generator
     * @return
     */
    //DA-START:com.vd.gu.client.impl.GURetrofitI.deleteGenerator.String.annotations:DA-START
    //DA-ELSE:com.vd.gu.client.impl.GURetrofitI.deleteGenerator.String.annotations:DA-ELSE
    @DELETE(value="v1/generation/{id}")
    //DA-END:com.vd.gu.client.impl.GURetrofitI.deleteGenerator.String.annotations:DA-END
    Call<ResponseBody> deleteGenerator(@Path(value="id") String id);
    
    /**
     * load the generator with a model
     *
     * @param id the id of the previously loaded generator
     * @param request the model files
     * @return
     */
    //DA-START:com.vd.gu.client.impl.GURetrofitI.loadGenerator.String.List.annotations:DA-START
    //DA-ELSE:com.vd.gu.client.impl.GURetrofitI.loadGenerator.String.List.annotations:DA-ELSE
    @POST(value="v1/generation/{id}")
    @Headers(value={"Accept: application/json"})
    //DA-END:com.vd.gu.client.impl.GURetrofitI.loadGenerator.String.List.annotations:DA-END
    Call<LoadResultBean> loadGenerator(@Path(value="id") String id, @Body List<FileBean> request);
    
    /**
     * transform a file
     *
     * @param id the id of the previously loaded generator
     * @param reference the reference of the content
     * @return
     */
    //DA-START:com.vd.gu.client.impl.GURetrofitI.transform.String.String.annotations:DA-START
    //DA-ELSE:com.vd.gu.client.impl.GURetrofitI.transform.String.String.annotations:DA-ELSE
    @GET(value="v1/generation/{id}/content/{reference}")
    @Headers(value={"Accept: application/json"})
    //DA-END:com.vd.gu.client.impl.GURetrofitI.transform.String.String.annotations:DA-END
    Call<TargetContentBean> transform(@Path(value="id") String id, @Path(value="reference") String reference);
    
    /**
     *
     * @param id
     * @param reference
     * @return
     */
    //DA-START:com.vd.gu.client.impl.GURetrofitI.transformHead.String.String.annotations:DA-START
    //DA-ELSE:com.vd.gu.client.impl.GURetrofitI.transformHead.String.String.annotations:DA-ELSE
    @HEAD(value="v1/generation/{id}/content/{reference}")
    @Headers(value={"Accept: application/json"})
    //DA-END:com.vd.gu.client.impl.GURetrofitI.transformHead.String.String.annotations:DA-END
    Call<Void> transformHead(@Path(value="id") String id, @Path(value="reference") String reference);
    
    /**
     * transform a file with old content
     *
     * @param id the id of the previously loaded generator
     * @param reference the reference of the content
     * @param request the old content
     * @return
     */
    //DA-START:com.vd.gu.client.impl.GURetrofitI.transformWithOldContent.String.String.FileBean.annotations:DA-START
    //DA-ELSE:com.vd.gu.client.impl.GURetrofitI.transformWithOldContent.String.String.FileBean.annotations:DA-ELSE
    @PUT(value="v1/generation/{id}/content/{reference}")
    @Headers(value={"Accept: application/json"})
    //DA-END:com.vd.gu.client.impl.GURetrofitI.transformWithOldContent.String.String.FileBean.annotations:DA-END
    Call<TargetContentBean> transformWithOldContent(@Path(value="id") String id, @Path(value="reference") String reference, @Body FileBean request);
    
    /**
     * transform a list of files
     *
     * @param id the id of the previously loaded generator
     * @param request a list of references to load
     * @return
     */
    //DA-START:com.vd.gu.client.impl.GURetrofitI.transformList.String.List.annotations:DA-START
    //DA-ELSE:com.vd.gu.client.impl.GURetrofitI.transformList.String.List.annotations:DA-ELSE
    @POST(value="v1/generation/{id}/content/")
    @Headers(value={"Accept: application/json"})
    //DA-END:com.vd.gu.client.impl.GURetrofitI.transformList.String.List.annotations:DA-END
    Call<Map<String, TargetContentBean>> transformList(@Path(value="id") String id, @Body List<String> request);
    
    /**
     * transform a list of files with old content
     *
     * @param id the id of the previously loaded generator
     * @param request a key value map with the reference and the old content to load
     * @return
     */
    //DA-START:com.vd.gu.client.impl.GURetrofitI.transformListWithOldContent.String.Map.annotations:DA-START
    //DA-ELSE:com.vd.gu.client.impl.GURetrofitI.transformListWithOldContent.String.Map.annotations:DA-ELSE
    @PUT(value="v1/generation/{id}/content/")
    @Headers(value={"Accept: application/json"})
    //DA-END:com.vd.gu.client.impl.GURetrofitI.transformListWithOldContent.String.Map.annotations:DA-END
    Call<Map<String, TargetContentBean>> transformListWithOldContent(@Path(value="id") String id, @Body Map<String, FileBean> request);
    
    //DA-START:com.vd.gu.client.impl.GURetrofitI.additional.elements.in.type:DA-START
    //DA-ELSE:com.vd.gu.client.impl.GURetrofitI.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.vd.gu.client.impl.GURetrofitI.additional.elements.in.type:DA-END
} // end of java type
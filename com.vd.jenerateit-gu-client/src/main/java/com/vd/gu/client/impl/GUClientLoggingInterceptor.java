package com.vd.gu.client.impl;

import okhttp3.Interceptor;
import okhttp3.Response;
import java.io.IOException;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Connection;
import okhttp3.Headers;
import java.util.concurrent.TimeUnit;
import com.vd.gu.client.impl.GUClientLoggingInterceptorSettings.Level;


/**
 * <table>
 * <tr><td>Generator</td><td>com.gs.vd.gen.java.retrofit.openapi:1.0</td></tr>
 * <tr><td>Element</td><td>GU</td></tr>
 * <tr><td>Type</td><td>Collection of OpenAPI3 models</td></tr>
 * <tr><td>Module</td><td>none</td></tr>
 * </table>
 */
class GUClientLoggingInterceptor extends GUClientInterceptor implements Interceptor { // start of class

    /**
     * settings that are needed for the logic in the interceptor's intercept() method
     */
    private final GUClientLoggingInterceptorSettings settings;
    
    /**
     * creates an instance of GUClientLoggingInterceptor
     *
     * @param settings  the settings
     */
    public GUClientLoggingInterceptor(GUClientLoggingInterceptorSettings settings) {
        //DA-START:com.vd.gu.client.impl.GUClientLoggingInterceptor.GUClientLoggingInterceptorSettings:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClientLoggingInterceptor.GUClientLoggingInterceptorSettings:DA-ELSE
        super();
        this.settings = settings;
        //DA-END:com.vd.gu.client.impl.GUClientLoggingInterceptor.GUClientLoggingInterceptorSettings:DA-END
    }
    
    
    /**
     * getter for the field settings
     *
     * settings that are needed for the logic in the interceptor's intercept() method
     *
     * @return
     */
    public GUClientLoggingInterceptorSettings getSettings() {
        //DA-START:com.vd.gu.client.impl.GUClientLoggingInterceptor.getSettings.GUClientLoggingInterceptorSettings:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClientLoggingInterceptor.getSettings.GUClientLoggingInterceptorSettings:DA-ELSE
        return this.settings;
        //DA-END:com.vd.gu.client.impl.GUClientLoggingInterceptor.getSettings.GUClientLoggingInterceptorSettings:DA-END
    }
    
    /**
     *
     * @param chain  the chain
     * @return
     * @throws IOException which is a checked exception
     */
    @Override
    public Response intercept(Interceptor.Chain chain) throws IOException {
        //DA-START:com.vd.gu.client.impl.GUClientLoggingInterceptor.intercept.Chain.Response:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClientLoggingInterceptor.intercept.Chain.Response:DA-ELSE
        Request request = chain.request();
        if (settings.getLogLevel() == Level.NONE) {
            return chain.proceed(request);
        }
        // log request 
        RequestBody requestBody = request.body();
        boolean hasRequestBody = requestBody != null;
        Connection connection = chain.connection();
        String requestStartMessage = "--> " + request.method() + ' ' + request.url() + (connection != null ? " " + connection.protocol() : "");
        if (hasRequestBody) {
            requestStartMessage += " (" + requestBody.contentLength() + "-byte body)";
        }
        settings.log(requestStartMessage);
        
        if (settings.getLogLevel() == Level.HEADERS) {
            if (hasRequestBody) {
                if (requestBody.contentType() != null) {
                    settings.log("Content-Type: " + requestBody.contentType());
                }
                if (requestBody.contentLength() != -1) {
                    settings.log("Content-Length: " + requestBody.contentLength());
                }
            }
            Headers headers = request.headers();
            for (int i = 0, count = headers.size(); i < count; i++) {
                String name = headers.name(i);
                if (!"Content-Type".equalsIgnoreCase(name) && !"Content-Length".equalsIgnoreCase(name)) {
                    settings.log(name + ": " + headers.value(i));
                }
            }
        }
        settings.log("--> END " + request.method());
        // log response 
        long startNs = System.nanoTime();
        Response response;
        try {
            response = chain.proceed(request);
        } catch (Exception e) {
            settings.log("<-- HTTP FAILED: " + e);
            throw e;
        }
        long tookMs = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startNs);
        settings.log("<-- " + response.code() + (response.message().isEmpty() ? "" : ' ' + response.message()) + ' ' + response.request().url() + " (" + tookMs + "ms)");
        if (settings.getLogLevel() == Level.HEADERS) {
            Headers headers = response.headers();
            for (int i = 0, count = headers.size(); i < count; i++) {
                settings.log(headers.name(i) + ": " + headers.value(i));
            }
        }
        settings.log("<-- END HTTP");
        return response;
        //DA-END:com.vd.gu.client.impl.GUClientLoggingInterceptor.intercept.Chain.Response:DA-END
    }
    
    //DA-START:com.vd.gu.client.impl.GUClientLoggingInterceptor.additional.elements.in.type:DA-START
    //DA-ELSE:com.vd.gu.client.impl.GUClientLoggingInterceptor.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.vd.gu.client.impl.GUClientLoggingInterceptor.additional.elements.in.type:DA-END
} // end of java type
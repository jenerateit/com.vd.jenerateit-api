package com.vd.gu.client.impl;

import okhttp3.Interceptor;
import okhttp3.Response;
import java.io.IOException;
import okhttp3.Request.Builder;


/**
 * <table>
 * <tr><td>Generator</td><td>com.gs.vd.gen.java.retrofit.openapi:1.0</td></tr>
 * <tr><td>Element</td><td>GU</td></tr>
 * <tr><td>Type</td><td>Collection of OpenAPI3 models</td></tr>
 * <tr><td>Module</td><td>none</td></tr>
 * </table>
 */
class GUClientNetworkInterceptor extends GUClientInterceptor implements Interceptor { // start of class

    /**
     * settings that are needed for the logic in the interceptor's intercept() method
     */
    private final GUClientNetworkInterceptorSettings settings;
    
    /**
     * creates an instance of GUClientNetworkInterceptor
     *
     * @param settings  the settings
     */
    public GUClientNetworkInterceptor(GUClientNetworkInterceptorSettings settings) {
        //DA-START:com.vd.gu.client.impl.GUClientNetworkInterceptor.GUClientNetworkInterceptorSettings:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClientNetworkInterceptor.GUClientNetworkInterceptorSettings:DA-ELSE
        super();
        this.settings = settings;
        //DA-END:com.vd.gu.client.impl.GUClientNetworkInterceptor.GUClientNetworkInterceptorSettings:DA-END
    }
    
    
    /**
     * getter for the field settings
     *
     * settings that are needed for the logic in the interceptor's intercept() method
     *
     * @return
     */
    public GUClientNetworkInterceptorSettings getSettings() {
        //DA-START:com.vd.gu.client.impl.GUClientNetworkInterceptor.getSettings.GUClientNetworkInterceptorSettings:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClientNetworkInterceptor.getSettings.GUClientNetworkInterceptorSettings:DA-ELSE
        return this.settings;
        //DA-END:com.vd.gu.client.impl.GUClientNetworkInterceptor.getSettings.GUClientNetworkInterceptorSettings:DA-END
    }
    
    /**
     *
     * @param chain  the chain
     * @return
     * @throws IOException which is a checked exception
     */
    @Override
    public Response intercept(Interceptor.Chain chain) throws IOException {
        //DA-START:com.vd.gu.client.impl.GUClientNetworkInterceptor.intercept.Chain.Response:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClientNetworkInterceptor.intercept.Chain.Response:DA-ELSE
        okhttp3.Request request = chain.request();
        boolean jsonAndXmlInAcceptHeader = isRequestIncludingJsonAndXmlAcceptHeader(request);
        if (!this.settings.getHttpHeaders().isEmpty()) {
            Builder builder = request.newBuilder();
            for (String header : this.settings.getHttpHeaders().keySet()) {
                if ("Accept".equalsIgnoreCase(header)) {
                    if (jsonAndXmlInAcceptHeader) {
                        builder.header(header, String.join(",", settings.getHttpHeaders().get(header)));
                    }
                } else {
                    builder.header(header, String.join(",", settings.getHttpHeaders().get(header)));
                }
            }
            request = builder.method(request.method(), request.body()).build();
        }
        Response response = chain.proceed(request);
        return response;
        //DA-END:com.vd.gu.client.impl.GUClientNetworkInterceptor.intercept.Chain.Response:DA-END
    }
    
    //DA-START:com.vd.gu.client.impl.GUClientNetworkInterceptor.additional.elements.in.type:DA-START
    //DA-ELSE:com.vd.gu.client.impl.GUClientNetworkInterceptor.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.vd.gu.client.impl.GUClientNetworkInterceptor.additional.elements.in.type:DA-END
} // end of java type
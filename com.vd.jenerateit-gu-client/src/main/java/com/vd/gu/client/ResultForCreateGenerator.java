package com.vd.gu.client;



/**
 * <table>
 * <tr><td>Generator</td><td>com.gs.vd.gen.java.retrofit.openapi:1.0</td></tr>
 * <tr><td>Element</td><td>createGenerator</td></tr>
 * <tr><td>Type</td><td>Operation</td></tr>
 * <tr><td>Module</td><td>none</td></tr>
 * </table>
 */
public class ResultForCreateGenerator { // start of class

    private String location;
    
    /**
     * creates an instance of ResultForCreateGenerator
     *
     * @param location  the location
     */
    public ResultForCreateGenerator(String location) {
        //DA-START:com.vd.gu.client.ResultForCreateGenerator.String:DA-START
        //DA-ELSE:com.vd.gu.client.ResultForCreateGenerator.String:DA-ELSE
        this.location = location;
        //DA-END:com.vd.gu.client.ResultForCreateGenerator.String:DA-END
    }
    
    
    /**
     * getter for the field location
     *
     *
     *
     * @return
     */
    public String getLocation() {
        //DA-START:com.vd.gu.client.ResultForCreateGenerator.getLocation.String:DA-START
        //DA-ELSE:com.vd.gu.client.ResultForCreateGenerator.getLocation.String:DA-ELSE
        return this.location;
        //DA-END:com.vd.gu.client.ResultForCreateGenerator.getLocation.String:DA-END
    }
    
    /**
     * setter for the field location
     *
     *
     *
     * @param location  the location
     */
    public void setLocation(String location) {
        //DA-START:com.vd.gu.client.ResultForCreateGenerator.setLocation.String:DA-START
        //DA-ELSE:com.vd.gu.client.ResultForCreateGenerator.setLocation.String:DA-ELSE
        this.location = location;
        //DA-END:com.vd.gu.client.ResultForCreateGenerator.setLocation.String:DA-END
    }
    
    //DA-START:com.vd.gu.client.ResultForCreateGenerator.additional.elements.in.type:DA-START
    public String getId() {
    	return this.location.substring(this.location.lastIndexOf("/") + 1);
    }
    //DA-ELSE:com.vd.gu.client.ResultForCreateGenerator.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.vd.gu.client.ResultForCreateGenerator.additional.elements.in.type:DA-END
} // end of java type
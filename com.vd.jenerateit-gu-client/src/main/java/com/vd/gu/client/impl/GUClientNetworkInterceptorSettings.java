package com.vd.gu.client.impl;

import java.util.LinkedHashMap;
import java.util.List;


/**
 * <table>
 * <tr><td>Generator</td><td>com.gs.vd.gen.java.retrofit.openapi:1.0</td></tr>
 * <tr><td>Element</td><td>GU</td></tr>
 * <tr><td>Type</td><td>Collection of OpenAPI3 models</td></tr>
 * <tr><td>Module</td><td>none</td></tr>
 * </table>
 */
class GUClientNetworkInterceptorSettings { // start of class

    /**
     * the rest client object that holds the main logic and makes use of retrofit interfaces
     */
    private final GUClient restClient;
    
    /**
     * This field holds HTTP headers that should always be set on every request that is sent through the REST client. Its values override the values that are statially set by means of Retrofit annotations.
     */
    private LinkedHashMap<String, List<String>> httpHeaders = new LinkedHashMap<>();
    
    /**
     * creates an instance of GUClientNetworkInterceptorSettings
     *
     * @param restClient  the restClient
     */
    public GUClientNetworkInterceptorSettings(GUClient restClient) {
        //DA-START:com.vd.gu.client.impl.GUClientNetworkInterceptorSettings.GUClient:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClientNetworkInterceptorSettings.GUClient:DA-ELSE
        this.restClient = restClient;
        //DA-END:com.vd.gu.client.impl.GUClientNetworkInterceptorSettings.GUClient:DA-END
    }
    
    
    /**
     * setter for the field httpHeaders
     *
     * This field holds HTTP headers that should always be set on every request that is sent through the REST client. Its values override the values that are statially set by means of Retrofit annotations.
     *
     * @param httpHeaders  the httpHeaders
     */
    public void setHttpHeaders(LinkedHashMap<String, List<String>> httpHeaders) {
        //DA-START:com.vd.gu.client.impl.GUClientNetworkInterceptorSettings.setHttpHeaders.LinkedHashMap:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClientNetworkInterceptorSettings.setHttpHeaders.LinkedHashMap:DA-ELSE
        this.httpHeaders = httpHeaders;
        //DA-END:com.vd.gu.client.impl.GUClientNetworkInterceptorSettings.setHttpHeaders.LinkedHashMap:DA-END
    }
    
    /**
     * getter for the field httpHeaders
     *
     * This field holds HTTP headers that should always be set on every request that is sent through the REST client. Its values override the values that are statially set by means of Retrofit annotations.
     *
     * @return
     */
    public LinkedHashMap<String, List<String>> getHttpHeaders() {
        //DA-START:com.vd.gu.client.impl.GUClientNetworkInterceptorSettings.getHttpHeaders.LinkedHashMap:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClientNetworkInterceptorSettings.getHttpHeaders.LinkedHashMap:DA-ELSE
        return this.httpHeaders;
        //DA-END:com.vd.gu.client.impl.GUClientNetworkInterceptorSettings.getHttpHeaders.LinkedHashMap:DA-END
    }
    
    //DA-START:com.vd.gu.client.impl.GUClientNetworkInterceptorSettings.additional.elements.in.type:DA-START
    //DA-ELSE:com.vd.gu.client.impl.GUClientNetworkInterceptorSettings.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.vd.gu.client.impl.GUClientNetworkInterceptorSettings.additional.elements.in.type:DA-END
} // end of java type
package com.vd.gu.client.impl;

import java.net.URL;

import okhttp3.OkHttpClient;
import java.util.HashSet;
import okhttp3.Interceptor;
import java.util.Map;
import okhttp3.Response;
import java.io.IOException;


/**
 * <table>
 * <tr><td>Generator</td><td>com.gs.vd.gen.java.retrofit.openapi:1.0</td></tr>
 * <tr><td>Element</td><td>GU</td></tr>
 * <tr><td>Type</td><td>Collection of OpenAPI3 models</td></tr>
 * <tr><td>Module</td><td>none</td></tr>
 * </table>
 */
public class GUClientConfiguration { // start of class

    /**
     * the url to be used to connect to a server to call the service
     */
    private final URL url;
    
    /**
     * number of milliseconds to be used as the connection timeout for the http connection
     */
    private final Integer timeoutConnect;
    
    /**
     * number of milliseconds to be used as the timeout for reading from an http connection
     */
    private final Integer timeoutRead;
    
    /**
     * number of milliseconds to be used as the timeout for writing to an http connection
     */
    private final Integer timeoutWrite;
    
    /**
     * The media type to be used for HTTP Accept header and de-/serialization.
     * This setting has an effect only when 'application/json' _and_ 'application/xml' are set in the Retrofit interface, like so:
     * @Headers(value={"Accept: application/xml", "Accept: application/json"})
     */
    private final String mediaType;
    
    /**
     * collection of interceptors to add to the connection. Can be used to pass headers
     */
    private HashSet<Interceptor> interceptors = new HashSet<>();
    
    /**
     * creates an instance of GUClientConfiguration
     *
     * @param url  the url
     * @param timeoutConnect  the timeoutConnect
     * @param timeoutRead  the timeoutRead
     * @param timeoutWrite  the timeoutWrite
     * @param mediaType  the mediaType
     */
    public GUClientConfiguration(URL url, Integer timeoutConnect, Integer timeoutRead, Integer timeoutWrite, String mediaType) {
        //DA-START:com.vd.gu.client.impl.GUClientConfiguration.URL.Integer.Integer.Integer.String:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClientConfiguration.URL.Integer.Integer.Integer.String:DA-ELSE
        this.url = url;
        this.timeoutConnect = timeoutConnect;
        this.timeoutRead = timeoutRead;
        this.timeoutWrite = timeoutWrite;
        this.mediaType = mediaType;
        //DA-END:com.vd.gu.client.impl.GUClientConfiguration.URL.Integer.Integer.Integer.String:DA-END
    }
    
    
    /**
     * getter for the field url
     *
     * the url to be used to connect to a server to call the service
     *
     * @return
     */
    public URL getUrl() {
        //DA-START:com.vd.gu.client.impl.GUClientConfiguration.getUrl.URL:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClientConfiguration.getUrl.URL:DA-ELSE
        return this.url;
        //DA-END:com.vd.gu.client.impl.GUClientConfiguration.getUrl.URL:DA-END
    }
    
    /**
     * getter for the field timeoutConnect
     *
     * number of milliseconds to be used as the connection timeout for the http connection
     *
     * @return
     */
    public Integer getTimeoutConnect() {
        //DA-START:com.vd.gu.client.impl.GUClientConfiguration.getTimeoutConnect.Integer:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClientConfiguration.getTimeoutConnect.Integer:DA-ELSE
        return this.timeoutConnect;
        //DA-END:com.vd.gu.client.impl.GUClientConfiguration.getTimeoutConnect.Integer:DA-END
    }
    
    /**
     * getter for the field timeoutRead
     *
     * number of milliseconds to be used as the timeout for reading from an http connection
     *
     * @return
     */
    public Integer getTimeoutRead() {
        //DA-START:com.vd.gu.client.impl.GUClientConfiguration.getTimeoutRead.Integer:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClientConfiguration.getTimeoutRead.Integer:DA-ELSE
        return this.timeoutRead;
        //DA-END:com.vd.gu.client.impl.GUClientConfiguration.getTimeoutRead.Integer:DA-END
    }
    
    /**
     * getter for the field timeoutWrite
     *
     * number of milliseconds to be used as the timeout for writing to an http connection
     *
     * @return
     */
    public Integer getTimeoutWrite() {
        //DA-START:com.vd.gu.client.impl.GUClientConfiguration.getTimeoutWrite.Integer:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClientConfiguration.getTimeoutWrite.Integer:DA-ELSE
        return this.timeoutWrite;
        //DA-END:com.vd.gu.client.impl.GUClientConfiguration.getTimeoutWrite.Integer:DA-END
    }
    
    /**
     * getter for the field mediaType
     *
     * The media type to be used for HTTP Accept header and de-/serialization.
     * This setting has an effect only when 'application/json' _and_ 'application/xml' are set in the Retrofit interface, like so:
     * @Headers(value={"Accept: application/xml", "Accept: application/json"})
     *
     * @return
     */
    public String getMediaType() {
        //DA-START:com.vd.gu.client.impl.GUClientConfiguration.getMediaType.String:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClientConfiguration.getMediaType.String:DA-ELSE
        return this.mediaType;
        //DA-END:com.vd.gu.client.impl.GUClientConfiguration.getMediaType.String:DA-END
    }
    
    /**
     * adder for the field interceptors
     *
     * collection of interceptors to add to the connection. Can be used to pass headers
     *
     * @param element  the element
     * @return
     */
    public boolean addInterceptors(Interceptor element) {
        //DA-START:com.vd.gu.client.impl.GUClientConfiguration.addInterceptors.Interceptor.boolean:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClientConfiguration.addInterceptors.Interceptor.boolean:DA-ELSE
        return this.getInterceptors().add(element);
        //DA-END:com.vd.gu.client.impl.GUClientConfiguration.addInterceptors.Interceptor.boolean:DA-END
    }
    
    /**
     * remover for the field interceptors
     *
     * collection of interceptors to add to the connection. Can be used to pass headers
     *
     * @param element  the element
     * @return
     */
    public boolean removeInterceptors(Interceptor element) {
        //DA-START:com.vd.gu.client.impl.GUClientConfiguration.removeInterceptors.Interceptor.boolean:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClientConfiguration.removeInterceptors.Interceptor.boolean:DA-ELSE
        return this.interceptors.remove(element);
        //DA-END:com.vd.gu.client.impl.GUClientConfiguration.removeInterceptors.Interceptor.boolean:DA-END
    }
    
    /**
     * getter for the field interceptors
     *
     * collection of interceptors to add to the connection. Can be used to pass headers
     *
     * @return
     */
    public HashSet<Interceptor> getInterceptors() {
        //DA-START:com.vd.gu.client.impl.GUClientConfiguration.getInterceptors.HashSet:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClientConfiguration.getInterceptors.HashSet:DA-ELSE
        return this.interceptors;
        //DA-END:com.vd.gu.client.impl.GUClientConfiguration.getInterceptors.HashSet:DA-END
    }
    
    /**
     * This is the callback where the OkHttp client builder can be customized before the client is built.
     * Change the method implementation to add a proxy, set an authenticator or perform similar configuration tasks.
     *
     * @param builder  the builder
     * @return
     */
    public void customizeOkHttpClientBuilder(OkHttpClient.Builder builder) {
        //DA-START:com.vd.gu.client.impl.GUClientConfiguration.customizeOkHttpClientBuilder.Builder.void:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClientConfiguration.customizeOkHttpClientBuilder.Builder.void:DA-ELSE
        return;
        //DA-END:com.vd.gu.client.impl.GUClientConfiguration.customizeOkHttpClientBuilder.Builder.void:DA-END
    }
    
    /**
     * Add key values as headers to each request.
     * To add dynamic headers use the addInterceptors method and create an Interceptor.
     *
     * @param headers  the headers
     * @return
     */
    public void setHeaders(Map<String, String> headers) {
        //DA-START:com.vd.gu.client.impl.GUClientConfiguration.setHeaders.Map.void:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClientConfiguration.setHeaders.Map.void:DA-ELSE
        this.addInterceptors(new HeaderInterceptor(headers));
        //DA-END:com.vd.gu.client.impl.GUClientConfiguration.setHeaders.Map.void:DA-END
    }
    
    
    
    /**
     * <table>
     * <tr><td>Element</td><td>BasicAuthInterceptor</td></tr>
     * <tr><td>Type</td><td>JavaClass</td></tr>
     * <tr><td>Module</td><td>none</td></tr>
     * </table>
     */
    public static class BasicAuthInterceptor implements Interceptor { // start of class
    
        private final String credentials;
        
        /**
         * creates an instance of BasicAuthInterceptor
         *
         * @param user  the user
         * @param password  the password
         */
        public BasicAuthInterceptor(String user, String password) {
            //DA-START:com.vd.gu.client.impl.GUClientConfiguration.BasicAuthInterceptor.String.String:DA-START
            //DA-ELSE:com.vd.gu.client.impl.GUClientConfiguration.BasicAuthInterceptor.String.String:DA-ELSE
            this.credentials = okhttp3.Credentials.basic(user, password);
            //DA-END:com.vd.gu.client.impl.GUClientConfiguration.BasicAuthInterceptor.String.String:DA-END
        }
        
        
        /**
         *
         * @param chain  the chain
         * @return
         * @throws IOException which is a checked exception
         */
        public Response intercept(Interceptor.Chain chain) throws IOException {
            //DA-START:com.vd.gu.client.impl.GUClientConfiguration.BasicAuthInterceptor.intercept.Chain.Response:DA-START
            //DA-ELSE:com.vd.gu.client.impl.GUClientConfiguration.BasicAuthInterceptor.intercept.Chain.Response:DA-ELSE
            okhttp3.Request request = chain.request();
            okhttp3.Request authenticatedRequest = request.newBuilder().header("Authorization", credentials).build();
            return chain.proceed(authenticatedRequest);
            //DA-END:com.vd.gu.client.impl.GUClientConfiguration.BasicAuthInterceptor.intercept.Chain.Response:DA-END
        }
        //DA-START:com.vd.gu.client.impl.GUClientConfiguration.BasicAuthInterceptor.additional.elements.in.type:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClientConfiguration.BasicAuthInterceptor.additional.elements.in.type:DA-ELSE
        // add any additional element in here that you need in your Java type but is not generated 
        //DA-END:com.vd.gu.client.impl.GUClientConfiguration.BasicAuthInterceptor.additional.elements.in.type:DA-END
    } // end of java type
    
    
    /**
     * <table>
     * <tr><td>Element</td><td>HeaderInterceptor</td></tr>
     * <tr><td>Type</td><td>JavaClass</td></tr>
     * <tr><td>Module</td><td>none</td></tr>
     * </table>
     */
    public static class HeaderInterceptor implements Interceptor { // start of class
    
        private final Map<String, String> headers;
        
        /**
         * creates an instance of HeaderInterceptor
         *
         * @param headers  the headers
         */
        public HeaderInterceptor(Map<String, String> headers) {
            //DA-START:com.vd.gu.client.impl.GUClientConfiguration.HeaderInterceptor.Map:DA-START
            //DA-ELSE:com.vd.gu.client.impl.GUClientConfiguration.HeaderInterceptor.Map:DA-ELSE
            this.headers = headers;
            //DA-END:com.vd.gu.client.impl.GUClientConfiguration.HeaderInterceptor.Map:DA-END
        }
        
        
        /**
         *
         * @param chain  the chain
         * @return
         * @throws IOException which is a checked exception
         */
        public Response intercept(Interceptor.Chain chain) throws IOException {
            //DA-START:com.vd.gu.client.impl.GUClientConfiguration.HeaderInterceptor.intercept.Chain.Response:DA-START
            //DA-ELSE:com.vd.gu.client.impl.GUClientConfiguration.HeaderInterceptor.intercept.Chain.Response:DA-ELSE
            okhttp3.Request request = chain.request();
            okhttp3.Request authenticatedRequest = request.newBuilder().headers(okhttp3.Headers.of(this.headers)).build();
            return chain.proceed(authenticatedRequest);
            //DA-END:com.vd.gu.client.impl.GUClientConfiguration.HeaderInterceptor.intercept.Chain.Response:DA-END
        }
        //DA-START:com.vd.gu.client.impl.GUClientConfiguration.HeaderInterceptor.additional.elements.in.type:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClientConfiguration.HeaderInterceptor.additional.elements.in.type:DA-ELSE
        // add any additional element in here that you need in your Java type but is not generated 
        //DA-END:com.vd.gu.client.impl.GUClientConfiguration.HeaderInterceptor.additional.elements.in.type:DA-END
    } // end of java type
    
    //DA-START:com.vd.gu.client.impl.GUClientConfiguration.additional.elements.in.type:DA-START
    /**
     * @param url
     * @param timeoutConnect
     * @param timeoutRead
     * @param timeoutWrite
     */
    public GUClientConfiguration(URL url, Integer timeoutConnect, Integer timeoutRead, Integer timeoutWrite) {
    	this(url, timeoutConnect, timeoutRead, timeoutWrite, "application/json");
    }
    
    public static interface Logging {
    	void log(String message);
    }
    
    private Logging logging;
    
    public void setLogging(Logging logging) {
		this.logging = logging;
	}
    
    public Logging getLogging() {
		return logging;
	}
    //DA-ELSE:com.vd.gu.client.impl.GUClientConfiguration.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.vd.gu.client.impl.GUClientConfiguration.additional.elements.in.type:DA-END
} // end of java type
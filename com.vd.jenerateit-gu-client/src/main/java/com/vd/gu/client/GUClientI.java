package com.vd.gu.client;

import java.util.Map;
import com.vd.gu.TechnicalExceptionCreateGeneratorDefaultException;
import com.vd.gu.TechnicalExceptionDeleteGeneratorDefaultException;
import com.vd.gu.definition.basic.LoadResultBean;
import java.util.List;
import com.vd.gu.definition.basic.FileBean;
import com.vd.gu.TechnicalExceptionLoadGeneratorDefaultException;
import com.vd.gu.definition.basic.TargetContentBean;
import com.vd.gu.TechnicalExceptionTransformDefaultException;
import com.vd.gu.TechnicalExceptionTransformWithOldContentDefaultException;
import com.vd.gu.TechnicalExceptionTransformListDefaultException;
import com.vd.gu.TechnicalExceptionTransformListWithOldContentDefaultException;
import com.vd.gu.definition.basic.OptionBean;


/**
 * <table>
 * <tr><td>Generator</td><td>com.gs.vd.gen.java.retrofit.openapi:1.0</td></tr>
 * <tr><td>Element</td><td>GU</td></tr>
 * <tr><td>Type</td><td>Collection of OpenAPI3 models</td></tr>
 * <tr><td>Module</td><td>none</td></tr>
 * </table>
 */
public interface GUClientI { // start of class

    
    /**
     *
     * @param generatorId  the generatorId
     * @param generatorVersion  the generatorVersion
     * @param request  the request
     * @return
     * @throws TechnicalExceptionCreateGeneratorDefaultException which is a runtime exception
     */
    ResultForCreateGenerator createGenerator(String generatorId, String generatorVersion, List<OptionBean> request) throws TechnicalExceptionCreateGeneratorDefaultException;
    
    /**
     *
     * @param generatorId  the generatorId
     * @param generatorVersion  the generatorVersion
     * @param request  the request
     * @param callback  the callback
     */
    void createGeneratorWithCallback(String generatorId, String generatorVersion, List<OptionBean> request, GUClientI.CreateGeneratorCallback callback);
    
    /**
     *
     * @param id  the id
     * @throws TechnicalExceptionDeleteGeneratorDefaultException which is a runtime exception
     */
    void deleteGenerator(String id) throws TechnicalExceptionDeleteGeneratorDefaultException;
    
    /**
     *
     * @param id  the id
     * @param callback  the callback
     */
    void deleteGeneratorWithCallback(String id, GUClientI.DeleteGeneratorCallback callback);
    
    /**
     *
     * @param id  the id
     * @param request  the request
     * @return
     * @throws TechnicalExceptionLoadGeneratorDefaultException which is a runtime exception
     */
    LoadResultBean loadGenerator(String id, List<FileBean> request) throws TechnicalExceptionLoadGeneratorDefaultException;
    
    /**
     *
     * @param id  the id
     * @param request  the request
     * @param callback  the callback
     */
    void loadGeneratorWithCallback(String id, List<FileBean> request, GUClientI.LoadGeneratorCallback callback);
    
    /**
     *
     * @param id  the id
     * @param reference  the reference
     * @return
     * @throws TechnicalExceptionTransformDefaultException which is a runtime exception
     */
    TargetContentBean transform(String id, String reference) throws TechnicalExceptionTransformDefaultException;
    
    /**
     *
     * @param id  the id
     * @param reference  the reference
     * @param callback  the callback
     */
    void transformWithCallback(String id, String reference, GUClientI.TransformCallback callback);
    
    /**
     *
     * @param id  the id
     * @param reference  the reference
     * @return
     */
    Map<String, List<String>> transformHead(String id, String reference);
    
    /**
     *
     * @param id  the id
     * @param reference  the reference
     * @param callback  the callback
     */
    void transformHeadWithCallback(String id, String reference, GUClientI.TransformHeadCallback callback);
    
    /**
     *
     * @param id  the id
     * @param reference  the reference
     * @param request  the request
     * @return
     * @throws TechnicalExceptionTransformWithOldContentDefaultException which is a runtime exception
     */
    TargetContentBean transformWithOldContent(String id, String reference, FileBean request) throws TechnicalExceptionTransformWithOldContentDefaultException;
    
    /**
     *
     * @param id  the id
     * @param reference  the reference
     * @param request  the request
     * @param callback  the callback
     */
    void transformWithOldContentWithCallback(String id, String reference, FileBean request, GUClientI.TransformWithOldContentCallback callback);
    
    /**
     *
     * @param id  the id
     * @param request  the request
     * @return
     * @throws TechnicalExceptionTransformListDefaultException which is a runtime exception
     */
    Map<String, TargetContentBean> transformList(String id, List<String> request) throws TechnicalExceptionTransformListDefaultException;
    
    /**
     *
     * @param id  the id
     * @param request  the request
     * @param callback  the callback
     */
    void transformListWithCallback(String id, List<String> request, GUClientI.TransformListCallback callback);
    
    /**
     *
     * @param id  the id
     * @param request  the request
     * @return
     * @throws TechnicalExceptionTransformListWithOldContentDefaultException which is a runtime exception
     */
    Map<String, TargetContentBean> transformListWithOldContent(String id, Map<String, FileBean> request) throws TechnicalExceptionTransformListWithOldContentDefaultException;
    
    /**
     *
     * @param id  the id
     * @param request  the request
     * @param callback  the callback
     */
    void transformListWithOldContentWithCallback(String id, Map<String, FileBean> request, GUClientI.TransformListWithOldContentCallback callback);
    
    
    
    /**
     * setup a new generator
     * <table>
     * <tr><td>Generator</td><td>com.gs.vd.gen.java.retrofit.openapi:1.0</td></tr>
     * <tr><td>Element</td><td>createGenerator</td></tr>
     * <tr><td>Type</td><td>Operation</td></tr>
     * <tr><td>Module</td><td>none</td></tr>
     * </table>
     */
    public static interface CreateGeneratorCallback { // start of class
    
        
        /**
         * gets called when the http request has successfully completed and returned and deserialized the data (if it is supposed to return some)
         *
         * @param result  the result
         */
        void onSuccess(ResultForCreateGenerator result);
        
        /**
         * gets called when a technical exception has occurred, e.g. network problems or http status 3xx or 5xx
         * <br>
         * error
         * exception type to be used as default, for cases where there is not an explicit exception type available
         *
         * @param exc  the exc
         */
        void onTechnicalExceptionCreateGeneratorDefault(TechnicalExceptionCreateGeneratorDefaultException exc);
        
        /**
         * When the Throwable passed to the failure callback is an IOException, this means that it was a network problem (socket timeout, unknown host, etc.). Any other exception means something broke either in serializing/deserializing the data or it's a configuration problem.
         *
         * @param throwable  the throwable
         */
        void onFailure(Throwable throwable);
        //DA-START:com.vd.gu.client.impl.GUClientI.CreateGeneratorCallback.additional.elements.in.type:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClientI.CreateGeneratorCallback.additional.elements.in.type:DA-ELSE
        // add any additional element in here that you need in your Java type but is not generated 
        //DA-END:com.vd.gu.client.impl.GUClientI.CreateGeneratorCallback.additional.elements.in.type:DA-END
    } // end of java type
    
    
    /**
     * remove the generator
     * <table>
     * <tr><td>Generator</td><td>com.gs.vd.gen.java.retrofit.openapi:1.0</td></tr>
     * <tr><td>Element</td><td>deleteGenerator</td></tr>
     * <tr><td>Type</td><td>Operation</td></tr>
     * <tr><td>Module</td><td>none</td></tr>
     * </table>
     */
    public static interface DeleteGeneratorCallback { // start of class
    
        
        /**
         * gets called when the http request has successfully completed and returned and deserialized the data (if it is supposed to return some)
         */
        void onSuccess();
        
        /**
         * gets called when a technical exception has occurred, e.g. network problems or http status 3xx or 5xx
         * <br>
         * error
         * exception type to be used as default, for cases where there is not an explicit exception type available
         *
         * @param exc  the exc
         */
        void onTechnicalExceptionDeleteGeneratorDefault(TechnicalExceptionDeleteGeneratorDefaultException exc);
        
        /**
         * When the Throwable passed to the failure callback is an IOException, this means that it was a network problem (socket timeout, unknown host, etc.). Any other exception means something broke either in serializing/deserializing the data or it's a configuration problem.
         *
         * @param throwable  the throwable
         */
        void onFailure(Throwable throwable);
        //DA-START:com.vd.gu.client.impl.GUClientI.DeleteGeneratorCallback.additional.elements.in.type:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClientI.DeleteGeneratorCallback.additional.elements.in.type:DA-ELSE
        // add any additional element in here that you need in your Java type but is not generated 
        //DA-END:com.vd.gu.client.impl.GUClientI.DeleteGeneratorCallback.additional.elements.in.type:DA-END
    } // end of java type
    
    
    /**
     * load the generator with a model
     * <table>
     * <tr><td>Generator</td><td>com.gs.vd.gen.java.retrofit.openapi:1.0</td></tr>
     * <tr><td>Element</td><td>loadGenerator</td></tr>
     * <tr><td>Type</td><td>Operation</td></tr>
     * <tr><td>Module</td><td>none</td></tr>
     * </table>
     */
    public static interface LoadGeneratorCallback { // start of class
    
        
        /**
         * gets called when the http request has successfully completed and returned and deserialized the data (if it is supposed to return some)
         *
         * @param result  the result
         */
        void onSuccess(LoadResultBean result);
        
        /**
         * gets called when a technical exception has occurred, e.g. network problems or http status 3xx or 5xx
         * <br>
         * error
         * exception type to be used as default, for cases where there is not an explicit exception type available
         *
         * @param exc  the exc
         */
        void onTechnicalExceptionLoadGeneratorDefault(TechnicalExceptionLoadGeneratorDefaultException exc);
        
        /**
         * When the Throwable passed to the failure callback is an IOException, this means that it was a network problem (socket timeout, unknown host, etc.). Any other exception means something broke either in serializing/deserializing the data or it's a configuration problem.
         *
         * @param throwable  the throwable
         */
        void onFailure(Throwable throwable);
        //DA-START:com.vd.gu.client.impl.GUClientI.LoadGeneratorCallback.additional.elements.in.type:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClientI.LoadGeneratorCallback.additional.elements.in.type:DA-ELSE
        // add any additional element in here that you need in your Java type but is not generated 
        //DA-END:com.vd.gu.client.impl.GUClientI.LoadGeneratorCallback.additional.elements.in.type:DA-END
    } // end of java type
    
    
    /**
     * transform a file
     * <table>
     * <tr><td>Generator</td><td>com.gs.vd.gen.java.retrofit.openapi:1.0</td></tr>
     * <tr><td>Element</td><td>transform</td></tr>
     * <tr><td>Type</td><td>Operation</td></tr>
     * <tr><td>Module</td><td>none</td></tr>
     * </table>
     */
    public static interface TransformCallback { // start of class
    
        
        /**
         * gets called when the http request has successfully completed and returned and deserialized the data (if it is supposed to return some)
         *
         * @param result  the result
         */
        void onSuccess(TargetContentBean result);
        
        /**
         * gets called when a technical exception has occurred, e.g. network problems or http status 3xx or 5xx
         * <br>
         * error
         * exception type to be used as default, for cases where there is not an explicit exception type available
         *
         * @param exc  the exc
         */
        void onTechnicalExceptionTransformDefault(TechnicalExceptionTransformDefaultException exc);
        
        /**
         * When the Throwable passed to the failure callback is an IOException, this means that it was a network problem (socket timeout, unknown host, etc.). Any other exception means something broke either in serializing/deserializing the data or it's a configuration problem.
         *
         * @param throwable  the throwable
         */
        void onFailure(Throwable throwable);
        //DA-START:com.vd.gu.client.impl.GUClientI.TransformCallback.additional.elements.in.type:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClientI.TransformCallback.additional.elements.in.type:DA-ELSE
        // add any additional element in here that you need in your Java type but is not generated 
        //DA-END:com.vd.gu.client.impl.GUClientI.TransformCallback.additional.elements.in.type:DA-END
    } // end of java type
    
    
    /**
     * <table>
     * <tr><td>Generator</td><td>com.gs.vd.gen.java.retrofit.openapi:1.0</td></tr>
     * <tr><td>Element</td><td>transform</td></tr>
     * <tr><td>Type</td><td>Operation</td></tr>
     * <tr><td>Module</td><td>none</td></tr>
     * </table>
     */
    public static interface TransformHeadCallback { // start of class
    
        
        /**
         * gets called when the http request has successfully completed and returned and deserialized the data (if it is supposed to return some)
         *
         * @param result  the result
         */
        void onSuccess(Map<String, List<String>> result);
        
        /**
         * When the Throwable passed to the failure callback is an IOException, this means that it was a network problem (socket timeout, unknown host, etc.). Any other exception means something broke either in serializing/deserializing the data or it's a configuration problem.
         *
         * @param throwable  the throwable
         */
        void onFailure(Throwable throwable);
        //DA-START:com.vd.gu.client.impl.GUClientI.TransformHeadCallback.additional.elements.in.type:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClientI.TransformHeadCallback.additional.elements.in.type:DA-ELSE
        // add any additional element in here that you need in your Java type but is not generated 
        //DA-END:com.vd.gu.client.impl.GUClientI.TransformHeadCallback.additional.elements.in.type:DA-END
    } // end of java type
    
    
    /**
     * transform a file with old content
     * <table>
     * <tr><td>Generator</td><td>com.gs.vd.gen.java.retrofit.openapi:1.0</td></tr>
     * <tr><td>Element</td><td>transformWithOldContent</td></tr>
     * <tr><td>Type</td><td>Operation</td></tr>
     * <tr><td>Module</td><td>none</td></tr>
     * </table>
     */
    public static interface TransformWithOldContentCallback { // start of class
    
        
        /**
         * gets called when the http request has successfully completed and returned and deserialized the data (if it is supposed to return some)
         *
         * @param result  the result
         */
        void onSuccess(TargetContentBean result);
        
        /**
         * gets called when a technical exception has occurred, e.g. network problems or http status 3xx or 5xx
         * <br>
         * error
         * exception type to be used as default, for cases where there is not an explicit exception type available
         *
         * @param exc  the exc
         */
        void onTechnicalExceptionTransformWithOldContentDefault(TechnicalExceptionTransformWithOldContentDefaultException exc);
        
        /**
         * When the Throwable passed to the failure callback is an IOException, this means that it was a network problem (socket timeout, unknown host, etc.). Any other exception means something broke either in serializing/deserializing the data or it's a configuration problem.
         *
         * @param throwable  the throwable
         */
        void onFailure(Throwable throwable);
        //DA-START:com.vd.gu.client.impl.GUClientI.TransformWithOldContentCallback.additional.elements.in.type:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClientI.TransformWithOldContentCallback.additional.elements.in.type:DA-ELSE
        // add any additional element in here that you need in your Java type but is not generated 
        //DA-END:com.vd.gu.client.impl.GUClientI.TransformWithOldContentCallback.additional.elements.in.type:DA-END
    } // end of java type
    
    
    /**
     * transform a list of files
     * <table>
     * <tr><td>Generator</td><td>com.gs.vd.gen.java.retrofit.openapi:1.0</td></tr>
     * <tr><td>Element</td><td>transformList</td></tr>
     * <tr><td>Type</td><td>Operation</td></tr>
     * <tr><td>Module</td><td>none</td></tr>
     * </table>
     */
    public static interface TransformListCallback { // start of class
    
        
        /**
         * gets called when the http request has successfully completed and returned and deserialized the data (if it is supposed to return some)
         *
         * @param result  the result
         */
        void onSuccess(Map<String, TargetContentBean> result);
        
        /**
         * gets called when a technical exception has occurred, e.g. network problems or http status 3xx or 5xx
         * <br>
         * error
         * exception type to be used as default, for cases where there is not an explicit exception type available
         *
         * @param exc  the exc
         */
        void onTechnicalExceptionTransformListDefault(TechnicalExceptionTransformListDefaultException exc);
        
        /**
         * When the Throwable passed to the failure callback is an IOException, this means that it was a network problem (socket timeout, unknown host, etc.). Any other exception means something broke either in serializing/deserializing the data or it's a configuration problem.
         *
         * @param throwable  the throwable
         */
        void onFailure(Throwable throwable);
        //DA-START:com.vd.gu.client.impl.GUClientI.TransformListCallback.additional.elements.in.type:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClientI.TransformListCallback.additional.elements.in.type:DA-ELSE
        // add any additional element in here that you need in your Java type but is not generated 
        //DA-END:com.vd.gu.client.impl.GUClientI.TransformListCallback.additional.elements.in.type:DA-END
    } // end of java type
    
    
    /**
     * transform a list of files with old content
     * <table>
     * <tr><td>Generator</td><td>com.gs.vd.gen.java.retrofit.openapi:1.0</td></tr>
     * <tr><td>Element</td><td>transformListWithOldContent</td></tr>
     * <tr><td>Type</td><td>Operation</td></tr>
     * <tr><td>Module</td><td>none</td></tr>
     * </table>
     */
    public static interface TransformListWithOldContentCallback { // start of class
    
        
        /**
         * gets called when the http request has successfully completed and returned and deserialized the data (if it is supposed to return some)
         *
         * @param result  the result
         */
        void onSuccess(Map<String, TargetContentBean> result);
        
        /**
         * gets called when a technical exception has occurred, e.g. network problems or http status 3xx or 5xx
         * <br>
         * error
         * exception type to be used as default, for cases where there is not an explicit exception type available
         *
         * @param exc  the exc
         */
        void onTechnicalExceptionTransformListWithOldContentDefault(TechnicalExceptionTransformListWithOldContentDefaultException exc);
        
        /**
         * When the Throwable passed to the failure callback is an IOException, this means that it was a network problem (socket timeout, unknown host, etc.). Any other exception means something broke either in serializing/deserializing the data or it's a configuration problem.
         *
         * @param throwable  the throwable
         */
        void onFailure(Throwable throwable);
        //DA-START:com.vd.gu.client.impl.GUClientI.TransformListWithOldContentCallback.additional.elements.in.type:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClientI.TransformListWithOldContentCallback.additional.elements.in.type:DA-ELSE
        // add any additional element in here that you need in your Java type but is not generated 
        //DA-END:com.vd.gu.client.impl.GUClientI.TransformListWithOldContentCallback.additional.elements.in.type:DA-END
    } // end of java type
    
    //DA-START:com.vd.gu.client.GUClientI.additional.elements.in.type:DA-START
    //DA-ELSE:com.vd.gu.client.GUClientI.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.vd.gu.client.GUClientI.additional.elements.in.type:DA-END
} // end of java type
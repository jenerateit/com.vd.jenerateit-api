package com.vd.gu.client.impl;

import okhttp3.Request;
import java.util.List;


/**
 * <table>
 * <tr><td>Generator</td><td>com.gs.vd.gen.java.retrofit.openapi:1.0</td></tr>
 * <tr><td>Element</td><td>GU</td></tr>
 * <tr><td>Type</td><td>Collection of OpenAPI3 models</td></tr>
 * <tr><td>Module</td><td>none</td></tr>
 * </table>
 */
public abstract class GUClientInterceptor { // start of class

    
    /**
     * checks, whether a request includes HTTP 'Accept' headers for 'application/json' and 'application/xml'
     *
     * @param request  the request
     * @return
     */
    protected boolean isRequestIncludingJsonAndXmlAcceptHeader(Request request) {
        //DA-START:com.vd.gu.client.impl.GUClientInterceptor.isRequestIncludingJsonAndXmlAcceptHeader.Request.boolean:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClientInterceptor.isRequestIncludingJsonAndXmlAcceptHeader.Request.boolean:DA-ELSE
        boolean jsonFound = false;
        boolean xmlFound = false;
        List<String> acceptHeaders = request.headers("Accept");
        if (acceptHeaders != null) {
        	for (String acceptHeader : acceptHeaders) {
        		if (acceptHeader.toLowerCase().contains("application/json")) {
        			jsonFound = true;
        		} else if (acceptHeader.toLowerCase().contains("application/xml")) {
        			xmlFound = true;
        		}
        	}
        }
        
        return jsonFound && xmlFound;
        //DA-END:com.vd.gu.client.impl.GUClientInterceptor.isRequestIncludingJsonAndXmlAcceptHeader.Request.boolean:DA-END
    }
    
    //DA-START:com.vd.gu.client.impl.GUClientInterceptor.additional.elements.in.type:DA-START
    //DA-ELSE:com.vd.gu.client.impl.GUClientInterceptor.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.vd.gu.client.impl.GUClientInterceptor.additional.elements.in.type:DA-END
} // end of java type
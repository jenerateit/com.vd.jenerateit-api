package com.vd.gu.client.impl;



/**
 * <table>
 * <tr><td>Generator</td><td>com.gs.vd.gen.java.retrofit.openapi:1.0</td></tr>
 * <tr><td>Element</td><td>GU</td></tr>
 * <tr><td>Type</td><td>Collection of OpenAPI3 models</td></tr>
 * <tr><td>Module</td><td>none</td></tr>
 * </table>
 */
class GUClientApplicationInterceptorSettings { // start of class

    /**
     * the rest client object that holds the main logic and makes use of retrofit interfaces
     */
    private final GUClient restClient;
    
    /**
     * creates an instance of GUClientApplicationInterceptorSettings
     *
     * @param restClient  the restClient
     */
    public GUClientApplicationInterceptorSettings(GUClient restClient) {
        //DA-START:com.vd.gu.client.impl.GUClientApplicationInterceptorSettings.GUClient:DA-START
        //DA-ELSE:com.vd.gu.client.impl.GUClientApplicationInterceptorSettings.GUClient:DA-ELSE
        this.restClient = restClient;
        //DA-END:com.vd.gu.client.impl.GUClientApplicationInterceptorSettings.GUClient:DA-END
    }
    
    
    //DA-START:com.vd.gu.client.impl.GUClientApplicationInterceptorSettings.additional.elements.in.type:DA-START
    //DA-ELSE:com.vd.gu.client.impl.GUClientApplicationInterceptorSettings.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.vd.gu.client.impl.GUClientApplicationInterceptorSettings.additional.elements.in.type:DA-END
} // end of java type
version 4.1.0
==============
- BUGFIX JIT-141: Add support for the same generation groups with different model converters
- BUGFIX JIT-143: All empty result Exceptions are removed

version 4.0.0
==============
- First open source version of JenerateIT lightweight generation

/*
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit.generationgroup;

import java.io.Serializable;
import java.lang.ref.SoftReference;
import java.util.Queue;
import java.util.Set;
import java.util.SortedSet;
import java.util.concurrent.ConcurrentMap;

import org.jenerateit.GenerationProtocolI;
import org.jenerateit.project.TargetProjectI;
import org.jenerateit.target.TargetDocumentI;
import org.jenerateit.target.TargetI;
import org.jenerateit.util.DetailsI;
import org.jenerateit.writer.WriterI;



/**
 * Interface for all generation groups.
 * 
 * @author hrr
 */
public interface GenerationGroupI extends Comparable<GenerationGroupI>, GenerationProtocolI, DetailsI, WriterLocatorI {

    /**
     * Returns the current implementation Version of this generation group.
     * 
     * @return the version as a String object
     */
    String getVersion();
    
    /**
     * Returns the target project this generation group belongs to.
     * 
     * @return the target project
     */
    TargetProjectI getTargetProject();
    
//	/**
//	 * Returns a list of {@link TargetI} for the given model elements.
//	 * 
//	 * @param metaModels a set of model elements to create the targets for
//	 * @return a list of target objects
//	 */
//	public Set<TargetI<?>> getTargetList(Set<Serializable> metaModels);

	/**
	 * Finds a list of {@link TargetI} instances from this generation group for the given model elements.
	 * 
	 * @param metaModels a set of model elements to look for
	 * @param targetFilter a class file to filter the target instances (null = do not filter)
	 * @param writerFilter a class file to filter the base writer of the target instances (null = do not filter)
	 * @return a list of target objects
	 */
	SortedSet<TargetI<? extends TargetDocumentI>> findTargets(
			Set<? extends Object> metaModels, 
			Class<? extends TargetI<? extends TargetDocumentI>> targetFilter, 
			Class<? extends WriterI> writerFilter);
	
	/**
	 * Getter to request an option value. This options may be used during transformation.
	 * 
	 * @see GenerationGroupOptions
	 * @param key the key to access the {@link GenerationGroupOptions option}
	 * @return a value if the key is found or null
	 */
	Serializable getOption(String key);
	
	/**
	 * Getter to request all options of this generation generation group
	 * 
	 * @return all options or null if not set
	 */
	GenerationGroupOptions getOptions();
	
	/**
	 * Helper class to control the number of writer instances. Clients should not use this method.
	 * 
	 * @return the Writer cache instance
	 */
	ConcurrentMap<Class<? extends WriterI>, Queue<SoftReference<WriterI>>> getWriterCache();
	
	/**
	 * Access to the user implemented {@link GenerationGroupConfigI} object.
	 * 
	 * @return the configuration object
	 */
	GenerationGroupConfigI getConfig();
}


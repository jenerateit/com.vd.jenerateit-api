/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package org.jenerateit.generationgroup;

import org.jenerateit.target.TargetI;
import org.jenerateit.writer.WriterI;

/**
 * @author hrr
 *
 */
public interface WriterLocatorI {

    /**
     * Getter for the base writer class. 
     * This method is quite important, since JenerateIT will check this method to proof, 
     * if the target class is valid for the model element. 
     * 
     * @param element the element to get the 
     * @param targetClass the target class to search a base writer for
     * @return a {@link WriterI} class
     */
    Class<? extends WriterI> getWriterClass(Object element, Class<? extends TargetI<?>> targetClass);
    
    /**
     * Getter for the writer class. 
     * 
     * @param element the element to get the 
     * @param target the target to search a writer for
     * @return a {@link WriterI} class
     */
    Class<? extends WriterI> getWriterClass(Object element, TargetI<?> target);
}

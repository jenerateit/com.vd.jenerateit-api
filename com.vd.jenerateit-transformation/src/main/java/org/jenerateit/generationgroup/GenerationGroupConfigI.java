/*
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit.generationgroup;

import java.util.Set;

import org.jenerateit.target.TargetI;
import org.jenerateit.util.DetailsI;



/**
 * Interface for all generation groups.
 * 
 * @author hrr
 */
public interface GenerationGroupConfigI extends DetailsI, WriterLocatorI {

    /**
     * Getter for all {@link TargetI} classes belonging to this generation group.
     * 
     * @return the complete target list
     */
    public Set<Class<? extends TargetI<?>>> getAllTargets();
    
}


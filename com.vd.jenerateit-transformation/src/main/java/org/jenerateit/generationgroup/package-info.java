/**
 * Collection of generation group related classes. 
 *
 * @since 1.0
 */
package org.jenerateit.generationgroup;
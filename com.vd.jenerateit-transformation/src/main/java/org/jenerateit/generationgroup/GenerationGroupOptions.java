/*
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit.generationgroup;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Configuration options for a JenerateIT {@link GenerationGroupI}.
 * The options may be accessed by {@link GenerationGroupI#getOption(String)} while transforming.
 * 
 * @author hrr
 *
 */
public class GenerationGroupOptions extends HashMap<String, Serializable> {

	
	/**
	 * the serial version UID
	 */
	private static final long serialVersionUID = 5151004639367510642L;

	/**
	 * Constructor.
	 */
	public GenerationGroupOptions() {
		super();
	}

	/**
	 * Constructor.
	 * 
	 * @param initialCapacity the initial capacity
	 */
	public GenerationGroupOptions(int initialCapacity) {
		super(initialCapacity);
	}

	/**
	 * Constructor.
	 * 
	 * @param m the map whose mappings are to be placed in this map 
	 */
	public GenerationGroupOptions(Map<? extends String, ? extends Serializable> m) {
		super(m);
	}

	/**
	 * Constructor.
	 * 
	 * @param initialCapacity the initial capacity
	 * @param loadFactor the load factor 
	 */
	public GenerationGroupOptions(int initialCapacity, float loadFactor) {
		super(initialCapacity, loadFactor);
	}

	/**
	 * Check if the configuration option is present.
	 * 
	 * @param key the key of the configuration option to look for
	 * @return true if the configuration option is found otherwise false
	 * @see java.util.HashMap#containsKey(java.lang.Object)
	 */
	public boolean containsKey(String key) {
		return super.containsKey(key);
	}

	/**
	 * Returns the configuration value for the key.
	 * 
	 * @param key the key of the configuration option to look for
	 * @return the configuration value if found otherwise null
	 * @see java.util.HashMap#get(java.lang.Object)
	 */
	public Serializable get(String key) {
		return super.get(key);
	}

	
}

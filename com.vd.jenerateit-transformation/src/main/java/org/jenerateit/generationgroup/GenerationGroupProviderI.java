/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package org.jenerateit.generationgroup;

/**
 * @author hrr
 *
 */
public interface GenerationGroupProviderI {

	/**
	 * Provides an implementation clas of a {@link GenerationGroupConfigI} instance.
	 * 
	 * @return a new instance
	 */
	GenerationGroupConfigI getGenerationGroupConfig();
}

package org.jenerateit;

public enum ErrorMessage {
	
	UNSUPPORTED_BYTE_ORDER_MARK_ENCODING ("VD-0001",
			"The previous content for the target file contains a byte order mark (BOM) for the encoding %s. This encoding is not supported by Virtual Developer."),
	;
	
	private final String id;
	private final String message;
	private final String instruction;
	
	private ErrorMessage(String id, String message, String instruction) {
		this.id = id;
		this.message = message;
		this.instruction = instruction;
	}
	
	private ErrorMessage(String id, String message) {
		this.id = id;
		this.message = message;
		this.instruction = null;
	}
	
	public String getId() {
		return id;
	}

	public String getMessage() {
		return message;
	}
	
	public String getMessage(Object...parameters) {
		if (parameters != null) {
			String messageText = String.format(message, parameters);
			return messageText;
		}
		return message;
	}
	
	public String getInstruction() {
		return instruction;
	}
	
	public String getInstruction(Object...parameters) {
		if (parameters != null) {
			String messageText = String.format(instruction, parameters);
			return messageText;
		}
		return instruction;
	}
}
/*
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit.target;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.SortedSet;

import org.jenerateit.writer.WriterInfoProviderI;



/**
 * Interface to describe a binary target document (the content of a target).
 * Implementations should subclass {@link AbstractTargetDocument} rather implement this interface directly.
 * 
 * @author hrr
 */
public interface TargetDocumentI extends Comparable<TargetDocumentI>, WriterInfoProviderI, Serializable {

	/**
	 * Return the content of the document as byte array.
	 * 
	 * @return the document as byte array
	 */
	byte[] toByte();
	
	/**
	 * Return the length of this target document.
	 * 
	 * @return the length
	 */
	int length();
	
	/**
	 * Return all target sections this document type supports.
	 * 
	 * @return all target sections
	 */
	SortedSet<TargetSection> getTargetSections();
	
	/**
	 * Load the content out of the input stream into this target document and parse it.
	 * 
	 * @param input the input stream to load the target document from
	 * @throws IOException if the load fails
	 */
	void load(InputStream input) throws IOException;
	
	/**
	 * Write the content of this target document to the output stream.
	 * 
	 * @param output the output stream to write to
	 * @throws IOException if the write operation fails
	 */
	void save(OutputStream output) throws IOException;
	
	/**
	 * Append data (bytes) to this document at the end of the given target section.
	 * 
	 * @param data the bytes to append
	 * @param targetSection the target section to add the data to
	 * @throws IllegalArgumentException it the data or the target section is not valid
	 */
	void addData(TargetSection targetSection, byte[] data) throws IllegalArgumentException;

	/**
	 * Method to append an error and its stack trace. The stack trace will be used from the {@link Throwable} parameter.
	 * 
	 * @param targetSection the target section to append the data to
	 * @param data the bytes to append
	 * @param t the throwable with it's stack trace to add
	 * @throws IllegalArgumentException if one parameter is not valid
	 */
	void addData(TargetSection targetSection, byte[] data, Throwable t) throws IllegalArgumentException;
	
	/**
	 * Getter for the number of addData calls this target document executes.
	 * 
	 * @return the amount of write calls
	 */
	int getWriteCalls();
}

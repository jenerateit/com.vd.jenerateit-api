/*
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit.target;

import java.io.InputStream;
import java.net.URI;
import java.util.SortedSet;

import org.jenerateit.GenerationProtocolI;
import org.jenerateit.generationgroup.GenerationGroupI;
import org.jenerateit.util.MessageProviderI;
import org.jenerateit.writer.WriterException;
import org.jenerateit.writer.WriterI;
import org.jenerateit.writer.WriterInfoProviderI;

import com.vd.transformation.data.TargetInfo;
import com.vd.transformation.data.TransformationRequest;
import com.vd.transformation.data.TransformationResponse;


/**
 * Interface for all classes representing a target file. 
 * Clients should subclass {@link AbstractTarget} for binary targets or
 * {@link AbstractTextTarget} for text based targets and not implement this interface directly.
 * 
 * @author hrr
 * @see AbstractTarget
 * @see AbstractTextTarget
 *
 */
public interface TargetI<V extends TargetDocumentI> 
	extends Comparable<TargetI<V>>, GenerationProtocolI, WriterInfoProviderI, MessageProviderI {

	
	
	
	
	/**
	 * Method to initialize this target (will be called right after target creation).
	 */
	void init();
	
    /**
     * Getter for a base writer instance.
     * 
     * @return a base writer instance of this target
     */
    WriterI getBaseWriter();
    
    /**
     * Getter for an writer based on the given element.
     * 
     * @param element the element to get the writer for
     * @return the writer instance
     */
    WriterI getWriterInstance(Object element);

    /**
     * Getter for a new {@link WriterI} instance based on the given class.
     * If the injection of the element fails, an {@link TargetException} will be thrown.
     * 
     * @param writerClass the writer class to create an instance from
     * @param element the element to get the writer for
     * @param <T> the writer class to create an instance for
     * @return a writer instance
     */
    <T extends WriterI> T getWriterInstance(Object element, Class<T> writerClass);

    /**
     * Returns this target path.
     * 
     * @return the path of this target
     */
    URI getTargetPath();
    
    /**
     * Getter for the base element this target is based on.
     * 
     * @return the base element
     */
    Object getBaseElement();
    
    /**
     * Getter for the generation group this target belongs to.
     * 
     * @return the generation group
     */
    GenerationGroupI getGenerationGroup();
    
    /**
     * Method to execute the transformation for this target.
     * 
     * @param request flags to inform the target about the client side status
     * @param input the input stream to read the original file from
     * @return the response flags (result flags)
     * @throws TargetException if something went wrong while prepare the target
     * @throws WriterException if something went wrong while transform the target
     */
    TransformationResponse transform(TransformationRequest request, InputStream input) throws TargetException, WriterException;

    /**
     * Getter for the target transformed currently.
     * <b>Note:</b> This might be a different target than this target 
     * in case the transformation logic use the findTarget methods within a writer.
     * 
     * @return the target transformed currently
     */
    TargetI<?> getTransformationTarget();

    /**
     * add a message to the list of messages
     *  
     * @param message the message to add
     */
    void addInfo(String message);
    
    /**
     * add a message to the list of messages
     *  
     * @param message the message to add
     */
    void addWarning(String message);
    
    /**
     * add a message to the list of messages
     *  
     * @param message the message to add
     * @param t the exception to add as part of the message
     */
    void addWarning(String message, Throwable t);
    
    /**
     * add a message to the list of messages
     *  
     * @param message the message to add
     */
    void addError(String message);
    
    /**
     * add a message to the list of messages
     *  
     * @param message the message to add
     * @param t the exception to add as part of the message
     */
    void addError(String message, Throwable t);
    
	/**
	 * Return all target sections this target has.
	 * 
	 * @see TargetDocumentI#getTargetSections()
	 * @return all target sections
	 */
	SortedSet<TargetSection> getTargetSections();

	/**
	 * Getter to access the current target section in transformation.
	 *  
	 * @return the current transformation section
	 */
	TargetSection getCurrentTransformationSection();
	
    /**
     * Getter for the previous target document (content of the last generation).
     * 
     * @return the previous target document of this target or null if not available
     */
    V getPreviousTargetDocument();
    
    /**
     * Getter for the new target document.
     * 
     * @return the new target document
     */
    V getNewTargetDocument();
    
    /**
     * Getter for the transformation information (statistics) of this target.
     * 
     * @return the transformation information
     */
    TargetInfo getTargetInfo();
    
    /**
     * Append a text to the end of the current transforming target section.
     * 
     * @param data the data to append
     */
    void write(byte[] data);
    
    /**
     * Append a text to the end of the given target section.
     * 
     * @param targetSection the section to append to
     * @param data the data to append
     */
    void write(TargetSection targetSection, byte[] data);
}


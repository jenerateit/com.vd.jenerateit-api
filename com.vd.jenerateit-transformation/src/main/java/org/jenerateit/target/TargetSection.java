/*
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit.target;

import java.io.Serializable;

import org.jenerateit.util.StringTools;


/**
 * Description of a section within a {@link TargetDocumentI TargetDocument}.
 * Some default settings for the TargetDocumentI object sections are used from this section.
 * <ul>
 * <li>code: The code of the section</li>
 * <li>order: The order number of the sections within the TargetDocumentI object</li>
 * <li>extensionDisabled: Switch for the generation process to ignore certain sections</li>
 * <li>startPrefix: The default prefix to start with for this section</li>
 * </ul>
 * @author hrr
 *
 */
public final class TargetSection implements Comparable<TargetSection>, Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 654194294671218789L;
	private String sectionCode = null;
    private int order = -1;
    private boolean extensionDisabled = false;
    private int startPrefix = 0;
    
    /**
     * Constructor
     * 
     * @param code the code of this target section
     * @param order the order number of this section
     */
    public TargetSection(String code, int order) {
    	this(code, order, false);
    }    
    
    /**
     * Constructor
     * 
     * @param code the code of this target section
     * @param order the order number of this section
     * @param extensionDisabled true if this section is disabled for transformation otherwise false
     */
    public TargetSection(String code, int order, boolean extensionDisabled) {
    	this(code, order, extensionDisabled, 0);
    }
    
    /**
     * Constructor
     * 
     * @param code the code of this target section
     * @param order the order number of this section
     * @param startPrefix the number of prefix characters to use by default
     */
    public TargetSection(String code, int order, int startPrefix) {
    	this(code, order, false, startPrefix);
    }
    
    /**
     * Constructor
     * 
     * @param code the code of this target section
     * @param order the order number of this section
     * @param extensionDisabled true if this section is disabled for transformation otherwise false
     * @param startPrefix the number of prefix characters to use by default
     */
    public TargetSection(String code, int order, boolean extensionDisabled, int startPrefix) {
    	if (!StringTools.isText(code)) {
    		throw new IllegalArgumentException("The code may not be null");
    		
    	}
    	
    	this.order = order;
        this.sectionCode = code;
        setExtensionDisabled(extensionDisabled);
        this.startPrefix = startPrefix;
    }    
    
    /**
	 * Getter for the section code.
	 * 
	 * @return the section code
	 */
	public String getCode()
	{
		return this.sectionCode;
	}

	/**
	 * Flag if the transformation of this section is disabled
	 * 
	 * @return true if the transformation of this section is disabled otherwise false
	 */
    public boolean isExtensionDisabled() {
        return extensionDisabled;
    }

    /**
     * Setter for the disabled flag
     * 
     * @param extensionDisabled true if the transformation of this section is disabled otherwise false
     */
    public void setExtensionDisabled(boolean extensionDisabled) {
        this.extensionDisabled = extensionDisabled;
    }

	/**
	 * Getter for startPrefix
	 *
	 * @return the startPrefix
	 */
	public int getStartPrefix() {
		return this.startPrefix;
	}
	
	
   /**
     * This compare methods compare the order number of this target section with
     * the order number of target section o
     * 
     * @see Comparable#compareTo(Object)
     * @param o the target section to compare to
     * @return a negative integer, zero, or a positive integer as this object is less than, equal to, or greater than the specified object.
     */
	public int compareTo(TargetSection o) {
		return new Integer(order).compareTo(new Integer(o.order));
	}

	/**
	 * The obj is equals if it is a TargetSection object and has the same code (case insensitive)
	 * 
	 * @param obj the object to compare
	 * @return true if the obj is equals
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		return ((obj instanceof TargetSection) 
				&& getCode().toLowerCase().equals(((TargetSection)obj).getCode().toLowerCase()));
	}

	
	/**
	 * Returns the hash code of the code information
	 * 
	 * @return the hash code of this target section (code)
	 * @see #getCode()
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return getCode().toLowerCase().hashCode();
	}

	/**
	 * A human readable target section
	 * 
	 * @return a String representation of this target section
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(this.sectionCode).append(": ");
		sb.append("order=").append(this.order).append("; ");
		sb.append("prefix=").append(this.startPrefix).append("; ");
		sb.append("extension disabled=").append(this.extensionDisabled);
		return sb.toString();
	}

	
}


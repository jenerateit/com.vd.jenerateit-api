/*
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit.target;



/**
 * Exception for unsupported elements
 * 
 * @author hrr
 *
 */
public class ElementNotSupportedException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1917560943765045533L;
	
	private Object element = null;
	
	/**
	 * Constructor
	 * 
	 * @param element the element that is not supported
	 */
	public ElementNotSupportedException(Object element) {
		super();
		this.element = element;
	}

	/**
	 * Constructor
	 * 
	 * @param message the error message
	 * @param element the element that is not supported 
	 */
	public ElementNotSupportedException(String message, Object element) {
		super(message);
		this.element = element;
	}

	/**
	 * Getter to access the not supported element
	 * 
	 * @return the element
	 */
	public Object getElement() {
		return element;
	}

	/* (non-Javadoc)
	 * @see java.lang.Throwable#getMessage()
	 */
	@Override
	public String getMessage() {
		StringBuffer sb = new StringBuffer();
		sb.append("The following element:").append("\n  ");
		sb.append(this.element != null ? this.element.toString() : "<not available>").append("\n");
		sb.append("is not supported").append("\n");
		sb.append(super.getMessage());
		return sb.toString();
	}

}

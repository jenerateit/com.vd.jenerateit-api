package org.jenerateit.target;

import java.util.Map;
import java.util.Optional;

public enum TextFormattingConfigItemDefinition implements TextFormattingConfigItemDefinitionI {
	WRITE_INDENTATION_ON_EMPTY_LINE (true, Boolean.class),
	;

	private boolean enabledByDefault;
	private Class<?> valueType;
	
	private TextFormattingConfigItemDefinition(boolean enabledByDefault, Class<?> valueType) {
		this.enabledByDefault = enabledByDefault;
		this.valueType = valueType;
	}
	
	@Override
	public String getKey() {
		return this.name();
	}
	
	@Override
	public Class<?> getValueType() {
		return valueType;
	}
	
	@Override
	public boolean isEnabledByDefault() {
		return enabledByDefault;
	}
	
	public Optional<Boolean> getBooleanValue(Map<TextFormattingConfigItemDefinitionI,TextFormattingConfigItem> configItems) {
		TextFormattingConfigItem configItem = configItems.get(this);
		if (configItem != null) {
			if (configItem.getConfigItemDefinition().getValueType() == Boolean.class) {
			    return Optional.of((Boolean)configItem.getValue());
			}
			
			throw new IllegalArgumentException("The value for the configuration item " + this.getKey() + " is not Boolean but has to be of type " + this.getValueType());
		}
		return Optional.empty();
	}
	
	public Optional<Integer> getIntegerValue(Map<TextFormattingConfigItemDefinitionI,TextFormattingConfigItem> configItems) {
		TextFormattingConfigItem configItem = configItems.get(this);
		if (configItem != null) {
			if (configItem.getConfigItemDefinition().getValueType() == Integer.class) {
			    return Optional.of((Integer)configItem.getValue());
			}
			
			throw new IllegalArgumentException("The value for the configuration item " + this.getKey() + " is not Integer but has to be of type " + this.getValueType());
		}
		return Optional.empty();
	}
}

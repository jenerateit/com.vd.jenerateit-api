package org.jenerateit.target;

public class TextFormattingConfigItem {
	
	private final TextFormattingConfigItemDefinitionI configItemDefinition;
	
	private final Object value;

	public TextFormattingConfigItem(TextFormattingConfigItemDefinitionI configItemDefinition, Object value) {
		super();
		this.configItemDefinition = configItemDefinition;
		this.value = value;
		
		if (!configItemDefinition.getValueType().isAssignableFrom(value.getClass())) {
			throw new IllegalArgumentException("The value for the configuration item " + configItemDefinition.getKey() + " has to be of type " + configItemDefinition.getValueType());
		}
	}

	public TextFormattingConfigItemDefinitionI getConfigItemDefinition() {
		return configItemDefinition;
	}

	public Object getValue() {
		return value;
	}
}

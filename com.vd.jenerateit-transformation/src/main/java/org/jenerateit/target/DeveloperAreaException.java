/*
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2016 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit.target;

import org.jenerateit.exception.JenerateITException;

/**
 * Exception to report developer area problems.
 * Within the transformation of a target, this Exception is caught and reported as 
 * {@link AbstractTarget#addError(String) error}.
 *  
 * @author hrr
 *
 */
public final class DeveloperAreaException extends JenerateITException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -746352538495905L;
	
	private final int lineNumber;
	private final int position;
	private final String id;
	
	/**
	 * Constructor
	 * 
	 * @param position the position within the file the problem occurs
	 * @param lineNumber the line number the problem occurs
	 * @param id the identifier of the developer area the problem occurs
	 * @param msg the detailed message of the problem
	 * @see #getMessage() for the detailed message
	 */
	public DeveloperAreaException(final int position, final int lineNumber, final String id, final String msg) {
		super(msg);
		this.lineNumber = lineNumber;
		this.position = position;
		this.id = id;
	}

	/**
	 * Get the line number of the developer area the problem occurred
	 * 
	 * @return the line number within the file
	 */
	public int getLineNumber() {
		return lineNumber;
	}

	/**
	 * Get the exact parsing position within the file the problem occurred
	 * 
	 * @return the exact parsing position  
	 */
	public int getPosition() {
		return position;
	}

	/**
	 * The identifier of the developer area where the problem occurred
	 * 
	 * @return the developer area identifier
	 */
	public String getId() {
		return id;
	}
	
}

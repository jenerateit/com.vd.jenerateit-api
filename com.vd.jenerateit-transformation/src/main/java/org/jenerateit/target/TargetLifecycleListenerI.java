/*
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit.target;

import javax.swing.text.AbstractWriter;

/**
 * Interface for a target lifecycle.
 * 
 * @author hrr
 *
 */
public interface TargetLifecycleListenerI {

	/**
	 * Right before a transformation starts, this event will be called.
	 * 
	 * @param target the target will be transformed right now.
	 */
	void preTransform(TargetI<?> target);
	
	/**
	 * Right after a target was transformed, this event will be called.
	 * 
	 * @param target the target was transformed right before.
	 */
	void postTransform(TargetI<?> target);

	/**
	 * Right after a target was loaded and the element and generation group was set.
	 * 
	 * @param target the target that was loaded
	 */
	void onLoaded(TargetI<?> target);
	
	/**
	 * Right after an {@link AbstractWriter}.find(...) method was called
	 * 
	 * @param target the target that was loaded
	 */
	void onFound(TargetI<?> target);
}

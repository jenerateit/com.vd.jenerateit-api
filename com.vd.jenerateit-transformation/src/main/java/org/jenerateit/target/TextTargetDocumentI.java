/*
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit.target;

import java.util.Collections;
import java.util.List;

import org.jenerateit.exception.JenerateITException;
import org.jenerateit.generationgroup.GenerationGroupConfigI;
import org.jenerateit.util.StringTools.Encoding;

/**
 * @author hrr
 *
 */
public interface TextTargetDocumentI extends TargetDocumentI, CharSequence {

	/**
	 * Getter for a developer area
	 * 
	 * @param code the code of the developer area to look for
	 * @return a developer area or null
	 */
	DeveloperAreaInfoI getDeveloperArea(String code);
	
	/**
	 * Getter for a list of developer area within the given area in the document content
	 * 
	 * @param start the start position within the document content, inclusive
	 * @param end the end position within the document content, exclusive
	 * @return a list of developer areas or an empty list
	 */
	List<DeveloperAreaInfoI> getDeveloperArea(int start, int end);
	
	/**
	 * Add some more text to this document
	 * 
	 * @param text the text to add
	 * @param targetSection the target section to add the text to
	 * @throws IllegalArgumentException it the text or the writer is not valid
	 */
	void addText(TargetSection targetSection, CharSequence... text) throws IllegalArgumentException;

	/**
	 * Method to add errors and their stack trace
	 * 
	 * @param text the text to add
	 * @param targetSection the target section to add the text to
	 * @param t the throwable with it's stack trace to add
	 * @throws IllegalArgumentException if one parameter is not valid
	 */
	void addText(TargetSection targetSection, Throwable t, CharSequence... text) throws IllegalArgumentException;

    /**
     * Returns the start of a comment for this type of target
     * 
     * @return the start sequence of a comment
     * @deprecated please use the {@link TextTargetI#getCommentStart()} method since this class has access to {@link GenerationGroupConfigI}
     */
	@Deprecated
    CharSequence getCommentStart();
    
    /**
     * Returns the end of a comment for this type of target
     * 
     * @return the end sequence of a comment
     * @deprecated please use the {@link TextTargetI#getCommentEnd()} method since this class has access to {@link GenerationGroupConfigI}
     */
	@Deprecated
    CharSequence getCommentEnd();
    
    /**
     * Returns the indent character
     * 
     * @return the indent character as byte
     */
    char getPrefixChar();

	/**
	 * Set a new prefix for the given target section
	 * 
	 * @param ts the target section to increase its prefix
	 * @param newPrefix the new prefix to add
	 * @return the new prefix size
	 */
	int incrementPrefixSize(TargetSection ts, int newPrefix);

	/**
	 * Set a new prefix for the given target section
	 * 
	 * @param ts the target section to decrease its prefix
	 * @param newPrefix the new prefix to decrement
	 * @return the new prefix size
	 */
	int decrementPrefixSize(TargetSection ts, int newPrefix);
	
	/**
	 * Get the current prefix size for the given target section
	 * 
	 * @param ts the target section to get the prefix size for
	 * @return the current prefix size
	 */
	int getPrefixSize(TargetSection ts);

	/**
	 * Activate/deactivate the prefix of the given target section
	 * 
	 * @param ts the target section to set its prefix
	 * @param prefix true if to add a prefix false otherwise
	 */
	void setPrefixEnabled(TargetSection ts, boolean prefix);
	
	/**
	 * Checks if the given developer area ID is valid
	 * 
	 * @param id the id to check
	 */
	void checkDeveloperAreaId(String id) throws JenerateITException;
	
	/**
	 * Override this when you want to change the default behavior of a text target document in regards to text formatting.
	 * 
	 * @return configuration items to control the text formatting
	 */
	default List<TextFormattingConfigItem> getTextFormattingConfigurationItems() {
		return Collections.emptyList();
	}
	
	/**
	 * This method returns true, when the content of the document included
	 * a byte order mark (BOM).
	 * 
	 * <p>Virtual Developer removes a BOM when the file content of an already
	 * existing file that was sent from a cloud connector includes one.
	 * There is no other case when a BOM is removed. So it would be perfectly
	 * possible to generate a file _with_ a BOM.
	 * 
	 * @return
	 */
	boolean isByteOrderMarkRemoved();
	
	/**
	 * This method returns the encoding that was specified by means of a byte order mark (BOM).
	 * 
	 * @return null, if the file content doesn't include a BOM
	 */
	Encoding getByteOrderMarkEncoding();
}

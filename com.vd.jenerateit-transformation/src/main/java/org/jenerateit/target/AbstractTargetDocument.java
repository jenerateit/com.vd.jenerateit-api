/*
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit.target;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicInteger;

import org.jenerateit.exception.JenerateITException;
import org.jenerateit.transformation.TransformationTarget;

import com.vd.transformation.data.WriterInfo;
import com.vd.transformation.data.WriterInfoList;



/**
 * Abstract class for all binary {@link TargetDocumentI} implementations.
 * 
 * @author hrr
 */
public abstract class AbstractTargetDocument implements TargetDocumentI {

	/**
	 * serial Version UID
	 */
	private static final long serialVersionUID = -5668888460289339117L;

	// internal target section buffer
	private final SortedMap<TargetSection, DocumentSection> sections = new TreeMap<TargetSection, DocumentSection>();
	
	private boolean documentDirty = false;
	private boolean documentAnalyzed = false;
	private byte[] document = null;
	private Set<WriterInfo> writers = null;
	private transient static final TransformationTarget currentTransformationTarget = new TransformationTarget();
	private final AtomicInteger writeCalls = new AtomicInteger(0);

	/**
	 * Default constructor, initializes all {@link TargetSection}.
	 *
	 * @see TargetDocumentI#getTargetSections()
	 * @throws JenerateITException if no {@link TargetSection TargetSection} is configured
	 */
	public AbstractTargetDocument() {
		Set<TargetSection> tss = getTargetSections();
		if (tss == null || tss.isEmpty()) {
			throw new JenerateITException("The target document of type '" + this.getClass().getName() + 
					"' must have at least one TargetSection");
			
		} else {
			for (TargetSection ts : tss) {
				sections.put(ts, new DocumentSection());
			}
		}
	}

	/**
	 * Method to append data to this target document.
	 * 
	 * @param data the bytes to append
	 * @param targetSection the section to append to
	 * @throws IllegalArgumentException if one parameter is not valid
	 * @see AbstractTargetDocument#addData(TargetSection, byte[], Throwable)
	 * @see TargetDocumentI#addData(TargetSection, byte[])
	 */
	public void addData(TargetSection targetSection, byte[] data) throws IllegalArgumentException
	{
		addData(targetSection, data, null);
	}

	/**
	 * Method to add errors and their stack trace
	 * 
	 * @param data the bytes to append
	 * @param targetSection the section to append to
	 * @param t the throwable with it's stack trace to append
	 * @throws IllegalArgumentException if one parameter is not valid
	 * @see TargetDocumentI#addData(TargetSection, byte[], Throwable)
	 */
	public void addData(TargetSection targetSection, byte[] data, Throwable t) throws IllegalArgumentException
	{
		addData(targetSection, data, 0, data != null ? data.length : 0, t);
	}

	/**
	 * Append data to the target section.
	 * 
	 * @param targetSection the target section to append to
	 * @param data the data to append
	 * @param start the start position in the byte buffer
	 * @param length the length of bytes to append
	 * @param t the throwable to use it's stack trace
	 * @throws IllegalArgumentException if on or more argumetns are invalid
	 */
	private void addData(TargetSection targetSection, byte[] data, int start, int length, Throwable t) throws IllegalArgumentException
	{
		DocumentSection ds = null;
		
		this.writeCalls.incrementAndGet();
		
		if (data == null) {
			throw new IllegalArgumentException("The line may not be null");
			
		} else if (targetSection == null) {
			throw new IllegalArgumentException("The target section may not be null");
			
		} else if ((ds = sections.get(targetSection)) == null) {
			throw new IllegalArgumentException("The target section '" + targetSection.getCode() + "' is unknown");
				
		} else if (data.length == 0) {
			
		} else if (t != null) {
			markDirty();
			ds.addData(data, start, length, t.getStackTrace());
			
		} else {
			markDirty();
			TargetI<?> target = currentTransformationTarget.getCurrentTarget();
			if (target != null && target.isGenerationProtocol()) {
				ds.addData(data, start, length, filterStackTrace(Thread.currentThread().getStackTrace()));
			} else {
				ds.addData(data, start, length, null);
			}
			
		}
	}
	
	/**
	 * Method to set the document state to dirty.
	 *
	 */
	protected void markDirty() {
		this.documentDirty = true;
		this.documentAnalyzed = false;
		this.writers = null;	
	}
	
	/**
	 * Getter for document dirty flag.
	 *
	 * @return the document dirty flag
	 */
	protected boolean isDocumentDirty() {
		return documentDirty;
	}

	
	/**
	 * Getter for document analyzed flag.
	 *
	 * @return the document analyzed flag
	 */
	protected boolean isDocumentAnalyzed() {
		return documentAnalyzed && !isDocumentDirty();
	}

	/**
	 * Getter to access all writer information this target document has.
	 * 
	 * @return a list of writer information 
	 * @see TargetDocumentI#getWriterInfo()
	 */
	public WriterInfoList getWriterInfo() {
		if (this.writers == null || !isDocumentAnalyzed()) {
			analyzeDocument();
		}
		final WriterInfoList result = new WriterInfoList();
		result.getElements().addAll(this.writers);
		return result;
	}

	/**
	 * Getter to access all writer information involved while write from start to end position.
	 * 
	 * @param start the start position to look the writer information for
	 * @param end the end position to look the writer information for
	 * @return a list of writer information
	 * @see TargetDocumentI#getWriterInfo(int, int)
	 */
	public WriterInfoList getWriterInfo(int start, int end) {
		if (this.writers == null || !isDocumentAnalyzed()) {
			analyzeDocument();
		}
		final WriterInfoList result = new WriterInfoList();
		for (final WriterInfo wi : this.writers) {
			if (wi.getStart() < end
					&& wi.getEnd() > start) {
				result.getElements().add(wi);
			}					
		}
		return result;
	}
	
	/**
	 * Method to update the cache of writers. 
	 * All DocumentSections will be checked for {@link WriterInfo} objects.
	 *
	 */
	protected void analyzeDocument() {
		this.writers = new LinkedHashSet<WriterInfo>();
		
		int offset = 0;
		for (final DocumentSection ds : sections.values()) {
			for (final WriterInfo wi : ds.writerList) {
				final WriterInfo newInfo = new WriterInfo();
				newInfo.setStart(wi.getStart() + offset);
				newInfo.setEnd(wi.getEnd() + offset);
				newInfo.getStackTraces().addAll(wi.getStackTraces());
				writers.add(newInfo);
			}
			offset += ds.lastWritePosition;
		}
		this.documentAnalyzed = true;
	}
	
	private static final String ABSTRACT_TARGET = AbstractTarget.class.getName();
	/**
	 * Filters the list of stack traces.
	 * 
	 * @param writerClasses the original list of StackTraceElements to filter
	 * @return the filtered list
	 */
	private StackTraceElement[] filterStackTrace(StackTraceElement[] writerClasses) {
		if (writerClasses == null || writerClasses.length <= 1) {
			return new StackTraceElement[0];
			
		} else {
			int end = 0;
			int start = 0;
			for (int i = writerClasses.length - 1; i >= 0; i--) {
				// find the end { end = AbstractTarget.transform(InputStream input, OutpuStream output) } 
				if (end == 0 &&
					ABSTRACT_TARGET.compareTo(writerClasses[i].getClassName()) == 0 &&
					writerClasses[i].getMethodName().startsWith("transform")) {
					
					end = i;
	
				// find the start { start = first call of AbstractTargetDocument }
				} else if (start == 0 &&
					checkForTargetDocumentClass(writerClasses[i].getClassName()) &&
					writerClasses[i].getMethodName().startsWith("load")) {
					
					start = i;
					break;

				// find the start { start = first call of AbstractTargetDocument }
				} else if (start == 0 &&
					checkForTargetDocumentClass(writerClasses[i].getClassName())) {
					
					start = i + 1;
					break;
				}
			}
			end = end == 0 ? writerClasses.length - 1 : end;
			StackTraceElement[] writers = new StackTraceElement[end - start];
			System.arraycopy(writerClasses, start, writers, 0, end - start);
			return writers;
		}
	}
	
	private static final String ABSTRACT_TARGET_DOCUMENT = AbstractTargetDocument.class.getName();
	private static final String ABSTRACT_TEXT_TARGET_DOCUMENT = AbstractTextTargetDocument.class.getName();
	/**
	 * Check if the given class is an abstract target document implementation of JenerateIT.
	 * @param className the class name to check
	 * @return true if the given class is an abstract {@link TargetDocumentI} implementation of JenerateIT
	 */
	private boolean checkForTargetDocumentClass(String className) {
		if (ABSTRACT_TARGET_DOCUMENT.compareTo(className) == 0 ||
				ABSTRACT_TEXT_TARGET_DOCUMENT.compareTo(className) == 0) {
			return true;
		}
		return false;
	}
	
	/**
	 * Read all bytes from the input stream and writes into the first target section,
	 * since there is no chance so far to find the section borders.
	 * 
	 * @param input the input stream to read from
	 * @throws IOException if the read fails
	 * @see TargetDocumentI#load(InputStream)
	 */
	public void load(InputStream input) throws IOException {
		
		if (input != null) {
			for (DocumentSection ds : sections.values()) {
				ds.clear();
			}

			TargetSection ts = sections.firstKey();
			byte[] buffer = new byte[2048];
			int length = -1;
			int position = 0;
			boolean firstAddition = true;
			while ((length = input.read(buffer)) != -1) {
				// JIT-103 (BOM removal)
				if (firstAddition) {
					position = getPositionForFirstBuffer(buffer);
				} else {
					position = 0;
				}
				addData(ts, buffer, position, length-position, null);
				firstAddition = false;
			}
		} else {
			throw new IOException("The input stream may not be null");
		}
	}
	
	protected int getPositionForFirstBuffer(byte[] buffer) {
		return 0;
	}

	/**
	 * Saves this target document to the given output stream.
	 * The stream will be flushed and closed after the save operation.
	 * 
	 * @param output the output stream to write to
	 * @throws IOException if the write fails
	 * @see TargetDocumentI#save(OutputStream)
	 */
	public void save(OutputStream output) throws IOException {
		if (output != null) {
			output.write(toByte());
			output.flush();
			output.close();
		} else {
			throw new IOException("The output stream may not be null");
		}
	}

	/**
	 * Returns the content of the document as byte array.
	 * 
	 * @return the document as byte array
	 */
	public byte[] toByte() {
		if (this.document == null || isDocumentDirty()) {
			this.document = new byte[length()];
			int pos = 0;
			for (DocumentSection ds : sections.values()) {
				System.arraycopy(ds.content, 0, this.document, pos, ds.lastWritePosition);
				pos += ds.lastWritePosition;
			}
			this.documentDirty = false;
		}
		return this.document;
	}
	
	/**
	 * Returns the content of the given target section as byte array.
	 * 
	 * @param ts the target section to look for
	 * @return the content of the given target section
	 */
	protected byte[] toByte(TargetSection ts) {
		DocumentSection ds = this.sections.get(ts);
		if (ds == null) {
			throw new JenerateITException("The target section '" + ts + "' is unknown");
			
		} else {
			byte[] data = new byte[ds.lastWritePosition];
			System.arraycopy(ds.content, 0, data, 0, ds.lastWritePosition);
			return data;
		}
	}
	
	/**
	 * Returns the length of this target document.
	 * 
	 * @return the length of this target document
	 * @see TargetDocumentI#length()
	 */
	public int length() {
		int length = 0;
		for (DocumentSection ds : sections.values()) {
			length += ds.lastWritePosition;
		}
		return length;
	}

	/**
	 * Getter for the number of addData calls this target document has.
	 * 
	 * @return the amount of write calls
	 */
	public final int getWriteCalls() {
		return this.writeCalls.get();
	}
	
	/**
	 * Compares this {@link AbstractTargetDocument} with the {@link TargetDocumentI doc}
	 * 
	 * @param doc the target document to compare with
	 * @return A negative integer, zero, or a positive integer as this TargetDocumentI is less than, equal to, or greater than the given TargetDocumentI
	 * @see java.lang.Comparable#compareTo(Object)
	 * @see java.nio.ByteBuffer#compareTo(ByteBuffer)
	 */
	public int compareTo(TargetDocumentI doc) {
		if (doc != null) {
			ByteBuffer b1 = ByteBuffer.wrap(toByte());
			ByteBuffer b2 = ByteBuffer.wrap(doc.toByte());
			
			return b1.compareTo(b2);
		} else {
			return -1;
		}
	}

	/**
	 * Represents the content of a target section.
	 */
	private class DocumentSection implements Serializable {
		
		/**
		 * The serial version UID.
		 */
		private static final long serialVersionUID = -1901543409972691090L;

		private static final int BUFFER_SIZE = 8192;
		
		// writer cache
	    private final Set<WriterInfo> writerList = new LinkedHashSet<WriterInfo>();
	    
	    private int lastWritePosition = 0;
		private byte[] content = null;

		/**
		 * Constructor
		 *  
		 */
		public DocumentSection() {
			this.content = new byte[BUFFER_SIZE];
		}

		/**
		 * Append the buffer to this target section.
		 * 
		 * @param buffer the buffer contains
		 * @param off the offset to start reading out of the buffer
		 * @param len the length to read from the buffer
		 * @param writerClasses the stack trace elements of the writer classes
		 * @throws IllegalArgumentException
		 */
		protected void addData(byte[] buffer, int off, int len, StackTraceElement[] writerClasses) throws IllegalArgumentException {
			if (buffer == null) {
				throw new IllegalArgumentException("The buffer to add may not be null");
				
			} else if (len > buffer.length) {
				throw new IllegalArgumentException("The len '" + len + "'increases the buffer size '" + buffer.length + "'");
				
			} else if (off < 0 || off + len > buffer.length) {
				throw new IllegalArgumentException("The off '" + off + "' and the len '" + len + "'increases the buffer size '" + buffer.length + "'");
				
			} else {
				// calculate the missing bytes
				int missingBytes = this.lastWritePosition + len - this.content.length;
				if (missingBytes > 0) {
					byte[] tmp = this.content;
					
					int factor = missingBytes / BUFFER_SIZE + ((missingBytes % BUFFER_SIZE) > 0 ? 1 : 0);
					this.content = new byte[tmp.length + BUFFER_SIZE * factor];
					System.arraycopy(tmp, 0, this.content, 0, this.lastWritePosition);
					
				}
				int start = this.lastWritePosition;
				this.lastWritePosition += len;
				if (writerClasses != null) {
					WriterInfo wi = new WriterInfo();
					wi.setStart(start);
					wi.setEnd(this.lastWritePosition);
					if (writerClasses.length > 0) {
						for (final StackTraceElement ste : writerClasses) {
							final com.vd.transformation.data.StackTraceElement element = new com.vd.transformation.data.StackTraceElement();
							element.setClazz(ste.getClassName());
							element.setMethod(ste.getMethodName());
							element.setLine(ste.getLineNumber());
							element.setFile(ste.getFileName());
							wi.getStackTraces().add(element);
						}
					}
					if (!this.writerList.add(wi)) {
						throw new JenerateITException("Found a duplicated writer info: " + wi);
					}
				}
				System.arraycopy(buffer, off, this.content, start, len);
			}
		}


		/**
		 * Clears the entire document
		 *
		 */
		protected void clear() {
			// set the lastWritePosition to zero
			this.lastWritePosition = 0;
			this.writerList.clear();
		}
	}
}

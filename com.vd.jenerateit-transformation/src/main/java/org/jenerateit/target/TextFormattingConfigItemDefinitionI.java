package org.jenerateit.target;

public interface TextFormattingConfigItemDefinitionI {
	
	String getKey();
	
	/**
     * Accessor for checking whether this feature is enabled by default.
     * 
     * <p>Note that his information only makes sense for boolean typed configuration items.
     * 
     * @return true when this config item is enabled in case when no configuration has been provided
     */
    boolean isEnabledByDefault();
    
    /**
     * @return the type of the value
     */
    Class<?> getValueType();
}

/*
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */

package org.jenerateit.target;

import org.jenerateit.exception.JenerateITException;

/**
 * Use this Exception, if you decide not to write the content to the original file while transforming.
 * <p>
 * JenerateIT will not write the content, produced up the point the Exception was thrown, at all.
 * </p> 
 * 
 * @author hrr
 *
 */
public class DoNotWriteException extends JenerateITException {

	/**
	 * the serial version UID
	 */
	private static final long serialVersionUID = 6823980776337605309L;

	/**
	 * Constructor.
	 */
	public DoNotWriteException() {
		super();
	}

	/**
	 * Constructor.
	 * 
	 * @param message a short message why not to write the content (will be shown in log file and warning section)
	 */
	public DoNotWriteException(String message) {
		super(message);
	}

}

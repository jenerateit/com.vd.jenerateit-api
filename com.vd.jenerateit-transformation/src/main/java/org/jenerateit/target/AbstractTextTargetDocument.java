/*
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit.target;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jenerateit.exception.JenerateITException;
import org.jenerateit.util.StringTools;
import org.jenerateit.util.StringTools.Encoding;


/**
 * Abstract class for all ASCII {@link TextTargetDocumentI} implementations.
 * 
 * @author hrr
 *
 */
public abstract class AbstractTextTargetDocument extends AbstractTargetDocument implements TextTargetDocumentI {

	private static final int INVALID = Integer.MIN_VALUE;
	
	private List<DeveloperAreaInfoI> developerAreas = null;
	private final SortedMap<TargetSection, PrefixInfo> sections = new TreeMap<TargetSection, PrefixInfo>();

	/** regular expression pattern for developer area start tag */
	private final Pattern DA_START;
	/** regular expression pattern for developer area else tag */
	private final Pattern DA_ELSE;
	/** regular expression pattern for developer area end tag */
	private final Pattern DA_END;
	
	private final Map<TextFormattingConfigItemDefinitionI,TextFormattingConfigItem> textFormattingConfiguration = new HashMap<>();
	
	private boolean byteOrderMarkRemoved = false;
	
	private Encoding byteOrderMarkEncoding;
	
	/**
	 * The serial version UID.
	 */
	private static final long serialVersionUID = 7925830675236803697L;

	/**
	 * Container class for developer area information.
	 * 
	 * @author hrr
	 */
	private class DevInfo implements Serializable{
		/**
		 * The serial version UID.
		 */
		private static final long serialVersionUID = -7604834831380014609L;
		public String id = null;
		public int start = INVALID;
		public int end = INVALID;
		public int contentStart = INVALID;
		public int contentEnd = INVALID;
	}
	
	/**
	 * Container class for prefix information.
	 * 
	 * @author hrr
	 *
	 */
	private class PrefixInfo implements Serializable {
		/**
		 * The serial version UID.
		 */
		private static final long serialVersionUID = -2789734052861895054L;
		public int prefixSize = 0;
		public boolean enabled = true;
		
		/**
		 * Constructor, setup the attributes.
		 * 
		 * @param prefixSize
		 * @param enabled
		 */
		public PrefixInfo(int prefixSize, boolean enabled) {
			this.prefixSize = prefixSize;
			this.enabled = enabled;
		}
	}
	
	/**
	 * @author Heinz Rohmer
	 *
	 */
	protected class DeveloperAreaInfo implements DeveloperAreaInfoI, Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = -1204610771564865128L;

		protected int start = 0;
		protected int end = 0;
		protected int contentStart = 0;
		protected int contentEnd = 0;
		private String id = null;
		
		/**
		 * Constructor
		 * 
		 * @param id the developer area identifier
		 * @param start start position of the developer area
		 * @param end end position of the developer area
		 * @param contentStart start position of the content
		 * @param contentEnd end position of the content
		 * @throws NullPointerException if the document is null
		 * @throws IllegalArgumentException if the id is invalid of the different positions don't fit
		 */
		public DeveloperAreaInfo(String id, int start, int end, int contentStart, int contentEnd) {
			if (start < 0) {
				throw new IllegalArgumentException("The start position may not be less zero");

			} else if (end < (start + 54)) {
				throw new IllegalArgumentException("The minimum size of a developer are (54 characters) is not reached");
				
			} else if ((start + 20) > contentStart) {
				throw new IllegalArgumentException("The content start position is to close to the start position");

			} else if (contentStart > contentEnd) {
				throw new IllegalArgumentException("The content end is less than the content start position");
				
			} else if ((contentEnd + 34) > end) {
				throw new IllegalArgumentException("The content end is to close to the end position");
				
			} else if (end > length()) {
				throw new IllegalArgumentException("The end position is behind the document end");
				
			} else {
				checkDeveloperAreaId(id);
				this.id = id;
				this.start = start;
				this.end = end;
				this.contentStart = contentStart;
				this.contentEnd = contentEnd;
			}
		}

		/**
		 * Getter for the end positon within the document content
		 * 
		 * @return the end position within the document content
		 */
		@Override
		public int getEnd() {
			return this.end;
		}

		/**
		 * Getter for the start position within the document content
		 * 
		 * @return the start position within the document content
		 */
		@Override
		public int getStart() {
			return this.start;
		}

		/* (non-Javadoc)
		 * @see com.gs.jenerateit.transformation.target.DeveloperAreaInfoI#getCode()
		 */
		@Override
		public String getId() {
			return this.id;
		}

		/* (non-Javadoc)
		 * @see com.gs.jenerateit.transformation.target.DeveloperAreaInfoI#getUserContent()
		 */
		@Override
		public CharSequence getUserContent() {
			return subSequence(contentStart, contentEnd);
		}

		/* (non-Javadoc)
		 * @see com.gs.jenerateit.transformation.target.DeveloperAreaInfoI#getStatus()
		 */
		public DeveloperAreaStatus getStatus() {
			return StringTools.isText(getUserContent().toString()) ? DeveloperAreaStatus.LOCKED : DeveloperAreaStatus.UNLOCKED;
		}

		/* (non-Javadoc)
		 * @see com.gs.jenerateit.transformation.target.DeveloperAreaInfoI#getText()
		 */
		@Override
		public CharSequence getText() {
			return subSequence(start, end).toString();
		}

		
		/**
		 * Checks if the given object is equals. 
		 * The following condition needs to be true in order both objects are equals:
		 * <ul>
		 * <li>the object obj must not be null</li>
		 * <li>the object obj must be of type {@link DeveloperAreaInfoI DeveloperAreaInfoI}</li>
		 * <li>the IDs of both developer areas needs to be the same (case insensitive)</li>
		 * </ul> 
		 * @param obj the object to compare to
		 * @return true if the objects are equals otherwise false
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object obj) {
			return 
				obj != null && 
				DeveloperAreaInfoI.class.isAssignableFrom(obj.getClass()) &&
				getId().compareToIgnoreCase(((DeveloperAreaInfoI)obj).getId()) == 0;
		}

		/**
		 * Returns the hash code of this developer area.
		 * The id only is used to calculate the hash code.
		 * <code>
		 * getId().toLowerCase().hashCode()
		 * </code>
		 * @return the hash code of this developer area.
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			return getId().toLowerCase().hashCode();
		}

		/**
		 * Method to compare two DeveloperAreaInfo objects. Only the id will be compared.
		 * 
		 * @return 0 if the objcts are equals otherwise 1 or -1
		 * 
		 * @see java.lang.Comparable#compareTo(java.lang.Object)
		 * @see org.jenerateit.target.DeveloperAreaInfoI#getId()
		 */
		public int compareTo(DeveloperAreaInfoI lai) {
			if (lai != null) {
				return getId().compareToIgnoreCase(lai.getId());
			}
			return -1;
		}

//		/**
//		 * Checks if the given developer areas fit the conditions
//		 * 
//		 * @param id the id to check
//		 * @return true if the id is valid otherwise false
//		 */
//	    public static boolean checkDeveloperAreaId(String id) {
//			try {
//				if (StringTools.isText(id) && DEVELOPER_AREA_ID_PATTERN.matcher(id).matches()) {
//					return true;
//				}
//			} catch (Exception e) {
//			}    	
//			return false;
//	    }
	    
//		/**
//		 * Finds the developer area with the given id within the given list.
//		 * Note: The id is insensitive
//		 * 
//		 * @param areas a list of developer areas to search in
//		 * @param id the id to look for
//		 * @return a developer area or null if not found
//		 * @throws NullPointerException if the areas are null
//		 * @throws IllegalArgumentException if the id is invalid
//		 */
//		public static DeveloperAreaInfoI getDeveloperArea(List<DeveloperAreaInfoI> areas, String id) {
//			if (areas == null) {
//				throw new NullPointerException("The developer area list may not be null");
//				
//			} else if (!checkDeveloperAreaId(id)) {
//				throw new IllegalArgumentException("The developer id '" + id + "' is not valid");
//				
//			} else {
//				for (DeveloperAreaInfoI dai : areas) {
//					if (dai.getId().compareToIgnoreCase(id) == 0) {
//						return dai;
//					}
//				}
//				return null;
//			}
//		}
//
//		/**
//		 * Finds all develper areas in the given list between the start and end position.
//		 * 
//		 * @param areas a list of developer areas to search in
//		 * @param start the start position in the content to search for
//		 * @param end the end position in the content to search for
//		 * @return a list of developer areas
//		 * @throws NullPointerException if the areas are null
//		 * @throws IllegalArgumentException if the start and end position are invalid
//		 */
//		public static List<DeveloperAreaInfoI> getDeveloperArea(List<DeveloperAreaInfoI> areas, int start, int end) {
//			if (areas == null) {
//				throw new NullPointerException("The developer area list may not be null");
//				
//			} else if (end < 0) {
//				throw new IllegalArgumentException("The end '" + end + "' may not be less than zero");
//				
//			} else if (end < start) {
//				throw new IllegalArgumentException("The end '" + end + "' is less than the start '" + start + "'");
//				
//			} else {
//				List<DeveloperAreaInfoI> newAreas = new ArrayList<DeveloperAreaInfoI>();
//				for (DeveloperAreaInfoI dai : areas) {
//					if (dai.getStart() < end
//							&& dai.getEnd() > start) {
//						newAreas.add(dai);
//					}
//				}
//				return newAreas;
//			}
//		}
	}

	/**
	 * Default constructor, initializes all {@link TargetSection TargetSection}.
	 *
	 * @see TargetDocumentI#getTargetSections()
	 * @throws JenerateITException if no {@link TargetSection TargetSection} is configured
	 */
	public AbstractTextTargetDocument() {

		// --- initialize the configuration items for text formatting JIT-89
		List<TextFormattingConfigItem> textFormattingConfigurationItems = this.getTextFormattingConfigurationItems();
		if (textFormattingConfigurationItems != null) {
			textFormattingConfigurationItems.stream().forEach(configItem -> {
				this.textFormattingConfiguration.put(configItem.getConfigItemDefinition(), configItem);
			});
		}
		
		// <comment start> DA-START:abc.def:DA-START<comment end>
		DA_START = getPattern(DeveloperAreaInfoI.DA_START);
		// <comment start> DA-START:abc.def:DA-START<comment end>
		DA_ELSE = getPattern(DeveloperAreaInfoI.DA_ELSE);
		// <comment start> DA-START:abc.def:DA-START<comment end>
		DA_END = getPattern(DeveloperAreaInfoI.DA_END);
		
		for (TargetSection ts : getTargetSections()) {
			sections.put(ts, new PrefixInfo(ts.getStartPrefix(), true));
		}
	}
	
	/**
	 * Creates a regular expression pattern for the given developer area tag.
	 * 
	 * @param tag the tag to use for the pattern
	 * @return a regular expression pattern
	 */
	private Pattern getPattern(String tag) {
		if (getCommentStart() == null
				|| !StringTools.isText(getCommentStart().toString())) {
			throw new IllegalArgumentException("The comment start character(s) is/are not valid");
		}
		return Pattern.compile(new StringBuilder()
				.append("^") 
				.append("\\s*") 
				.append(convertToPatternText(getCommentStart())) 
				.append("\\s*")
				.append(tag)
				.append(DeveloperAreaInfoI.DA_SEPARATOR)
				.append("(")
				.append(DeveloperAreaInfoI.ID_PATTERN)
				.append(")")
				.append(DeveloperAreaInfoI.DA_SEPARATOR)
				.append(tag)
				.append("\\s*")
				.append(getCommentEnd() != null ?
						convertToPatternText(getCommentEnd())
						: StringTools.EMPTY_STRING)
				.append("\\s*")
				.append("$").toString());
	}

	/**
	 * Escape special character of the pattern.
	 * 
	 * @param text the pattern to escape
	 * @return the escaped pattern text
	 */
	private CharSequence convertToPatternText(CharSequence text) {
		if (text != null && StringTools.isText(text.toString())) {
			return text.toString().replace("\\", "\\\\") // the '\' must be the first one to escape
				.replace("*", "\\*")
				.replace("?", "\\?")
				.replace("+", "\\+")
				.replace("{", "\\{")
				.replace("}", "\\}")
				.replace("[", "\\[")
				.replace("]", "\\]")
				.replace("(", "\\(")
				.replace(")", "\\)")
				.replace(".", "\\.")
				.replace("^", "\\^")
				.replace("$", "\\$");
		} else {
			return text;
		}
	}
	/**
	 * Set the document state as dirty.
	 *
	 */
	@Override
	protected void markDirty() {
		this.developerAreas = null;
		super.markDirty();
	}
	/**
	 * Add some more text to this document.
	 * 
	 * @param text the text to add
	 * @param targetSection the target section to add the text to
	 * @throws IllegalArgumentException it the text or the writer is not valid
	 * @see TextTargetDocumentI#addText(TargetSection, CharSequence...)
	 */
	@Override
	public void addText(TargetSection targetSection, CharSequence... text) throws IllegalArgumentException {
		addText(targetSection, null, text);
	}
	
	/**
	 * Method to append errors and their stack trace.
	 * 
	 * @param targetSection the target section to append the text to
	 * @param text the text to append
	 * @param t the throwable with it's stack trace to append
	 * @throws IllegalArgumentException if one parameter is not valid
	 * @see TextTargetDocumentI#addText(TargetSection, Throwable, CharSequence...)
	 */
	@Override
	public void addText(TargetSection targetSection, Throwable t, CharSequence... text) throws IllegalArgumentException
	{
		if (text == null) {
			throw new IllegalArgumentException("The line may not be null");
			
		} else if (!this.sections.containsKey(targetSection)) {
			throw new IllegalArgumentException("The target section '" + targetSection.getCode() + "' is unknown");
		} else if (text.length == 0) {
			// nothing has to be done since we do not have any text to be written
		} else {
			StringBuffer sb = new StringBuffer();
			PrefixInfo pi = this.sections.get(targetSection);
			
			// --- handle the indentation first JIT-89
			Optional<Boolean> writeIndentationOnEmptyLine = TextFormattingConfigItemDefinition.WRITE_INDENTATION_ON_EMPTY_LINE.getBooleanValue(textFormattingConfiguration);
			if (writeIndentationOnEmptyLine.isPresent() && writeIndentationOnEmptyLine.get().equals(Boolean.FALSE)) {
				if (pi.enabled && pi.prefixSize > 0 && text[0].length() > 0 && !StringTools.NEWLINE.equals(text[0])) {
					sb.append(getPrefixChars(targetSection));
				}
			} else {
				if (pi.enabled && pi.prefixSize > 0) {
					sb.append(getPrefixChars(targetSection));
				}
			}
			
			// --- append the text and add it to the target section's data buffer
			for (CharSequence cs : text) {
				sb.append(cs);
			}
			super.addData(targetSection, sb.toString().getBytes(), t);
		}
	}
	
	
    /**
     * Append data to the given target section.
     * 
     * @param targetSection the target section to append data to.
     * @param data the data to append
     * @throws IllegalArgumentException if the parameters are not valid
	 * @see AbstractTargetDocument#addData(TargetSection, byte[])
	 * @see #addData(TargetSection, byte[], Throwable)
	 */
	@Override
	public void addData(TargetSection targetSection, byte[] data) throws IllegalArgumentException {
		addData(targetSection, data, null);
	}

	/**
	 * 
     * Append data and the error to the given target section.
     * 
     * @param targetSection the target section to append data to.
     * @param data the data to append
     * @param t the throwable to append
     * @throws IllegalArgumentException if the parameters are not valid
	 * @see AbstractTargetDocument#addData(TargetSection, byte[], Throwable)
	 */
	@Override
	public void addData(TargetSection targetSection, byte[] data, Throwable t) throws IllegalArgumentException {
		if (data == null) {
			throw new IllegalArgumentException("The data to add to the target document may not be null");

		} else if (!this.sections.containsKey(targetSection)) {
			throw new IllegalArgumentException("The target section '" + targetSection.getCode() + "' is unknown");
			
		} else {
			
			PrefixInfo pi = this.sections.get(targetSection);
			if (pi.enabled) {
				byte[] prefix = new String(getPrefixChars(targetSection)).getBytes();
				byte[] dataWithPrefix = new byte[data.length + prefix.length];
				System.arraycopy(prefix, 0, dataWithPrefix, 0, prefix.length);
				System.arraycopy(data, 0, dataWithPrefix, prefix.length, data.length);
				super.addData(targetSection, dataWithPrefix, t);
			} else {
				super.addData(targetSection, data, t);
			}
		}
	}

	/**
     * Prepare the prefix for write operations.
     * 
     * @param ts The target section to prepare the prefix for
     * @return characters with the length of the indent
     */
    private char[] getPrefixChars(TargetSection ts) {
    	PrefixInfo pi = this.sections.get(ts);
    	if (pi.prefixSize < 0) {
    		throw new JenerateITException("The indent may not be less than zero");
    		
    	} else if (pi.prefixSize == 0) {
    		return new char[0];
    		
    	} else {
	    	char[] pref = new char[pi.prefixSize];
	    	Arrays.fill(pref, getPrefixChar());
	    	return pref;
    	}
    }

    /**
     * Enable/Disable the prefix for the given target section.
     * 
     * @param ts the target section to modify
     * @param enabled true if the prefix is enabled otherwise false
     */
	@Override
	public final void setPrefixEnabled(TargetSection ts, boolean enabled) {
		PrefixInfo pi = null;

		if (ts == null) {
			throw new IllegalArgumentException("The target section may not be null");
			
		} else if ((pi = sections.get(ts)) == null) {
			throw new IllegalArgumentException("The target section '" + ts.getCode() + "' is unknown");
			
		} else {
			pi.enabled = enabled;
		}
	}
	
	/**
	 * Set the prefix within the given target section to the new value.
	 *  
	 * @param ts the target section to modify.
	 * @param newPrefix the new prefix size
	 * @return the new prefix size
	 */
	@Override
	public final int decrementPrefixSize(TargetSection ts, int newPrefix) {
		PrefixInfo pi = this.sections.get(ts);
		if (pi == null) {
			throw new JenerateITException("The target section '" + ts + "' is unknown");
			
		} else if (newPrefix <= 0) {
			throw new JenerateITException("The prefix to decrement (" + newPrefix +
					") must be a positive integer value");
			
		} else if ((pi.prefixSize - Math.abs(newPrefix)) < 0) {
			throw new JenerateITException("The prefix to decrement (" + newPrefix +
					") would result in a negative prefix value");
			
		} else {
			pi.prefixSize -= newPrefix;
			return pi.prefixSize;
		}
	}
	
	/**
	 * Set a new prefix within the given target section to the new value.
	 * 
	 * @param ts the target section to modify its prefix
	 * @param newPrefix the new prefix to set
	 * @return the new prefix size
	 */
	@Override
	public final int incrementPrefixSize(TargetSection ts, int newPrefix) {
		PrefixInfo pi = this.sections.get(ts);
		if (pi == null) {
			throw new JenerateITException("The target section '" + ts + "' is unknown");
			
		} else if (newPrefix <= 0) {
			throw new JenerateITException("The prefix to increment (" + newPrefix +
					") must be a positive integer value");
			
		} else {
			pi.prefixSize += newPrefix;
			return pi.prefixSize;
		}
	}
	
	/**
	 * Get the current prefix size for the given target section
	 * 
	 * @param ts the target section to get the prefix size for
	 * @return the current prefix size
	 */
	@Override
	public int getPrefixSize(TargetSection ts) {
		PrefixInfo pi = this.sections.get(ts);
		return pi.prefixSize;
	}

	/**
	 * Find all developer areas in the content of this document between the start and end position.
	 * 
	 * @param start the start position in the content to search for
	 * @param end the end position in the content to search for
	 * @return a list of developer areas 
	 * @see TextTargetDocumentI#getDeveloperArea(int, int)
	 */
	@Override
	public final List<DeveloperAreaInfoI> getDeveloperArea(int start, int end) {
		if (this.developerAreas == null || !isDocumentAnalyzed()) {
			analyzeDocument();
		}
		if (this.developerAreas == null) {
			return new ArrayList<DeveloperAreaInfoI>();
		} else {
			List<DeveloperAreaInfoI> newAreas = new ArrayList<DeveloperAreaInfoI>();
			for (final DeveloperAreaInfoI dai : this.developerAreas) {
				if (dai.getStart() < end
						&& dai.getEnd() > start) {
					newAreas.add(dai);
				}
			}
			return newAreas;
//			return DeveloperAreaInfo.getDeveloperArea(this.developerAreas, start, end);
		}
	}

	/**
	 * Finds the developer area with the given code within this target document.
	 * Note: The code is case sensitive.
	 * 
	 * @param code the code to look for
	 * @return a developer area or null if not found
	 * @see TextTargetDocumentI#getDeveloperArea(java.lang.String)
	 */
	@Override
	public final DeveloperAreaInfoI getDeveloperArea(String code) {
		checkDeveloperAreaId(code);

		if (this.developerAreas == null || !isDocumentAnalyzed()) {
			analyzeDocument();
		}
		for (final DeveloperAreaInfoI dai : this.developerAreas) {
			if (dai.getId().compareToIgnoreCase(code) == 0) {
				return dai;
			}
		}
		return null;
//		return DeveloperAreaInfo.getDeveloperArea(this.developerAreas, code);
	}

	/**
	 * Method to analyze the content of a target document. This method will 
	 * read line by line and call the {@link #analyzeLine(int, String, int, int) method}
	 * 
	 * @see #analyzeLine(int, String, int, int)
	 */
	@Override
	protected void analyzeDocument() {
		try {
			this.developerAreas = new ArrayList<DeveloperAreaInfoI>();
			this.devInfoStack = new ArrayDeque<>();
			
			byte[] buffer = toByte();
			int lastLineEnd = 0;
			int lineNumber = 0;
			
			//'\r\n'	newline DOS
			//'\r'		MAC
			//'\n'		UNIX
			for (int i = 0; i < buffer.length; i++) {
				// 1. check if we are at the end of the buffer
				if ((i + 1) == buffer.length) {
					byte[] line = new byte[(i + 1) - lastLineEnd];
					System.arraycopy(buffer, lastLineEnd, line, 0, line.length);
					analyzeLine(++lineNumber, new String(line), lastLineEnd, i + 1);
					
				// 2. check if the current byte is a line break (see comment above for possible line break)	
				} else if ((buffer[i] == '\r' && buffer[i + 1] != '\n') ||
						(buffer[i] == '\n')) {
					byte[] line = new byte[(i + 1) - lastLineEnd];
					System.arraycopy(buffer, lastLineEnd, line, 0, line.length);
					analyzeLine(++lineNumber, new String(line), lastLineEnd, i + 1);
					lastLineEnd = i + 1;

				}	
			}
		} catch (JenerateITException e) {
			throw e;
		} catch (Exception e) {
			throw new JenerateITException("Error while read and analyze a text target document", e);
		}

		super.analyzeDocument();
	}

	/*
	 * temporary container for DA 
	 */
	private Deque<DevInfo> devInfoStack = null;
	
	/**
	 * Parse a line of the target document and try to find developer area tags.
	 * 
	 * Clients may overwrite this method to parse for other stuff.
	 * <pre>
	 * <code>
	 * protected void analyzeLine(String line, int start, int end) {
	 *     analyzeLine(line, start, end);
	 *     
	 *     if (line.startsWith("import")) {
	 *         ....
	 *     }
	 * }
	 * </code>
	 * </pre>
	 * 
	 * @param lineNumber the number of the line to parse
	 * @param line the line to parse
	 * @param start the start position of the line in the target document
	 * @param end the end position of the line in the target document
	 */
	protected void analyzeLine(int lineNumber, String line, int start, int end) {
		
		if (StringTools.isText(line)) {
			Matcher m = null;
			if ((m = DA_START.matcher(line)) != null
					&& m.matches()) {
				
//				if (this.devInfo != null) {
//					throw new DeveloperAreaException(start, lineNumber, m.group(1),
//							"Found a new developer area start with ID ‘"
//							+ m.group(1) + "' at position " + start + " in line " + lineNumber 
//							+ ", but there is already a developer area open with ID '"
//							+ this.devInfo.id + "' at position " + devInfo.start 
//							+ ". Recursive developer areas are not permitted.");
//					
//				} else {
//					this.devInfo = new DevInfo();
//					this.devInfo.start = start;
//					this.devInfo.contentStart = end;
//					this.devInfo.id = m.group(1);
//				}
				
				DevInfo devInfo = new DevInfo();
				devInfo.start = start;
				devInfo.contentStart = end;
				devInfo.id = m.group(1);
				this.devInfoStack.push(devInfo);
				
			} else if ((m = DA_ELSE.matcher(line)) != null
					&& m.matches()) {
				
				if (this.devInfoStack.isEmpty()) {
					throw new DeveloperAreaException(start, lineNumber, m.group(1),
							"Found a developer area else path with ID '"
							+ m.group(1) + "' at position " + start + " in line " + lineNumber 
							+ ", but there is no developer area with this ID started");
					
				} else if (this.devInfoStack.peek().contentEnd != INVALID) {
					throw new DeveloperAreaException(start, lineNumber, m.group(1),
							"Found a developer area else path with ID '"
							+ m.group(1) + "' at position " + start + " in line " + lineNumber 
							+ ", but there is already a developer area else path at position " + 
							this.devInfoStack.peek().contentStart);
					
				} else if (m.group(1).compareToIgnoreCase(this.devInfoStack.peek().id) != 0) {
					throw new DeveloperAreaException(start, lineNumber, m.group(1),
							"The developer area else path with ID '"
							+ m.group(1) + "' at postiont " + start + " in line " + lineNumber 
							+ " does not fit to developer area start with ID '" + this.devInfoStack.peek().id + 
							"' at position " + this.devInfoStack.peek().start);
					
				} else {
					this.devInfoStack.peek().contentEnd = start;
				}
				
			} else if ((m = DA_END.matcher(line)) != null
					&& m.matches()) {
				
				if (this.devInfoStack.isEmpty()) {
					throw new DeveloperAreaException(start, lineNumber, m.group(1),
							"Found a developer area end path with ID '"
							+ m.group(1) + "' at position " + start + " in line " + lineNumber 
							+ ", but there is no developer area started");
					
				} else if (this.devInfoStack.peek().contentEnd == INVALID) {
					throw new DeveloperAreaException(start, lineNumber, m.group(1),
							"Found a developer area end path with ID '"
							+ m.group(1) + "' at position " + start + " in line " + lineNumber 
							+ ", but there is no developer area else path");
					
				} else if (m.group(1).compareToIgnoreCase(this.devInfoStack.peek().id) != 0) {
					throw new DeveloperAreaException(start, lineNumber, m.group(1),
							"The developer area end path with ID '"
							+ m.group(1) + "' at postiont " + start + " in line " + lineNumber 
							+ " does not fit to developer area start with ID '" + this.devInfoStack.peek().id + 
							"' at position " + this.devInfoStack.peek().start);
					
				} else {
					DevInfo devInfo = this.devInfoStack.pop();
					devInfo.end = end;
					DeveloperAreaInfo dai = new DeveloperAreaInfo(
							devInfo.id,
							devInfo.start,
							devInfo.end,
							devInfo.contentStart,
							devInfo.contentEnd);
					devInfo = null;
					if (this.developerAreas.contains(dai)) {
						throw new DeveloperAreaException(start, lineNumber, m.group(1),
								"Found a new developer area with ID '"
								+ dai.getId() + "' at position " + dai.getStart() + " in line " 
								+ lineNumber + ", but there is already a developer area with the same ID at position " 
								+ this.developerAreas.get(this.developerAreas.indexOf(dai)).getStart());
					} else {					
						this.developerAreas.add(dai);
					}
				}				
			}
		}
	}
	
//	private static final Pattern WHITESPACES = Pattern.compile("[\\s]");
	private static final Pattern DA_ID = Pattern.compile(new StringBuilder("").append(DeveloperAreaInfoI.ID_PATTERN).append("$").toString());
	public final void checkDeveloperAreaId(String id) throws JenerateITException {
		if (!StringTools.isText(id)) {  
			throw new JenerateITException("An empty developer area id is not valid");
		
		} else if (getCommentStart() != null && StringTools.isText(getCommentStart().toString()) && id.contains(getCommentStart())) {
			throw new JenerateITException("A developer area id may not contain the comment start sequence '" + getCommentStart() + "'");

		} else if (getCommentEnd() != null && StringTools.isText(getCommentEnd().toString()) && id.contains(getCommentEnd())) {
			throw new JenerateITException("A developer area id may not contain the comment end sequence '" + getCommentEnd() + "'");
			
		} else if (!DA_ID.matcher(id).matches()) {
			throw new JenerateITException("The developer area id '" + id + "' does not match the rule");
//		} else if (WHITESPACES.matcher(id).find()) {
//			throw new JenerateITException("A developer area id may not contain all kind of whitespace characters");
			
		} else if (id.indexOf(':') >= 0) {
			throw new JenerateITException("A developer area id may not contain a ':' caracter at all");
		}
	}
	
	/**
	 * Helper method to make invisible characters like spaces, linefeeds ... visible.
	 * 
	 * @param s the string to convert
	 * @return a string with converted characters
	 */
	protected String getLoggingString(String s) {
		if (StringTools.isText(s)) {
			return s.replace("\n", "\\n").replace("\r", "\\r").replace("\t", "\\t");
		}
		return s;
	}

	/**
	 * Returns the char value at the specified index.
	 *  
	 * @param index the index of the char value to be returned
	 * @return the char value of the specified index
	 * @throws IndexOutOfBoundsException if the index argument is negative of not less the {@link #length() length()}
	 * @see java.lang.CharSequence#charAt(int)
	 */
	public char charAt(int index) {
		if (index < 0) {
			throw new IndexOutOfBoundsException("The index '" + index + "' is negative");
			
		} else if (index >= length()) {
			throw new IndexOutOfBoundsException("The index '" + index + 
					"' is not less than the length of this target document (" + length() + ")");
			
		} else {
			return new String(toByte()).charAt(index);
		}
	}

	/**
	 * Returns the text out of this target document content.
	 * 
	 * @param start the start position within this document content, inclusive
	 * @param end the end position within this document content, exclusive
	 * @return the text within start and end
	 * @throws IndexOutOfBoundsException one of the parameter is outside the size of the current document or the end is less than the start
	 * @see java.lang.CharSequence#subSequence(int, int)
	 */
	public CharSequence subSequence(int start, int end) {
		byte[] ba = toByte();
		if (ba == null) {
			return null;
			
		} else if (start < 0) {
			throw new IndexOutOfBoundsException("The start position may not be less than zeor");
			
		} else if (start > end) {
			throw new IllegalArgumentException("The end position may not be less then the start position");
			
		} else if (end > ba.length) {
			throw new IndexOutOfBoundsException("The end position reaches the end of content");
			
		} else {
			byte[] buffer = new byte[end  - start];
			System.arraycopy(ba, start, buffer, 0, buffer.length);
			return new String(buffer);
		}
	}

	/**
	 * Converts the content to a single String.
	 * 
	 * @return the content as String
	 * @see java.lang.CharSequence#toString()
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return new String(toByte());
	}

	/**
	 * Disable the prefix and load.
	 * 
	 * @param input the input to load from
	 * @throws IOException if the load operation fails
	 * @see org.jenerateit.target.AbstractTargetDocument#load(java.io.InputStream)
	 */
	@Override
	public void load(InputStream input) throws IOException {
		setPrefixEnabled(this.sections.firstKey(), false);
		super.load(input);
	}
	
	/**
	 * This got added for JIT-103 (BOM removal).
	 */
	public boolean isByteOrderMarkRemoved() {
		return byteOrderMarkRemoved;
	}
	
	/**
	 * This method returns the encoding that was specified by means of a byte order mark (BOM).
	 * 
	 * @return null, if the file content doesn't include a BOM
	 */
	public Encoding getByteOrderMarkEncoding() {
		return byteOrderMarkEncoding;
	}

	/**
	 * This got added for JIT-103 (BOM removal).
	 */
	protected int getPositionForFirstBuffer(byte[] buffer) {
		int result = StringTools.getNumberOfByteOrderMarkBytes(buffer);
		if (result > 0) {
			byteOrderMarkRemoved = true;
			byteOrderMarkEncoding = StringTools.getByteOrderMarkEncoding(buffer);
		}
		return result;
	}
}

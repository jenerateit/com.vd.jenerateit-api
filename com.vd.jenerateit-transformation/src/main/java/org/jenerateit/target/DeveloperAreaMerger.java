/*
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */

package org.jenerateit.target;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.jenerateit.exception.JenerateITException;
import org.jenerateit.target.DeveloperAreaInfoI.DeveloperAreaStatus;
import org.jenerateit.util.StringTools;
import org.jenerateit.writer.TextWriterI;

/**
 * @author hrr
 *
 */
public final class DeveloperAreaMerger implements TargetLifecycleListenerI {

	/* (non-Javadoc)
	 * @see org.jenerateit.target.TargetLifecycleListenerI#preTransform(org.jenerateit.target.TargetI)
	 */
	@Override
	public void preTransform(TargetI<?> target) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.jenerateit.target.TargetLifecycleListenerI#postTransform(org.jenerateit.target.TargetI)
	 */
	@Override
	public void postTransform(TargetI<?> target) {
		
		if (!TextTargetI.class.isInstance(target)) {
			throw new JenerateITException("Got a postTransform(TargetI) event with a target of type '" +
					target.getClass() + "' but I expect a target of type '" + TextTargetI.class + "'");
			
		} else {
//		// in case an error occured, clear the comment flag
//		this.commentWriteOperationSection = null;
			TextTargetI<?> textTarget = (TextTargetI<?>) target;
			
			/*
			 * write old (not used any more) LOCKED developer areas to the end
			 * of the file
			 */
			if (textTarget.getPreviousTargetDocument() != null
					&& textTarget.getPreviousTargetDocument().getDeveloperArea(Integer.MIN_VALUE, Integer.MAX_VALUE) != null) {
				// first prepare a list of LOCKED developer areas from the previous target document
				List<DeveloperAreaInfoI> previousDeveleoperAreas = new ArrayList<DeveloperAreaInfoI>();
				for (DeveloperAreaInfoI da : textTarget.getPreviousTargetDocument().getDeveloperArea(Integer.MIN_VALUE, Integer.MAX_VALUE)) {
					if (da.getStatus() == DeveloperAreaStatus.LOCKED) {
						previousDeveleoperAreas.add(da);
					}
				}
	
				// if some available, remove all developer areas from the new target document
				if (!previousDeveleoperAreas.isEmpty()) {
					if (textTarget.getNewTargetDocument().getDeveloperArea(Integer.MIN_VALUE, Integer.MAX_VALUE) != null) {
						previousDeveleoperAreas.removeAll(textTarget.getNewTargetDocument().getDeveloperArea(Integer.MIN_VALUE,
								Integer.MAX_VALUE));
					}
					if (!previousDeveleoperAreas.isEmpty()) {
						TargetSection ts = textTarget.getNewTargetDocument().getTargetSections().last();
						((TextWriterI)textTarget.getBaseWriter()).write(ts, 
								textTarget.getCommentStart(),
								"************************************************************",
								textTarget.getCommentEnd(),
								StringTools.NEWLINE,
								textTarget.getCommentStart(),
								"* Lost and Found of unused developer area(s)               *",
								textTarget.getCommentEnd(),
								StringTools.NEWLINE,
								textTarget.getCommentStart(),
								"************************************************************",
								textTarget.getCommentEnd(),
								StringTools.NEWLINE,
								textTarget.getCommentStart(),
								"* Please move this code back into the corresponding places *",
								textTarget.getCommentEnd(),
								StringTools.NEWLINE,
								textTarget.getCommentStart(),
								"* or delete the developer areas if not needed any more.    *",
								textTarget.getCommentEnd(),
								StringTools.NEWLINE,
								textTarget.getCommentStart(),
								"* In case you want to keep it, just coment it to           *",
								textTarget.getCommentEnd(),
								StringTools.NEWLINE,
								textTarget.getCommentStart(),
								"* prevent your target of possible compiler errors.         *",
								textTarget.getCommentEnd(),
								StringTools.NEWLINE,
								textTarget.getCommentStart(),
								"************************************************************",
								textTarget.getCommentEnd(),
								StringTools.NEWLINE);
						String message = "Found an unused developer ''{0}'' area in the previous target content."
								+ StringTools.NEWLINE;
						AbstractTextTarget<?> att = AbstractTextTarget.class.isInstance(target) ? 
								(AbstractTextTarget<?>) target : null;
						for (DeveloperAreaInfoI da : previousDeveleoperAreas) {
							if (att != null) {
								att.addError(MessageFormat.format(message, da.getId()), null);
							}
							textTarget.beginDeveloperArea(da.getId());
							textTarget.endDeveloperArea();
						}
					}
				}
			}
		}
	}

	/* (non-Javadoc)
	 * @see org.jenerateit.target.TargetLifecycleListenerI#onLoaded(org.jenerateit.target.TargetI)
	 */
	@Override
	public void onLoaded(TargetI<?> target) {
		
	}

	/* (non-Javadoc)
	 * @see org.jenerateit.target.TargetLifecycleListenerI#onFound(org.jenerateit.target.TargetI)
	 */
	@Override
	public void onFound(TargetI<?> target) {
		
	}
	
	
}

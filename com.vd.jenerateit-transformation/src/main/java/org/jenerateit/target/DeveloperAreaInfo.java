///*
// *
// *  JenerateIT - Lightweight Generation
// *
// *  http://jenerateit.org
// *
// *  Copyright:
// *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
// *
// *  License:
// *    LGPL: http://www.gnu.org/licenses/lgpl.html
// *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
// *    See the LICENSE file in the project's top-level directory for details.
// *
// *  Authors:
// *    Heinz Rohmer (hrr)
// * 
// */
//package org.jenerateit.target;
//
//import java.io.Serializable;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.regex.Pattern;
//
//import org.jenerateit.util.StringTools;
//
//
//
///**
// * @author hrr
// *
// */
//public final class DeveloperAreaInfo implements DeveloperAreaInfoI, Serializable {
//
//	/**
//	 * 
//	 */
//	private static final long serialVersionUID = -1204610771564865128L;
//
//	private static final Pattern DEVELOPER_AREA_ID_PATTERN = Pattern.compile("(^" + DeveloperAreaInfoI.ID_PATTERN + "$)");
//	
//	
//	private TextTargetDocumentI document = null;
//	protected int start = 0;
//	protected int end = 0;
//	protected int contentStart = 0;
//	protected int contentEnd = 0;
//	private String id = null;
//	
//	/**
//	 * Constructor
//	 * 
//	 * @param document
//	 * @param id
//	 * @param start
//	 * @param end
//	 * @param contentStart
//	 * @param contentEnd
//	 * @throws NullPointerException if the document is null
//	 * @throws IllegalArgumentException if the id is invalid of the different positions don't fit
//	 */
//	public DeveloperAreaInfo(TextTargetDocumentI document, String id, int start, int end, int contentStart, int contentEnd) {
//		if (document == null) {
//			throw new NullPointerException("the document may not be null");
//			
//		} else if (!checkDeveloperAreaId(id)) {
//			throw new IllegalArgumentException("The id is not valid");
//			
//		} else if (start < 0) {
//			throw new IllegalArgumentException("The start position may not be less zero");
//
//		} else if (end < (start + 54)) {
//			throw new IllegalArgumentException("The minimum size of a developer are (54 characters) is not reached");
//			
//		} else if ((start + 20) > contentStart) {
//			throw new IllegalArgumentException("The content start position is to close to the start position");
//
//		} else if (contentStart > contentEnd) {
//			throw new IllegalArgumentException("The content end is less than the content start position");
//			
//		} else if ((contentEnd + 34) > end) {
//			throw new IllegalArgumentException("The content end is to close to the end position");
//			
//		} else if (end > document.length()) {
//			throw new IllegalArgumentException("The end position is behind the document end");
//			
//		} else {
//			this.document = document;
//			this.id = id;
//			this.start = start;
//			this.end = end;
//			this.contentStart = contentStart;
//			this.contentEnd = contentEnd;
//		}
//	}
//
//	/**
//	 * Getter for the end positon within the document content
//	 * 
//	 * @return the end position within the document content
//	 */
//	@Override
//	public int getEnd() {
//		return this.end;
//	}
//
//	/**
//	 * Getter for the start position within the document content
//	 * 
//	 * @return the start position within the document content
//	 */
//	@Override
//	public int getStart() {
//		return this.start;
//	}
//
//	/* (non-Javadoc)
//	 * @see com.gs.jenerateit.transformation.target.DeveloperAreaInfoI#getCode()
//	 */
//	@Override
//	public String getId() {
//		return this.id;
//	}
//
//	/* (non-Javadoc)
//	 * @see com.gs.jenerateit.transformation.target.DeveloperAreaInfoI#getUserContent()
//	 */
//	@Override
//	public CharSequence getUserContent() {
//		return document.subSequence(contentStart, contentEnd);
//	}
//
//	/* (non-Javadoc)
//	 * @see com.gs.jenerateit.transformation.target.DeveloperAreaInfoI#getStatus()
//	 */
//	public DeveloperAreaStatus getStatus() {
//		return StringTools.isText(getUserContent().toString()) ? DeveloperAreaStatus.LOCKED : DeveloperAreaStatus.UNLOCKED;
//	}
//
//	/* (non-Javadoc)
//	 * @see com.gs.jenerateit.transformation.target.DeveloperAreaInfoI#getText()
//	 */
//	@Override
//	public CharSequence getText() {
//		return document.subSequence(start, end).toString();
//	}
//
//	
//	/**
//	 * Checks if the given object is equals. 
//	 * The following condition needs to be true in order both objects are equals:
//	 * <ul>
//	 * <li>the object obj must not be null</li>
//	 * <li>the object obj must be of type {@link DeveloperAreaInfoI DeveloperAreaInfoI}</li>
//	 * <li>the IDs of both developer areas needs to be the same (case insensitive)</li>
//	 * </ul> 
//	 * @param obj
//	 * @return true if the objects are equals otherwise false
//	 * @see java.lang.Object#equals(java.lang.Object)
//	 */
//	@Override
//	public boolean equals(Object obj) {
//		return 
//			obj != null && 
//			DeveloperAreaInfoI.class.isAssignableFrom(obj.getClass()) &&
//			getId().compareToIgnoreCase(((DeveloperAreaInfoI)obj).getId()) == 0;
//	}
//
//	/**
//	 * Returns the hash code of this developer area.
//	 * The id only is used to calculate the hash code.
//	 * <code>
//	 * getId().toLowerCase().hashCode()
//	 * </code>
//	 * @return the hash code of this developer area.
//	 * @see java.lang.Object#hashCode()
//	 */
//	@Override
//	public int hashCode() {
//		return getId().toLowerCase().hashCode();
//	}
//
//	/**
//	 * Method to compare two DeveloperAreaInfo objects. Only the id will be compared.
//	 * 
//	 * @return 0 if the objcts are equals otherwise 1 or -1
//	 * 
//	 * @see java.lang.Comparable#compareTo(java.lang.Object)
//	 * @see org.jenerateit.target.DeveloperAreaInfoI#getId()
//	 */
//	public int compareTo(DeveloperAreaInfoI lai) {
//		if (lai != null) {
//			return getId().compareToIgnoreCase(lai.getId());
//		}
//		return -1;
//	}
//
////	/**
////	 * Checks if the given developer areas fit the conditions
////	 * 
////	 * @param id the id to check
////	 * @return true if the id is valid otherwise false
////	 */
////    public static boolean checkDeveloperAreaId(String id) {
////		try {
////			if (StringTools.isText(id) && DEVELOPER_AREA_ID_PATTERN.matcher(id).matches()) {
////				return true;
////			}
////		} catch (Exception e) {
////		}    	
////		return false;
////    }
//    
//	/**
//	 * Finds the developer area with the given id within the given list.
//	 * Note: The id is insensitive
//	 * 
//	 * @param areas a list of developer areas to search in
//	 * @param id the id to look for
//	 * @return a developer area or null if not found
//	 * @throws NullPointerException if the areas are null
//	 * @throws IllegalArgumentException if the id is invalid
//	 */
//	public static DeveloperAreaInfoI getDeveloperArea(List<DeveloperAreaInfoI> areas, String id) {
//		if (areas == null) {
//			throw new NullPointerException("The developer area list may not be null");
//			
//		} else if (!checkDeveloperAreaId(id)) {
//			throw new IllegalArgumentException("The developer id '" + id + "' is not valid");
//			
//		} else {
//			for (DeveloperAreaInfoI dai : areas) {
//				if (dai.getId().compareToIgnoreCase(id) == 0) {
//					return dai;
//				}
//			}
//			return null;
//		}
//	}
//
//	/**
//	 * Finds all develper areas in the given list between the start and end position.
//	 * 
//	 * @param areas a list of developer areas to search in
//	 * @param start the start position in the content to search for
//	 * @param end the end position in the content to search for
//	 * @return a list of developer areas
//	 * @throws NullPointerException if the areas are null
//	 * @throws IllegalArgumentException if the start and end position are invalid
//	 */
//	public static List<DeveloperAreaInfoI> getDeveloperArea(List<DeveloperAreaInfoI> areas, int start, int end) {
//		if (areas == null) {
//			throw new NullPointerException("The developer area list may not be null");
//			
//		} else if (end < 0) {
//			throw new IllegalArgumentException("The end '" + end + "' may not be less than zero");
//			
//		} else if (end < start) {
//			throw new IllegalArgumentException("The end '" + end + "' is less than the start '" + start + "'");
//			
//		} else {
//			List<DeveloperAreaInfoI> newAreas = new ArrayList<DeveloperAreaInfoI>();
//			for (DeveloperAreaInfoI dai : areas) {
//				if (dai.getStart() < end
//						&& dai.getEnd() > start) {
//					newAreas.add(dai);
//				}
//			}
//			return newAreas;
//		}
//	}
//}

/*
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit.target;

import java.io.InputStream;

/**
 * Interface to describe an ASCII target.
 * 
 * @author hrr
 */
public interface TextTargetI<V extends TextTargetDocumentI> extends TargetI<V> {

    /**
     * Returns the start of a comment for this type of target.
     * 
     * @return the start sequence of a comment
     */
    CharSequence getCommentStart();
    
    /**
     * Returns the end of a comment for this type of target.
     * 
     * @return the end sequence of a comment
     */
    CharSequence getCommentEnd();
    
    /**
     * Starts a developer area in the target file.
     * 
     * @param id the id of the area to lock
     */
    void beginDeveloperArea(String id);

    /**
     * Ends the developer area in the target file.
     * 
     */
    void endDeveloperArea();
    
    /**
     * Get the id of the current developer area.
     * 
     * @return the id of the developer area or {@code null} if now developer area is started
     */
    String getCurrentDeveloperAreaId();

    /**
     * Get the stack of current developer area ids.
     * 
     * @return an array of developer area ids, where the first element
     * ({@code getCurrentDeveloperAreaIds()[0]}) is the currently started
     * developer area. An Empty array is returned if no developer area is started.
     */
    String[] getCurrentDeveloperAreaIds();

    /**
     * Increases the indent of the current active target section by a number of spaces.
     * 
     * @param number the number of spaces
     */
    void incrIndent(int number);

    /**
     * Increases the indent of the given target section by a number of spaces.
     * 
     * @param number the number of spaces
     * @param section the target section modify
     */
    void incrIndent(TargetSection section, int number);

    /**
     * Decreases the indent of the current active target section by a number of spaces.
     * 
     * @param number the number of spaces
     */
    void decrIndent(int number);

    /**
     * Decreases the indent of the given target section by a number of spaces.
     * 
     * @param number the number of spaces
     * @param section the target section to modify
     */
    void decrIndent(TargetSection section, int number);

    /**
     * Writes a {@link CharSequence} to the end of the current target section.
     * The target section is set inside the {@link AbstractTarget#transform(com.vd.transformation.data.TransformationRequest, InputStream)}.
     * 
     * @param text the text to write
     */
    void write(CharSequence... text);
    
    /**
     * Writes a text to the end of the given target section.
     * 
     * @param text the text to write
     * @param section the target section to write to
     */
    void write(TargetSection section, CharSequence... text);
    
    /**
     * Writes a newline character to the end of the current target section.
     */
    void writeNL();
    
    /**
     * Writes a newline character to the end of the given target section.
     * 
     * @param section the target section to write to
     */
    void writeNL(TargetSection section);
}

/*
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit.target;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;

import org.jenerateit.target.DeveloperAreaInfoI.DeveloperAreaStatus;
import org.jenerateit.util.StringTools;

import com.vd.transformation.data.TargetInfo;


/**
 * Abstract target implementation for ASCII targets. 
 * 
 * @author hrr
 *
 */
public abstract class AbstractTextTarget<V extends TextTargetDocumentI> extends AbstractTarget<V> implements TextTargetI<V> {

	private final CharSequence commentStart;
	private final CharSequence commentEnd;
	private final Deque<CurrentDeveloperArea> currentDeveloperAreaStack = new ArrayDeque<>();
	private static final TargetLifecycleListenerI DEVELOPER_AREA_MERGER = new DeveloperAreaMerger();
	
	/**
	 * 
	 * @author hrr
	 *
	 */
	private class CurrentDeveloperArea {
		private final String id;
		private TargetSection commentSection = null;
		private boolean startCommentWritten = false;
		private boolean endCommentWritten = true; // in case a new developer area starts the end comment is valid -> true
		private boolean writeUserContent = false;

		/**
		 * @param id
		 */
		public CurrentDeveloperArea(String id) {
			super();
			this.id = id;
		}
		
		
	}


	/**
	 * Constructor.
	 */
	protected AbstractTextTarget() {
		super();
		
		final V td = getTargetDocument();
		this.commentStart = td.getCommentStart();
		this.commentEnd = td.getCommentEnd();
		
		addTargetLifecycleListener(DEVELOPER_AREA_MERGER);
	}

	// --------------------------------------------------------------
	// Operations that handle developer areas


	/**
	 * Starts a developer area.
	 * 
	 * @param id the id of the developer area
	 * @see TextTargetI#beginDeveloperArea(String)
	 */
	public final void beginDeveloperArea(String id) {
//		if (this.currentDeveloperArea != null) {
//			throw new TargetException("Starting a new developer area while an other developer area '"
//					+ this.currentDeveloperArea.id + "'is not ended is not allowed", this);
//
//		} else if (!DeveloperAreaInfo.checkDeveloperAreaId(id)) {
//			throw new TargetException("The developer area id '" + id + "' is not valid. "
//					+ "Please use only characters like [a-zA-Z_0-9] for words and seperate the words by a dot '.'.\n"
//					+ "E.g.: abc.def.GHI_JKL.1", this);
//		}

		getNewTargetDocument().checkDeveloperAreaId(id);
		
		CurrentDeveloperArea currentDeveloperArea = new CurrentDeveloperArea(id);
		this.currentDeveloperAreaStack.push(currentDeveloperArea);
		checkNewLineInCurrentSection();
		write(getCommentStart(),
				DeveloperAreaInfoI.DA_START,
				DeveloperAreaInfoI.DA_SEPARATOR,
				currentDeveloperArea.id,
				DeveloperAreaInfoI.DA_SEPARATOR,
				DeveloperAreaInfoI.DA_START,
				getCommentEnd());
		writeNL();
		DeveloperAreaInfoI devArea = null;
		if (getPreviousTargetDocument() != null
				&& (devArea = getPreviousTargetDocument().getDeveloperArea(currentDeveloperArea.id)) != null
				&& devArea.getStatus() == DeveloperAreaStatus.LOCKED) {
			setPrefixEnabled(false);
			try {
				currentDeveloperArea.writeUserContent = true;
				write(getPreviousTargetDocument().getDeveloperArea(currentDeveloperArea.id).getUserContent());
			} finally {
				currentDeveloperArea.writeUserContent = false;
			}
			setPrefixEnabled(true);
			checkNewLineInCurrentSection();
			write(getCommentStart(),
				DeveloperAreaInfoI.DA_ELSE,
				DeveloperAreaInfoI.DA_SEPARATOR,
				currentDeveloperArea.id,
				DeveloperAreaInfoI.DA_SEPARATOR,
				DeveloperAreaInfoI.DA_ELSE,
				getCommentEnd());
			writeNL();
			currentDeveloperArea.commentSection = getCurrentTransformationSection();
		} else {
			checkNewLineInCurrentSection();
			write(getCommentStart(),
				DeveloperAreaInfoI.DA_ELSE,
				DeveloperAreaInfoI.DA_SEPARATOR,
				currentDeveloperArea.id,
				DeveloperAreaInfoI.DA_SEPARATOR,
				DeveloperAreaInfoI.DA_ELSE,
				getCommentEnd());
			writeNL();
		}
	}

	/**
	 * Ends the developer area.
	 * 
	 * @see TextTargetI#endDeveloperArea()
	 */
	public final void endDeveloperArea() {
		if (currentDeveloperAreaStack.isEmpty()) {
			throw new TargetException("There is no developer area to end");
		}
		CurrentDeveloperArea currentDeveloperArea = currentDeveloperAreaStack.pop();
		try {
			// disable special handling for writing within a developer area 
			currentDeveloperArea.commentSection = null;
			
			// check if the comment end is necessary and written
			if (getCommentEnd() != null
					&& !currentDeveloperArea.endCommentWritten) {
				write(getCommentEnd());
			}
			// newline before DA-END
			checkNewLineInCurrentSection();
			// write DA-END
			write(getCommentStart(),
					DeveloperAreaInfoI.DA_END,
					DeveloperAreaInfoI.DA_SEPARATOR,
					currentDeveloperArea.id,
					DeveloperAreaInfoI.DA_SEPARATOR,
					DeveloperAreaInfoI.DA_END,
					getCommentEnd());
			writeNL();
		} finally {
			currentDeveloperArea = null;
		}
	}
	
	@Override
	public String getCurrentDeveloperAreaId() {
		if (!this.currentDeveloperAreaStack.isEmpty()) {
			return this.currentDeveloperAreaStack.peek().id;
		}
		return null;
	}
	
	@Override
	public String[] getCurrentDeveloperAreaIds() {
		return this.currentDeveloperAreaStack.stream()
				.map(cda -> cda.id)
				.toArray(String[]::new);
	}

	/**
	 * Checks the current active {@link TargetSection section} in the
	 * {@link TargetDocumentI target document} ends with a newline character. If
	 * no newline character at the end is found or the current target document
	 * can not be determined a {@link #writeNL() newline} is added.
	 */
	protected void checkNewLineInCurrentSection() {
		TargetDocumentI td = getNewTargetDocument();
		if (td == null) {
			throw new TargetException(
					"There is no target document to write to -> can not check for newline at the end of the current section '"
							+ this.currentDeveloperAreaStack.peek().id + "'", this);

		} else if (AbstractTargetDocument.class.isAssignableFrom(td.getClass())) {
			AbstractTargetDocument atd = (AbstractTargetDocument) td;
			byte[] content = atd.toByte(getCurrentTransformationSection());
			if (content != null && content.length > 0
					&& (content[content.length - 1] == '\n' || content[content.length - 1] == '\r')) {
				// there is a newline at the end -> do nothing
				return;
			}
		}
		writeNL();
	}

	
	
	/**
	 * Add some more statistics to the target info.
	 * 
	 * @see TargetI#getTargetInfo()
	 * @see org.jenerateit.target.AbstractTarget#getTargetInfo()
	 */
	@Override
	public TargetInfo getTargetInfo() {
		TargetInfo ti = super.getTargetInfo();
		
		int lockedDA = -1;
		int allDA = -1;

		List<DeveloperAreaInfoI> devInfo = null;
		if (getNewTargetDocument() != null &&
				(devInfo = getNewTargetDocument().getDeveloperArea(0, Integer.MAX_VALUE)) != null) {
			lockedDA = 0;
			allDA = 0;
			for (DeveloperAreaInfoI la : devInfo) {
				allDA++;
				if (la.getStatus() == DeveloperAreaStatus.LOCKED) {
					lockedDA++;
				}
			}
		}
		
		ti.setDeveloperAreaCount(allDA);
		ti.setDeveloperAreaModified(lockedDA);

		return ti;
	}

	/**
	 * Append a text to the end of the current target section.
	 * 
	 * @param text the text to append
	 * @see TextTargetI#write(CharSequence...)
	 * @see #write(TargetSection, CharSequence...)
	 */
	@Override
	public final void write(CharSequence... text) {
		write(getCurrentTransformationSection(), text);
	}

	/**
	 * Append a text to the end of the given target section.
	 * 
	 * @param text the text to append
	 * @param section the target section to append to
	 * @see TextTargetI#write(TargetSection, CharSequence...)
	 */
	public final void write(final TargetSection section, final CharSequence... text) {
		// check if in transformation state
		getCurrentTransformationSection();

		if (text == null) {
			throw new TargetException("The string to write in a target may not be null.", this);

		} else if (section == null) {
			throw new TargetException("The target section to write to must not be null", this);

		} else {
			
			// loop over all text segments
			for (final CharSequence cs : text) {
				if (cs != null && cs.length() > 0) {
					final int length = cs.length();
					int lastLineEnds = 0;
					int endBeforeNLCR = 0;
					CharSequence foundLine = null;
					// check for new lines
					for (int i = 0; i < length; i++) {
						char c = cs.charAt(i);
						if (c == '\n') {
							endBeforeNLCR = i;
							if ((i + 1) < length && cs.charAt(i + 1) == '\r') {
								i++;
							}
							foundLine = cs.subSequence(lastLineEnds, endBeforeNLCR);
							lastLineEnds = i + 1;
						} else if (c == '\r') {
							endBeforeNLCR = i;
							if ((i + 1) < length && cs.charAt(i + 1) == '\n') {
								i++;
							}
							foundLine = cs.subSequence(lastLineEnds, endBeforeNLCR);
							lastLineEnds = i + 1;
						}
						
						if (foundLine != null) {
							CurrentDeveloperArea currentDeveloperArea = currentDeveloperAreaStack.peek();
							TargetSection commentSection = currentDeveloperAreaStack.stream()
									.filter(cda -> cda.commentSection != null)
									.map(cda -> cda.commentSection)
									.findAny().orElse(null);
							if (currentDeveloperArea != null && 
									commentSection != null &&
									commentSection.equals(section)) {
								// BUGFIX JIT 65: remove comment signs within commented developer areas
								if (commentStart != null) {
									foundLine = foundLine.toString().replace(commentStart, StringTools.EMPTY_STRING);
								}
								
								if (commentEnd != null) {
									foundLine = foundLine.toString().replace(commentEnd, StringTools.EMPTY_STRING);
									if (!currentDeveloperArea.startCommentWritten) {
										addText(section, commentStart, foundLine, commentEnd, getLineBreakChars());
									} else {
										addText(section, foundLine, commentEnd, getLineBreakChars());
									}
								} else {
									if (!currentDeveloperArea.startCommentWritten) {
										addText(section, commentStart, foundLine, getLineBreakChars());
									} else {
										addText(section, foundLine, getLineBreakChars());
									}
									
								}
								
								currentDeveloperArea.startCommentWritten = false; // reset start comment since end comment is written
								currentDeveloperArea.endCommentWritten = true; // end comment written within this line
							} else {
								addText(section, foundLine, getLineBreakChars());
							}
							setPrefixEnabled(section, currentDeveloperArea == null || !currentDeveloperArea.writeUserContent);// true);
							foundLine = null;
						}
					} // for loop
					// is there some characters left without an newline at the end?
					if (lastLineEnds < length) {
						CharSequence content = cs.subSequence(lastLineEnds, length);
						CurrentDeveloperArea currentDeveloperArea = currentDeveloperAreaStack.peek();
						TargetSection commentSection = currentDeveloperAreaStack.stream()
								.filter(cda -> cda.commentSection != null)
								.map(cda -> cda.commentSection)
								.findAny().orElse(null);
						if (currentDeveloperArea != null && 
								commentSection != null &&
								commentSection.equals(section)) {
							// BUGFIX JIT 65: remove comment signs within commented developer areas
							if (commentStart != null) {
								content = content.toString().replace(commentStart, StringTools.EMPTY_STRING);
							}
							if (commentEnd != null) {
								content = content.toString().replace(commentEnd, StringTools.EMPTY_STRING);
							}
							
							if (!currentDeveloperArea.startCommentWritten) {
								addText(section, commentStart, content);
								currentDeveloperArea.startCommentWritten = true;
							} else {
								addText(section, content);
							}
							currentDeveloperArea.endCommentWritten = false;
						} else {
							addText(section, content);
						}
					}
				}
			}
		}
	}

    /**
     * Writes a newline character to the end of the current target section.
     */
    public void writeNL() {
    	writeNL(getCurrentTransformationSection());
    }
    
    /**
     * Writes a newline character to the end of the given target section.
     * 
     * @param section the target section to write to
     */
    public void writeNL(TargetSection section) {
    	write(section, StringTools.NEWLINE);
    	setPrefixEnabled(section, true);
    }
    
    private void addText(final TargetSection section, final CharSequence... text) {
    	getNewTargetDocument().addText(section, text);
		setPrefixEnabled(section, false);
    }
    
	/**
	 * Set the prefix flag in the current target section.
	 * 
	 * @see #setPrefix(TargetSection, boolean)
	 * @param prefix true if to write the prefix, false if not have to write the prefix
	 */
	private void setPrefixEnabled(boolean prefix) {
		setPrefixEnabled(getCurrentTransformationSection(), prefix);
	}

	/**
	 * Set the prefix flag in the given target section.
	 * 
	 * @param section the target section modify the prefix flag
	 * @param prefix true if to write the prefix, false if not have to write the prefix
	 */
	private void setPrefixEnabled(TargetSection section, boolean prefix) {
		getNewTargetDocument().setPrefixEnabled(section, prefix);
	}

	/**
	 * Increases the indent of the current target section by a number of spaces.
	 * 
	 * @param increment the number of spaces
	 * @see TextTargetI#incrIndent(int)
	 */
	public void incrIndent(int increment) {
		incrIndent(getCurrentTransformationSection(), increment);
	}

	/**
	 * Increase the indent of the given target section by the number of spaces.
	 * 
	 * @param ts the target section to modify
	 * @param increment the number of spaces
	 * @see TextTargetI#incrIndent(TargetSection, int)
	 */
	public void incrIndent(TargetSection ts, int increment) {
		getNewTargetDocument().incrementPrefixSize(ts, increment);
	}

	/**
	 * Decrease the decrement of the current target section by a number of spaces.
	 * 
	 * @param decrement the number of spaces
	 * @see TextTargetI#decrIndent(int)
	 */
	public void decrIndent(int decrement) {
		decrIndent(getCurrentTransformationSection(), decrement);
	}

	/**
	 * Decrease the decrement of the given target section by a number of spaces.
	 * 
	 * @param ts the target section to modify
	 * @param decrement the number of spaces
	 * @see TextTargetI#decrIndent(TargetSection, int)
	 */
	public void decrIndent(TargetSection ts, int decrement) {
		getNewTargetDocument().decrementPrefixSize(ts, decrement);
	}

	/**
	 * Getter for the end sequence of a comment.
	 * In case a developer w'd change the characters of a comment end, please 
	 * change the implementation within {@link AbstractTextTargetDocument#getCommentEnd()}
	 * 
	 * @return the comment end string
	 * @see TextTargetDocumentI#getCommentEnd()
	 * @see TextTargetI#getCommentEnd()
	 */
	@Override
	public final CharSequence getCommentEnd() {
		return this.commentEnd;
	}

	/**
	 * Getter for the start sequence of a comment.
	 * In case a developer w'd change the characters of a comment start, please 
	 * change the implementation within {@link AbstractTextTargetDocument#getCommentStart()}
	 * 
	 * @return the comment start string
	 * @see TextTargetDocumentI#getCommentStart()
	 * @see TextTargetI#getCommentStart()
	 */
	@Override
	public final CharSequence getCommentStart() {
		return this.commentStart;
	}

    /**
     * Returns the character(s) representing a line break in a text document.
     * The default implementation of this method returns the system newline character(s). 
     * 
     * @see StringTools#NEWLINE
     * @return the line break character(s)
     */
	public CharSequence getLineBreakChars() {
		return StringTools.NEWLINE;
	}

}

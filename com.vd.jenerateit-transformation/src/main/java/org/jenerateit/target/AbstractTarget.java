/*
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit.target;

import java.io.InputStream;
import java.io.Serializable;
import java.lang.ref.SoftReference;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.SortedSet;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Logger;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

import org.jenerateit.ErrorMessage;
import org.jenerateit.GenerationProtocolI;
import org.jenerateit.InfoMessage;
import org.jenerateit.annotation.ContextObject;
import org.jenerateit.annotation.CreationTarget;
import org.jenerateit.annotation.GenerationGroup;
import org.jenerateit.annotation.ModelElement;
import org.jenerateit.annotation.OneOff;
import org.jenerateit.exception.JenerateITException;
import org.jenerateit.generationgroup.GenerationGroupI;
import org.jenerateit.generationgroup.WriterLocatorI;
import org.jenerateit.transformation.TransformationTarget;
import org.jenerateit.util.StringTools;
import org.jenerateit.util.UriTools;
import org.jenerateit.util.StringTools.Encoding;
import org.jenerateit.writer.AbstractTextWriter;
import org.jenerateit.writer.AbstractWriter;
import org.jenerateit.writer.WriterException;
import org.jenerateit.writer.WriterI;
import org.jenerateit.writer.WriterInfoProviderI;

import com.vd.transformation.data.TargetInfo;
import com.vd.transformation.data.TargetState;
import com.vd.transformation.data.TransformationRequest;
import com.vd.transformation.data.TransformationResponse;
import com.vd.transformation.data.WriterInfo;
import com.vd.transformation.data.WriterInfoList;

/**
 * Base class for all binary target implementation.
 * 
 * @author hrr
 * 
 */
public abstract class AbstractTarget<V extends TargetDocumentI> implements TargetI<V> {

//	private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss: ");

	/* caches */
	private final Map<Class<? extends WriterI>, Set<WriterI>> myWriterInstances = new HashMap<Class<? extends WriterI>, Set<WriterI>>();
	
	/* listeners */
	private final Set<TargetLifecycleListenerI> targetLifecycleListener = new HashSet<TargetLifecycleListenerI>();
	
	/* statistics */
	private long newWriterCounter = 0L;
	private long reusedWriterCounter = 0L;
	private Long start = null;
	private Long end = null;

	/* injection variables */
	@ModelElement
	private Object baseElement = null;
	@GenerationGroup
	private GenerationGroupI generationGroup;
	@ContextObject
	private Serializable context = null;

	private final TargetState status = new TargetState();
	private TargetSection currentTransformationSection = null;

	/* ThreadLocal variable */
	private final TransformationTarget transformationTarget = new TransformationTarget();

	private final List<String> infoMessageBuffer = new ArrayList<String>();
	private final List<String> warningMessageBuffer = new ArrayList<String>();
	private final List<String> errorMessageBuffer = new ArrayList<String>();
	
	private boolean forceDevelopmentMode = false;
	private final V internalPerformanceDocument;
	private V previousDocument = null;
	private V newDocument = null;
	private URI myTargetPath = null;
	
	/**
	 * Constructor.
	 */
	protected AbstractTarget() {
		this.internalPerformanceDocument = getTargetDocument();

	}
	
	/**
	 * Initialize this target.
	 * This method will be executed right after the target was created. 
	 * Child classes may overwrite this method an initialize stuff right after calling the super class.
	 * 
	 */
	public final void init() {

		this.status.setText(AbstractTextTarget.class.isInstance(this));
		this.status.setNew(true);
		this.status.setContext(this.context != null);

		// Check OneOff annotation
		this.status.setOneOff(this.getClass().isAnnotationPresent(OneOff.class));
		// set the dev mode flag for the client
		this.status.setGenerationProtocol(isGenerationProtocol());

		this.status.setReading(false);
		this.status.setReaded(false);
		this.status.setTransforming(false);
		this.status.setTransformed(false);

		myTargetPath = UriTools.checkAndModify(getTargetURI());
		
		fireOnLoaded();
	}

	/**
	 * Needs to be implemented by clients to provide their target Paths
	 * 
	 * @return the target path of the target instance
	 */
	protected abstract URI getTargetURI();
	
	/**
	 * Getter to access the target path of this Target instance
	 * 
	 * @return the target URI of this instance
	 */
	public final URI getTargetPath() {
		return myTargetPath;
	}
	
	/**
	 * sends the {@link TargetLifecycleListenerI#postTransform(TargetI)} event to all listeners.
	 */
	private void firePostTransform() {
		for (final TargetLifecycleListenerI tlcl : this.targetLifecycleListener) {
			tlcl.postTransform(this);
		}
	}
	
	/**
	 * sends the {@link TargetLifecycleListenerI#preTransform(TargetI)} event to all listeners.
	 */
	private void firePreTransform() {
		for (final TargetLifecycleListenerI tlcl : this.targetLifecycleListener) {
			tlcl.preTransform(this);
		}
	}
	
	/**
	 * sends the {@link TargetLifecycleListenerI#onLoaded(TargetI)} event to all listeners.
	 */
	private void fireOnLoaded() {
		for (final TargetLifecycleListenerI tlcl : this.targetLifecycleListener) {
			tlcl.onLoaded(this);
		}
	}
	
	/**
	 * This method will be called by reflection
	 */
	@SuppressWarnings("unused")
	private void fireOnFound() {
		for (final TargetLifecycleListenerI tlcl : this.targetLifecycleListener) {
			tlcl.onFound(this);
		}
	}
	
	/**
	 * Helper method to check the given writer class may be instantiated. 
	 * 
	 * @param writerClass the class to check
	 * @throws TargetException if the check fails (e.g. writerClass is abstract)
	 */
	private void checkWriterClass(Class<? extends WriterI> writerClass) throws TargetException {
		if (writerClass == null) {
			throw new TargetException("Detected an attempt to obtain a writer instance with a writer class is NULL",
					this);
	
		} else if (Modifier.isAbstract(writerClass.getModifiers())) {
			throw new TargetException("Detected an attempt to obtain a writer instance with an abstract class '" + writerClass +
					"'", this);
			
		} else if (!Modifier.isPublic(writerClass.getModifiers())) {
			throw new TargetException("Detected an attempt to obtain a writer instance with a class '" + writerClass +
					"' is not public", this);
	
		}
	}
	
	/**
	 * Add a target lifecycle listener to this target instance.
	 * 
	 * @param t the target lifecycle listener to add
	 * @return true if this set did not already contain the specified element
	 * @see Set#add(Object)
	 */
	public boolean addTargetLifecycleListener(TargetLifecycleListenerI t) {
		return this.targetLifecycleListener.add(t);
	}
	
	/* === statistic part ================================================== */
	private final AtomicLong findCalls = new AtomicLong(0L);
	private final AtomicLong findDuration = new AtomicLong(0L);
	private final AtomicLong findMin = new AtomicLong(Long.MAX_VALUE);
	private final AtomicLong findMax = new AtomicLong(0L);

	/**
	 * Clears the statistic data
	 */
	private void clearStatistic() {
		findCalls.set(0L);
		findDuration.set(0L);
		findMin.set(Long.MAX_VALUE);
		findMax.set(0L);
	}

	private final ReentrantLock lock = new ReentrantLock();

	public void updateDuration(long duration) {
		findCalls.incrementAndGet();
		findDuration.addAndGet(duration);
		lock.lock();
		try {
			if (findMin.get() > duration) {
				findMin.set(duration);
			}
			if (findMax.get() < duration) {
				findMax.set(duration);
			}
		} finally {
			lock.unlock();
		}
	}

	/**
	 * Getter for the transformation information for this AbstractTarget.
	 * 
	 * @return the transformation information
	 * @see TargetI#getTargetInfo()
	 */
	public TargetInfo getTargetInfo() {
		TargetInfo ti = new TargetInfo();
		ti.setTargetClass(this.getClass().getName());
		ti.setBaseWriterClass(getBaseWriter().getClass().getName());
		ti.setModelElement(this.baseElement.toString());

		try {
			if (this.start != null) {
				GregorianCalendar gc = new GregorianCalendar();
				gc.setTimeInMillis(this.start);	
				ti.setStart(DatatypeFactory.newInstance().newXMLGregorianCalendar(gc));
			}
			if (this.end != null) {
				GregorianCalendar gc = new GregorianCalendar();
				gc.setTimeInMillis(this.end);
				ti.setEnd(DatatypeFactory.newInstance().newXMLGregorianCalendar(gc));
			}
		} catch (DatatypeConfigurationException e) {
			
		}
	
		ti.setFindCount(this.findCalls.get());
		ti.setFindDuration(this.findDuration.get());
		ti.setFindDurationMin(this.findMin.get() == Integer.MAX_VALUE ? 0 : this.findMin.get());
		ti.setFindDurationMax(this.findMax.get());
		ti.setWriteCount(getNewTargetDocument() != null ? getNewTargetDocument().getWriteCalls() : 0L);
		ti.setNewWriter(newWriterCounter);
		ti.setReusedWriter(reusedWriterCounter);
		ti.setStatus(this.status);
		return ti;
	}

	/* === end statistic stuff ============================================= */

	/**
	 * Method to execute the transformation for this AbstractTarget.
	 * <p>
	 * The input stream will be closed after the read is done.
	 * <p>
	 * The output stream is closed after the write is done. NOTE: If the input
	 * content is equals to the output content, no write, flush and close
	 * operation is done to the output stream
	 * 
	 * @param input
	 *            the input stream to read the original file from
	 * @throws NullPointerException
	 *             if the request object is null
	 */
	@Override
	public final TransformationResponse transform(final TransformationRequest request, final InputStream input) throws NullPointerException, TargetException, WriterException {
		try {
			myWriterInstances.clear();
			
			newWriterCounter = 0L;
			reusedWriterCounter = 0L;

			final TransformationResponse result = new TransformationResponse();

			if (request == null) {
				throw new NullPointerException("The request object may not be null");
				
			// check if it is not a OneOff (content may be corrupt) 
			// and the file is not marked as new
		    // and modified with a input is null
			} else if (!this.status.isOneOff()
					&& request.isNew() != null
					&& !request.isNew()
					&& request.isModified() != null
					&& request.isModified()
					&& input == null) {
				throw new JenerateITException("The modified taget content (InputStream) must not be null since the previous content is marked as modified");

			} else {
				try {
					// 1. set the start time
					this.start = System.currentTimeMillis();
					this.status.setNew(false);
					this.status.setTransformed(false);
					clearStatistic();

					this.infoMessageBuffer.clear();
					this.warningMessageBuffer.clear();
					this.errorMessageBuffer.clear();

					// TargetUtils.clearStatistic();
					forceDevelopmentMode = false;

					// first, check for a one off (use a new document, since it may be corrupt)
					// or it is a new one (even if an input is sent or a cached document is present)
					if (this.status.isOneOff() // a oneOff do not have a history -> create a new empty document with no DA
							|| (request.isNew() != null 
								&& request.isNew())) { // a new document does not have any content on client side
						// TODO do we need a cache for target documents?
						this.previousDocument = getTargetDocument();
					
					} else if (request.isModified() != null 
							&& request.isModified()) {
						// second, if the content is modified 
						// If so, read the content anyway
						try {
							// OK a modified file was found read and parse it again
							this.status.setReaded(false);
							this.status.setReading(true);
							this.previousDocument = getTargetDocument();
							this.previousDocument.load(input);
							if (AbstractTextTargetDocument.class.isInstance(this.previousDocument)) {
								// try to find a dummy developer area to start analyzing the sent content
								// for developer area errors. So we can provide better error messages to 
								// the user
								AbstractTextTargetDocument.class.cast(this.previousDocument).getDeveloperArea("dummy");
							}
							this.status.setReading(false);
							this.status.setReaded(true);
						} catch (DeveloperAreaException e) {// only if AbstractTextTargetDocument
							throw new TargetException("While parsing the modified content from the file sent by the user, a problem with a developer area occurred: " 
									+ e.getMessage(), this);
						} catch (Exception e) {
							throw new TargetException("Error while read the last file content out of the given input stream",
									e, this);
						}

					// third: If a cached content is found use this one
					} else if (this.previousDocument != null) {
						// looks like a previous transformed target document was found in a cache and set by injection
					
					} else if (input != null) {
						// in case an input is given but no cached target document is found read it (fallback)
						try {
							this.status.setReaded(false);
							this.status.setReading(true);
							this.previousDocument = getTargetDocument();
							this.previousDocument.load(input);
							if (AbstractTextTargetDocument.class.isInstance(this.previousDocument)) {
								// try to find a dummy developer area to start analyzing the sent content
								// for developer area errors. So we can provide better error messages to 
								// the user
								AbstractTextTargetDocument.class.cast(this.previousDocument).getDeveloperArea("dummy");
							}
							this.status.setReading(false);
							this.status.setReaded(true);
						} catch (DeveloperAreaException e) {// only if AbstractTextTargetDocument
							throw new TargetException("While parsing the content from the file sent by the user, a problem with a developer area occurred: " 
									+ e.getMessage(), this);
						} catch (Exception e) {
							throw new TargetException("Error while read the last file content out of the given input stream",
									e, this);
						}
						
					} else {
						throw new TargetException("Can not detect the previous target document right before start transformation", this);
					}
					
					if (this.previousDocument != null) {
						if (this.previousDocument instanceof TextTargetDocumentI) {
							TextTargetDocumentI textTargetDocument = (TextTargetDocumentI) this.previousDocument;
							if (textTargetDocument.isByteOrderMarkRemoved()) {
								addInfo(InfoMessage.BYTE_ORDER_MARK_REMOVED.getMessage());
								if (textTargetDocument.getByteOrderMarkEncoding() != Encoding.UTF_8) {
									throw new TargetException(ErrorMessage.UNSUPPORTED_BYTE_ORDER_MARK_ENCODING.getMessage(textTargetDocument.getByteOrderMarkEncoding()), this);
								}
							}
						}
					}

					// 3. transform the actual transformation
					try {
						newDocument = getTargetDocument();

						transform();
						result.setDoNotWrite(false);

					} catch (DoNotWriteException e) {
						result.setDoNotWrite(true);
					
					} catch (DeveloperAreaException e) { // only if AbstractTextTargetDocument
						throw new TargetException("While transform the new target, a problem with a developer area occurred: " 
								+ e.getMessage(), this);
					}

					// 4. set the content specific response flags
					result.setContentEmpty(newDocument == null || newDocument.length() == 0);
					result.setContentEquals(previousDocument != null && previousDocument.compareTo(newDocument) == 0);
					
				} finally {

					this.end = System.currentTimeMillis();

				}
			}
			
			result.setTargetInfo(getTargetInfo());
			
			return result;
//		} catch (TargetException e) {
//			addError("Error in transformation process", e);
//			throw e;
//		
//		} catch (Throwable t) {
//			addError("Error in transformation process", t);
//			throw new TargetException("Error in transformation process", t, this);
		
		} finally {
			// after generation clear the old content
			// TODO do we need a cache for unused target documents?
			this.previousDocument = null;

			// save new created writers even in case of an Exception
			final ConcurrentMap<Class<? extends WriterI>, Queue<SoftReference<WriterI>>> writerCache = getGenerationGroup().getWriterCache();
			for (final Class<? extends WriterI> wc : myWriterInstances.keySet()) {
				final Set<WriterI> writers = myWriterInstances.get(wc);
				Queue<SoftReference<WriterI>> writerList = writerCache.get(wc);
				if (writerList == null) {
					writerList = writerCache.putIfAbsent(wc, new ConcurrentLinkedQueue<SoftReference<WriterI>>());
					// if the new created queue is added get it
					if (writerList == null) {
						writerList = writerCache.get(wc);
					}
				}
				for (WriterI w : writers) {
					writerList.offer(new SoftReference<WriterI>(w));
				}
				writers.clear();
			}
			
			myWriterInstances.clear();
		}
	}

	/**
	 * Getter to access the target section in transformation currently.
	 *  
	 * @return the current transformation section
	 */
	@Override
	public TargetSection getCurrentTransformationSection() {
		if (this.currentTransformationSection == null || !this.status.isTransforming()) {
			throw new TargetException(
					"Try to access the current target section while not in transformation state", 
					this);
		}
		return this.currentTransformationSection;
	}
	
	/**
	 * Method to start the transformation.
	 */
	private void transform() {
		// set the development mode flag first, in case of an transformation
		// error
		this.status.setTransformed(false);
		this.status.setTransforming(true);;
		try {
			transformationTarget.setCurrentTarget(this);

			firePreTransform();
			
			WriterI writer = getBaseWriter();
			// This is the entry point to the target generation.
			for (TargetSection ts : getTargetSections()) {
				this.currentTransformationSection = ts;
				/*this.baseWriter*/writer.transform(ts);
				// this.writeNL();
				// this.writeNL();
			}

			firePostTransform();
			
		} finally {
			this.currentTransformationSection = null;
			transformationTarget.setCurrentTarget(null);
		}
		this.status.setTransforming(false);
		this.status.setTransformed(true);
	}

	// --------------------------------------------------------------
	// Miscelaneous operations

	/**
	 * Getter to access the target that is transformed currently.
	 * 
	 * @return the target is transformed currently
	 * @see org.jenerateit.target.TargetI#getTransformationTarget()
	 */
	@Override
	public TargetI<?> getTransformationTarget() {
		final TargetI<?> t = transformationTarget.getCurrentTarget();
		if (t == null) {
			throw new TargetException("The transformation has not started yet and so there is no transformation target.\n" +
					"Please use the AbstractTarget#getTransformationTarget() method only in writers w() methods, not while initializing a writer or target.", this);
		}
		return t;
	}

	
	/**
	 * Compares two {@link TargetI} objects by its {@link #getTargetURI()
	 * target file}.
	 * 
	 * @param target
	 *            the target to compare to
	 * @return 0 if equals otherwise +1 or -1
	 * @see Comparable#compareTo(Object)
	 * @see #getTargetURI()
	 */
	public int compareTo(TargetI<V> target) {
		int result = getTargetPath().compareTo(target.getTargetPath());
		return result;
	}

	/**
	 * Checks if the given object is a {@link TargetI} instance and has the same
	 * target path.
	 * 
	 * @param obj
	 *            the TargetI object to compare to
	 * @return true if the target paths are equals otherwise false
	 * @see Object#equals(Object)
	 * @see #getTargetURI()
	 */
	@Override
	public boolean equals(Object obj) {
		return obj != null && TargetI.class.isInstance(obj)
				&& getTargetPath().equals(
						((TargetI<?>) obj).getTargetPath());
	}

	/**
	 * Calculates the hash code of this TargetI object by its target path.
	 * 
	 * @return the hash code
	 * @see Object#hashCode()
	 * @see #getTargetURI()
	 */
	@Override
	public int hashCode() {
		return this.getTargetPath().hashCode();
	}

	/**
	 * Getter for the generation group.
	 * 
	 * @return the value of generation group
	 * @see org.jenerateit.target.TargetI#getGenerationGroup()
	 */
	public final GenerationGroupI getGenerationGroup() {
		return this.generationGroup;
	}

	// --------------------------------------------------------------
	// private operations

	// === writer section =====================================================

	/**
	 * Finds the responsible writer class for the given element.
	 * 
	 * @param element the element to find the writer class for
	 * @return the writer class for the given element.
	 */
//	protected abstract Class<? extends WriterI> getWriterClass(Serializable element);

	/**
	 * Getter for a base writer instance.
	 * 
	 * @return the base writer instance
	 * @see org.jenerateit.target.TargetI#getBaseWriter()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public final WriterI getBaseWriter() {
		return getWriterInstance(this.baseElement, getGenerationGroup().getWriterClass(baseElement, (Class<? extends TargetI<?>>)this.getClass()));
	}

	/**
	 * Getter for the base element this target is based on.
	 * 
	 * @return the base element
	 * @see org.jenerateit.target.TargetI#getBaseElement()
	 */
	public final Object getBaseElement() {
		return this.baseElement;
	}

	/**
	 * Creates a writer instance for the given element.
	 * 
	 * @param element
	 *            the element to create a writer for
	 * @return the writer instance
	 * @see TargetI#getWriterInstance(Object)
	 * @see TargetI#getWriterInstance(Object, Class)
	 * @see #getWriterInstance(Object, Class)
	 */
	public WriterI getWriterInstance(Object element) {
		if (element == null) {
			throw new TargetException("Detected an attempt to obtain a writer instance with a NULL value as model element", this);

		} else {
			final Class<? extends WriterI> writerClass = getGenerationGroup().getWriterClass(element, this);
			if (writerClass == null) {
				throw new TargetException("Detected an attempt to obtain a writer instance with a model element of type '" +
						element.getClass().getName() + "' but the " + WriterLocatorI.class.getSimpleName() + 
						" in '" + getGenerationGroup().getClass().getClass().getName() + "' does not provide a writer class", this);
			}
			return getWriterInstance(element, writerClass);
		}
	}


	/**
	 * Creates a writer instance for the given model element and writer class.
	 * 
	 * @param element the model element
	 * @param writerClass the writer class to create an instance from
	 */
	@SuppressWarnings("unchecked")
	public <T extends WriterI> T getWriterInstance(Object element, Class<T> writerClass) {
		if (element == null) {
			throw new TargetException("Detected an attempt to obtain a writer instance with a NULL value as model element", this);

		} else {
			checkWriterClass(writerClass);
			
			T writer = null;
			ConcurrentMap<Class<? extends WriterI>, Queue<SoftReference<WriterI>>> writerCache = getGenerationGroup().getWriterCache();
			
			Queue<SoftReference<WriterI>> writerList = writerCache.get(writerClass);
		
			if (writerList != null) {
				SoftReference<WriterI> sr = null;
				while ((sr = writerList.poll()) != null) {
					writer = (T) sr.get();
					if (writer != null) {
						reusedWriterCounter++;
						break;
					}
				}
			}
			
			try {
				if (writer == null) {
					writer = writerClass.newInstance();
					newWriterCounter++;
				}
				
				Class<?> sc = writerClass;
				int unknownVariables = 0;
				while (sc != null) {
					
					for (Field f : sc.getDeclaredFields()) {
						if (f.isAnnotationPresent(ModelElement.class)) {
							if (!f.getType().isInstance(element)) {
								throw new JenerateITException("Found a field '" + f.getName() + "' of type '" + f.getType() + "' in class '" + 
										sc.getName() + "' with the annotation '" + ModelElement.class.getSimpleName() + 
										"' but the type (" + element.getClass().getName() + ") does not match");
							}
							f.setAccessible(true);
							f.set(writer, element);
							
						} else if (f.isAnnotationPresent(CreationTarget.class)) {
							if (!f.getType().isInstance(this)) {
								throw new JenerateITException("Found a field '" + f.getName() + "' of type '" + f.getType() + "' in class '" + 
										sc.getName() + "' with the annotation '" +	CreationTarget.class.getSimpleName() + 
										"' but the type (" + this.getClass().getName() + ") does not match");
								
							}
							f.setAccessible(true);
							f.set(writer, this);
							
						} else {
							// check if the field is not part of a known base class, and not a logger
							if (!AbstractWriter.class.equals(sc) &&
									!AbstractTextWriter.class.equals(sc) &&
									!Logger.class.equals(f.getType())) {
								
								// found a variable in the writer class 
								unknownVariables++;
							}
						}
					}
					sc = sc.getSuperclass();
				}
				
				
				if (unknownVariables == 0) {
					Set<WriterI> writersByClass = myWriterInstances.get(writerClass);
					if (writersByClass == null) {
						writersByClass = new HashSet<WriterI>();
						myWriterInstances.put(writerClass, writersByClass);
					}
					writersByClass.add(writer);
				}
				return writer;
				
			} catch (SecurityException se) {
				throw new JenerateITException("Problems while access the writer object fields", se);
			} catch (InstantiationException e1) {
				throw new JenerateITException("Error while create a new writer object", e1);
			} catch (IllegalAccessException e1) {
				throw new JenerateITException("Error whilc create a new writer object", e1);
			}
			
		}
	}

//	// === context section
//	// ======================================================
//	/**
//	 * Getter for the context this target uses.
//	 * 
//	 * @return the context of this target
//	 */
//	public final Serializable getContext() {
//		return this.context;
//	}
//
	// === information section =================================================
	/**
	 * Add an info message to the message buffer.
	 * <p>
	 * <b>Note:</b> This message will be show to the end user.
	 * </p>
	 * 
	 * @param message
	 *            the message
	 */
	public final void addInfo(final String message) {
		if (StringTools.isNotEmpty(message)) {
			this.infoMessageBuffer.add(message);
		}
	}

	/**
	 * Add a warning message to the message buffer.
	 * <p>
	 * <b>Note:</b> This message will be show to the end user.
	 * </p>
	 * 
	 * @param message
	 *            the message
	 */
	public final void addWarning(final String message) {
		addWarning(message, null);
	}
	
	private StringBuffer addThrowable(final StringBuffer sb, final Throwable t) {
		return addThrowable(sb, 2, t);
	}
	
	private StringBuffer addThrowable(final StringBuffer sb, final int fill, final Throwable t) {
		final char[] filledBuffer = new char[fill];
		Arrays.fill(filledBuffer, ' ');
		sb.append(filledBuffer).append(t.getClass().getSimpleName()).append(": ");
		if (StringTools.isText(t.getMessage())) {
			sb.append(t.getMessage());
		}
		sb.append(StringTools.NEWLINE);
		return t.getCause() != null ? addThrowable(sb, fill + 2, t.getCause()) : sb;
	}
	
	/**
	 * Add a warning message to the message buffer.
	 * <p>
	 * <b>Note:</b> This message will be show to the end user.
	 * </p>
	 * 
	 * @param message
	 *            the message
	 * @param t
	 *            the exception if available
	 */
	public final void addWarning(final String message, final Throwable t) {
		final StringBuffer sb = new StringBuffer();
		if (StringTools.isText(message)) {
			sb.append(message);
		}
		
		if (t != null) {
			if (sb.length() > 0) {
				sb.append(StringTools.NEWLINE);
			} else {
				sb.append("<exception only, no message available>").append(StringTools.NEWLINE);
			}
			addThrowable(sb, t);
		}
		
		if (sb.length() > 0) {
			this.warningMessageBuffer.add(sb.toString());
		}
	}

	/**
	 * Add an error message to the message buffer.
	 * <p>
	 * <b>Note:</b> This message will be show to the end user.
	 * </p>
	 * 
	 * @param message
	 *            the message
	 */
	public final void addError(final String message) {
		addError(message, null);
	}
	
	/**
	 * Add an error message to the message buffer.
	 * <p>
	 * <b>Note:</b> This message will be show to the end user.
	 * </p>
	 * 
	 * @param message
	 *            the message
	 * @param t
	 *            the exception if available
	 */
	public final void addError(final String message, final Throwable t) {
		final StringBuffer sb = new StringBuffer();
		if (StringTools.isText(message)) {
			sb.append(message);
		}
		
		if (t != null) {
			if (sb.length() > 0) {
				sb.append(StringTools.NEWLINE);
			} else {
				sb.append("<exception only, no message available>").append(StringTools.NEWLINE);
			}
			addThrowable(sb, t);
		}
		
		if (sb.length() > 0) {
			this.errorMessageBuffer.add(sb.toString());
		}
	}

	/**
	 * Getter for the info messages.
	 * 
	 * @return the info messages
	 * @see org.jenerateit.target.TargetI#getInfos()
	 */
	@Override
	public final List<String> getInfos() {
		return Collections.unmodifiableList(this.infoMessageBuffer);
	}

	/**
	 * Getter for the warning messages.
	 * 
	 * @return the warning messages
	 * @see org.jenerateit.target.TargetI#getWarnings()
	 */
	@Override
	public final List<String> getWarnings() {
		return Collections.unmodifiableList(this.warningMessageBuffer);
	}

	/**
	 * Getter for the error messages.
	 * 
	 * @return the error messages
	 * @see org.jenerateit.target.TargetI#getErrors()
	 */
	@Override
	public final List<String> getErrors() {
		return Collections.unmodifiableList(this.errorMessageBuffer);
	}

	// === target document handling ============================================
	/**
	 * This method is called whenever the transforomation needs an empty target
	 * document. Special target document implementations can be provided by this
	 * class to the framework (e.g. Java target documents, property target
	 * documents)
	 * 
	 * @return a new empty target document
	 */
	@SuppressWarnings("unchecked")
	protected final V getTargetDocument() {
		Type clazz = getClass();
		while (clazz != null && !(clazz instanceof ParameterizedType)) {
			clazz = ((Class<?>)clazz).getGenericSuperclass();
		}
		
		if (clazz == null) {
			throw new TargetException("Can not find the TargetI implementation class witht the TargetDocumentI Java generics settings", this);
			
		} else {
			Type[] args = ((ParameterizedType)clazz).getActualTypeArguments();
			if (args == null || args.length < 1) {
				throw new TargetException("Can not find the TargetDocumentI within the Java generics settings", this);
			}
			try {
				return (V) ((Class<V>)args[0]).newInstance();
			} catch (Exception e) {
				throw new TargetException("Error while create the target document of type '" + args[0] + "'", e, this);
			}
		}
	}

	/**
	 * Returns the new generated target document
	 * 
	 * @see org.jenerateit.target.TargetI#getNewTargetDocument()
	 * @return the new generated target document
	 */
	public final V getNewTargetDocument() {
		return this.newDocument;
	}

	/**
	 * Returns the previous generated target document that was read at the
	 * beginning of the {@link TargetI#transform(TransformationRequest, InputStream)
	 * transform} method.
	 * 
	 * @see TargetI#transform(TransformationRequest, InputStream)
	 * @see org.jenerateit.target.TargetI#getPreviousTargetDocument()
	 * @return the previous generated target document
	 */
	public final V getPreviousTargetDocument() {
		return this.previousDocument;
	}

	/**
	 * Getter for all targets sections this target supports.
	 * 
	 * @return all target sections
	 * @see TargetDocumentI#getTargetSections()
	 */
	@Override
	public SortedSet<TargetSection> getTargetSections() {
		return Collections.unmodifiableSortedSet(this.internalPerformanceDocument.getTargetSections());
	}

	/**
	 * Returns the flag for generation protocol.
	 * 
	 * @return the generation protocol flag
	 * @see GenerationProtocolI#isGenerationProtocol()
	 */
	public boolean isGenerationProtocol() {
		return this.forceDevelopmentMode || this.getGenerationGroup().isGenerationProtocol()
				|| this.getGenerationGroup().getTargetProject().isGenerationProtocol();
	}

	/**
	 * Getter for a list of writers responsible for this target.
	 * 
	 * @return a list of writer information or an empty list
	 * @see WriterInfoProviderI#getWriterInfo()
	 */
	public final WriterInfoList getWriterInfo() {
		if (this.status.isNew()) {
			throw new JenerateITException("This target was not transformed since its creation");

		} else if (!isGenerationProtocol()) {// this.status.get(FLAG_GENERATION_PROTOCOL)) {
			this.forceDevelopmentMode = true;
			try {
				this.newDocument = getTargetDocument();
				transform();
			} catch (Throwable e) {

//				StringWriter sw = new StringWriter();
//				e.printStackTrace(new PrintWriter(sw));
//				addError(new StringBuffer()
//					.append("=== DEBUG INFO FOR EXCEPTION '")
//					.append(e.getMessage())
//					.append("' ====")
//					.append(StringTools.NEWLINE)
//					.append(sw.toString()).toString(), e);

			}

		}
		return this.newDocument.getWriterInfo();
	}

	/**
	 * Getter for a list of writers responsible for the given area.
	 * 
	 * @param start
	 *            the start position in the content
	 * @param end
	 *            the end position in the content
	 * @return a list of writer information or an empty list
	 * @see WriterInfoProviderI#getWriterInfo(int, int)
	 */
	public WriterInfoList getWriterInfo(int start, int end) {
		final WriterInfoList allWriterInfos = getWriterInfo();
		final WriterInfoList result = new WriterInfoList();
		for (final WriterInfo wi : allWriterInfos.getElements()) {
			if (wi.getStart() < end
					&& wi.getEnd() > start) {
				result.getElements().add(wi);
			}	
		}
		return result;
	}

	/**
	 * Append data to current transformation section in the new target document.
	 * 
	 * @param data the data to append.
	 * @see org.jenerateit.target.TargetI#write(byte[])
	 */
	@Override
	public final void write(byte[] data) {
		write(getCurrentTransformationSection(), data);
	}

	/**
	 * Append data to the given target section in the new target document.
	 * 
	 * @param data the data to append
	 * @param targetSection the target section to write to
	 * @see org.jenerateit.target.TargetI#write(org.jenerateit.target.TargetSection, byte[])
	 */
	@Override
	public final void write(TargetSection targetSection, byte[] data) {
		if (!this.status.isTransforming()) {
			throw new TargetException("Got a write request but this target is not in transformation state. "
					+ "Please do only write into targets that are currently transformed.", this);
		}
		getNewTargetDocument().addData(targetSection, data);
	}
	
	
}

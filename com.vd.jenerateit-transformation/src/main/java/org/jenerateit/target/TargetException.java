/*
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit.target;

import org.jenerateit.exception.JenerateITException;

/**
 * @author hrr
 *
 */
public class TargetException extends JenerateITException {

	/**
	 * serial ID
	 * 
	 */
	private static final long serialVersionUID = -987885885709371271L;
	private final TargetI<? extends TargetDocumentI> target;

	
	public TargetException(final String message, final Throwable cause) {
		this(message, cause, null);
	}

	public TargetException(final String message) {
		this(message, null, null);
	}

	public TargetException(final Throwable cause) {
		this(null, cause, null);
	}

	/**
	 * Constructor
	 * 
	 * @param target the target object the error occured in
	 */
	public TargetException(final TargetI<? extends TargetDocumentI> target) {
		this(null, null, target);
	}

	/**
	 * Constructor
	 * 
	 * @param message the error message
	 * @param target the target object the error occured in
	 */
	public TargetException(final String message, final TargetI<? extends TargetDocumentI> target) {
		this(message, null, target);
	}

	/**
	 * Constructor
	 * 
	 * @param cause the exception caucht inside of the given target 
	 * @param target the target object the error occured in
	 */
	public TargetException(final Throwable cause, final TargetI<? extends TargetDocumentI> target) {
		this(null, cause, target);
	}

	/**
	 * Constructor
	 * 
	 * @param message the error message
	 * @param cause the exception caucht inside of the given target 
	 * @param target the target object the error occured in
	 */
	public TargetException(final String message, final Throwable cause, final TargetI<? extends TargetDocumentI> target) {
		super(message, cause);

		this.target = target;
	}

	/**
	 * Getter for the target this exception occured in
	 * 
	 * @return the target
	 */
	public TargetI<? extends TargetDocumentI> getTarget() {
		return target;
	}

	/**
	 * In case the target is set, it's path will be displayed
	 * 
	 * @see java.lang.Throwable#getMessage()
	 * @return the description
	 */
	@Override
	public String getMessage() {
		final StringBuilder sb = new StringBuilder();
		if (this.target != null && this.target.getTargetPath() != null) {
			sb.append(this.target.getTargetPath().getPath()).append(": ");
		}
//		sb.append(TargetException.class.getName()).append(": ");
		if (super.getMessage() != null) {
			sb.append(super.getMessage());
		}
		return sb.toString();
	}

	
}

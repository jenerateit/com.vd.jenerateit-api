/**
 * Collection of target related classes. 
 *
 * @since 1.0
 */
package org.jenerateit.target;
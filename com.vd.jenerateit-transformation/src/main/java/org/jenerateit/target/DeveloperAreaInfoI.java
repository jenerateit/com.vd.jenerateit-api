/*
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit.target;


/**
 * @author hrr
 *
 * Interface for a developer area within a TargetDocumentI
 */
public interface DeveloperAreaInfoI extends Comparable<DeveloperAreaInfoI> {

    // -----------------------------------------------
    // constants
    /** start tag name of a developer area */
    static public final String DA_START					= "DA-START";
    /** else path of a developer area */
    static public final String DA_ELSE					= "DA-ELSE";
    /** end tag name of a developer area */
    static public final String DA_END					= "DA-END";
    /** sparator flag of a developer area */
    static public final String DA_SEPARATOR				= ":";
    /** regular expressions pattern of the developer area id (former code) */
    static public final String ID_PATTERN				= "[^:\\n\\x0B\\f\\r]+";//"[\\w]+(\\.[\\w]+)*";

    /**
     * 
     * Enumeration for developer area status
     *
     */
    public enum DeveloperAreaStatus {
    	UNLOCKED, 
    	LOCKED
    }
    
	/**
	 * Getter for the end positon within the document content
	 * 
	 * @return the end position within the document content
	 */
	public int getEnd();

	/**
	 * Getter for the start position within the document content
	 * 
	 * @return the start position within the document content
	 */
	public int getStart();

	/**
	 * Getter for the status of this developer area
	 * 
	 * @return the status
	 */
	public DeveloperAreaStatus getStatus();
	
	/**
	 * The complete text of this developer area
	 * 
	 * @return the text
	 */
	public CharSequence getText();
	
	/**
	 * Getter for the id of this developer area
	 * NOTE: The id itself is case insensitive
	 * 
	 * @return the id of this developer area
	 */
	public String getId();
	
	/**
	 * Getter for content of this developer area
	 * 
	 * @return the content of this developer area
	 */
	public CharSequence getUserContent();
}

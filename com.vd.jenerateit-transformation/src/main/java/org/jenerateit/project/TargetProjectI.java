/*
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit.project;

import java.util.List;
import java.util.Set;
import java.util.SortedSet;

import org.jenerateit.GenerationProtocolI;
import org.jenerateit.JenerateITI;
import org.jenerateit.generationgroup.GenerationGroupI;
import org.jenerateit.target.TargetDocumentI;
import org.jenerateit.target.TargetI;
import org.jenerateit.util.DetailsI;
import org.jenerateit.writer.WriterI;


/**
 * Interface of a target project.
 * 
 * @author hrr
 *
 */
public interface TargetProjectI extends Comparable<TargetProjectI>, GenerationProtocolI, DetailsI {
	
	/**
	 * Getter for the JenerateIT runtime.
	 * 
	 * @return the JenerateIT instance
	 */
	JenerateITI getJenerateIT();
	
	/**
	 * Getter for all generation groups.
	 * 
	 * @return all generation groups
	 */
	List<GenerationGroupI> getGenerationGroup();
	
	/**
	 * Getter for a certain generation group with the given name or null if not found.
	 * 
	 * @param name the name of the generation group to look for
	 * @return a generation group or null if not found
	 */
//	GenerationGroupI getGenerationGroup(String name);
	
	/**
	 * Finds a list of {@link TargetI} instances in this target project for the given meta model elements.
	 * 
	 * @param metaModels a set of meta models to look the targets for
	 * @param targetFilter a class file to filter the target instances (null = do not filter)
	 * @param writerFilter a class file to filter the base writer of the target instances (null = do not filter)
	 * @return a list of target objects
	 */
	SortedSet<TargetI<? extends TargetDocumentI>> findTargets(
			Set<? extends Object> metaModels, 
			Class<? extends TargetI<? extends TargetDocumentI>> targetFilter, 
			Class<? extends WriterI> writerFilter);

}

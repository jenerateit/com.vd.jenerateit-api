package org.jenerateit;

/**
 * By convention, all numbers of INFO messages begin with "2".
 * 
 * @author mmt
 *
 */
public enum InfoMessage {
	
	PREVIOUS_DOCUMENT_AVAILABLE ("VD-2001",
			"There is a previous document available, with a size of %s bytes."),
	BYTE_ORDER_MARK_REMOVED ("VD-2002",
			"The previous content for the target file contains a byte order mark (BOM). The BOM got removed. The generated file will not contain a BOM anymore."),
	;
	
	private final String id;
	private final String message;
	private final String instruction;
	
	private InfoMessage(String id, String message, String instruction) {
		this.id = id;
		this.message = message;
		this.instruction = instruction;
	}
	
	private InfoMessage(String id, String message) {
		this.id = id;
		this.message = message;
		this.instruction = null;
	}
	
	public String getId() {
		return id;
	}

	public String getMessage() {
		return message;
	}
	
	public String getMessage(Object...parameters) {
		if (parameters != null) {
			String messageText = String.format(message, parameters);
			return messageText;
		}
		return message;
	}
	
	public String getInstruction() {
		return instruction;
	}
	
	public String getInstruction(Object...parameters) {
		if (parameters != null) {
			String messageText = String.format(instruction, parameters);
			return messageText;
		}
		return instruction;
	}
}
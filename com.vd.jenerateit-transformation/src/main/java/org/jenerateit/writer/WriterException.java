/*
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit.writer;

import org.jenerateit.exception.JenerateITException;
import org.jenerateit.target.TargetI;


/**
 * Exception to report problems within a {@link WriterI} while transform targets.
 *  
 * @author hrr
 *
 */
public class WriterException extends JenerateITException {

	/**
	 * The serial version UID.
	 */
	private static final long serialVersionUID = 625386801219336816L;
	private final TargetI<?> transformationTarget;
	private final WriterI writer;

	
	public WriterException(final String message, final Throwable cause) {
		this(message, cause, null, null);
	}

	public WriterException(final String message) {
		this(message, null, null, null);
	}

	public WriterException(final Throwable cause) {
		this(null, cause, null, null);
	}

	/**
	 * Constructor.
	 * 
	 * @param transformationTarget the transformation target the {@link WriterI} was working on while the error raised
	 * @param writer the writer instance the error raised
	 */
	public WriterException(final TargetI<?> transformationTarget, final WriterI writer) {
		this(null, null, transformationTarget, writer);
	}

	/**
	 * Constructor.
	 * 
	 * @param message the error message
	 * @param transformationTarget the transformation target the {@link WriterI} was working on while the error raised
	 * @param writer the writer instance the error raised
	 */
	public WriterException(final String message, final TargetI<?> transformationTarget, final WriterI writer) {
		this(message, null, transformationTarget, writer);
	}

	/**
	 * Constructor.
	 * 
	 * @param cause the exception caught inside of the given writer 
	 * @param transformationTarget the transformation target the {@link WriterI} was working on while the error raised
	 * @param writer the writer instance the error raised
	 */
	public WriterException(final Throwable cause, final TargetI<?> transformationTarget, final WriterI writer) {
		this(null, cause, transformationTarget, writer);

	}

	/**
	 * Constructor 
	 * 
	 * @param message the error message
	 * @param cause the exception caught inside of the given writer 
	 * @param transformationTarget the transformation target the {@link WriterI} was working on while the error raised
	 * @param writer the writer instance the error raised
	 */
	public WriterException(final String message, final Throwable cause, final TargetI<?> transformationTarget, final WriterI writer) {
		super(message, cause);

		this.transformationTarget = transformationTarget;
		this.writer = writer;
	}

	/**
	 * Getter for the transformation target the writer was working on while the error raised.
	 * 
	 * @return the transformation target
	 */
	public TargetI<?> getTransformationTarget() {
		return transformationTarget;
	}

	/**
	 * Getter for the writer the error raised in
	 * 
	 * @return the writer
	 */
	public WriterI getWriter() {
		return writer;
	}

	/**
	 * Creates a human readable description of this Exception
	 * 
	 * @see java.lang.Throwable#getMessage()
	 * @return the description
	 */
	@Override
	public String getMessage() {
		final StringBuilder sb = new StringBuilder();
		if (this.transformationTarget != null) {
			sb.append(this.transformationTarget.getTargetPath().getPath()).append(": ");
		} else if (this.writer != null && this.writer.getTransformationTarget() != null) {
			sb.append(this.writer.getTransformationTarget().getTargetPath().getPath()).append(": ");
		}
		sb.append(WriterException.class.getName()).append(": ");
		if (super.getMessage() != null) {
			sb.append(super.getMessage());
		}
		return sb.toString();
	}

	
}

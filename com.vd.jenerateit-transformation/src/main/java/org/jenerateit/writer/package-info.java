/**
 * Collection of writer related classes. 
 *
 * @since 1.0
 */
package org.jenerateit.writer;
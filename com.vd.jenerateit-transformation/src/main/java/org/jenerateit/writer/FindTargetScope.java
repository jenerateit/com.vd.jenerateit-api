/*
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */

package org.jenerateit.writer;

import org.jenerateit.generationgroup.GenerationGroupI;
import org.jenerateit.target.TargetI;

/**
 * Scope for finding {@link TargetI} instances.
 * 
 * @author hrr
 *
 */
public enum FindTargetScope {

   	/**
	 * Try to find targets in the entire project (within all {@link GenerationGroupI} belong to this project) 
	 * 
	 */
	PROJECT,
	
	/**
	 * Try to find targets in the {@link GenerationGroupI} this writers target belongs to
	 * 
	 * @see WriterI#getTarget()
	 * @see TargetI#getGenerationGroup()
	 */
	GENERATION_GROUP
}

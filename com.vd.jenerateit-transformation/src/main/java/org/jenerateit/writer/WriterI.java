/*
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit.writer;

import org.jenerateit.target.TargetI;
import org.jenerateit.target.TargetSection;


/**
 * Interface for a Writer. A Writer is responsible to create content that is forwarded to a {@link TargetI} instance.
 * Please use the abstract implementing class {@link AbstractWriter} for writer implementations. 
 * 
 * @author hrr
 * @see AbstractWriter
 */
public interface WriterI {
    
	/**
	 * Starts the transformation for a certain target section.
	 * 
	 * @param ts the target section to transform
	 * @throws WriterException if something went wrong while transforming
	 */
    void transform(TargetSection ts) throws WriterException;
    
    /**
     * Getter for the model element this writer is based on (base element).
     * 
     * @return the model element
     */
    Object getElement();
    
    /**
     * Getter for the target transforming currently.
     * 
     * @return the target.
     */
    TargetI<?> getTransformationTarget();
    
    /**
     * Getter for the target this writer was created in.
     * 
     * @return the target
     */
    TargetI<?> getTarget();
    
    /**
     * Writes data to the end of the current target document section.
     * 
     * @param data the data to append
     * @return a writer instance
     */
    WriterI write(byte[] data);
    
    /**
     * Writes data to the end of the given target document section.
     * 
     * @param section the section to append to
     * @param data the data to append
     * @return a writer instance
     */
    WriterI write(TargetSection section, byte[] data);

}

/*
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit.writer;

import org.jenerateit.target.TargetSection;
import org.jenerateit.target.TextTargetI;

/**
 * Abstract super class of an {@link TextWriterI}.
 * 
 * @author hrr
 *
 */
public abstract class AbstractTextWriter extends AbstractWriter implements TextWriterI {

	/**
	 * Constructor.
	 */
	public AbstractTextWriter() {
		super();
	}
	
	/**
	 * Method to check if the TransformationTarget is of type {@link TextTargetI} and return it.
	 * 
	 * @return the TransformtionTarget as {@link TextTargetI}. 
	 */
	protected TextTargetI<?> getTextTransformationTarget() {
		if (TextTargetI.class.isInstance(getTransformationTarget())) {
			return TextTargetI.class.cast(getTransformationTarget());
		} else {
			throw new WriterException("Got a write call for a " + TextTargetI.class.getName() + 
					" but the TransformationTarget of type '" +
					getTransformationTarget().getClass().getName() + "' (not a TextTargetI instance)", 
					getTransformationTarget(), this);
		}
	}
	
    /**
     * This is a convenience method to have a shortcut to call 
     * <code>getTextTransformationTarget().beginDeveloperArea(String)</code>
     *
     * @param daCode the name of the developer area
     * @see TextTargetI#beginDeveloperArea(String)
     * @return this writer
     */
    public final TextWriterI bDA(String daCode) {
    	getTextTransformationTarget().beginDeveloperArea(daCode);
        return this;
    }
    
    /**
     * This is a convenience method to have a shortcut to call 
     * <code>getTextTransformationTarget().endDeveloperArea()</code>
     * 
     * @see TextTargetI#endDeveloperArea()
     * @return this writer
     */
    public final TextWriterI eDA() {
    	getTextTransformationTarget().endDeveloperArea();
        return this;
    }

    /**
     * Append the character sequences to the transformation target document at the end of the current active target section.
     * 
     * @param text the text to write
	 * @see org.jenerateit.writer.TextWriterI#write(java.lang.CharSequence[])
	 * @see TextTargetI#write(CharSequence...)
	 * @return this writer
	 */
	@Override
	public final TextWriterI write(CharSequence... text) {
		getTextTransformationTarget().write(text);
		return this;
	}

    /**
     * Append the character sequences to the transformation target document at the end of the given target section.
     * 
     * @param text the text to write
     * @param section the target section to write to
	 * @see org.jenerateit.writer.TextWriterI#write(TargetSection, CharSequence...)
	 * @see TextTargetI#write(TargetSection, CharSequence...)
	 * @return this writer
	 */
	@Override
	public final TextWriterI write(TargetSection section, CharSequence... text) {
		getTextTransformationTarget().write(section, text);
		return this;
	}

	/**
	 * Append the character sequences to the transformation target document at the end 
	 * of the current target section, append a newline and indent the new line.
	 * 
	 * @param text the text to append
	 * @return this writer
	 * @see #indent()
	 * @see #wNL(CharSequence...)
	 */
	public final AbstractTextWriter wNLI(CharSequence... text) {
        return wNL(text).indent();
    }
    
	/**
	 * Append the character sequences to the transformation target document at the end 
	 * of the given target section, append a newline and indent the new line.
	 * 
	 * @param text the text to append
	 * @param section the target section to append to
	 * @return this writer
	 * @see #indent(TargetSection)
	 * @see #wNL(TargetSection, CharSequence...)
	 */
    public final AbstractTextWriter wNLI(TargetSection section, CharSequence... text) {
        return wNL(section, text).indent(section);
    }
    
	/**
	 * Append the character sequences to the transformation target document at the end 
	 * of the current target section, append a newline and outdent the new line.
	 * 
	 * @param text the text to append
	 * @return this writer
	 * @see #outdent()
	 * @see #wNL(CharSequence...)
	 */
    public final AbstractTextWriter wNLO(CharSequence... text) {
        return wNL(text).outdent();
    }
    
	/**
	 * Append the character sequences to the transformation target document at the end 
	 * of the given target section, append a newline and outdent the new line.
	 * 
	 * @param text the text to append
	 * @param section the target section to append to
	 * @return this writer
	 * @see #outdent(TargetSection)
	 * @see #wNL(TargetSection, CharSequence...)
	 */
    public final AbstractTextWriter wNLO(TargetSection section, CharSequence... text) {
        return wNL(section, text).outdent(section);
    }
    
	/**
	 * Indent the current line, append the character sequences to the transformation target document at the end 
	 * of the current target section and append a newline.
	 * 
	 * @param text the text to append
	 * @return this writer
	 * @see #indent()
	 * @see #wNL(CharSequence...)
	 */
    public final AbstractTextWriter wINL(CharSequence... text) {
    	return indent().wNL(text);
    }
    
	/**
	 * Indent the current line, append the character sequences to the transformation target document at the end 
	 * of the given target section and append a newline.
	 * 
	 * @param text the text to append
	 * @param section the target section to append to
	 * @return this writer
	 * @see #indent(TargetSection)
	 * @see #wNL(TargetSection, CharSequence...)
	 */
    public final AbstractTextWriter wINL(TargetSection section, CharSequence... text) {
    	return indent(section).wNL(section, text);
    }
    
	/**
	 * Outdent the current line, append the character sequences to the transformation target document at the end 
	 * of the current target section and append a newline.
	 * 
	 * @param text the text to append
	 * @return this writer
	 * @see #outdent()
	 * @see #wNL(CharSequence...)
	 */
    public final AbstractTextWriter wONL(CharSequence... text) {
    	return outdent().wNL(text);
    }
    
	/**
	 * Outdent the current line, append the character sequences to the transformation target document at the end 
	 * of the given target section and append a newline.
	 * 
	 * @param text the text to append
	 * @param section the target section to append to
	 * @return this writer
	 * @see #outdent(TargetSection)
	 * @see #wNL(TargetSection, CharSequence...)
	 */
    public final AbstractTextWriter wONL(TargetSection section, CharSequence... text) {
    	return outdent(section).wNL(section, text);
    }
    
	/**
	 * Append the character sequences to the transformation target document at the end 
	 * of the current target section and append a newline.
	 * 
	 * @param text the text to append
	 * @return this writer
	 * @see #w(CharSequence...)
	 * @see #wNL()
	 */
    public final AbstractTextWriter wNL(CharSequence... text) {
        return w(text).wNL();
    }
    
	/**
	 * Append the character sequences to the transformation target document at the end 
	 * of the given target section and append a newline.
	 * 
	 * @param text the text to append
	 * @param section the target section to append to
	 * @return this writer
	 * @see #w(TargetSection, CharSequence...)
	 * @see #wNL(TargetSection)
	 */
    public final AbstractTextWriter wNL(TargetSection section, CharSequence... text) {
        return w(section, text).wNL(section);
    }
    
	/**
	 * Append the character sequences to the transformation target document at the end 
	 * of the current target section.
	 * 
	 * @param text the text to append
	 * @return this writer
	 * @see #write(CharSequence...)
	 */
   public final AbstractTextWriter w(CharSequence... text) {
        return (AbstractTextWriter)write(text);
    }
    
	/**
	 * Append the character sequences to the transformation target document at the end 
	 * of the given target section.
	 * 
	 * @param text the text to append
	 * @param section the target section to append to
	 * @return this writer
	 * @see #write(TargetSection, CharSequence...)
	 */
    public final AbstractTextWriter w(TargetSection section, CharSequence... text) {
        return (AbstractTextWriter)write(section, text);
    }
    
	/**
	 * Append number newline(s) to the transformation target document at the end 
	 * of the current target section.
	 * 
	 * @param number the amount of newlines to add
	 * @return this writer
	 * @see #wNL()
	 */
    public final AbstractTextWriter wNL(int number) {
        for (int ii=0; ii<number; ii++) {
            wNL();
        }
        return this;
    }
    
	/**
	 * Append number newline(s) to the transformation target document at the end 
	 * of the current target section.
	 * 
	 * @param number the amount of newlines to add
	 * @param section the target section to append to
	 * @return this writer
	 * @see #wNL(TargetSection)
	 */
    public final AbstractTextWriter wNL(TargetSection section, int number) {
        for (int ii=0; ii<number; ii++) {
            wNL(section);
        }
        return this;
    }
    
	/**
	 * Append a newline to the transformation target document at the end 
	 * of the current target section.
	 * 
	 * @return this writer
	 * @see TextTargetI#writeNL()
	 */
    public final AbstractTextWriter wNL() {
    	getTextTransformationTarget().writeNL();
        return this;
    }
    
	/**
	 * Append a newline to the transformation target document at the end 
	 * of the given target section.
	 * 
	 * @param section the target section to append to
	 * @return this writer
	 * @see TextTargetI#writeNL(TargetSection)
	 */
    public final AbstractTextWriter wNL(TargetSection section) {
    	getTextTransformationTarget().writeNL(section);
        return this;
    }
    
    /**************************************************************************
     * Tabbing
     *************************************************************************/
    private static final int TAB_SIZE = 4;
    
    /**
     * Returns the size of spaces an {@link #outdent()} or {@link #indent()} uses.
     * Clients may overwrite this method. The default is a tab size of 4.
     * 
     * @return the number of spaces represent one indent or outdent
     */
    protected int getTabSize() {
    	return TAB_SIZE;
    }
    
    /**
     * Increments the indentation by the {@link #getTabSize()} number of blanks.
     *
     * @return this writer
     * @see #indent(int)
     */
    public final AbstractTextWriter indent() {
    	return indent(getTabSize());
    }
    
    /**
     * Increments the indentation by the {@link #getTabSize()} number of blanks.
     *
     * @param section the target document section to modify its indent size
     * @return this writer
     * @see #indent(TargetSection, int)
     */
    public final AbstractTextWriter indent(TargetSection section) {
    	return indent(section, getTabSize());
    }
    
   /**
     * This is a convenience method to have a shortcut to call 
     * <code>getTextTransformationTarget().incrIndent(int)</code>.
     * 
     * @param numberOfBlanks This can be a negative or positive integer value, it specifies the
     *        number of leading blanks before any write call writes to the target file.
     * @return this writer
     * @see #getTextTransformationTarget()
     * @see TextTargetI#incrIndent(int)
     */
    public final AbstractTextWriter indent(int numberOfBlanks) {
    	getTextTransformationTarget().incrIndent(numberOfBlanks);
    	return this;
    }
    
    /**
     * This is a convenience method to have a shortcut to call 
     * <code>getTC().getTarget().incrIndent(int)</code>.
     * 
     * @param numberOfBlanks This can be a negative or positive integer value, it specifies the
     *        number of leading blanks before any write call writes to the target file.
     * @param section the target document section to modify its indent size
     * @return this writer
     * @see #getTextTransformationTarget()
     * @see TextTargetI#incrIndent(TargetSection, int)
     */
    public final AbstractTextWriter indent(TargetSection section, int numberOfBlanks) {
    	getTextTransformationTarget().incrIndent(section, numberOfBlanks);
    	return this;
    }
    
    /**
     * This is a convenience method to have a shortcut to call 
     * <code>getTextTransformationTarget().decrIndent(int)</code>.
     * 
     * @param numberOfBlanks This can be a negative or positive integer value, it specifies the
     *        number of leading blanks before any write call writes to the target file.
     * @return this writer
     * @see #getTextTransformationTarget()
     * @see TextTargetI#decrIndent(int)
     */
    public final AbstractTextWriter outdent(int numberOfBlanks) {
    	getTextTransformationTarget().decrIndent(numberOfBlanks);
    	return this;
    }
    
    /**
     * This is a convenience method to have a shortcut to call 
     * <code>getTC().getTarget().decrIndent(int)</code>.
     * 
     * @param numberOfBlanks This can be a negative or positive integer value, it specifies the
     *        number of leading blanks before any write call writes to the target file.
     * @param section the target document section to modify its indent size
     * @return this writer
     * @see #getTextTransformationTarget()
     * @see TextTargetI#decrIndent(TargetSection, int)
     */
    public final AbstractTextWriter outdent(TargetSection section, int numberOfBlanks) {
    	getTextTransformationTarget().decrIndent(section, numberOfBlanks);
    	return this;
    }
    
    /**
     * Decrements the indentation by the {@link #getTabSize()} number of blanks.
     * 
     * @return this writer
     * @see #outdent(int)
     */
    public final AbstractTextWriter outdent() {
    	return outdent(getTabSize());
    }
    
    /**
     * Decrements the indentation by the {@link #getTabSize()} number of blanks.
     *
     * @param section the target document section to modify its indent size
     * @return this writer
     * @see #outdent(TargetSection, int)
     */
    public final AbstractTextWriter outdent(TargetSection section) {
    	return outdent(section, getTabSize());
    }
    
}

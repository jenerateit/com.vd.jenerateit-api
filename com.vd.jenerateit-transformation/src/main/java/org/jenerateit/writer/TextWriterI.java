/*
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit.writer;

import org.jenerateit.target.TargetSection;

/**
 * Interface extending {@link WriterI} for text based writers.
 * 
 * @author hrr
 *
 */
public interface TextWriterI extends WriterI {

    /**
     * Starts a developer area with the given name.
     *
     * @param daCode the developer area code
     * @return a writer instance
     */
	TextWriterI bDA(String daCode);
    
    /**
     * End the current active developer area.
     * 
     * @return a writer instance
     */
	TextWriterI eDA();

    /**
     * Append a text to the end of the current target section.
     * 
     * @param text the text to write
     * @return a writer instance
     */
	TextWriterI write(CharSequence... text);
    
    /**
     * Append a text to the end of the given target section.
     * 
     * @param text the text to write
     * @param section the section to write to
     * @return a writer instance
     */
	TextWriterI write(TargetSection section, CharSequence... text);

}

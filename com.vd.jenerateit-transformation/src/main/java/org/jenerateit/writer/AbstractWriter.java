/*
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit.writer;


import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.SortedSet;

import org.jenerateit.annotation.CreationTarget;
import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.AbstractTarget;
import org.jenerateit.target.TargetDocumentI;
import org.jenerateit.target.TargetI;
import org.jenerateit.target.TargetSection;
import org.jenerateit.transformation.TransformationTarget;


/**
 * Base class for all {@link WriterI} implementations.
 * 
 * @author hrr
 *
 */
public abstract class AbstractWriter implements WriterI {
    
	@ModelElement
    private Object element;
	@CreationTarget
    private TargetI<?> target;
	
    private final TransformationTarget currentTarget = new TransformationTarget();
    
    /**
     * Constructor.
     */
    public AbstractWriter() {
    	super();
    }

    /**
     * Getter for the target that gets transformed at the moment.
     * 
     * @return the target transformed currently 
	 * @see org.jenerateit.writer.WriterI#getTransformationTarget()
	 */
	@Override
	public TargetI<?> getTransformationTarget() {
		return currentTarget.getCurrentTarget();
	}

    /**
     * Getter for the model element this Writer is based on
     * 
     * @return the model element
     */
    public final Object getElement() {
        return this.element;
    }

    /**
     * Getter for the {@link TargetI} this {@link WriterI} was created in.
     * 
     * @return a target instance
     */
    public final TargetI<?> getTarget() {
        return target;
    }

    /**
     * Finder to search a single target.
     * 
     * 
     * @param findScope the scope to search in
     * @param metaModel the model element to search a target for
     * @param targetFilter a filter for target class
     * @param writerFilter a filter for the base writer class
     * @return a single target or null if not found
     * @throws WriterException if more than one target was found
     */
    protected final TargetI<? extends TargetDocumentI> findTarget(
    		FindTargetScope findScope,
    		Object metaModel, 
    		Class<? extends TargetI<?>> targetFilter, 
    		Class<? extends WriterI> writerFilter) throws WriterException {
    	SortedSet<TargetI<? extends TargetDocumentI>> result = findTargets(findScope, metaModel, targetFilter, writerFilter);
    	if (result == null || result.isEmpty()) {
    		return null;
    	} else if (result.size() == 1) {
    		return result.first();
    	} else {
    		throw new WriterException("Found more than one targets (" + result.size() + ")", getTransformationTarget(), this);
    	}
    }
    
    /**
     * Finder to search targets for a single model element.
     * 
     * @param findScope the scope to search in
     * @param metaModel the model element to search targets for
     * @param targetFilter a filter for target class
     * @param writerFilter a filter for the base writer class
     * @return a sorted set of targets
     */
    protected final SortedSet<TargetI<? extends TargetDocumentI>> findTargets(
    		FindTargetScope findScope,
    		Object metaModel, 
    		Class<? extends TargetI<?>> targetFilter, 
    		Class<? extends WriterI> writerFilter) {
    	return findTargets(findScope, (Set<Object>)new HashSet<Object>(Arrays.asList(new Object[] {metaModel})), targetFilter, writerFilter);
    }
    
    /**
     * Try to find {@link TargetI} instances for the given model elements and the filters set
     *    
     * @param findScope the scope to search in
     * @param metaModels the models to look for the related {@link TargetI} instances
     * @param targetFilter a filter for target class
     * @param writerFilter a filter for the base writer class
     * @return a list of targets
     */
    protected final SortedSet<TargetI<? extends TargetDocumentI>> findTargets(
    		FindTargetScope findScope,
    		Set<? extends Object> metaModels, 
    		Class<? extends TargetI<?>> targetFilter, 
    		Class<? extends WriterI> writerFilter) {
    	
     	if (!getTransformationTarget().getTargetInfo().getStatus().isTransforming()) {
    		throw new WriterException("The method findTargets(FindTargetScope, Set<? extends ElementI>, Class<? extends TargetI>, Class<? extends WriterI>) is only available during generation", 
    				getTransformationTarget(),
    				this);
    	}
    	
    	SortedSet<TargetI<? extends TargetDocumentI>> result = null;
    	long duration = System.currentTimeMillis();
    	
    	if (findScope == FindTargetScope.GENERATION_GROUP) {
    		result = getTarget().getGenerationGroup().findTargets(metaModels, targetFilter, writerFilter);
    		
    	} else if (findScope == FindTargetScope.PROJECT) {
    		result = getTarget().getGenerationGroup().getTargetProject().findTargets(metaModels, targetFilter, writerFilter);
    		
    	} else {
    		throw new WriterException("The FindTargetScope '" + findScope + "' is unknown",
    				getTransformationTarget(),
    				this);
    	}
    	
    	// update the statistics
    	duration = System.currentTimeMillis() - duration;
    	if (AbstractTarget.class.isInstance(getTransformationTarget())) {
    		AbstractTarget.class.cast(getTransformationTarget()).updateDuration(duration);
    	}
    	return result;
    }
    
    // -------------------------------------------------------------------------------
    // Convenience methods for writing to a target file
    
    
    /**
     * Append data to the end of the current target document section.
     * 
     * @param data the data to append
     * @return this writer
	 * @see org.jenerateit.writer.WriterI#write(byte[])
	 * @see #getTransformationTarget()
	 * @see TargetI#write(byte[])
	 */
	@Override
	public final WriterI write(byte[] data) {
		getTransformationTarget().write(data);
		return this;
	}

    /**
     * Append data to the end of the given target document section.
     * 
     * @param data the data to append
     * @param section the target document section to append to
     * @return this writer
	 * @see WriterI#write(TargetSection, byte[])
	 * @see #getTransformationTarget()
	 * @see TargetI#write(TargetSection, byte[])
	 */
	@Override
	public final WriterI write(TargetSection section, byte[] data) {
		getTransformationTarget().write(section, data);
		return this;
	}

}
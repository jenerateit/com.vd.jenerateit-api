/*
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit.writer;

import com.vd.transformation.data.WriterInfo;
import com.vd.transformation.data.WriterInfoList;

/**
 * This interface to access the {@link WriterInfo} of a target.
 * 
 * @author hrr
 *
 */
public interface WriterInfoProviderI {

	/**
	 * Getter for all writers involved to write the underlying target.
	 * 
	 * @return a list of writer information or an empty list
	 */
	WriterInfoList getWriterInfo();
	
	/**
	 * Getter for all writers involved to write the underlying target area.
	 * 
	 * @param start the start position in the content
	 * @param end the end position in the content
	 * @return a list of writer information or an empty list
	 */
	WriterInfoList getWriterInfo(int start, int end);
	
}

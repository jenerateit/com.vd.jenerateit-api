/*
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit.annotation;

import java.lang.annotation.Annotation;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.jenerateit.generationgroup.GenerationGroupI;
import org.jenerateit.target.TargetI;

/**
 * Annotation to mark a field inside a {@link TargetI} as a {@link GenerationGroupI generation group} variable.
 * JenerateIT will set the generation group instance within a {@link TargetI} for all variables marked 
 * with this annotation.
 * 
 * <p>
 * This {@link Annotation} can be used as follow:
 * </p>
 * 
 * <pre><code>
 * 
 * public class MyTarget extends AbstractTextTarget {
 *     
 *     &#64;GenerationGroup
 *     private MyGenerationGroup theGenerationGroupIBelongTo;
 *     
 *     public void init() {
 *         // do your init here
 *         
 *         super.init();
 *     }
 *     
 *     ....
 * }
 * </code></pre>
 * 
 *  
 * @author hrr
 *
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface GenerationGroup {

}

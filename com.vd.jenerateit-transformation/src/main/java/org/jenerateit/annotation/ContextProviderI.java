/*
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit.annotation;

import java.io.Serializable;

import org.jenerateit.exception.JenerateITException;
import org.jenerateit.target.ElementNotSupportedException;
import org.jenerateit.target.TargetDocumentI;
import org.jenerateit.target.TargetI;


/**
 * Interface for context provider implementations. 
 * A context provider calculates the context instances for a {@link TargetI} based on the target class and the base element.
 * 
 * <p>
 * Following implementation shows a ContextProvider for languages support
 * </p>
 * 
 * <pre><code>
 * public Serializable[] getAllContext(Class&lt;? extends TargetI&gt; clazz, Serializable element) throws ElementNotSupportedException {
 *     return new String[] {"en", "de", "es", "fr"};
 * }
 * </code></pre>
 * 
 * <p>
 * or by use of the given model element...
 * </p>
 * 
 * <pre><code>
 * public Serializable[] getAllContext(Class&lt;? extends TargetI&gt; clazz, Serializable element) 
 *   throws ElementNotSupportedException, JenerateITException {
 * 	   if (Application.class.isAssignableFrom(element.getClass())) {
 *         Application a = (Application) element;
 *         return a.getSupportedLanguages();
 *     }
 *     throw new ElementNotSupportedException("This element is not supported", element);
 * }
 * </code></pre>
 * 
 * <p>
 * or by use of the Target class name...
 * </p>
 * 
 * <pre><code>
 * public Serializable[] getAllContext(Class&lt;? extends TargetI&gt; clazz, Serializable element) 
 *   throws ElementNotSupportedException, JenerateITException {
 * 	   if (LanguageProperiesTarget.class.isAssignableFrom(clazz)) {
 *         return new String[] {"en", "de", "es", "fr"};
 *     }
 *     throw new JenerateITException("This target class '" + clazz + "' is not supported");
 * }
 * </code></pre>
 * 
 * 
 * @author hrr
 *
 */
public interface ContextProviderI {

    /**
     * Returns all possible Context objects or null if not available.
     * Target needed a context must have an annotation {@link Context} providing a class implementing this interface. 
     * 
     * @param clazz the target class to prepare the context objects for
     * @param element the element {@link TargetI} object is based on with this interface is annotated.
     * @return all possible context objects.
     * @throws ElementNotSupportedException if the given element does not fit into this context provider.
     * @throws JenerateITException if the context can not be provided for the given parameter 
     * <b>Please note:</b> If this exception is thrown, the work on this target with this element is stopped.
     */
    public Serializable[] getAllContext(Class<? extends TargetI<? extends TargetDocumentI>> clazz, Object element) 
    	throws ElementNotSupportedException, JenerateITException;
}

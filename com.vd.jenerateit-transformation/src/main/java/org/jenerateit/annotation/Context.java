/*
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit.annotation;

import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.jenerateit.exception.JenerateITException;
import org.jenerateit.target.ElementNotSupportedException;
import org.jenerateit.target.TargetDocumentI;
import org.jenerateit.target.TargetI;


/**
 * Annotation to provide context information for a {@link TargetI} object.
 *
 * <p>
 * This {@link Annotation} can be used as follow:
 * </p>
 * <ul>
 * <li>
 * Use a context provider class to provide context objects based on class names or model elements
 * 
 * <pre><code>
 * 
 * public class MyTargetContextProvider implements ContextProviderI {
 *     public Serializable[] getAllContext(Class&lt;? extends TargetI&gt; clazz, Serializable element) throws ElementNotSupportedException {
 *         if (GUIElement.class.isInstance(element)) {
 *             return new String[] {((GUIElement)element).getLanguages()};
 *         }
 *         return null;
 *     }
 * }
 * 
 * &#64;Context(provider=MyTargetContextProvider.class, contexts=null)
 * public class MyTarget extends AbstractTarget {
 *     
 *     protected MyTarget() {
 *         super();
 *     }
 *     
 *     ....
 * }
 * </code></pre>
 * 
 * </li>
 * <li>
 * Use simple string objects for context objects
 * 
 * <pre><code>
 * 
 * &#64;Context(contexts={"de", "en", "es"}, provider=null)
 * public class MyTarget extends AbstractTarget {
 *     
 *     protected MyTarget() {
 *         super();
 *     }
 *     
 *     ....
 * }
 * </code></pre>
 * 
 * </li>
 * </ul>
 * 
 * @author hrr
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Context {
	
	static final String defaultContextString = "jenereateit_default_context_18012007"; 
	class DefaultInternalContextProvider implements ContextProviderI {

		public Serializable[] getAllContext(Class<? extends TargetI<? extends TargetDocumentI>> clazz,
				Object element) throws ElementNotSupportedException,
				JenerateITException {
			return null;
		}
		
	};
	
	/**
	 * Returns the implementing class of the context provider implementation.
	 * 
	 * @return the class name of the context provider
	 */
	Class<? extends ContextProviderI> provider() default DefaultInternalContextProvider.class;

	/**
	 * Returns an array of string contexts.
	 * 
	 * @return string contexts
	 */
	String[] contexts() default {defaultContextString};
}

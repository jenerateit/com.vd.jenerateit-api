/*
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit;




/**
 * Interface for the JenerateIT runtime.
 * 
 * @author hrr
 */
public interface JenerateITI {

	
	/**
     * Getter for the development mode
     * 
     * @return true if JenerateIT is in development mode
     */
    boolean isDevelopmentMode();
    
}

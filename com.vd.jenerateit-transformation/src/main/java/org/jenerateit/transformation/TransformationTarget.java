/*
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit.transformation;

import org.jenerateit.target.TargetI;

/**
 * {@link ThreadLocal} variable to keep the current transforming {@link TargetI}.
 * 
 * @author hrr
 */
public class TransformationTarget {

	private static final ThreadLocal<TargetI<?>> currentTarget = new ThreadLocal<TargetI<?>>();
	
	/**
	 * Set the current transforming {@link TargetI}.
	 * 
	 * @param target the target to set
	 */
	public void setCurrentTarget(TargetI<?> target) {
		currentTarget.set(target);
	}
	
	/**
	 * Getter to request the current transforming {@link TargetI}.
	 * 
	 * @return the current transforming target
	 */
	public TargetI<?> getCurrentTarget() {
		return currentTarget.get();
	}
}

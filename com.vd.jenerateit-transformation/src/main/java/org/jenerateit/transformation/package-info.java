/**
 * Collection of classes used while transforming.
 *
 * @since 4.0
 */
package org.jenerateit.transformation;
/*
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit;




/**
 * Interface to control the generation protocol of JenerateIT.
 * 
 * If method {@link #isGenerationProtocol()} returns true JenerateIT has to create
 * {@link WriterInfo} information while transform if possible. 
 * 
 * @author hrr
 *
 */
public interface GenerationProtocolI {

	/**
     * Returns the flag for generation protocol.
     * 
     * @return the generation protocol flag
     */
    public boolean isGenerationProtocol();
}

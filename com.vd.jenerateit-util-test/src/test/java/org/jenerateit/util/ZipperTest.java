/**
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit.util;

import org.junit.Test;


/**
 * @author mmt
 *
 */
public class ZipperTest {

	public ZipperTest() {}
	
	@Test
	public void testNoFile() {
		Zipper zipper = new Zipper(this.getClass().getClassLoader());
		assert(zipper.getInputStream() != null);
	}
	
	@Test
	public void testSingleExistingFile() {
		Zipper zipper = new Zipper(this.getClass().getClassLoader(), "ZipperTest.properties");
		assert(zipper.getInputStream() != null);
	}
	
	@Test
	public void testMultipleExistingFile() {
		Zipper zipper = new Zipper(this.getClass().getClassLoader(), "ZipperTest.properties", "ZipperTest2.properties");
		assert(zipper.getInputStream() != null);
	}
}

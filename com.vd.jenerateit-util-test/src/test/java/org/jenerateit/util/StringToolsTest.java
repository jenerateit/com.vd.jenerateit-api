/**
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit.util;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;


/**
 * @author hrr
 *
 */
public class StringToolsTest {

	public StringToolsTest() {
	}
	
	@Test
	public void isNotEmpty() {
		assert StringTools.isNotEmpty("not empty") : "'not empty' is not empty";
		assert StringTools.isNotEmpty("e") : "'e' is not empty";
		assert StringTools.isNotEmpty(" e ") : "' e ' is not empty";
		assert StringTools.isNotEmpty("	e	") : "'	e	' is not empty";
		assert StringTools.isNotEmpty("   ") : "'   ' is not empty";
	}
	
	@Test
	public void isEmpty() {
		assert StringTools.isEmpty(null) : "null is empty";
		assert StringTools.isEmpty("") : "'' is empty";
		assert !StringTools.isEmpty(" ") : "' ' is not empty";
		assert !StringTools.isEmpty("	") : "'	' is not empty";
	}

	@Test
	public void isText() {
		assert !StringTools.isText(null) : "null is not a text";
		assert !StringTools.isText("") : "'' is not a text";
		assert !StringTools.isText(" ") : "' ' is not a text";
		assert !StringTools.isText("	") : "'\\t' is not a text";
		assert StringTools.isText("abc") : "'abc' is a text";
		assert StringTools.isText("   abc") : "'   abc' is a text";
		assert StringTools.isText("abc   ") : "'abc   ' is a text";
		assert StringTools.isText("   abc   ") : "'   abc   ' is a text";
	}

	@Test
	public void firstUpperCase() {
		assert StringTools.firstUpperCase(null) == null : "null string gets converted into something";
		assert "".equals(StringTools.firstUpperCase("")) : "empty string gets converted into something";
		assert "A".equals(StringTools.firstUpperCase("a")) : "string 'a' gets converted into something";
		assert "A".equals(StringTools.firstUpperCase("A")) : "string 'A' gets converted into something";
		assert "Abc".equals(StringTools.firstUpperCase("abc")) : "string 'Abc' gets converted into something";
		assert "Abc".equals(StringTools.firstUpperCase("Abc")) : "string 'Abc' gets converted into something";
		assert "ABC".equals(StringTools.firstUpperCase("ABC")) : "string 'ABC' gets converted into something";
		assert "1abc".equals(StringTools.firstUpperCase("1abc")) : "string '1abc' gets converted into something";
		assert "1ABC".equals(StringTools.firstUpperCase("1ABC")) : "string '1ABC' gets converted into something";
		assert "1".equals(StringTools.firstUpperCase("1")) : "string '1' gets converted into something";
	}

	@Test
	public void firstLowerCase() {
		assert StringTools.firstLowerCase(null) == null : "null string gets converted into something";
		assert "".equals(StringTools.firstLowerCase("")) : "empty string gets converted into something";
		assert "a".equals(StringTools.firstLowerCase("a")) : "string 'a' gets converted into something";
		assert "a".equals(StringTools.firstLowerCase("A")) : "string 'A' gets converted into something";
		assert "abc".equals(StringTools.firstLowerCase("abc")) : "string 'Abc' gets converted into something";
		assert "abc".equals(StringTools.firstLowerCase("Abc")) : "string 'Abc' gets converted into something";
		assert "aBC".equals(StringTools.firstLowerCase("ABC")) : "string 'ABC' gets converted into something";
		assert "1abc".equals(StringTools.firstLowerCase("1abc")) : "string '1abc' gets converted into something";
		assert "1ABC".equals(StringTools.firstLowerCase("1ABC")) : "string '1ABC' gets converted into something";
		assert "1".equals(StringTools.firstLowerCase("1")) : "string '1' gets converted into something";
	}
	
	@Test
	public void getLinesNull() {
		String[] result = StringTools.getLines(null);
		Assert.assertNotNull(result);
		Assert.assertEquals("The result should be empty", 0, result.length);
	}

	@Test
	public void getLinesEmpty() {
		String[] result = StringTools.getLines("");
		Assert.assertNotNull(result);
		Assert.assertEquals("Can not find a single line in the result", 0, result.length);
		Assert.assertEquals("The result should be empty", 0, result.length);
	}

	@Test
	public void getLinesSpaces() {
		String[] result = StringTools.getLines("    ");
		Assert.assertNotNull(result);
		Assert.assertEquals("Can not find a single line in the result", 1, result.length);
		Assert.assertEquals("The single line does not match", "    ", result[0]);
	}
	
	@Test
	public void getLinesCharSequencesSingleLineOneChar() {
		final StringBuffer sb = new StringBuffer();
		sb.append("1");
		List<CharSequence> result = StringTools.getLines(sb);
		Assert.assertNotNull(result);
		Assert.assertEquals(1, result.size());
		Assert.assertEquals("1", result.get(0));
	}
	@Test
	public void getLinesCharSequencesOneChar() {
		final StringBuffer sb = new StringBuffer();
		sb.append("1").append('\n');
		sb.append("2").append('\r');
		sb.append("3").append('\r').append('\n');
		sb.append("4").append('\n').append('\r');
		sb.append("5");
		List<CharSequence> result = StringTools.getLines(sb);
		Assert.assertNotNull(result);
		Assert.assertEquals(5, result.size());
		Assert.assertEquals("1", result.get(0));
	}
	@Test
	public void getLinesCharSequences() {
		final StringBuffer sb = new StringBuffer();
		sb.append("line1").append('\n');
		sb.append("line2").append('\r');
		sb.append("line3").append('\r').append('\n');
		sb.append("line4").append('\n').append('\r');
		sb.append("line5");
		List<CharSequence> result = StringTools.getLines(sb);
		Assert.assertNotNull(result);
		Assert.assertEquals(5, result.size());
		Assert.assertEquals("line1", result.get(0));
		Assert.assertEquals("line2", result.get(1));
		Assert.assertEquals("line3", result.get(2));
		Assert.assertEquals("line4", result.get(3));
		Assert.assertEquals("line5", result.get(4));
	}
	
	@Test
	public void getLinesCharSequencesEndingLN() {
		final StringBuffer sb = new StringBuffer();
		sb.append("line1").append('\n');
		sb.append("line2").append('\r');
		sb.append("line3").append('\r').append('\n');
		sb.append("line4").append('\n').append('\r');
		sb.append("line5").append('\n');
		List<CharSequence> result = StringTools.getLines(sb);
		Assert.assertNotNull(result);
		Assert.assertEquals(5, result.size());
		Assert.assertEquals("line1", result.get(0));
		Assert.assertEquals("line2", result.get(1));
		Assert.assertEquals("line3", result.get(2));
		Assert.assertEquals("line4", result.get(3));
		Assert.assertEquals("line5", result.get(4));
	}

	@Test
	public void getLinesCharSequencesEndingCR() {
		final StringBuffer sb = new StringBuffer();
		sb.append("line1").append('\n');
		sb.append("line2").append('\r');
		sb.append("line3").append('\r').append('\n');
		sb.append("line4").append('\n').append('\r');
		sb.append("line5").append('\r');
		List<CharSequence> result = StringTools.getLines(sb);
		Assert.assertNotNull(result);
		Assert.assertEquals(5, result.size());
		Assert.assertEquals("line1", result.get(0));
		Assert.assertEquals("line2", result.get(1));
		Assert.assertEquals("line3", result.get(2));
		Assert.assertEquals("line4", result.get(3));
		Assert.assertEquals("line5", result.get(4));
	}

	@Test
	public void getLinesCharSequencesEndingLNCR() {
		final StringBuffer sb = new StringBuffer();
		sb.append("line1").append('\n');
		sb.append("line2").append('\r');
		sb.append("line3").append('\r').append('\n');
		sb.append("line4").append('\n').append('\r');
		sb.append("line5").append('\n').append('\r');
		List<CharSequence> result = StringTools.getLines(sb);
		Assert.assertNotNull(result);
		Assert.assertEquals(5, result.size());
		Assert.assertEquals("line1", result.get(0));
		Assert.assertEquals("line2", result.get(1));
		Assert.assertEquals("line3", result.get(2));
		Assert.assertEquals("line4", result.get(3));
		Assert.assertEquals("line5", result.get(4));
	}

	@Test
	public void getLinesCharSequencesEndingCRLN() {
		final StringBuffer sb = new StringBuffer();
		sb.append("line1").append('\n');
		sb.append("line2").append('\r');
		sb.append("line3").append('\r').append('\n');
		sb.append("line4").append('\n').append('\r');
		sb.append("line5").append('\r').append('\n');
		List<CharSequence> result = StringTools.getLines(sb);
		Assert.assertNotNull(result);
		Assert.assertEquals(5, result.size());
		Assert.assertEquals("line1", result.get(0));
		Assert.assertEquals("line2", result.get(1));
		Assert.assertEquals("line3", result.get(2));
		Assert.assertEquals("line4", result.get(3));
		Assert.assertEquals("line5", result.get(4));
	}

	@Test
	public void getLinesCharSequencesBeginningEndingLN() {
		final StringBuffer sb = new StringBuffer();
		sb.append('\n').append("line1").append('\n');
		sb.append("line2").append('\r');
		sb.append("line3").append('\r').append('\n');
		sb.append("line4").append('\n').append('\r');
		sb.append("line5").append('\n');
		List<CharSequence> result = StringTools.getLines(sb);
		Assert.assertNotNull(result);
		Assert.assertEquals(6, result.size());
	}

	@Test
	public void getLinesCharSequencesBeginningEndingCR() {
		final StringBuffer sb = new StringBuffer();
		sb.append('\r').append("line1").append('\n');
		sb.append("line2").append('\r');
		sb.append("line3").append('\r').append('\n');
		sb.append("line4").append('\n').append('\r');
		sb.append("line5").append('\r');
		List<CharSequence> result = StringTools.getLines(sb);
		Assert.assertNotNull(result);
		Assert.assertEquals(6, result.size());
	}

	@Test
	public void getLinesCharSequencesBeginningEndingCRLN() {
		final StringBuffer sb = new StringBuffer();
		sb.append('\r').append('\n').append("line1").append('\n');
		sb.append("line2").append('\r');
		sb.append("line3").append('\r').append('\n');
		sb.append("line4").append('\n').append('\r');
		sb.append("line5").append('\r').append('\n');
		List<CharSequence> result = StringTools.getLines(sb);
		Assert.assertNotNull(result);
		Assert.assertEquals(6, result.size());
	}

	@Test
	public void getLinesCharSequencesBeginningEndingLNCR() {
		final StringBuffer sb = new StringBuffer();
		sb.append('\n').append('\r').append("line1").append('\n');
		sb.append("line2").append('\r');
		sb.append("line3").append('\r').append('\n');
		sb.append("line4").append('\n').append('\r');
		sb.append("line5").append('\n').append('\r');
		List<CharSequence> result = StringTools.getLines(sb);
		Assert.assertNotNull(result);
		Assert.assertEquals(6, result.size());
	}

	@Test
	public void getLinesCharSequencesEndingNLNL() {
		final StringBuffer sb = new StringBuffer();
		sb.append("line1").append('\n');
		sb.append("line2").append('\r');
		sb.append("line3").append('\r').append('\n');
		sb.append("line4").append('\n').append('\r');
		sb.append("line5").append('\n').append('\n');
		List<CharSequence> result = StringTools.getLines(sb);
		Assert.assertNotNull(result);
		Assert.assertEquals(6, result.size());
	}
	
	@Test
	public void getLinesCharSequencesBeginningEndingNLNL() {
		final StringBuffer sb = new StringBuffer();
		sb.append('\n').append('\n').append("line1").append('\n');
		sb.append("line2").append('\r');
		sb.append("line3").append('\r').append('\n');
		sb.append("line4").append('\n').append('\r');
		sb.append("line5").append('\n').append('\n');
		List<CharSequence> result = StringTools.getLines(sb);
		Assert.assertNotNull(result);
		Assert.assertEquals(8, result.size());
	}
	
//	@DataProvider(name="goodlines")
//	public Object[][] getGoodLines() {
//		return new Object[][] {
//				{new StringBuffer()
//					.append("first line").append(StringTools.NEWLINE)
//					.append("second line").append(StringTools.NEWLINE)
//					.append("third line").append(StringTools.NEWLINE)
//					.append("fourth line").append(StringTools.NEWLINE)
//					.append("fifth line").append(StringTools.NEWLINE)
//					.toString(), 
//				5},
//				{new StringBuffer()
//					.append("first line").append("\n")
//					.append("second line").append("\n")
//					.append("third line").append("\n")
//					.append("fourth line").append("\n")
//					.append("fifth line").append("\n")
//					.toString(), 
//				5},
//					{new StringBuffer()
//					.append("first line").append("\r")
//					.append("second line").append("\r")
//					.append("third line").append("\r")
//					.append("fourth line").append("\r")
//					.append("fifth line").append("\r")
//					.toString(), 
//				5},
//				{new StringBuffer()
//					.append("first line").append("\r\n")
//					.append("second line").append("\r\n")
//					.append("third line").append("\r\n")
//					.append("fourth line").append("\r\n")
//					.append("fifth line").append("\r\n")
//					.toString(), 
//				5}
//		};
//	}
//	
//	@Test(dataProvider="goodlines")
//	public void getLines(String s, int linesAmount) {
//		String[] lines = StringTools.getLines(s);
//		Assert.assertNotNull("The lines are null", lines);
//		Assert.assertEquals("The amount of lines lines are not correct", lines.length, linesAmount);
//		for (String line : lines) {
//			Assert.assertTrue("The string is not correct", line.endsWith("line"));
//		}
//	}
}

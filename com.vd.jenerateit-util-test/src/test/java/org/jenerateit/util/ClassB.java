/**
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit.util;

public class ClassB extends ClassA {

	/* (non-Javadoc)
	 * @see com.gs.util.ClassA#getElement()
	 */
	@Override
	public StackTraceElement getElement() {
		// TODO Auto-generated method stub
		return super.getElement();
	}

}

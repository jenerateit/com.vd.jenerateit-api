package org.jenerateit.util;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.BitSet;

import org.junit.Assert;
import org.junit.Test;
import org.osgi.framework.Version;


public class UriParserTest {

	@Test 
	public void targetProjectHashTest() {
		Assert.assertEquals("Hash code does not match any more", 3556498, "test".hashCode());
	}
	
	@Test
	public void targetTrojectTest() {
		URI u = new UriParser().setProjectId(3556498).getTargetProjectURI();
		Assert.assertNotNull(u);
		try {
			Assert.assertEquals(u, new URI("tp/3556498/"));
		} catch (URISyntaxException e) {
			Assert.fail("Error while create compare URI");
		}
	}

	@Test
	public void virtualDeveloperTest() {
		final URI u = new UriParser().setProjectId(3556498).setVirtualDeveloper("vid", Version.parseVersion("0.0.2")).getVirtualDeveloperURI();
		Assert.assertNotNull(u);
		try {
			Assert.assertEquals(u, new URI("tp/3556498/vd/112229424/"));
		} catch (URISyntaxException e) {
			Assert.fail("Error while create compare URI");
		}
	}

	@Test
	public void modelAccessTest() {
		URI u = new UriParser().setProjectId(3556498).setVirtualDeveloperId(112229424).getVirtualDeveloperURI();
		u = new UriParser(u).setModelAccess(u, "abc.def.Ghi", Version.parseVersion("1.1.1")).getModelAccessURI();
		Assert.assertNotNull(u);
		try {
			Assert.assertEquals(new URI("tp/3556498/vd/112229424/ma/-2119739739/"), u);
		} catch (URISyntaxException e) {
			Assert.fail("Error while create compare URI");
		}
	}


	@Test
	public void modelConverterTest() {
		URI u = new UriParser().setProjectId(3556498).setVirtualDeveloperId(112229424).setModelAccessId(-1287422468).getModelAccessURI();
		u = new UriParser(u).setModelConverter(u, "abc.def.Ghi", Version.parseVersion("1.1.1")).getModelConverterURI();
		Assert.assertNotNull(u);
		try {
			Assert.assertEquals(new URI("tp/3556498/vd/112229424/mc/-926635928/"), u);
		} catch (URISyntaxException e) {
			Assert.fail("Error while create compare URI");
		}
	}

	@Test
	public void generationGroupTest() {
		URI u = new UriParser().setProjectId(3556498).setVirtualDeveloperId(112229424).setModelConverterId(1339057305).getModelConverterURI();
		u = new UriParser(u).setGenerationGroup(u, "abc.def.Ghi", Version.parseVersion("1.1.1")).getGenerationGroupURI();
		Assert.assertNotNull(u);
		try {
			Assert.assertEquals(new URI("tp/3556498/vd/112229424/gg/1902927965/"), u);
		} catch (URISyntaxException e) {
			Assert.fail("Error while create compare URI");
		}
	}


	@Test
	public void generationGroupTargetTest() {
		try {
			URI u = new UriParser().setProjectId(3556498).setVirtualDeveloperId(112229424).setGenerationGroupId(-1138775228).getGenerationGroupURI();
			u = new UriParser(u).setTargetPath(new URI("src/main/java/com/gs/example/helloworld/TestClass.java")).getTargetPathURI();
			Assert.assertNotNull(u);
			Assert.assertEquals(new URI("tp/3556498/vd/112229424/gg/-1138775228/target/src/main/java/com/gs/example/helloworld/TestClass.java"), u);
		} catch (URISyntaxException e) {
			Assert.fail("Error while create compare URI");
		}
	}
	
	@Test
	public void convertBackAndForth() {
		BitSet bs = new BitSet();
		bs.set(1);
		bs.set(5);
		bs.set(9);
		
		String s = UriParser.convert(bs);
		Assert.assertNotNull(s);
		Assert.assertEquals(s, "0100010001");
		
		BitSet bsNew = UriParser.convert(s);
		Assert.assertTrue(bs.equals(bsNew));
	}
}

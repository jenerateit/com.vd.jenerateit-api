package org.jenerateit.util;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.io.InputStream;

import org.jenerateit.util.GensetParser.ModelAccess;
import org.junit.Test;

public class GensetParserTest {

	
	@Test
	public void noAttributeActive() {
		@SuppressWarnings("resource")
		final InputStream in = this.getClass().getClassLoader().getResourceAsStream("genset/gg_no_attribute_active.genset");
		assertThat(in, notNullValue());
		final ModelAccess ma = new GensetParser(in).getModelAccess();
		assertThat(ma, notNullValue());
	}
}

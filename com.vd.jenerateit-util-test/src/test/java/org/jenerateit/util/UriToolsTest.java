package org.jenerateit.util;

import java.net.URI;
import java.net.URISyntaxException;

import org.junit.Assert;
import org.junit.Test;



public class UriToolsTest {

	@Test
	public void testEclipseProjectFile() {
		try {
			URI u = new URI(".project");
			Assert.assertNotNull(u);
			URI un = UriTools.checkAndModify(u);
			Assert.assertNotNull(un);
			Assert.assertEquals(".project", un.toASCIIString());
		} catch (URISyntaxException e) {
			Assert.fail("Can not create URI");
		}
	}

	@Test
	public void testDotFile() {
		try {
			URI u = new URI("/src/main/test/.htaccess");
			Assert.assertNotNull(u);
			URI un = UriTools.checkAndModify(u);
			Assert.assertNotNull(un);
			Assert.assertEquals("src/main/test/.htaccess", un.toASCIIString());
		} catch (URISyntaxException e) {
			Assert.fail("Can not create URI");
		}
	}

	@Test
	public void testTwoSlash() {
		try {
			URI u = new URI("src/main/java//com/gs/test/Test.java");
			Assert.assertNotNull(u);
			URI un = UriTools.checkAndModify(u);
			Assert.assertNotNull(un);
			Assert.assertEquals("src/main/java/com/gs/test/Test.java", un.toASCIIString());
		} catch (URISyntaxException e) {
			Assert.fail("Can not create URI");
		}
	}

	@Test
	public void testStartDotSlash() {
		try {
			URI u = new URI("./src/main/java//com/gs/test/Test.java");
			Assert.assertNotNull(u);
			URI un = UriTools.checkAndModify(u);
			Assert.assertNotNull(un);
			Assert.assertEquals("src/main/java/com/gs/test/Test.java", un.toASCIIString());
		} catch (URISyntaxException e) {
			Assert.fail("Can not create URI");
		}
	}

	@Test
	public void testStartSlash() {
		try {
			URI u = new URI("/src/main/java//com/gs/test/Test.java");
			Assert.assertNotNull(u);
			URI un = UriTools.checkAndModify(u);
			Assert.assertNotNull(un);
			Assert.assertEquals("src/main/java/com/gs/test/Test.java", un.toASCIIString());
		} catch (URISyntaxException e) {
			Assert.fail("Can not create URI");
		}
	}

	@Test(expected=IllegalArgumentException.class)
	public void testStartDotDotSlash() {
		try {
			URI u = new URI("../src/main/java//com/gs/test/Test.java");
			Assert.assertNotNull(u);
			UriTools.checkAndModify(u);
		} catch (URISyntaxException e) {
			Assert.fail("Can not create URI");
		}
	}

	@Test
	public void testMiddleDotDotSlash() {
		try {
			URI u = new URI("src/main/java/../java/com/gs/test/Test.java");
			Assert.assertNotNull(u);
			URI un = UriTools.checkAndModify(u);
			Assert.assertNotNull(un);
			Assert.assertEquals("src/main/java/com/gs/test/Test.java", un.toASCIIString());
		} catch (URISyntaxException e) {
			Assert.fail("Can not create URI");
		}
	}

	@Test
	public void testMiddleNewPathDotDotSlash() {
		try {
			URI u = new URI("src/main/java/../java/com/gs/../../test/Test.java");
			Assert.assertNotNull(u);
			URI un = UriTools.checkAndModify(u);
			Assert.assertNotNull(un);
			Assert.assertEquals("src/main/java/test/Test.java", un.toASCIIString());
		} catch (URISyntaxException e) {
			Assert.fail("Can not create URI");
		}
	}

	@Test(expected=IllegalArgumentException.class)
	public void testBeginningMiddleSeveralDotDotSlash() {
		try {
			URI u = new URI("../../../../../src/main/java/../java/com/gs/test/Test.java");
			Assert.assertNotNull(u);
			UriTools.checkAndModify(u);
		} catch (URISyntaxException e) {
			Assert.fail("Can not create URI");
		}
	}

	@Test(expected=IllegalArgumentException.class)
	public void testMiddleSeveralDotDotSlash() {
		try {
			URI u = new URI("/src/main/java/../java/com/gs/../../../../../../test/Test.java");
			Assert.assertNotNull(u);
			UriTools.checkAndModify(u);
		} catch (URISyntaxException e) {
			Assert.fail("Can not create URI");
		}
	}

	@Test
	public void testMiddleCorrectDotDotSlash() {
		try {
			URI u = new URI("/src/main/java/../java/com/gs/../../../../../test/Test.java");
			Assert.assertNotNull(u);
			URI un = UriTools.checkAndModify(u);
			Assert.assertNotNull(un);
			Assert.assertEquals("test/Test.java", un.toASCIIString());
		} catch (URISyntaxException e) {
			Assert.fail("Can not create URI");
		}
	}

	@Test(expected=IllegalArgumentException.class)
	public void testBeginningTilde() {
		try {
			URI u = new URI("~hrr/src/main/java/../java/com/gs/test/Test.java");
			Assert.assertNotNull(u);
			UriTools.checkAndModify(u);
		} catch (URISyntaxException e) {
			Assert.fail("Can not create URI");
		}
	}

	@Test(expected=IllegalArgumentException.class)
	public void testBeginningSlashTilde() {
		try {
			URI u = new URI("/~hrr/src/main/java/../java/com/gs/test/Test.java");
			Assert.assertNotNull(u);
			UriTools.checkAndModify(u);
		} catch (URISyntaxException e) {
			Assert.fail("Can not create URI");
		}
	}

	@Test()
	public void testMiddleTilde() {
		try {
			URI u = new URI("src/main/java/~hrr/com/gs/test/Test.java");
			Assert.assertNotNull(u);
			URI un = UriTools.checkAndModify(u);
			Assert.assertNotNull(un);
			Assert.assertEquals("src/main/java/~hrr/com/gs/test/Test.java", un.toASCIIString());
		} catch (URISyntaxException e) {
			Assert.fail("Can not create URI");
		}
	}


}

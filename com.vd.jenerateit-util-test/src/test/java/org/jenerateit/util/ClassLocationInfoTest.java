/**
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit.util;

import org.junit.Assert;
import org.junit.Test;


/**
 * @author hrr
 *
 */
public class ClassLocationInfoTest {

	public ClassLocationInfoTest() {
	}
	
	@Test
	public void ClassB() {
		ClassC c = new ClassC();
		final Class<?> b = ClassB.class;
		c.setClazz(b);
		Assert.assertNotNull(c.getElement());
		Assert.assertEquals(ClassC.class.getName(), c.getElement().getClassName());
	}
	
	@Test
	public void ClassA() {
		ClassC c = new ClassC();
		c.setClazz(ClassA.class);
		Assert.assertNotNull(c.getElement());
		Assert.assertEquals(ClassB.class.getName(), c.getElement().getClassName());
	}
}

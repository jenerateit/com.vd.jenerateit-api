/**
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package org.jenerateit.util;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

public class JavaArchiveToolsTest {

	@Test(expected = IllegalArgumentException.class)
	public void testGetJavaArchiveFilesNull() throws Exception 
	{
		JavaArchiveTools.getJavaArchiveFiles((File)null);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testGetJavaArchiveFilesEmpty() throws Exception
	{
		JavaArchiveTools.getJavaArchiveFiles(new ArrayList<File>(0));
	}

	@Test
	public void loadSingleJar() {
		try {
			File jarDir = new File("./src/test/resources/");
			List<File> list = JavaArchiveTools.getJavaArchiveFiles(jarDir);
			
			Assert.assertEquals("Found more or less then one archive", 1, list.size());
			Assert.assertTrue("can not find 'cvsclient.jar'", list.contains(new File("./src/test/resources/cvsclient.jar")));
		} catch (Exception e) {
			Assert.fail("Unexpected exception: " + e.getMessage());
		}
	}

	@Test
	public void getJavaArchiveURLs() {
		try {
			File jarDir = new File("./src/test/resources/");
			Set<URL> list = JavaArchiveTools.getJavaArchiveURLs(jarDir, true);
			
			Assert.assertEquals("Found more or less then three archive", 4, list.size());
			Assert.assertTrue("can not find 'cvsclient.jar'", list.contains(new File("./src/test/resources/cvsclient.jar").toURI().normalize().toURL()));
			Assert.assertTrue("can not find 'batik.jar'", list.contains(new File("./src/test/resources/subdir/batik.jar").toURI().normalize().toURL()));
			Assert.assertTrue("can not find 'fop.jar'", list.contains(new File("./src/test/resources/subdir/fop.jar").toURI().normalize().toURL()));
			Assert.assertTrue("can not find 'test/resources/dir with spaces/binding-2.0.2.jar'", list.contains(new File("./src/test/resources/dir with spaces/binding-2.0.2.jar").toURI().normalize().toURL()));
		} catch (Exception e) {
			Assert.fail("Unexpected exception: " + e.getMessage());
		}
	}
	
	@Test
	public void loadMultipleJars() {
		try {
			File jarDir = new File("./src/test/resources/");
			List<File> list = JavaArchiveTools.getJavaArchiveFiles(jarDir, true);
			
			Assert.assertEquals("Found more or less then three archive", 4, list.size());
			Assert.assertTrue("can not find 'cvsclient.jar'", list.contains(new File("./src/test/resources/cvsclient.jar")));
			Assert.assertTrue("can not find 'batik.jar'", list.contains(new File("./src/test/resources/subdir/batik.jar")));
			Assert.assertTrue("can not find 'fop.jar'", list.contains(new File("./src/test/resources/subdir/fop.jar")));
			Assert.assertTrue("can not find 'binding-2.0.2.jar'", list.contains(new File("./src/test/resources/dir with spaces/binding-2.0.2.jar")));
		} catch (Exception e) {
			Assert.fail("Unexpected exception: " + e.getMessage());
		}
	}

	@Test
	public void loadMultipleMixedJars() {
		try {
			List<File> jars = new ArrayList<>();
			jars.add(new File("./src/test/resources/cvsclient.jar"));
			jars.add(new File("./src/test/resources/subdir/"));
			List<File> list = JavaArchiveTools.getJavaArchiveFiles(jars);
			
			Assert.assertEquals("Found more or less then three archive", 3, list.size());
			Assert.assertTrue("can not find 'cvsclient.jar'", list.contains(new File("./src/test/resources/cvsclient.jar")));
			Assert.assertTrue("can not find 'batik.jar'", list.contains(new File("./src/test/resources/subdir/batik.jar")));
			Assert.assertTrue("can not find 'fop.jar'", list.contains(new File("./src/test/resources/subdir/fop.jar")));
		} catch (Exception e) {
			Assert.fail("Unexpected exception: " + e.getMessage());
		}
	}

	@Test
	public void loadJars() {
		try {
			List<File> jars = new ArrayList<>();
			jars.add(new File("./src/test/resources/cvsclient.jar"));
			jars.add(new File("./src/test/resources/subdir/fop.jar"));
			List<File> list = JavaArchiveTools.getJavaArchiveFiles(jars);
			
			Assert.assertEquals("Found more or less then two archive", 2, list.size());
			Assert.assertTrue("can not find 'cvsclient.jar'", list.contains(new File("./src/test/resources/cvsclient.jar")));
			Assert.assertFalse("can not find 'batik.jar'", list.contains(new File("./src/test/resources/subdir/batik.jar")));
			Assert.assertTrue("can not find 'fop.jar'", list.contains(new File("./src/test/resources/subdir/fop.jar")));
		} catch (Exception e) {
			Assert.fail("Unexpected exception: " + e.getMessage());
		}
	}
	
	@Test(expected = NullPointerException.class)
	public void getJavaArchiveLocationNull() {
		JavaArchiveTools.getJavaArchiveLocation(null);
	}
	
	@Test
	public void getJavaArchiveLocationTest() {
		URL u = JavaArchiveTools.getJavaArchiveLocation(org.junit.runners.JUnit4.class);
		Assert.assertNotNull("The jar file can not be loacated", u);
		Assert.assertTrue("The path did not match", u.getPath().contains("junit"));
		
		u = JavaArchiveTools.getJavaArchiveLocation(ClassA.class);
		Assert.assertNull("The ClassA is not in a jar file", u);
	}
	
	@Test(expected = NullPointerException.class)
	public void getJavaArchiveVersionNull() {
		JavaArchiveTools.getJavaArchiveVersion(null);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void getJavaArchiveVersionClass() {
		URL u = null;
		try {
			u = ClassA.class.getProtectionDomain().getCodeSource().getLocation();
		} catch (Throwable t) {
			Assert.fail("Can not get location of ClassA: " + t.getMessage());
		}
		JavaArchiveTools.getJavaArchiveVersion(u);
	}
	
	@Test
	public void getJavaArchiveVersion() {
		URL u = JavaArchiveTools.getJavaArchiveLocation(org.junit.runners.JUnit4.class);
		String v = JavaArchiveTools.getJavaArchiveVersion(u);
	
		// do not test for null, since some JUnit bundles do provide regular versions also
//		Assert.assertNull("The version of '" + u + "' is found but it is an eclipse bundle", v);
//		Assert.assertEquals("The version did not match", "1.6.1", v);
		
		try {
			u = new File("./src/test/resources/subdir/fop.jar").toURI().toURL();
		} catch (MalformedURLException e) {
			Assert.fail("Can not create an URL to a test jar file: " + e.getMessage());
		}
		v = JavaArchiveTools.getJavaArchiveVersion(u);
		Assert.assertNull("The version of '" + u + "' is found", v);
	}
	
	@Test
	public void dirWithSpaces() {
		URL[] urls = null;
		try {
			urls = new URL[] { new File("src/test/resources/dir with spaces/binding-2.0.2.jar").toURI().normalize().toURL()};
			Assert.assertNotNull("Can not find the URLs in the directory with empty spaces", urls);
			Assert.assertEquals("Did not find exactly one jar file", 1, urls.length);
		} catch (MalformedURLException e) {
			Assert.fail("Error while get JAR file: " + e.getMessage());
		}
		Class<?> clazz = null;
		try (final URLClassLoader cl = new URLClassLoader(urls, this.getClass().getClassLoader())) {
//		try {
			clazz = cl.loadClass("com.jgoodies.binding.BindingUtils");
		} catch (ClassNotFoundException e) {
			Assert.fail("Error while load class: " + e.getMessage());
		} catch (IOException e1) {
			Assert.fail("Error while close classloader");
		}
		
		URL u = JavaArchiveTools.getJavaArchiveLocation(clazz);
		String v = JavaArchiveTools.getJavaArchiveVersion(u);
		Assert.assertNotNull("The version of '" + u + "' is not found", v);
		Assert.assertEquals("The version did not match", "2.0.2 2008-01-18 10:01:08", v);
	}
	

//	@DataProvider(name = "version_data")
//	public Object[][] getVersionValues() {
//		return new Object[][] {
//				{"1.2.3", 3},
//				{"1.2.3.4.5", 5},
//				{"1", 1},
//				{"1.2.3   beta 5", 3},
//				{"1.2.3-jdk5", 3}};
//	}
//	
//	@Test(dataProvider = "version_data")
//	public void getVersionDetails(String versions, int length) {
//		String[] v = JavaArchiveTools.getVersionDetails(versions);
//		Assert.assertNotNull(v, "The versions are null");
//		Assert.assertEquals(v.length, length, "The length did not match");
//	}
//
//	@Test(dataProvider = "version_data")
//	public void getVersionNumbers(String versions, int length) {
//		int[] v = JavaArchiveTools.getVersionNumbers(versions);
//		Assert.assertNotNull(v, "The versions are null");
//		Assert.assertEquals(v.length, length, "The length did not match");
//	}
//	
	@Test
	public void testVersionString() {
		int[] v = JavaArchiveTools.getVersionNumbers("1.2.3.4.5.6.7.8");
		Assert.assertEquals(v.length, 8);
		Assert.assertEquals(v[0], 1);
		Assert.assertEquals(v[1], 2);
		Assert.assertEquals(v[2], 3);
		Assert.assertEquals(v[3], 4);
		Assert.assertEquals(v[4], 5);
		Assert.assertEquals(v[5], 6);
		Assert.assertEquals(v[6], 7);
		Assert.assertEquals(v[7], 8);
	}
}
